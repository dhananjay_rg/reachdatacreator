{
  "code": "reach_topic_2",
  "description": "Avoid eating outside food",
  "name": "Avoid eating outside food",
  "orgId": "reach_dev",
  "status": "active",
  "topicType": "consumer",
  "urlName": ""
}

======

{
  "code": "reach_topic_4",
  "description": "Avoid fried foods",
  "name": "Avoid fried foods",
  "orgId": "reach_dev",
  "status": "active",
  "topicType": "consumer",
  "urlName": ""
}

======

{
  "code": "reach_topic_5",
  "description": "Avoid processed refined foods",
  "name": "Avoid processed refined foods",
  "orgId": "reach_dev",
  "status": "active",
  "topicType": "consumer",
  "urlName": ""
}

======

{
  "code": "reach_topic_38",
  "description": "Pack healthy snacks when on-the-go",
  "name": "Pack healthy snacks when on-the-go",
  "orgId": "reach_dev",
  "status": "active",
  "topicType": "consumer",
  "urlName": ""
}

======

{
  "code": "reach_topic_39",
  "description": "Plan daily for next day's meals",
  "name": "Plan daily for next day's meals",
  "orgId": "reach_dev",
  "status": "active",
  "topicType": "consumer",
  "urlName": ""
}

======

{
  "code": "reach_topic_41",
  "description": "Switch comfort food with a healthy food",
  "name": "Switch comfort food with a healthy food",
  "orgId": "reach_dev",
  "status": "active",
  "topicType": "consumer",
  "urlName": ""
}

======

"reach_topic_2", "reach_topic_4", "reach_topic_5", "reach_topic_38", "reach_topic_39", "reach_topic_41"