{
"channelType": "consumer",
"code": "reach_ch_food",
"createdBy": "reach",
"description": "Food",
"name": "Food",
"orgId": "reach",
"status": "active",
"urlName": ""
}

======

{
"channelType": "consumer",
"code": "reach_ch_activity",
"createdBy": "reach",
"description": "Activity",
"name": "Activity",
"orgId": "reach",
"status": "active",
"urlName": ""
}

======

{
"channelType": "consumer",
"code": "reach_ch_time_management",
"createdBy": "reach",
"description": "Time Management",
"name": "Time Management",
"orgId": "reach",
"status": "active",
"urlName": ""
}

======

{
"channelType": "consumer",
"code": "reach_ch_sleep",
"createdBy": "reach",
"description": "Sleep",
"name": "Sleep",
"orgId": "reach",
"status": "active",
"urlName": ""
}

======

{
"channelType": "consumer",
"code": "reach_ch_motivation",
"createdBy": "reach",
"description": "Motivation",
"name": "Motivation",
"orgId": "reach",
"status": "active",
"urlName": ""
}         
  
======

{
"channelType": "consumer",
"code": "reach_ch_lifestyle",
"createdBy": "reach",
"description": "Lifestyle",
"name": "Lifestyle",
"orgId": "reach",
"status": "active",
"urlName": ""
}         

==========
            
{
"channelType": "consumer",
"code": "reach_ch_mindfulness",
"createdBy": "reach",
"description": "Mindfulness",
"name": "Mindfulness",
"orgId": "reach",
"status": "active",
"urlName": ""
}         

==========

{
"channelType": "consumer",
"code": "reach_ch_hygiene",
"createdBy": "reach",
"description": "Hygiene",
"name": "Hygiene",
"orgId": "reach",
"status": "active",
"urlName": ""
}         

==========

{
"channelType": "consumer",
"code": "reach_ch_relationships",
"createdBy": "reach",
"description": "Relationships",
"name": "Relationships",
"orgId": "reach",
"status": "active",
"urlName": ""
}         

==========

{
"channelType": "consumer",
"code": "reach_ch_emotional_health",
"createdBy": "reach",
"description": "Emotional Health",
"name": "Emotional Health",
"orgId": "reach",
"status": "active",
"urlName": ""
}         

==========       

"reach_ch_food", "reach_ch_time_management", "reach_ch_emotional_health", "reach_ch_relationships", "reach_ch_motivation", "reach_ch_hygiene", "reach_ch_mindfulness", "reach_ch_activity", "reach_ch_sleep", "reach_ch_lifestyle"     
      
          
    
