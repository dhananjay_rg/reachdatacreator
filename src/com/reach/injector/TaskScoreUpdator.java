package com.reach.injector;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.util.DBUtil;

public class TaskScoreUpdator {

	private static HashMap<String, Double> eventScoreMap = new HashMap<String, Double>();
	private static HashMap<Integer, Double> eventTypeMap = new HashMap<Integer, Double>();
	private static HashMap<String, Integer> tssType = new HashMap<String, Integer>();
	
	public static void main(String[] args) {
		loadEventScore();
		loadEventType();
		process();
	}
	
	private static void loadEventScore() {
		
		try {
			Workbook workbook = WorkbookFactory.create(new File("data/Scoring Actions.xlsx"));
			Sheet sheet = workbook.getSheet("Scoring Actions");
			Iterator<Row> rowIterator = sheet.rowIterator();
			if (rowIterator.hasNext())
				rowIterator.next();
			
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				
				if(row.getCell(0) != null && !row.getCell(0).equals("")) {
				
					if(!eventScoreMap.containsKey(row.getCell(0).toString().toLowerCase().trim())) {
						eventScoreMap.put(row.getCell(0).toString().toLowerCase().trim(), row.getCell(7).getNumericCellValue());
//						System.out.println(row.getCell(0) + "-----------" + row.getCell(7));
					}
				}
			}
			
			System.out.println(eventScoreMap.size());
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void loadEventType() {
		Connection conn = null;
		try {
			conn = DBUtil.getConnection();
			
			Statement stmt = conn.createStatement();
			String sql = "select * from task_sub_sub_types where status=1";
			ResultSet rs = stmt.executeQuery(sql);
			if (rs != null) {
				while (rs.next()) {
					int id = rs.getInt("id");
					String title = rs.getString("title");
					tssType.put(title.toLowerCase(), id);
				}
			}
			rs.close();
			stmt.close();
			
			
			Iterator it = eventScoreMap.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
//		        System.out.println("__________ " + pair.getKey() + " = " + pair.getValue());
		        if(tssType.get(pair.getKey().toString().toLowerCase()) == null) {
					System.out.println(pair.getKey().toString() + " not exist");
				} else {
					eventTypeMap.put(tssType.get(pair.getKey().toString()), eventScoreMap.get(pair.getKey().toString()));
				}
		    }
			
			System.out.println(eventTypeMap.size());
			
		} catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(conn!=null) { try { conn.close(); } catch (Exception e) { }}
		}
	}
	
	private static void process() {
		Connection conn = null;
		try {
			conn = DBUtil.getConnection();
			List<String> lst = new ArrayList<>();
			
			String sqlUp = "update task_masters set score=? where id=?";
			PreparedStatement psUp = null;
			
			
			Statement stmt = conn.createStatement();
			String selectVisionTypesQuery = "select tm.id,tm.title,tm.tsst_id,tss.title as type from task_masters tm join task_sub_sub_types tss on (tm.tsst_id=tss.id)";
			ResultSet rs = stmt.executeQuery(selectVisionTypesQuery);
			while (rs.next()) {
				if (eventTypeMap.containsKey(rs.getInt("tsst_id"))) {
					lst.add(rs.getString("id"));
					psUp = conn.prepareStatement(sqlUp);
					psUp.setDouble(1, eventTypeMap.get(rs.getInt("tsst_id")));
					psUp.setInt(2, rs.getInt("id"));
					psUp.executeUpdate();
					
					System.out.println("update task_masters set score=" + eventTypeMap.get(rs.getInt("tsst_id")) + " where id=" + rs.getInt("id") + ";");
					
				} else {
//					System.out.println( rs.getString("id") + "  --  " + type);
				}
			}

			System.out.println("___ size=" + lst.size());
			rs.close();
			stmt.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(conn!=null) { try { conn.close(); } catch (Exception e) { }}
		}
	}
}
