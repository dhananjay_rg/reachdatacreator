package com.reach.injector;

import com.reach.injector.profile.InsertProfileMasterData;
import com.reach.injector.profile.ReachDataTypeProcessor;
import com.reach.injector.visioning_task.DynamicDataProcessor;
import com.reach.injector.visioning_task.InsertBarrierMasterData;
import com.reach.injector.visioning_task.InsertHabitMasterData;
import com.reach.injector.visioning_task.InsertHabitQuestion;
import com.reach.injector.visioning_task.InsertTaskMasterData;
import com.reach.injector.visioning_task.InsertVisionMasterData;
import com.reach.injector.visioning_task.TaskScheduleMaster;
import com.reach.injector.visioning_task.TaskSubTypeRule;

public class MainProcessStarter {

	public static void main(String[] args) {
		try {
			System.out.println("_______ STARTED._________");
			
			System.out.println("... processing profile master data");
			InsertProfileMasterData.processProfileData();
			Thread.sleep(1000);
			
			System.out.println("... processing reach data type master data");
			ReachDataTypeProcessor.process();
			Thread.sleep(1000);
			
			System.out.println("... processing vision master data");
			InsertVisionMasterData.processVisionData();
			Thread.sleep(1000);
			
			System.out.println("... processing barrier master data");
			InsertBarrierMasterData.processBarrierData();
			Thread.sleep(1000);
			
			System.out.println("... processing habit master data");
			InsertHabitMasterData.processHabitData();
			Thread.sleep(1000);
			
			System.out.println("... processing vision barrier habit map master data");
//			InsertVisionBarrierHabitMapData.processVisionBarrierHabitMapData();
			Thread.sleep(1000);
			
			
			System.out.println("... processing task master data [Profile Task [Bundle Task]]");
			InsertTaskMasterData.processTaskData();
			Thread.sleep(1000);
			
			System.out.println("... processing habit benchmark questions data, [Bundle Task]");
			InsertHabitQuestion.process();
			Thread.sleep(1000);
			
			System.out.println("... processing task schedule data");
			TaskScheduleMaster.process();
			Thread.sleep(1000);
			
			System.out.println("... processing dynamic data, [Bundle Task]");
			DynamicDataProcessor.process();
			Thread.sleep(1000);
			
			System.out.println("... processing dynamic data, [Bundle Task]");
			TaskSubTypeRule.process();
			Thread.sleep(1000);
			
			System.out.println("_______ FINISHED._________");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
