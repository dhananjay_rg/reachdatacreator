package com.reach.injector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.HashMap;
import java.util.UUID;

import org.postgresql.util.PGobject;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.injector.content.vo.TaskMasterUtil;
import com.reach.injector.visioning_task.StaticFieldParams;
import com.reach.injector.vo.TaskMasterVo;
import com.reach.util.DBUtil;

public class CommonMasterDBQuery {

	private static HashMap<String,String> hmFieldMap = new HashMap<>(); 
	static {
		hmFieldMap = StaticFieldParams.getStaticFieldMap();
	}

	public static int GetTaskIDFromHabits(String profileQuestion) {
		int habitkey = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = DBUtil.getConnection();

			String insertTableSQL = "SELECT id FROM habits WHERE title = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, profileQuestion);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				habitkey = rs.getInt(1);
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return habitkey;
	}
	
	
	public static int InsertReachDataTyrpeForManual(String type_code, String refer_table, String data_domain)
			throws DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO reach_data_types(type_code, refer_table,version,data_domain) VALUES(?,?,?,?)";

		try {
			dbConnection = DBUtil.getConnection();

			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(data_domain);

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, type_code);
			preparedStatement.setString(2, refer_table);
			preparedStatement.setInt(3, 1);
			preparedStatement.setObject(4, jsonObject);

			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (int) key;
	}

	public static int InsertTaskMaster(String title, String csTaskId, int taskGroupId, int tt_id, int tst_id, int tsst_id, int level, boolean repeatable,
			int freq, int duration, Time time_start, Time time_end, int task_day, int score, boolean dare, String dare_title,
			boolean custom, Integer user_id, String description, String param_csv, boolean tagable_output,
			String uin_option, String uin_data_type, boolean global, boolean bundle_task, String bundle_task_csv,
			int status, String[] tag_list) {
		// TODO Auto-generated method stub
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

//		duration = duration * Constant.DURATION_MULTIPLIER;
		
		String insertTableSQL = "INSERT INTO task_masters"
				+ "(title, task_group_id, tt_id,tst_id,tsst_id,level,repeatable,freq,duration,time_start,time_end,task_day,score,dare,dare_title,custom,description,param_csv,tagable_output,uin_option,uin_data_type,global,bundle_task,bundle_task_csv,task_tags,status, cs_task_id, tuid) VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		try {
			dbConnection = DBUtil.getConnection();

			java.sql.Array sqlArray = null;
			try {
				sqlArray = dbConnection.createArrayOf("VARCHAR", tag_list);
//				if(tag_list != null && tag_list.length > 1) {
//					System.out.println("_____________ sqlArray=" + sqlArray);
//				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			int i = 0;

			preparedStatement.setString(++i, title);
			preparedStatement.setInt(++i, taskGroupId);
			preparedStatement.setInt(++i, tt_id);
			preparedStatement.setInt(++i, tst_id);
			preparedStatement.setInt(++i, tsst_id);
			preparedStatement.setInt(++i, level);
			preparedStatement.setBoolean(++i, repeatable);
			preparedStatement.setInt(++i, freq);
			preparedStatement.setInt(++i, duration);
			preparedStatement.setTime(++i, time_start);
			preparedStatement.setTime(++i, time_end);
			preparedStatement.setInt(++i, task_day);
			preparedStatement.setInt(++i, score);
			preparedStatement.setBoolean(++i, dare);
			preparedStatement.setString(++i, dare_title);
			preparedStatement.setBoolean(++i, custom);
			preparedStatement.setString(++i, description);
			preparedStatement.setString(++i, param_csv);
			preparedStatement.setBoolean(++i, tagable_output);
			preparedStatement.setString(++i, uin_option);
			if (uin_data_type.equals("")) {
				uin_data_type = null;
			}
			preparedStatement.setString(++i, uin_data_type);
			preparedStatement.setBoolean(++i, global);
			preparedStatement.setBoolean(++i, bundle_task);
			preparedStatement.setString(++i, bundle_task_csv);
			preparedStatement.setArray(++i, sqlArray);
			preparedStatement.setInt(++i, status);
			preparedStatement.setString(++i, csTaskId);
			preparedStatement.setObject(++i, UUID.randomUUID());
//			System.out.println(" _ insert Task Master SQL= " + preparedStatement);
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
				if(taskGroupId == 0) {
					String sqlUp = "update task_masters set task_group_id=? where id=?";
					PreparedStatement psUp = dbConnection.prepareStatement(sqlUp);
					try {
						psUp.setLong(1, key);
						psUp.setLong(2, key);
						psUp.executeUpdate();
					} catch(Exception e) {
						System.out.println("___ error sqlUp =" + sqlUp);
						e.printStackTrace();
					} finally {
						sqlUp = null;
						if(psUp != null) {
							psUp.close();
							psUp = null;
						}
					}
				}
//				System.out.println("Generated Key of task master=" + key);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return (int) key;

	}

	
	public static int updateTaskMaster(String title, String csTaskId, int taskGroupId, int tt_id, int tst_id, int tsst_id, int level, boolean repeatable,
			int freq, int duration, Time time_start, Time time_end, int task_day, int score, boolean dare, String dare_title,
			boolean custom, Integer user_id, String description, String param_csv, boolean tagable_output,
			String uin_option, String uin_data_type, boolean global, boolean bundle_task, String bundle_task_csv,
			int status, String[] tag_list, int id) {
		// TODO Auto-generated method stub
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

//		duration = duration * Constant.DURATION_MULTIPLIER;
		
		String updateTableSQL = "update task_masters"
				+ " set title=?, task_group_id=?, tt_id=?,tst_id=?,tsst_id=?,level=?,repeatable=?,freq=?,duration=?,time_start=?,time_end=?,task_day=?,score=?,dare=?,dare_title=?,custom=?,description=?,param_csv=?,tagable_output=?,uin_option=?,uin_data_type=?,global=?,bundle_task=?,bundle_task_csv=?,task_tags=?,status=?, cs_task_id=? where id=?";
		
		try {
			dbConnection = DBUtil.getConnection();

			java.sql.Array sqlArray = null;
			try {
				sqlArray = dbConnection.createArrayOf("VARCHAR", tag_list);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			preparedStatement = dbConnection.prepareStatement(updateTableSQL);

			int i = 0;

			preparedStatement.setString(++i, title);
			preparedStatement.setInt(++i, taskGroupId);
			preparedStatement.setInt(++i, tt_id);
			preparedStatement.setInt(++i, tst_id);
			preparedStatement.setInt(++i, tsst_id);
			preparedStatement.setInt(++i, level);
			preparedStatement.setBoolean(++i, repeatable);
			preparedStatement.setInt(++i, freq);
			preparedStatement.setInt(++i, duration);
			preparedStatement.setTime(++i, time_start);
			preparedStatement.setTime(++i, time_end);
			preparedStatement.setInt(++i, task_day);
			preparedStatement.setInt(++i, score);
			preparedStatement.setBoolean(++i, dare);
			preparedStatement.setString(++i, dare_title);
			preparedStatement.setBoolean(++i, custom);
			preparedStatement.setString(++i, description);
			preparedStatement.setString(++i, param_csv);
			preparedStatement.setBoolean(++i, tagable_output);
			preparedStatement.setString(++i, uin_option);
			if (uin_data_type.equals("")) {
				uin_data_type = null;
			}
			preparedStatement.setString(++i, uin_data_type);
			preparedStatement.setBoolean(++i, global);
			preparedStatement.setBoolean(++i, bundle_task);
			preparedStatement.setString(++i, bundle_task_csv);
			preparedStatement.setArray(++i, sqlArray);
			preparedStatement.setInt(++i, status);
			preparedStatement.setString(++i, csTaskId);
			preparedStatement.setInt(++i, id);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return (int) key;

	}
	
	public static void deleteTaskMaster(int id) {
		// TODO Auto-generated method stub
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

//		duration = duration * Constant.DURATION_MULTIPLIER;
		
		String updateTableSQL = "update task_masters"
				+ " set status= " + Constant.STATUS_INACTIVE + " where id=?";
		
		try {
			dbConnection = DBUtil.getConnection();

			
			preparedStatement = dbConnection.prepareStatement(updateTableSQL);

			int i = 0;

			preparedStatement.setInt(++i, id);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}
	
	public static void InsertTaskHabitMapping(long taskID, long taskGroupId, int habitkey, int seqid, String groupType) throws DBError, SQLException {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			String insertTableSQL = "INSERT INTO task_habit_map (task_id, habit_id, task_group_id, group_type, status) VALUES"
					+ "(?,?,?,?,?)";

				dbConnection = DBUtil.getConnection();

				preparedStatement = dbConnection.prepareStatement(insertTableSQL);

				preparedStatement.setLong(1, taskID);
				preparedStatement.setInt(2, habitkey);
				preparedStatement.setLong(3, taskGroupId);
				preparedStatement.setString(4, groupType);
				preparedStatement.setInt(5, 1);
//				System.out.println(preparedStatement);
				preparedStatement.executeUpdate();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	public static void deleteTaskHabitMappingByTaskId(long taskID) throws DBError, SQLException {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			String updateTableSQL = "update task_habit_map set status=" + Constant.STATUS_INACTIVE + " where task_id=?";

				dbConnection = DBUtil.getConnection();

				preparedStatement = dbConnection.prepareStatement(updateTableSQL);

				preparedStatement.setLong(1, taskID);
				preparedStatement.executeUpdate();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}
	
	public static void deleteTaskHabitMappingByHabitId(long habitId) throws DBError, SQLException {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			String updateTableSQL = "update task_habit_map set status=" + Constant.STATUS_INACTIVE + " where habit_id=?";

				dbConnection = DBUtil.getConnection();

				preparedStatement = dbConnection.prepareStatement(updateTableSQL);

				preparedStatement.setLong(1, habitId);
				preparedStatement.executeUpdate();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}
	
	public static int InsertTagData(String tag, boolean inclusioncriteria) throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO tags_criteria" + "(tag, include) VALUES" + "(?,?)";

		try {
			
			dbConnection = DBUtil.getConnection();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, tag);
			preparedStatement.setBoolean(2, inclusioncriteria);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}
	
	
	private static int insertTaskMaster(String title, int tt_id, int tst_id, int tsst_id, int level, boolean repeatable,
			int freq, int duration, Time time_start, Time time_end, int task_day, int score, boolean dare,
			boolean custom, Integer user_id, String full_desc, String param_csv, boolean tagable_output,
			String uin_option, String uin_data_type, boolean global, boolean bundle_task, String bundle_task_csv,
			int status, String[] tag_list) {
		// TODO Auto-generated method stub
		long key = 0;
		Connection con = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO task_masters"
				+ "(title, tt_id,tst_id,tsst_id,level,repeatable,freq,duration,time_start,time_end,task_day,score,dare,custom,full_desc,param_csv,tagable_output,uin_option,uin_data_type,global,bundle_task,bundle_task_csv,task_tags,status) VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
		try {
			con = DBUtil.getConnection();
			java.sql.Array sqlArray = null;
			try {
				sqlArray = con.createArrayOf("VARCHAR", tag_list);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			preparedStatement = con.prepareStatement(insertTableSQL);

			int i = 0;

			preparedStatement.setString(++i, title);
			preparedStatement.setInt(++i, tt_id);
			preparedStatement.setInt(++i, tst_id);
			preparedStatement.setInt(++i, tsst_id);
			preparedStatement.setInt(++i, level);
			preparedStatement.setBoolean(++i, repeatable);
			preparedStatement.setInt(++i, freq);
			preparedStatement.setInt(++i, duration);
			preparedStatement.setTime(++i, time_start);
			preparedStatement.setTime(++i, time_end);
			preparedStatement.setInt(++i, task_day);
			preparedStatement.setInt(++i, score);
			preparedStatement.setBoolean(++i, dare);
			preparedStatement.setBoolean(++i, custom);
			preparedStatement.setString(++i, full_desc);
			preparedStatement.setString(++i, param_csv);
			preparedStatement.setBoolean(++i, tagable_output);
			preparedStatement.setString(++i, uin_option);
			if (uin_data_type.equals("")) {
				uin_data_type = null;
			}
			preparedStatement.setString(++i, uin_data_type);
			preparedStatement.setBoolean(++i, global);
			preparedStatement.setBoolean(++i, bundle_task);
			preparedStatement.setString(++i, bundle_task_csv);
			preparedStatement.setArray(++i, sqlArray);
			preparedStatement.setInt(++i, status);
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return (int) key;

	}
	
	
	public static int GetHabitIDFromTable(String habit) {
		// TODO Auto-generated method stub
		int habitkey = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			
			dbConnection = DBUtil.getConnection();

			String insertTableSQL = "SELECT id FROM habits WHERE lower(title) = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, habit.toLowerCase());
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				habitkey = rs.getInt(1);
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return habitkey;
	}
	
	public static TaskMasterVo GetTaskIDFromTask(String title) {
		// TODO Auto-generated method stub
		TaskMasterVo taskMasterVo = null;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = DBUtil.getConnection();

			title = title.trim();
			
			// get 1st 
			if (title.contains("<") && title.contains(">")) {
				String str = title.substring(title.indexOf("<"), title.indexOf(">")+1);
				if(hmFieldMap.containsKey(str.toLowerCase())) {
					title = title.substring(0,title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">")+1,title.length());
				}
			}
			// get 2nd 
			if (title.contains("<") && title.contains(">")) {
				String str = title.substring(title.indexOf("<"), title.indexOf(">")+1);
				if(hmFieldMap.containsKey(str.toLowerCase())) {
					title = title.substring(0,title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">")+1,title.length());
				}
			}
			
			String insertTableSQL = "SELECT * FROM task_masters WHERE title = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, title);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
//				taskId = rs.getInt(1);
				taskMasterVo = new TaskMasterVo();
				taskMasterVo.setId(rs.getLong("id"));
				taskMasterVo.setTaskGroupId(rs.getLong("task_group_id"));
				taskMasterVo.setCsTaskId(rs.getString("cs_task_id"));
				taskMasterVo.setTitle(rs.getString("title"));
				taskMasterVo.setDuration(rs.getInt("duration"));
				taskMasterVo.setFrequency(rs.getInt("freq"));
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return taskMasterVo;

	}
	
	public static TaskMasterVo GetTaskIDFromTask(String title, String taskId) {
		// TODO Auto-generated method stub
		//int taskId = 0;
		TaskMasterVo taskMasterVo = null;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = DBUtil.getConnection();

			title = title.trim();
			
			// get 1st 
			if (title.contains("<") && title.contains(">")) {
				String str = title.substring(title.indexOf("<"), title.indexOf(">")+1);
				if(hmFieldMap.containsKey(str.toLowerCase())) {
					title = title.substring(0,title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">")+1,title.length());
				}
			}
			// get 2nd 
			if (title.contains("<") && title.contains(">")) {
				String str = title.substring(title.indexOf("<"), title.indexOf(">")+1);
				if(hmFieldMap.containsKey(str.toLowerCase())) {
					title = title.substring(0,title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">")+1,title.length());
				}
			}
			
			String insertTableSQL = "SELECT * FROM task_masters WHERE title = ? and cs_task_id=?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, title);
			preparedStatement.setString(2, taskId);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
//				taskId = rs.getInt(1);
				taskMasterVo = new TaskMasterVo();
				taskMasterVo.setId(rs.getLong("id"));
				taskMasterVo.setTaskGroupId(rs.getLong("task_group_id"));
				taskMasterVo.setCsTaskId(rs.getString("cs_task_id"));
				taskMasterVo.setTitle(rs.getString("title"));
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return taskMasterVo;

	}
	
	public static TaskMasterVo getTaskDetailBycsTaskId(String csTaskId) {
		// TODO Auto-generated method stub
		//int taskId = 0;
		TaskMasterVo taskMasterVo = null;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = DBUtil.getConnection();

			String insertTableSQL = "SELECT * FROM task_masters WHERE cs_task_id=?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, csTaskId);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				taskMasterVo = new TaskMasterVo();
				taskMasterVo.setId(rs.getLong("id"));
				taskMasterVo.setTaskGroupId(rs.getLong("task_group_id"));
				taskMasterVo.setCsTaskId(rs.getString("cs_task_id"));
				taskMasterVo.setTitle(rs.getString("title"));
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return taskMasterVo;

	}
	
	public static int UpdateTaskGlobalStatus(long taskID, boolean b) {
		// TODO Auto-generated method stub
		int habitkey = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = DBUtil.getConnection();

			String insertTableSQL = "UPDATE task_masters SET global = ?  WHERE id = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setBoolean(1, b);
			preparedStatement.setLong(2, taskID);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return habitkey;
	}

	
	public static int InsertTaskTypeData(String visiontype, int status) throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO task_types" + "(title, status) VALUES" + "(?,?)";

		try {
			dbConnection = DBUtil.getConnection();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, visiontype);
			preparedStatement.setInt(2, status);
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}

	
	public static int InsertSubTaskTypeData(String title, int status, int taskTypeId) throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO task_sub_types" + "(title,tt_id,status) VALUES" + "(?,?,?)";

		try {
			dbConnection = DBUtil.getConnection();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, title);
			preparedStatement.setInt(2, taskTypeId);
			preparedStatement.setInt(3, 1);

			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}
	
	
	public static int InsertSubSubTaskTypeData(String visiontype, int tst_id, boolean virtual, String label)
			throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO task_sub_sub_types" + "(title, tst_id,status,virtual,label) VALUES"
				+ "(?,?,?,?,?)";

		try {
			dbConnection = DBUtil.getConnection();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, visiontype);
			preparedStatement.setInt(2, tst_id);
			preparedStatement.setInt(3, 1);
			preparedStatement.setBoolean(4, virtual);
			preparedStatement.setString(5, label);
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}
	
	
	
	public static TaskMasterUtil getMasterTaskDetailsBySubSubTaskId(Integer dynamicData) {
		// TODO Auto-generated method stub
		TaskMasterUtil masterUtil = new TaskMasterUtil();

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			dbConnection = DBUtil.getConnection();

			String insertTableSQL = "SELECT * FROM task_masters WHERE tsst_id = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, dynamicData);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				masterUtil.setId(rs.getInt("id"));
				masterUtil.setDuration(rs.getInt("duration"));
				masterUtil.setFreq(rs.getInt("freq"));
				masterUtil.setTime_start(rs.getTime("time_start"));
				masterUtil.setTime_end(rs.getTime("time_end"));
				masterUtil.setLevel(rs.getInt("level"));
				masterUtil.setTask_day(rs.getInt("task_day"));
				masterUtil.setScore(rs.getInt("score"));
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return masterUtil;
	}

	
}
