package com.reach.injector.vo;

public class TaskMasterVo {

	private long id;
	private String csTaskId;
	private String title;
	private long taskGroupId;
	private int frequency;
	private int duration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCsTaskId() {
		return csTaskId;
	}

	public void setCsTaskId(String csTaskId) {
		this.csTaskId = csTaskId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getTaskGroupId() {
		return taskGroupId;
	}

	public void setTaskGroupId(long taskGroupId) {
		this.taskGroupId = taskGroupId;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
