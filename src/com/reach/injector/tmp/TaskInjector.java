package com.reach.injector.tmp;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.postgresql.util.PGobject;

import com.reach.err.DBError;

public class TaskInjector<T> {
/*	
	static Logger logger, loggerQueries;

	static HashMap<String, Integer> task_typeData;
	static HashMap<String, String> task_labelData;
	static HashMap<String, Integer> sutask_typeData;
	static HashMap<String, Integer> subsubtask_typeData;
	static HashMap<String, String> reachDataType;
	static HashMap<String, String> reachDataTypeManual;
	static FileWriter writer;
	public List<T> data;
	public List<T> tags;

	public static void main(String[] args) throws DBError {
		LogQueries();
		InitializeLogger();
		sutask_typeData = new HashMap<>();
		subsubtask_typeData = new HashMap<>();
		PopulateTaskLabels();

		PopulateTaskTypeData();

		PopulateSubTaskTypeData();

		PopulateSubSubTaskTypeData();

		PopulateReachDataType();

		PopulateHabitQuestions();

		PopulateTaskTags();

		PopulateTaskMaster();

		PopulateBundledTasks();

		PopulateDynamicData();

		BenchmarksTaskInjector();

		CloseWriter();

		// PopulateTaskLabelData();

	}

	private static void PopulateBundledTasks() {
		// TODO Auto-generated method stub
		Sheet sheet = LoadWorkbook(DataInjection.TASK);
		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (row.getRowNum() != 0) {
					for (int i = DataInjection.Tasks.TASK_HABIT1; i <= DataInjection.Tasks.TASK_HABIT10; i++) {
						try {
							String habit = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
							if (habit != null && !habit.trim().equals("")) {
								int habitID = GetHabitIDFromTable(habit);
								String task = row.getCell(DataInjection.Tasks.TASK_LIST).getStringCellValue().trim()
										.replaceAll(" +", " ");
								if (task != null) {
									TaskMasterUtil masterUtil = GetTaskIDFromTask(task);
									insertTaskHabitMapping(masterUtil.getId(), habitID, row.getRowNum(), "NGQ");
									updateTaskGlobalStatus(masterUtil.getId(), false);
								}
							} else {
								return;
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private static int updateTaskGlobalStatus(int taskID, boolean b) {
		// TODO Auto-generated method stub
		int habitkey = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			String insertTableSQL = "UPDATE task_masters SET global = ?  WHERE id = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setBoolean(1, b);
			preparedStatement.setInt(2, taskID);
			int rs = preparedStatement.executeUpdate();

			preparedStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return habitkey;
	}

	private static void PopulateTaskLabels() {
		// TODO Auto-generated method stub

		task_labelData = new HashMap<>();

		Sheet sheet = LoadWorkbook(DataInjection.TASK_LABELS);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				try {
					String subtype, label;
					// Now let's iterate over the columns of the current row
					if (row.getRowNum() != 0) {
						subtype = row.getCell(0).getStringCellValue().trim().replaceAll(" +", " ");
						label = row.getCell(1).getStringCellValue().trim().replaceAll(" +", " ");
						if (subtype != null && !subtype.equals("")) {
							task_labelData.put(subtype, label);
						}

					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					try {
						writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
								+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
								+ row.getCell(0).getAddress());
						writer.write("\r\n");
					} catch (IOException en) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					e.printStackTrace();
					logger.info(e + " at " + row.getRowNum() + " While reading data for TaskType .");
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {
			logger.info(e + " While reading data for HabitMaster .");
		}
	}

	private static void PopulateHabitQuestions() {
		// TODO Auto-generated method stub
		Row row = null;
		int habitid = 0;

		Time time_start = null;
		Time time_end = null;
		try {
			try {
				SimpleDateFormat format = new SimpleDateFormat("HH.mm");
				java.util.Date d2 = (java.util.Date) format.parse("0.0 am");
				java.util.Date d1 = (java.util.Date) format.parse("0.0 am");

				time_start = new java.sql.Time(d1.getTime());
				time_end = new java.sql.Time(d2.getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Sheet sheet = LoadWorkbook(DataInjection.HABIT);
			int tt_id_b = 0, tst_id_b = 0, tsst_id_b = 0;

			Iterator<Row> rowIteratorforvision = sheet.rowIterator();

			while (rowIteratorforvision.hasNext()) {

				String benchmarkq1 = "", benchmarkq2 = null, benchmarkq3 = null, assessmentq1 = null,
						assessmentq2 = null, assessmentq3 = null, trackingq = null;
				String benchmarkans1 = null, benchmarkans2 = null, benchmarkans3 = null, assessmentans1 = null,
						assessmentans2 = null, assessmentans3 = null, trackingans = null;

				String benchmarkans1input = null, benchmarkans2input = null, benchmarkans3input = null,
						assessmentans1input = null, assessmentans2input = null, assessmentans3input = null,
						trackingansinput = null;
				String asses1UinOptions = "", asses2UinOptions = "", asses3UinOptions = "", bench1UinOptions = "",
						bench2UinOptions = "", bench3UinOptions = "", trackingUinOptions = "";
				String habit = null;
				String data_domain = "";

				try {
					row = rowIteratorforvision.next();
					habit = row.getCell(0).getStringCellValue().trim().replaceAll(" +", " ");
					if (habit != null && !habit.equals("")) {
						habitid = GetTaskIDFromHabits(habit);
					}

				} catch (Exception e7) {
					// TODO Auto-generated catch block
					e7.printStackTrace();
				}
				if (row.getRowNum() != 0 && habit != null && habitid != 0) {

					try {
						tt_id_b = task_typeData.get("Assessment");
						tst_id_b = sutask_typeData.get("Assessment/(Habit)/(HabitBenchmarkQuestion)");
						tsst_id_b = subsubtask_typeData.get("Assessment/(Habit)/(HabitBenchmarkQuestion)");
						if (row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION1).getStringCellValue().trim()
								.replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION1).getStringCellValue()
										.trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkq1 = row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION1)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkans1 = row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS_INPUT)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS_INPUT)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkans1input = row
									.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS_INPUT)
									.getStringCellValue();
						}
						try {
							if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS_INPUT_TYPE)
									.getStringCellValue().trim().replaceAll(" +", " ") != null && !row
											.getCell(
													DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS_INPUT_TYPE)
											.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								if (row.getCell(
										DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS_INPUT_TYPE)
										.getStringCellValue().trim()
										.equals("User Input (Single Answer From Options)")) {
									bench1UinOptions = "SO";
								} else if (row
										.getCell(
												DataInjection.Habits.HABIT_BENCHMARK_QUESTION1_ANSWER_OPTIONS_INPUT_TYPE)
										.getStringCellValue().trim()
										.equals("User Input (Multiple Answers From Options)")) {
									bench1UinOptions = "MO";
								} else {
									bench1UinOptions = "TI";
								}

							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (benchmarkq1 != null && !benchmarkq1.equals("")) {
							String[] benchmarks = null;
							if (benchmarkans1.contains(" or more")) {
								benchmarkans1 = benchmarkans1.replace(" or more", "");
							}
							if (benchmarkans1.contains(",")) {
								benchmarks = benchmarkans1.split(",");
							}

							if (benchmarkans1input.contains("Number")) {
								IntListDomain intDomain = new IntListDomain();
								intDomain.data = new ArrayList<>();
								for (int i = 0; i < benchmarks.length; i++) {
									intDomain.data.add(Integer.parseInt(benchmarks[i].trim()));
								}
								data_domain = new ObjectMapper().writeValueAsString(intDomain);

							} else if (benchmarkans1input.contains("Text")) {
								StringListDomain stringDomain = new StringListDomain();
								stringDomain.data = new ArrayList<>();
								for (int i = 0; i < benchmarks.length; i++) {
									stringDomain.data.add(benchmarks[i].trim());

								}
								data_domain = new ObjectMapper().writeValueAsString(stringDomain);
							}

							InsertReachDataTyper("H" + habitid + "B1", "", data_domain);
							int bundletaskID = insertTaskMaster(benchmarkq1, tt_id_b, tst_id_b, tsst_id_b, 0, true, 1,
									1, time_start, time_end, 3, 0, false, false, null, "", "", false, bench1UinOptions,
									"H" + habitid + "B1", false, false, "", 1, null);
							insertTaskHabitMapping(bundletaskID, habitid, row.getRowNum(), "HBQ");
						}
					} catch (Exception e6) {
						// TODO Auto-generated catch block
						e6.printStackTrace();
					}

					try {

						if (row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION2).getStringCellValue().trim()
								.replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION2).getStringCellValue()
										.trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkq2 = row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION2)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkans2 = row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS_INPUT)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS_INPUT)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkans2input = row
									.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS_INPUT)
									.getStringCellValue();
						}

						try {
							if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Single Answer From Options)")) {
								bench2UinOptions = "SO";
							} else if (row
									.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION2_ANSWER_OPTIONS_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Multiple Answers From Options)")) {
								bench2UinOptions = "MO";
							} else {
								bench2UinOptions = "TI";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (benchmarkq2 != null && !benchmarkq2.equals("")) {

							String[] benchmarks = null;
							if (benchmarkans2.contains(" or more")) {
								benchmarkans2 = benchmarkans2.replace(" or more", "");
							}
							if (benchmarkans2.contains(",")) {
								benchmarks = benchmarkans2.split(",");
							}

							if (benchmarkans2input.contains("Number")) {
								IntListDomain intDomain = new IntListDomain();
								intDomain.data = new ArrayList<>();
								for (int i = 0; i < benchmarks.length; i++) {
									intDomain.data.add(Integer.parseInt(benchmarks[i].trim()));
								}
								data_domain = new ObjectMapper().writeValueAsString(intDomain);

							} else if (benchmarkans2input.contains("Text")) {
								StringListDomain stringDomain = new StringListDomain();
								stringDomain.data = new ArrayList<>();
								for (int i = 0; i < benchmarks.length; i++) {
									stringDomain.data.add(benchmarks[i].trim());

								}
								data_domain = new ObjectMapper().writeValueAsString(stringDomain);
							}

							InsertReachDataTyper("H" + habitid + "B2", "", data_domain);
							int bundletaskID = insertTaskMaster(benchmarkq2, tt_id_b, tst_id_b, tsst_id_b, 0, true, 1,
									1, time_start, time_end, 3, 0, false, false, null, "", "", false, bench2UinOptions,
									"H" + habitid + "B2", false, false, "", 1, null);
							insertTaskHabitMapping(bundletaskID, habitid, row.getRowNum(), "HBQ");
						}
					} catch (Exception e5) {
						// TODO Auto-generated catch block
						e5.printStackTrace();
					}

					try {

						if (row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION3).getStringCellValue().trim()
								.replaceAll(" +", " ") != null
								&& !row.getCell(18).getStringCellValue().trim().replaceAll(" +", " ").trim()
										.equals("")) {
							benchmarkq3 = row.getCell(DataInjection.Habits.HABIT_BENCHMAR_QUESTION3)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkans3 = row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS_INPUT)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS_INPUT)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							benchmarkans3input = row
									.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS_INPUT)
									.getStringCellValue();
						}

						try {
							if (row.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Single Answer From Options)")) {
								bench3UinOptions = "SO";
							} else if (row
									.getCell(DataInjection.Habits.HABIT_BENCHMARK_QUESTION3_ANSWER_OPTIONS_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Multiple Answers From Options)")) {
								bench3UinOptions = "MO";
							} else {
								bench3UinOptions = "TI";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (benchmarkq3 != null && !benchmarkq3.equals("")) {
							String[] benchmarks = null;
							if (benchmarkans3.contains(" or more")) {
								benchmarkans3 = benchmarkans3.replace(" or more", "");
							}
							if (benchmarkans3.contains(",")) {
								benchmarks = benchmarkans3.split(",");
							}
							if (benchmarkans3input.contains("Number")) {
								IntListDomain intDomain = new IntListDomain();
								intDomain.data = new ArrayList<>();
								for (int i = 0; i < benchmarks.length; i++) {
									intDomain.data.add(Integer.parseInt(benchmarks[i].trim()));
								}
								data_domain = new ObjectMapper().writeValueAsString(intDomain);

							} else if (benchmarkans3input.contains("Text")) {
								StringListDomain stringDomain = new StringListDomain();
								stringDomain.data = new ArrayList<>();
								for (int i = 0; i < benchmarks.length; i++) {
									stringDomain.data.add(benchmarks[i].trim());

								}
								data_domain = new ObjectMapper().writeValueAsString(stringDomain);
							}
							InsertReachDataTyper("H" + habitid + "B3", "", data_domain);
							int bundletaskID = insertTaskMaster(benchmarkq3, tt_id_b, tst_id_b, tsst_id_b, 0, true, 1,
									1, time_start, time_end, 3, 0, false, false, null, "", "", false, bench3UinOptions,
									"H" + habitid + "B3", false, false, "", 1, null);
							insertTaskHabitMapping(bundletaskID, habitid, row.getRowNum(), "HBQ");

						}
					} catch (Exception e4) {
						// TODO Auto-generated catch block
						e4.printStackTrace();
					}

					try {
						tt_id_b = task_typeData.get("Assessment");
						tst_id_b = sutask_typeData.get("Assessment/(Habit)/(HabitProgressAssessmentQuestion)");
						tsst_id_b = subsubtask_typeData.get("Assessment/(Habit)/(HabitProgressAssessmentQuestion)");
						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1).getStringCellValue()
								.trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentq1 = row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_ANSWER_OPTIONS)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_ANSWER_OPTIONS)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentans1 = row
									.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_ANSWER_OPTIONS)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_INPUT)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_INPUT)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentans1input = row
									.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_INPUT)
									.getStringCellValue();
						}

						try {
							if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Single Answer From Options)")) {
								asses1UinOptions = "SO";
							} else if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION1_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Multiple Answers From Options)")) {
								asses1UinOptions = "MO";
							} else {
								asses1UinOptions = "TI";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (assessmentq1 != null && !assessmentq1.equals("")) {
							String[] assessmentArray = null;
							if (assessmentans1.contains(" or more")) {
								assessmentans1 = assessmentans1.replace(" or more", "");
							}
							if (assessmentans1.contains(",")) {
								assessmentArray = assessmentans1.split(",");
							}
							if (assessmentans1input.contains("Number")) {
								IntListDomain intDomain = new IntListDomain();
								intDomain.data = new ArrayList<>();
								for (int i = 0; i < assessmentArray.length; i++) {
									intDomain.data.add(Integer.parseInt(assessmentArray[i].trim()));
								}
								data_domain = new ObjectMapper().writeValueAsString(intDomain);

							} else if (assessmentans1input.contains("Text")) {
								StringListDomain stringDomain = new StringListDomain();
								stringDomain.data = new ArrayList<>();
								for (int i = 0; i < assessmentArray.length; i++) {
									stringDomain.data.add(assessmentArray[i].trim());

								}
								data_domain = new ObjectMapper().writeValueAsString(stringDomain);
							}

							InsertReachDataTyper("H" + habitid + "A1", "", data_domain);
							int bundletaskID = insertTaskMaster(assessmentq1, tt_id_b, tst_id_b, tsst_id_b, 0, true, 1,
									1, time_start, time_end, 3, 0, false, false, null, "", "", false, asses1UinOptions,
									"H" + habitid + "A1", false, false, "", 1, null);

							insertTaskHabitMapping(bundletaskID, habitid, row.getRowNum(), "HBAQ");
						}
					} catch (Exception e3) {
						// TODO Auto-generated catch block
						e3.printStackTrace();
					}

					try {

						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2).getStringCellValue()
								.trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentq2 = row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_ANSWER_OPTIONS)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_ANSWER_OPTIONS)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentans2 = row
									.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_ANSWER_OPTIONS)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_INPUT)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_INPUT)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentans2input = row
									.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_INPUT)
									.getStringCellValue();
						}

						try {
							if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Single Answer From Options)")) {
								asses2UinOptions = "SO";
							} else if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION2_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Multiple Answers From Options)")) {
								asses2UinOptions = "MO";
							} else {
								asses2UinOptions = "TI";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (assessmentq2 != null && !assessmentq2.equals("")) {
							String[] assessmentArray = null;
							if (assessmentans2.contains(" or more")) {
								assessmentans2 = assessmentans2.replace(" or more", "");
							}
							if (assessmentans2.contains(",")) {
								assessmentArray = assessmentans2.split(",");
							}

							if (assessmentans2input.contains("Number")) {
								IntListDomain intDomain = new IntListDomain();
								intDomain.data = new ArrayList<>();
								for (int i = 0; i < assessmentArray.length; i++) {
									intDomain.data.add(Integer.parseInt(assessmentArray[i].trim()));
								}
								data_domain = new ObjectMapper().writeValueAsString(intDomain);

							} else if (assessmentans2input.contains("Text")) {
								StringListDomain stringDomain = new StringListDomain();
								stringDomain.data = new ArrayList<>();
								for (int i = 0; i < assessmentArray.length; i++) {
									stringDomain.data.add(assessmentArray[i].trim());

								}
								data_domain = new ObjectMapper().writeValueAsString(stringDomain);
							}

							InsertReachDataTyper("H" + habitid + "A2", "", data_domain);
							int bundletaskID = insertTaskMaster(assessmentq2, tt_id_b, tst_id_b, tsst_id_b, 0, true, 1,
									1, time_start, time_end, 3, 0, false, false, null, "", "", false, asses2UinOptions,
									"H" + habitid + "A2", false, false, "", 1, null);

							insertTaskHabitMapping(bundletaskID, habitid, row.getRowNum(), "HBAQ");
						}
					} catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}

					try {

						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3).getStringCellValue()
								.trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentq3 = row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_ANSWER_OPTIONS)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_ANSWER_OPTIONS)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentans3 = row
									.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_ANSWER_OPTIONS)
									.getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_INPUT)
								.getStringCellValue().trim().replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_INPUT)
										.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
							assessmentans3input = row
									.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_INPUT)
									.getStringCellValue();
						}

						try {
							if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Single Answer From Options)")) {
								asses3UinOptions = "SO";
							} else if (row.getCell(DataInjection.Habits.HABIT_PROGRESS_ASSESSMENT_QUESTION3_INPUT_TYPE)
									.getStringCellValue().trim().equals("User Input (Multiple Answers From Options)")) {
								asses3UinOptions = "MO";
							} else {
								asses3UinOptions = "TI";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (assessmentq3 != null && !assessmentq3.equals("")) {
							String[] assessmentArray = null;
							if (assessmentans3.contains(" or more")) {
								assessmentans3 = assessmentans3.replace(" or more", "");
							}
							if (assessmentans3.contains(",")) {
								assessmentArray = assessmentans3.split(",");
							}
							if (assessmentans3input.contains("Number")) {
								IntListDomain intDomain = new IntListDomain();
								intDomain.data = new ArrayList<>();
								for (int i = 0; i < assessmentArray.length; i++) {
									intDomain.data.add(Integer.parseInt(assessmentArray[i].trim()));
								}
								data_domain = new ObjectMapper().writeValueAsString(intDomain);

							} else if (assessmentans3input.contains("Text")) {
								StringListDomain stringDomain = new StringListDomain();
								stringDomain.data = new ArrayList<>();
								for (int i = 0; i < assessmentArray.length; i++) {
									stringDomain.data.add(assessmentArray[i].trim());

								}
								data_domain = new ObjectMapper().writeValueAsString(stringDomain);
							}

							InsertReachDataTyper("H" + habitid + "A3", "", data_domain);
							int bundletaskID = insertTaskMaster(assessmentq3, tt_id_b, tst_id_b, tsst_id_b, 0, true, 1,
									1, time_start, time_end, 3, 0, false, false, null, "", "", false, asses3UinOptions,
									"H" + habitid + "A3", false, false, "", 1, null);

							insertTaskHabitMapping(bundletaskID, habitid, row.getRowNum(), "HBAQ");
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					try {

						if (row.getCell(DataInjection.Habits.TRACKING_QUESTION).getStringCellValue().trim()
								.replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.TRACKING_QUESTION).getStringCellValue().trim()
										.replaceAll(" +", " ").trim().equals("")) {
							trackingq = row.getCell(DataInjection.Habits.TRACKING_QUESTION).getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.TRACKING_QUESTION_ANSWER_OPTIONS).getStringCellValue()
								.trim().replaceAll(" +", " ") != null
								&& !row.getCell(43).getStringCellValue().trim().replaceAll(" +", " ").trim()
										.equals("")) {
							trackingans = row.getCell(43).getStringCellValue();
						}
						if (row.getCell(DataInjection.Habits.TRACKING_INPUT).getStringCellValue().trim()
								.replaceAll(" +", " ") != null
								&& !row.getCell(DataInjection.Habits.TRACKING_INPUT).getStringCellValue().trim()
										.replaceAll(" +", " ").trim().equals("")) {
							trackingansinput = row.getCell(DataInjection.Habits.TRACKING_INPUT).getStringCellValue();
						}

						try {
							if (row.getCell(DataInjection.Habits.TRACKING_INPUT_TYPE).getStringCellValue().trim()
									.equals("User Input (Single Answer From Options)")) {
								trackingUinOptions = "SO";
							} else if (row.getCell(DataInjection.Habits.TRACKING_INPUT_TYPE).getStringCellValue().trim()
									.equals("User Input (Multiple Answers From Options)")) {
								trackingUinOptions = "MO";
							} else {
								trackingUinOptions = "TI";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (trackingq != null && !trackingq.equals("")) {
							String[] trackingArray = null;
							if (trackingans.contains(" or more")) {
								trackingans = trackingans.replace(" or more", "");
							}
							if (trackingans.contains(",")) {
								trackingArray = trackingans.split(",");
							}
							if (trackingansinput.contains("Number")) {
								IntListDomain intDomain = new IntListDomain();
								intDomain.data = new ArrayList<>();
								for (int i = 0; i < trackingArray.length; i++) {
									intDomain.data.add(Integer.parseInt(trackingArray[i].trim()));
								}
								data_domain = new ObjectMapper().writeValueAsString(intDomain);

							} else if (trackingansinput.contains("Text")) {
								StringListDomain stringDomain = new StringListDomain();
								stringDomain.data = new ArrayList<>();
								for (int i = 0; i < trackingArray.length; i++) {
									stringDomain.data.add(trackingArray[i].trim());

								}
								data_domain = new ObjectMapper().writeValueAsString(stringDomain);
							}

							tt_id_b = task_typeData.get("Track");
							tst_id_b = sutask_typeData.get("Track/(Habit)");
							tsst_id_b = subsubtask_typeData.get("Track/(Habit)");

							InsertReachDataTyper("H" + habitid + "T", "", data_domain);
							int bundletaskID = insertTaskMaster(trackingq, tt_id_b, tst_id_b, tsst_id_b, 0, true, 1, 1,
									time_start, time_end, 3, 0, false, false, null, "", "", false, trackingUinOptions,
									"H" + habitid + "T", false, false, "", 1, null);
							insertTaskHabitMapping(bundletaskID, habitid, row.getRowNum(), "TQ");
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void BenchmarksTaskInjector() {
		// TODO Auto-generated method stub
		Sheet sheet = LoadWorkbook(DataInjection.TASK_TAGS);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				try {
					String tasktag;
					boolean iclusioncriteria = false;
					// Now let's iterate over the columns of the current row

					tasktag = row.getCell(2).getStringCellValue().trim().replaceAll(" +", " ");
					iclusioncriteria = row.getCell(3).getStringCellValue().trim().replaceAll(" +", " ").equals("Y");

					if (!tasktag.trim().equals("") && row.getRowNum() != 0) {

						insertTagData(tasktag, iclusioncriteria);

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					try {
						writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
								+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
								+ row.getCell(2).getAddress() + "or" + row.getCell(3).getAddress());
						writer.write("\r\n");
					} catch (IOException en) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					e.printStackTrace();
					logger.info(e + " at " + row.getRowNum() + " While reading data for TaskType .");
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {

		}

	}

	private static void CloseWriter() {
		// TODO Auto-generated method stub
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void LogQueries() {
		// TODO Auto-generated method stub

		try {
			writer = new FileWriter("output/QueriesLogMaster3.txt", true);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void PopulateDynamicData() throws DBError {
		// TODO Auto-generated method stub

		AddTaskTypes();

	}

	private static void AddTaskTypes() throws DBError {
		// TODO Auto-generated method stub
		AddTaskType();
		AddSubTaskType();
		AddSubSubTaskTypesandTasks();

	}

	private static void AddTaskType() throws DBError {
		// TODO Auto-generated method stub
		try {
			insertTaskTypeData("Bundle", 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void AddSubTaskType() throws DBError {
		// TODO Auto-generated method stub

		try {
			insertSubTaskTypeData("Bundle/Profile", 1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void AddSubSubTaskTypesandTasks() {
		// TODO Auto-generated method stub

		InsertProfileTaskData();

		InsertDynamicDataBundle();

	}

	private static void InsertDynamicDataBundle() {
		// TODO Auto-generated method stub
		Sheet sheet = LoadWorkbook(DataInjection.HABIT);
		String label = "";
		try {
			for (String key : task_labelData.keySet()) {
				// search for value
				if ("UserData/DynamicData/".contains(key)) {
					label = task_labelData.get(key);
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			// 35 to 41
			String taskmasterUds = "";
			TaskMasterUtil masterUtil = null;
			Time startTime = null,endTime = null;
			int duration = 0;
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				for (int i = 35; i < 42; i++) {
					String dynamicData = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
					if (subsubtask_typeData.containsKey(dynamicData)) {
						int tsstid;
						masterUtil = GetIDFromTaskMaster(subsubtask_typeData.get(dynamicData));
						if (masterUtil.getId() != 0) {
							if (!taskmasterUds.equalsIgnoreCase("")) {
								taskmasterUds = taskmasterUds + ",";
							}
							if(startTime!=null){
								if(startTime.after(masterUtil.getTime_start())){
									startTime=masterUtil.getTime_start();
								}
							}else{
								startTime=masterUtil.getTime_start();
							}
							if(endTime!=null){
								if(endTime.before(masterUtil.getTime_end())){
									endTime=masterUtil.getTime_end();
								}
							}else{
								endTime=masterUtil.getTime_end();
							}
							
							
							duration = duration + masterUtil.getDuration();

							taskmasterUds = taskmasterUds + masterUtil.getId();

						}

					}
				}

				String habit = row.getCell(DataInjection.Habits.HABIT).getStringCellValue().trim().replaceAll(" +",
						" ");
				int habitkey = GetHabitIDFromTable(habit);

				if (habitkey != 0) {
					try {

						// insertSubSubTaskTypeData("UserData/DynamicData/" +
						// habitkey, 0, true, label);
						// SimpleDateFormat format = new
						// SimpleDateFormat("HH.mm");
						// java.util.Date d2 = (java.util.Date)
						// format.parse("0.0 am");
						// java.util.Date d1 = (java.util.Date)
						// format.parse("0.0 am");
						//
						// Time time_start = new java.sql.Time(d1.getTime());
						// Time time_end = new java.sql.Time(d2.getTime());

						if (!taskmasterUds.equals("")) {
							int bundletaskID = insertTaskMaster(
									"Help us know the effect of your habit journey on key parameters.",
									task_typeData.get("UserData"), sutask_typeData.get("UserData/DynamicData"),
									subsubtask_typeData.get("UserData/DynamicData"), masterUtil.getLevel(), true,
									masterUtil.getFreq(), duration, startTime,
									endTime, masterUtil.getTask_day(), masterUtil.getScore(),
									masterUtil.getDare(), false, 0, null, "", false, "SO", "", false, true,
									taskmasterUds, 1, null);
							insertTaskHabitMapping(bundletaskID, habitkey, row.getRowNum(), "DD");
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block

						e.printStackTrace();
					}
				}

			}

		}
	}

	private static TaskMasterUtil GetIDFromTaskMaster(Integer dynamicData) {
		// TODO Auto-generated method stub
		TaskMasterUtil masterUtil = new TaskMasterUtil();

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			String insertTableSQL = "SELECT * FROM task_masters WHERE tsst_id = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setInt(1, dynamicData);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				// System.out.print("Column 1 returned ");
				// System.out.println(rs.getInt(1));
				masterUtil.setId(rs.getInt("id"));
				masterUtil.setDare(rs.getBoolean("dare"));
				masterUtil.setDuration(rs.getInt("duration"));
				masterUtil.setFreq(rs.getInt("freq"));
				masterUtil.setTime_start(rs.getTime("time_start"));
				masterUtil.setTime_end(rs.getTime("time_end"));
				masterUtil.setLevel(rs.getInt("level"));
				masterUtil.setTask_day(rs.getInt("task_day"));
				masterUtil.setScore(rs.getInt("score"));
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return masterUtil;
	}

	private static int InsertProfileTaskData() {
		// TODO Auto-generated method stub
		Sheet sheet = LoadWorkbook(DataInjection.HABIT);
		int habitkey = 0;
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			String profileQuestion = "";
			String habit = "", label = "";
			if (row.getRowNum() != 0) {
				try {
					profileQuestion = row.getCell(DataInjection.Habits.PROFILE_QUESTION1).getStringCellValue().trim()
							.replaceAll(" +", " ");
					if (profileQuestion.trim().equals("")) {
						profileQuestion = row.getCell(DataInjection.Habits.PROFILE_QUESTION2).getStringCellValue()
								.trim().replaceAll(" +", " ");
						if (!profileQuestion.trim().equals("")) {
							habit = row.getCell(DataInjection.Habits.HABIT).getStringCellValue().trim().replaceAll(" +",
									" ");
							habitkey = GetHabitIDFromTable(habit);
							if (habitkey != 0) {
								try {
									try {
										for (String key : task_labelData.keySet()) {
											// search for value
											if ("UserData/Profile/".contains(key)) {
												label = task_labelData.get(key);
											}

										}
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									// insertSubSubTaskTypeData("Bundle/Profile/"
									// + habitkey, 0, true, label);
									// SimpleDateFormat format = new
									// SimpleDateFormat("HH.mm");
									// java.util.Date d2 = (java.util.Date)
									// format.parse("0.0 am");
									// java.util.Date d1 = (java.util.Date)
									// format.parse("0.0 am");
									//
									// Time time_start = new
									// java.sql.Time(d1.getTime());
									// Time time_end = new
									// java.sql.Time(d2.getTime());
									TaskMasterUtil masterUtil = GetTaskIDFromTask(profileQuestion);
									if (masterUtil.getId() != 0) {
										int bundletaskID = insertTaskMaster("Profile Questions",
												task_typeData.get("UserData"), sutask_typeData.get("UserData/Profile"),
												subsubtask_typeData.get("UserData/Profile"), masterUtil.getLevel(),
												false, masterUtil.getFreq(), masterUtil.getDuration(),
												masterUtil.getTime_start(), masterUtil.getTime_end(),
												masterUtil.getTask_day(), masterUtil.getScore(), false, false, 0, null,
												"", false, "TI", "", false, true, masterUtil.getId() + "", 1, null);
										insertTaskHabitMapping(bundletaskID, habitkey, row.getRowNum(), "PQ");
									}

								} catch (Exception e) {
									// TODO Auto-generated catch block
									try {
										writer.write("Issue while processing sheet [ " + sheet.getSheetName()
												+ " ] row [ " + row.getRowNum() + " ], Got error [ " + e.getMessage()
												+ " ] in cellNo -" + row.getCell(6).getAddress() + "or"
												+ row.getCell(6).getAddress());
										writer.write("\r\n");
									} catch (IOException en) {
										// TODO Auto-generated catch block
										en.printStackTrace();
									}
									e.printStackTrace();
								}
							}

						}
					} else {
						habit = row.getCell(DataInjection.Habits.HABIT).getStringCellValue().trim().replaceAll(" +",
								" ");
						habitkey = GetHabitIDFromTable(habit);
						if (habitkey != 0) {
							try {
								try {
									for (String key : task_labelData.keySet()) {
										// search for value
										if ("Bundle/Profile/".contains(key)) {
											label = task_labelData.get(key);
										}

									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								// insertSubSubTaskTypeData("UserData/Profile/"
								// + habitkey, 0, true, label);

								// SimpleDateFormat format = new
								// SimpleDateFormat("HH.mm");
								// java.util.Date d2 = (java.util.Date)
								// format.parse("0.0 am");
								// java.util.Date d1 = (java.util.Date)
								// format.parse("0.0 am");
								//
								// Time time_start = new
								// java.sql.Time(d1.getTime());
								// Time time_end = new
								// java.sql.Time(d2.getTime());

								String profileQuestion2 = row.getCell(DataInjection.Habits.PROFILE_QUESTION2)
										.getStringCellValue().trim().replaceAll(" +", " ");
								if (!profileQuestion2.trim().equals("")) {
									TaskMasterUtil masterUtil = GetTaskIDFromTask(profileQuestion);
									TaskMasterUtil masterUtil2 = GetTaskIDFromTask(profileQuestion2);
									String taskID = "";
									if (masterUtil.getId() != 0 && masterUtil2.getId() != 0) {
										taskID = masterUtil.getId() + "," + masterUtil2.getId();
									} else if (masterUtil.getId() != 0) {
										taskID = Integer.toString(masterUtil.getId());
									} else {
										taskID = Integer.toString(masterUtil2.getId());
									}
									int duration = masterUtil.getDuration() + masterUtil2.getDuration();
									Time startTime, endTime;
									if (masterUtil.getTime_start().before(masterUtil2.getTime_start())) {
										startTime = masterUtil.getTime_start();
									} else {
										startTime = masterUtil2.getTime_start();
									}
									if (masterUtil.getTime_end().before(masterUtil2.getTime_end())) {
										endTime = masterUtil2.getTime_end();
									} else {
										endTime = masterUtil.getTime_end();
									}

									int bundletaskID = insertTaskMaster("Profile Questions",
											task_typeData.get("UserData"), sutask_typeData.get("UserData/Profile"),
											subsubtask_typeData.get("UserData/Profile"), masterUtil.getLevel(), false,
											masterUtil.getFreq(), duration, startTime, endTime,
											masterUtil.getTask_day(), masterUtil.getScore(), false, false, 0, null, "",
											false, "TI", "", false, true, taskID, 1, null);
									insertTaskHabitMapping(bundletaskID, habitkey, row.getRowNum(), "PQ");

								} else {
									TaskMasterUtil masterUtil = GetTaskIDFromTask(profileQuestion);
									if (masterUtil.getId() != 0) {
										int bundletaskID = insertTaskMaster("Profile Questions",
												task_typeData.get("UserData"), sutask_typeData.get("UserData/Profile"),
												subsubtask_typeData.get("UserData/Profile"), masterUtil.getLevel(),
												false, masterUtil.getFreq(), masterUtil.getDuration(),
												masterUtil.getTime_start(), masterUtil.getTime_end(),
												masterUtil.getTask_day(), masterUtil.getScore(), false, false, 0, null,
												"", false, "TI", "", false, true, masterUtil.getId() + "", 1, null);
										insertTaskHabitMapping(bundletaskID, habitkey, row.getRowNum(), "PQ");
									}

								}

							} catch (Exception e) {
								// TODO Auto-generated catch block
								try {
									writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
											+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
											+ row.getCell(0).getAddress() + "or" + row.getCell(7).getAddress());
									writer.write("\r\n");
								} catch (IOException en) {
									// TODO Auto-generated catch block
									en.printStackTrace();
								}
								e.printStackTrace();
							}
						}
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return habitkey;
	}

	private static void insertTaskHabitMapping(int taskID, int habitkey, int seqid, String groupType) throws DBError {
		// TODO Auto-generated method stub

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			String insertTableSQL = "INSERT INTO task_habit_map" + "(task_id, habit_id,group_type,status) VALUES"
					+ "(?,?,?,?)";

			try {
				DataInjection dataInjection = new DataInjection();
				dbConnection = dataInjection.connect();

				preparedStatement = dbConnection.prepareStatement(insertTableSQL);

				preparedStatement.setInt(1, taskID);
				preparedStatement.setInt(2, habitkey);
				preparedStatement.setString(3, groupType);

				preparedStatement.setInt(4, 1);
				// try {
				// writer.write(preparedStatement.toString());
				// writer.write("\r\n");
				// } catch (IOException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				// execute insert SQL stetement

				preparedStatement.executeUpdate();

				// System.out.println("Record is inserted into barrier table!");

			} catch (SQLException e) {
				logger.info(e + " While writing data for Barriers .");
				// System.out.println(e.getMessage());

			} finally {

				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.info(e + " While writing data for barriers .");
		}

	}

	private static TaskMasterUtil GetTaskIDFromTask(String profileQuestion) {
		// TODO Auto-generated method stub
		TaskMasterUtil masterUtil = new TaskMasterUtil();

		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			String insertTableSQL = "SELECT * FROM task_masters WHERE title = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, profileQuestion);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				// System.out.print("Column 1 returned ");
				// System.out.println(rs.getInt(1));

				masterUtil.setId(rs.getInt("id"));
				masterUtil.setDare(rs.getBoolean("dare"));
				masterUtil.setDuration(rs.getInt("duration"));
				masterUtil.setFreq(rs.getInt("freq"));
				masterUtil.setTime_start(rs.getTime("time_start"));
				masterUtil.setTime_end(rs.getTime("time_end"));
				masterUtil.setLevel(rs.getInt("level"));
				masterUtil.setTask_day(rs.getInt("task_day"));
				masterUtil.setScore(rs.getInt("score"));
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return masterUtil;

	}

	private static int GetTaskIDFromHabits(String profileQuestion) {
		// TODO Auto-generated method stub
		int habitkey = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			String insertTableSQL = "SELECT id FROM habits WHERE title = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, profileQuestion);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				// System.out.print("Column 1 returned ");
				// System.out.println(rs.getInt(1));
				habitkey = rs.getInt(1);
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return habitkey;

	}

	private static int GetHabitIDFromTable(String habit) {
		// TODO Auto-generated method stub
		int habitkey = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			String insertTableSQL = "SELECT id FROM habits WHERE title = ?";

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);
			preparedStatement.setString(1, habit);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				// System.out.print("Column 1 returned ");
				// System.out.println(rs.getInt(1));
				habitkey = rs.getInt(1);
			}
			rs.close();
			preparedStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return habitkey;
	}

	@SuppressWarnings("deprecation")
	private static void PopulateTaskMaster() {
		// TODO Auto-generated method stub
		Sheet sheet = LoadWorkbook(DataInjection.TASK);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				try {
					String title, full_desc, param_csv, uin_option, uin_data_type, bundle_task_csv;
					String[] tag_list = null;
					int tt_id = 0, tst_id = 0, tsst_id = 0, level = 0, freq = 0, duration = 0, task_day = 0, score = 0,
							status = 1;
					Integer user_id = null;
					Time time_start = null, time_end = null;
					boolean repeatable = true, dare = false, custom = false, tagable_output = false, global = true,
							bundle_task;
					// Now let's iterate over the columns of the current row
					String statustitle = row.getCell(DataInjection.Tasks.STATUS).getStringCellValue().trim()
							.replaceAll(" +", " ");

					if (row.getRowNum() != 0 && statustitle.equals("Y")) {

						title = row.getCell(DataInjection.Tasks.TASK_LIST).getStringCellValue().trim().replaceAll(" +",
								" ");
						full_desc = "";
						param_csv = "";
						uin_option = "";
						uin_data_type = "";
						bundle_task_csv = "";
						try {
							tt_id = task_typeData.get(row.getCell(DataInjection.Tasks.TASK_TYPE).getStringCellValue()
									.trim().replaceAll(" +", " "));
						} catch (Exception e1) {
							try {
								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
										+ row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
										+ row.getCell(0).getAddress());
								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}

							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							tst_id = sutask_typeData.get(row.getCell(DataInjection.Tasks.TASK_SUBTYPE)
									.getStringCellValue().trim().replaceAll(" +", " "));
						} catch (Exception e1) {
							try {
								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
										+ row.getRowNum() + " ], Got error [ " + e1.toString() + " ] in cellNo - "
										+ row.getCell(2).getAddress());
								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							tsst_id = subsubtask_typeData.get(row.getCell(DataInjection.Tasks.TASK_SUB_SUBTYPE)
									.getStringCellValue().trim().replaceAll(" +", " "));
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							
							e1.printStackTrace();
						}
						if (tsst_id == 0) {
							try {
								tsst_id = subsubtask_typeData.get(row.getCell(DataInjection.Tasks.TASK_SUBTYPE)
										.getStringCellValue().trim().replaceAll(" +", " "));
							} catch (Exception e1) {
								try {
									writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
											+ row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
											+ row.getCell(DataInjection.Tasks.TASK_SUBTYPE).getAddress());
									writer.write("\r\n");
								} catch (Exception en) {
									// TODO Auto-generated catch block
									en.printStackTrace();
								}
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
						try {
							if (Cell.CELL_TYPE_STRING == row.getCell(DataInjection.Tasks.TASK_FREQUENCY)
									.getCellType()) {
								freq = Integer.parseInt(row.getCell(DataInjection.Tasks.TASK_FREQUENCY)
										.getStringCellValue().trim().replaceAll(" +", " ").substring(
												row.getCell(DataInjection.Tasks.TASK_FREQUENCY).getStringCellValue()
														.trim().replaceAll(" +", " ").indexOf("/") + 1,
												row.getCell(DataInjection.Tasks.TASK_FREQUENCY).getStringCellValue()
														.trim().replaceAll(" +", " ").indexOf("/") + 2));
							} else if (Cell.CELL_TYPE_NUMERIC == row.getCell(DataInjection.Tasks.TASK_FREQUENCY).getCellType()) {
								freq = (int) row.getCell(DataInjection.Tasks.TASK_FREQUENCY).getNumericCellValue();
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							try {
								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
										+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
										+ row.getCell(DataInjection.Tasks.TASK_FREQUENCY).getAddress());
								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}
							e.printStackTrace();
						}

						try {
							duration = (int) row.getCell(DataInjection.Tasks.TASK_DURATION).getNumericCellValue();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							try {
								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
										+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
										+ row.getCell(DataInjection.Tasks.TASK_DURATION).getAddress());
								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}
							e.printStackTrace();
						}

						try {
							if (row.getCell(DataInjection.Tasks.TASK_DAY).getStringCellValue().trim()
									.replaceAll(" +", " ").equals("Weekend")) {
								task_day = 3;
							} else if (row.getCell(DataInjection.Tasks.TASK_DAY).getStringCellValue().trim()
									.replaceAll(" +", " ").equals("All")) {
								task_day = 1;
							} else if (row.getCell(DataInjection.Tasks.TASK_DAY).getStringCellValue().trim()
									.replaceAll(" +", " ").equals("Weekday")) {
								task_day = 2;
							}
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							try {
								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
										+ row.getRowNum() + " ], Got error [ " + e2.getMessage() + " ] in cellNo -"
										+ row.getCell(DataInjection.Tasks.TASK_DAY).getAddress());
								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}
							e2.printStackTrace();
						}

						try {
							String time = row.getCell(DataInjection.Tasks.TASK_TIME_SLOT).getStringCellValue().trim()
									.replaceAll(" +", " ");
							String[] timearray = time.substring(time.indexOf("(") + 1, time.indexOf(")")).split("-");

							SimpleDateFormat format = new SimpleDateFormat("hh.mm a");
							java.util.Date d2 = (java.util.Date) format.parse(timearray[1].trim());
							java.util.Date d1 = (java.util.Date) format.parse(timearray[0].trim());

							time_start = new java.sql.Time(d1.getTime());
							time_end = new java.sql.Time(d2.getTime());
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							try {
								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
										+ row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
										+ row.getCell(DataInjection.Tasks.TASK_TIME_SLOT).getAddress());
								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}

							SimpleDateFormat format = new SimpleDateFormat("hh.mm a");
							java.util.Date d2 = (java.util.Date) format.parse("0.0 am");
							java.util.Date d1 = (java.util.Date) format.parse("0.0 am");

							time_start = new java.sql.Time(d1.getTime());
							time_end = new java.sql.Time(d2.getTime());

							e1.printStackTrace();
						}

						try {
							dare = row.getCell(DataInjection.Tasks.DARE_APPLICABILITY).getStringCellValue().trim()
									.replaceAll(" +", " ").trim().equals("Y");
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							try {
								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
										+ row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
										+ row.getCell(10).getAddress());
								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}
							e1.printStackTrace();
						}
						ArrayList<String> tagList = new ArrayList<>();
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG1).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG2).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG3).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG4).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG5).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG6).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG7).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG8).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG9).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}
						try {

							String tag = row.getCell(DataInjection.Tasks.TASk_TAG10).getStringCellValue().trim()
									.replaceAll(" +", " ");
							if (tag != null && !tag.equals("")) {
								tagList.add(tag);
							}

						} catch (Exception e1) {
							// TODO Auto-generated catch block

						}

						try {
							if (tagList.size() > 0) {
								StringtagListDomain listDomain = new StringtagListDomain();
								listDomain.tags = new ArrayList<>();
								for (String temp : tagList) {
									listDomain.tags.add(temp);
								}
								if (listDomain.tags.size() > 0) {

									tag_list = listDomain.tags.toArray(new String[listDomain.tags.size()]);
								}
							}
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}

						try {
							if (title.contains("<")) {
								param_csv = title.substring(title.indexOf("<") + 1, title.indexOf(">"));
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						String key = null;
						try {
							key = row.getCell(DataInjection.Tasks.TASK_SUB_SUBTYPE).getStringCellValue().trim()
									.replaceAll(" +", " ").split("/")[2];
							tagable_output = reachDataType.containsKey(key);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							try {
//								writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
//										+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
//										+ row.getCell(DataInjection.Tasks.TASK_SUB_SUBTYPE).getAddress());
//								writer.write("\r\n");
							} catch (Exception en) {
								// TODO Auto-generated catch block
								en.printStackTrace();
							}
							e.printStackTrace();
						}
						try {
							if (tagable_output) {
								uin_option = "SO";
							} else {
								uin_option = "TI";
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							if (key != null
									&& (reachDataType.containsKey(key) | reachDataTypeManual.containsKey(key))) {
								uin_data_type = key;
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							if (uin_data_type.equals("")) {
								String tasktype = row.getCell(1).getStringCellValue().trim().replaceAll(" +", " ");
								if (tasktype != null) {
									if (reachDataType.containsKey(tasktype)) {
										uin_data_type = tasktype;
										uin_option = "SO";

									}

								}
							}

						} catch (Exception e1) {

							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						bundle_task = false;
						bundle_task_csv = "";
						if (!title.trim().equals("")) {
							insertTaskMaster(title, tt_id, tst_id, tsst_id, level, repeatable, freq, duration,
									time_start, time_end, task_day, score, dare, custom, user_id, full_desc, param_csv,
									tagable_output, uin_option, uin_data_type, global, bundle_task, bundle_task_csv,
									status, tag_list);
						}

					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.info(e + " at " + row.getRowNum() + " While reading data for TaskType .");
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {
			logger.info(e + " While reading data for HabitMaster .");
		}
	}

	private static int insertTaskMaster(String title, int tt_id, int tst_id, int tsst_id, int level, boolean repeatable,
			int freq, int duration, Time time_start, Time time_end, int task_day, int score, boolean dare,
			boolean custom, Integer user_id, String full_desc, String param_csv, boolean tagable_output,
			String uin_option, String uin_data_type, boolean global, boolean bundle_task, String bundle_task_csv,
			int status, String[] tag_list) {
		// TODO Auto-generated method stub
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO task_masters"
				+ "(title, tt_id,tst_id,tsst_id,level,repeatable,freq,duration,time_start,time_end,task_day,score,dare,custom,full_desc,param_csv,tagable_output,uin_option,uin_data_type,global,bundle_task,bundle_task_csv,task_tags,status) VALUES"
				+ "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		 * PGobject jsonObject = new PGobject(); jsonObject.setType("json"); try
		 * {
		 * 
		 * 
		 * jsonObject.setValue(task_tags); } catch (SQLException e1) { // TODO
		 * Auto-generated catch block e1.printStackTrace(); }
		 

		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			java.sql.Array sqlArray = null;
			try {
				sqlArray = dbConnection.createArrayOf("VARCHAR", tag_list);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			int i = 0;

			preparedStatement.setString(++i, title);
			preparedStatement.setInt(++i, tt_id);
			preparedStatement.setInt(++i, tst_id);
			preparedStatement.setInt(++i, tsst_id);
			preparedStatement.setInt(++i, level);
			preparedStatement.setBoolean(++i, repeatable);
			preparedStatement.setInt(++i, freq);
			preparedStatement.setInt(++i, duration);
			preparedStatement.setTime(++i, time_start);
			preparedStatement.setTime(++i, time_end);
			preparedStatement.setInt(++i, task_day);
			preparedStatement.setInt(++i, score);
			preparedStatement.setBoolean(++i, dare);
			preparedStatement.setBoolean(++i, custom);
			preparedStatement.setString(++i, full_desc);
			preparedStatement.setString(++i, param_csv);
			preparedStatement.setBoolean(++i, tagable_output);
			preparedStatement.setString(++i, uin_option);
			if (uin_data_type.equals("")) {
				uin_data_type = null;
			}
			preparedStatement.setString(++i, uin_data_type);
			preparedStatement.setBoolean(++i, global);
			preparedStatement.setBoolean(++i, bundle_task);
			preparedStatement.setString(++i, bundle_task_csv);
			preparedStatement.setArray(++i, sqlArray);
			preparedStatement.setInt(++i, status);
			// execute insert SQL stetement
			System.out.println(preparedStatement);
			// try {
			// writer.write(preparedStatement.toString());
			// writer.write("\r\n");
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}

			System.out.println("Record is inserted into task_masters table!");
			logger.info(preparedStatement.toString());

		} catch (Exception e) {

			System.out.println(e.getMessage());
			logger.info(e + " While writing data for task_masters .");

		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return (int) key;

	}

	private static void PopulateTaskTags() {
		// TODO Auto-generated method stub

		Sheet sheet = LoadWorkbook(DataInjection.TASK_TAGS);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				try {
					String tasktag;
					boolean iclusioncriteria = false;
					// Now let's iterate over the columns of the current row

					tasktag = row.getCell(2).getStringCellValue().trim().replaceAll(" +", " ");
					iclusioncriteria = row.getCell(3).getStringCellValue().trim().replaceAll(" +", " ").equals("Y");

					if (!tasktag.trim().equals("") && row.getRowNum() != 0) {

						insertTagData(tasktag, iclusioncriteria);

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					try {
						writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
								+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
								+ row.getCell(2).getAddress() + "or" + row.getCell(3).getAddress());
						writer.write("\r\n");
					} catch (IOException en) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					e.printStackTrace();
					logger.info(e + " at " + row.getRowNum() + " While reading data for TaskType .");
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {
			logger.info(e + " While reading data for HabitMaster .");
		}
	}

	@SuppressWarnings("rawtypes")
	private static void PopulateReachDataType() throws DBError {
		// TODO Auto-generated method stub
		reachDataTypeManual = new HashMap<>();
		reachDataType = new HashMap<>();
		reachDataType.put("Gender", "genders");
		reachDataType.put("Goal", "goals");
		reachDataType.put("ActivityLevel", "activity_levels");
		reachDataType.put("CuisinePreferences", "cusine_prefs");
		reachDataType.put("FoodPreferences", "food_prefs");
		reachDataType.put("MealPreferences", "meal_prefs");
		reachDataType.put("FoodAllergyOrIntolerance", "allergy_intolrances");
		reachDataType.put("ExercisePreferences", "exer_prefs");
		reachDataType.put("RateOfChange", "rate_changes");
		reachDataType.put("Action", "action_task_ans");
		reachDataType.put("Content", "content_task_ans");
		reachDataType.put("Social", "social_task_ans");
		String emailExpression = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)* @[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$;";
		String phonenoExpression = "d{3}-d{7}";

		reachDataTypeManual.put("FirstName", "{\"type\":\"REGEX\", \"regEX\":\"([a-zA-Z]{3,30}*)+\"}");
		reachDataTypeManual.put("MiddleName", "{\"type\":\"REGEX\", \"regEX\":\"([a-zA-Z]{3,30}*)+\"}");

		reachDataTypeManual.put("LastName", "{\"type\":\"REGEX\", \"regEX\":\"([a-zA-Z]{3,30}*)+\"}");

		reachDataTypeManual.put("PersonalEmailId", "{\"type\":\"REGEX\", \"regEX\":\"" + emailExpression + "\"}");

		reachDataTypeManual.put("OfficialEmailId", "{\"type\":\"REGEX\", \"regEX\":\"" + emailExpression + "\"}");

		reachDataTypeManual.put("MobileNumber", "{\"type\":\"REGEX\", \"regEX\":\"" + phonenoExpression + "\"}");

		reachDataTypeManual.put("CountryCode", "{\"type\":\"REGEX\", \"regEX\":\"([a-zA-Z]{3,30}*)+\"}");

		reachDataTypeManual.put("DateOfBirth",
				"{ \"type\":\" Date\",\"min\":2922, \"max\":29210, \"format\":\"yyyy-mm-dd\"}");

		reachDataTypeManual.put("Height",
				"{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"feet\",\"inch\"],[\"cm\"]], \"min\":122, \"max\":214}");

		reachDataTypeManual.put("Weight",
				"{ \"type\":\"SCALE\",\"standard\":\"gram\",\"scales\":[[\"kg\"],[\"pound\"]], \"min\":30000, \"max\":170000}");

		reachDataTypeManual.put("Chest",
				"{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[\"inch\"], \"min\":63.5, \"max\":381}");

		reachDataTypeManual.put("Waist",
				"{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[\"inch\"], \"min\":50.8, \"max\":254}");

		reachDataTypeManual.put("Hip",
				"{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[\"inch\"], \"min\":63.5, \"max\":381}");

		reachDataTypeManual.put("Arm",
				"{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[\"inch\"], \"min\":12.7, \"max\":50.8}");

		reachDataTypeManual.put("Thigh",
				"{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[\"inch\"], \"min\":20.32, \"max\":101.6}");

		reachDataTypeManual.put("Calf",
				"{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[\"inch\"], \"min\":12.7, \"max\":63.5}");

		reachDataTypeManual.put("TargetWeight",
				"{ \"type\":\"SCALE\",\"standard\":\"gram\",\"scales\":[[\"kg\"],[\"pound\"]], \"min\":30000, \"max\":170000}");

		reachDataTypeManual.put("StepCountTarget",
				"{ \"type\":\"SCALE\",\"standard\":\"step\",\"scales\":[\"step\"], \"min\":0, \"max\":100000}");

		Iterator<?> it = reachDataType.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			InsertReachDataTyperForReferTable(pair.getKey().toString(), pair.getValue().toString(), "");
			// it.remove(); // avoids a ConcurrentModificationException
		}

		Iterator<?> itmanual = reachDataTypeManual.entrySet().iterator();
		while (itmanual.hasNext()) {
			Map.Entry pair = (Map.Entry) itmanual.next();
			InsertReachDataTyper(pair.getKey().toString(), "", pair.getValue().toString());
			// it.remove(); // avoids a ConcurrentModificationException
		}

	}

	private static int InsertReachDataTyperForReferTable(String type_code, String refer_table, String data_domain)
			throws DBError {
		// TODO Auto-generated method stub
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO reach_data_types" + "(type_code, refer_table,version) VALUES" + "(?,?,?)";

		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(data_domain);

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, type_code);
			preparedStatement.setString(2, refer_table);
			preparedStatement.setInt(3, 1);

			// try {
			// writer.write(preparedStatement.toString());
			// writer.write("\r\n");
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}

			// System.out.println("Record is inserted into reach_data_type
			// table!");

		} catch (SQLException e) {

			// System.out.println(e.getMessage());
			logger.info(e + " While writing data for vision_types .");

		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return (int) key;

	}

	private static int InsertReachDataTyper(String type_code, String refer_table, String data_domain) throws DBError {
		// TODO Auto-generated method stub
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO reach_data_types(type_code, refer_table,version,data_domain) VALUES(?,?,?,?)";

		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(data_domain);

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, type_code);
			preparedStatement.setString(2, refer_table);
			preparedStatement.setInt(3, 1);
			preparedStatement.setObject(4, jsonObject);
			// try {
			// writer.write(preparedStatement.toString());
			// writer.write("\r\n");
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			// System.out.println("Record is inserted into reach_data_type
			// table!");

		} catch (SQLException e) {

			// System.out.println(e.getMessage());
			logger.info(e + " While writing data for vision_types .");

		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return (int) key;

	}
	/*
	 * private static void PopulatetaskTimings() { // TODO Auto-generated method
	 * stub Sheet sheet = LoadWorkbook(DataInjection.TASK);
	 * 
	 * Iterator<Row> rowIterator = sheet.rowIterator(); try { while
	 * (rowIterator.hasNext()) { Row row = rowIterator.next(); try { String
	 * title, startTime, endTime, time; // Now let's iterate over the columns of
	 * the current row
	 * 
	 * title = row.getCell(0).getStringCellValue().trim().replaceAll(" +", " ");
	 * time = row.getCell(8).getStringCellValue().trim().replaceAll(" +", " ");
	 * startTime = time.substring(time.indexOf("(") + 1,
	 * time.indexOf(")")).split("-")[0].trim(); endTime =
	 * time.substring(time.indexOf("(") + 1,
	 * time.indexOf(")")).split("-")[1].trim(); SimpleDateFormat dateFormat =
	 * new SimpleDateFormat("hh.mm a"); Date parsedDate =
	 * dateFormat.parse(startTime); Timestamp timestampstart = new
	 * java.sql.Timestamp(parsedDate.getTime()); Date parsedDateend =
	 * dateFormat.parse(endTime); Timestamp timestampend = new
	 * java.sql.Timestamp(parsedDateend.getTime()); if (!title.trim().equals("")
	 * && row.getRowNum() != 0) { insertTaskTimings(title, timestampstart,
	 * timestampend, true);
	 * 
	 * } } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); logger.info(e + " at " + row.getRowNum() +
	 * " While reading data for TaskType ."); }
	 * 
	 * // System.out.println();
	 * 
	 * } } catch (NullPointerException e) { logger.info(e +
	 * " While reading data for HabitMaster ."); } }
	 */

	/*
	 * private static void insertTaskTimings(String title, Timestamp startTime,
	 * Timestamp endTime, boolean status) { // TODO Auto-generated method stub
	 * long key = 0; Connection dbConnection = null; PreparedStatement
	 * preparedStatement = null;
	 * 
	 * try { String insertTableSQL = "INSERT INTO tags" +
	 * "(title,start_time,end_time,status) VALUES" + "(?,?)";
	 * 
	 * try { DataInjection dataInjection = new DataInjection(); dbConnection =
	 * dataInjection.connect();
	 * 
	 * preparedStatement = dbConnection.prepareStatement(insertTableSQL,
	 * java.sql.Statement.RETURN_GENERATED_KEYS);
	 * 
	 * preparedStatement.setString(1, title); preparedStatement.setTimestamp(2,
	 * startTime); preparedStatement.setTimestamp(2, endTime);
	 * preparedStatement.setBoolean(2, status); // try { //
	 * writer.write(preparedStatement.toString()); // writer.write("\r\n"); // }
	 * catch (IOException e) { // // TODO Auto-generated catch block //
	 * e.printStackTrace(); // }
	 * 
	 * // execute insert SQL stetement preparedStatement.executeUpdate();
	 * 
	 * ResultSet rs = preparedStatement.getGeneratedKeys();
	 * 
	 * if (rs.next()) { key = rs.getLong(1); }
	 * 
	 * // System.out.println("Record is inserted into vision table!");
	 * 
	 * } catch (SQLException e) {
	 * 
	 * // System.out.println(e.getMessage()); logger.info(e +
	 * " While writing data for vision_types .");
	 * 
	 * } finally {
	 * 
	 * if (preparedStatement != null) { preparedStatement.close(); }
	 * 
	 * if (dbConnection != null) { dbConnection.close(); }
	 * 
	 * } } catch (SQLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } // return (int) key;
	 * 
	 * }
	 * 
	 * private static void PopulateTaskTag() { // TODO Auto-generated method
	 * stub
	 * 
	 * Sheet sheet = LoadWorkbook(DataInjection.DROPDOWNS);
	 * 
	 * Iterator<Row> rowIterator = sheet.rowIterator(); try { while
	 * (rowIterator.hasNext()) { Row row = rowIterator.next(); try { String
	 * tasktag; // Now let's iterate over the columns of the current row
	 * 
	 * tasktag = row.getCell(21).getStringCellValue().trim().replaceAll(" +",
	 * " "); if (!tasktag.trim().equals("") && row.getRowNum() != 0) { //
	 * insertTagData(tasktag, true);
	 * 
	 * } } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); logger.info(e + " at " + row.getRowNum() +
	 * " While reading data for TaskType ."); }
	 * 
	 * // System.out.println();
	 * 
	 * } } catch (NullPointerException e) { logger.info(e +
	 * " While reading data for HabitMaster ."); } }
	 
	private static int insertTagData(String tag, boolean inclusioncriteria) throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO tags_criteria" + "(tag, include) VALUES" + "(?,?)";

		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, tag);
			preparedStatement.setBoolean(2, inclusioncriteria);
			// try {
			// writer.write(preparedStatement.toString());
			// writer.write("\r\n");
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}

			// System.out.println("Record is inserted into vision table!");

		} catch (SQLException e) {

			// System.out.println(e.getMessage());
			logger.info(e + " While writing data for vision_types .");

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}

	private static int insertTaskTypeData(String visiontype, int status) throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO task_types" + "(title, status) VALUES" + "(?,?)";

		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, visiontype);
			preparedStatement.setInt(2, status);
			// try {
			// writer.write(preparedStatement.toString());
			// writer.write("\r\n");
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}

			task_typeData.put(visiontype, (int) key);
			// System.out.println("Record is inserted into vision table!");

		} catch (SQLException e) {

			// System.out.println(e.getMessage());
			logger.info(e + " While writing data for vision_types .");

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}

	private static void PopulateTaskTypeData() {
		// TODO Auto-generated method stub

		task_typeData = new HashMap<>();

		Sheet sheet = LoadWorkbook(DataInjection.DROPDOWNS);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				try {
					String visiontype;
					// Now let's iterate over the columns of the current row

					visiontype = row.getCell(9).getStringCellValue().trim().replaceAll(" +", " ");
					if (!visiontype.trim().equals("") && row.getRowNum() != 0) {
						insertTaskTypeData(visiontype, 1);

					} else if (row.getRowNum() != 0) {
						break;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					try {
						writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
								+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
								+ row.getCell(12).getAddress());
						writer.write("\r\n");
					} catch (IOException en) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					e.printStackTrace();
					logger.info(e + " at " + row.getRowNum() + " While reading data for TaskType .");
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {
			logger.info(e + " While reading data for HabitMaster .");
		}

	}

	private static int insertSubTaskTypeData(String title, int status) throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		int task_ID = task_typeData.get(title.split("/")[0]);

		String insertTableSQL = "INSERT INTO task_sub_types" + "(title,tt_id,status) VALUES" + "(?,?,?)";

		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, title);
			preparedStatement.setInt(2, task_ID);
			preparedStatement.setInt(3, 1);
			// try {
			// writer.write(preparedStatement.toString());
			// writer.write("\r\n");
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}
			String label = "";
			try {
				for (String keylabel : task_labelData.keySet()) {
					// search for value
					if (title.contains(keylabel)) {
						label = task_labelData.get(keylabel);
					}

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			insertSubSubTaskTypeData(title, (int) key, true, label);

			sutask_typeData.put(title, (int) key);

			// System.out.println("Record is inserted into vision table!");

		} catch (SQLException e) {

			// System.out.println(e.getMessage());
			logger.info(e + " While writing data for vision_types .");

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}

	private static void PopulateSubTaskTypeData() {
		// TODO Auto-generated method stub

		Sheet sheet = LoadWorkbook(DataInjection.DROPDOWNS);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				try {
					String visiontype;
					// Now let's iterate over the columns of the current row

					visiontype = row.getCell(10).getStringCellValue().trim().replaceAll(" +", " ");
					if (!visiontype.trim().equals("") && row.getRowNum() != 0) {
						insertSubTaskTypeData(visiontype, 1);

					} else if (row.getRowNum() != 0) {
						break;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					try {
						writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
								+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
								+ row.getCell(15).getAddress());
						writer.write("\r\n");
					} catch (IOException en) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					e.printStackTrace();
					logger.info(e + " at " + row.getRowNum() + " While reading data for VisionMaster .");
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {
			logger.info(e + " While reading data for HabitMaster .");
		}

	}

	private static int insertSubSubTaskTypeData(String visiontype, int tst_id, boolean virtual, String label)
			throws SQLException, DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		try {
			if (tst_id == 0) {
				tst_id = sutask_typeData.get(visiontype.substring(0, visiontype.lastIndexOf("/")));
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String insertTableSQL = "INSERT INTO task_sub_sub_types" + "(title, tst_id,status,virtual,label) VALUES"
				+ "(?,?,?,?,?)";

		try {
			DataInjection dataInjection = new DataInjection();
			dbConnection = dataInjection.connect();

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, visiontype);
			preparedStatement.setInt(2, tst_id);
			preparedStatement.setInt(3, 1);
			preparedStatement.setBoolean(4, virtual);
			preparedStatement.setString(5, label);
			// try {
			// writer.write(preparedStatement.toString());
			// writer.write("\r\n");
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// execute insert SQL stetement
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();

			if (rs.next()) {
				key = rs.getLong(1);
			}

			subsubtask_typeData.put(visiontype, (int) key);

			// System.out.println("Record is inserted into vision table!");

		} catch (SQLException e) {

			// System.out.println(e.getMessage());
			logger.info(e + " While writing data for vision_types .");

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}

		}
		return (int) key;

	}

	private static void PopulateSubSubTaskTypeData() {
		// TODO Auto-generated method stub

		Sheet sheet = LoadWorkbook(DataInjection.DROPDOWNS);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				try {
					String visiontype;
					String label = "";
					// Now let's iterate over the columns of the current row

					visiontype = row.getCell(11).getStringCellValue().trim().replaceAll(" +", " ").trim();

					try {
						for (String key : task_labelData.keySet()) {
							// search for value
							if (visiontype.contains(key)) {
								label = task_labelData.get(key);
							}

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (!visiontype.trim().equals("") && row.getRowNum() != 0) {
						insertSubSubTaskTypeData(visiontype, 0, false, label);

					} else if (row.getRowNum() != 0) {
						break;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					try {
						writer.write("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ "
								+ row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
								+ row.getCell(0).getAddress() + "or" + row.getCell(14).getAddress());
						writer.write("\r\n");
					} catch (IOException en) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					e.printStackTrace();
					logger.info(e + " at " + row.getRowNum() + " While reading data for VisionMaster .");
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {
			logger.info(e + " While reading data for HabitMaster .");
		}

	}

	
	 * private static int insertTaskLableData(String visiontype, int status)
	 * throws SQLException { long key = 0; Connection dbConnection = null;
	 * PreparedStatement preparedStatement = null;
	 * 
	 * String insertTableSQL = "INSERT INTO task_label" +
	 * "(tittle, status) VALUES" + "(?,?)";
	 * 
	 * try { DataInjection dataInjection = new DataInjection(); dbConnection =
	 * dataInjection.connect();
	 * 
	 * preparedStatement = dbConnection.prepareStatement(insertTableSQL,
	 * java.sql.Statement.RETURN_GENERATED_KEYS);
	 * 
	 * preparedStatement.setString(1, visiontype); preparedStatement.setInt(2,
	 * status); // try { // writer.write(preparedStatement.toString()); //
	 * writer.write("\r\n"); // } catch (IOException e) { // // TODO
	 * Auto-generated catch block // e.printStackTrace(); // }
	 * 
	 * // execute insert SQL stetement preparedStatement.executeUpdate();
	 * 
	 * ResultSet rs = preparedStatement.getGeneratedKeys();
	 * 
	 * if (rs.next()) { key = rs.getLong(1); }
	 * 
	 * // System.out.println("Record is inserted into vision table!");
	 * 
	 * } catch (SQLException e) {
	 * 
	 * // System.out.println(e.getMessage()); logger.info(e +
	 * " While writing data for vision_types .");
	 * 
	 * } finally {
	 * 
	 * if (preparedStatement != null) { preparedStatement.close(); }
	 * 
	 * if (dbConnection != null) { dbConnection.close(); }
	 * 
	 * } return (int) key;
	 * 
	 * }
	 */
	/*
	 * private static void PopulateTaskLabelData() { // TODO Auto-generated
	 * method stub
	 * 
	 * Sheet sheet = LoadWorkbook(DataInjection.DROPDOWNS);
	 * 
	 * Iterator<Row> rowIterator = sheet.rowIterator(); try { while
	 * (rowIterator.hasNext()) { Row row = rowIterator.next(); try { String
	 * visiontype; // Now let's iterate over the columns of the current row
	 * 
	 * visiontype = row.getCell(15).getStringCellValue().trim().replaceAll(" +",
	 * " "); if (!visiontype.trim().equals("") && row.getRowNum() != 0) {
	 * insertTaskLableData(visiontype, 1);
	 * 
	 * } } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); logger.info(e + " at " + row.getRowNum() +
	 * " While reading data for VisionMaster ."); }
	 * 
	 * // System.out.println();
	 * 
	 * } } catch (NullPointerException e) { logger.info(e +
	 * " While reading data for HabitMaster ."); }
	 * 
	 * }
	 
	private static void InitializeLogger() {
		// TODO Auto-generated method stub
		logger = Logger.getLogger("MyLog");
		loggerQueries = Logger.getLogger("Query Logger");
		FileHandler fh;

		try {

			// This block configure the logger with handler and formatter
			fh = new FileHandler("output/MyLogFile.log");
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

			// the following statement is used to log any messages
			logger.info("My first log");

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {

			// This block configure the logger with handler and formatter
			fh = new FileHandler("output/QueriesLogTask3.log");
			loggerQueries.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);

		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static Sheet LoadWorkbook(int sheetno) {
		// TODO Auto-generated method stub
		Sheet sheet = null;
		Workbook workbook = null;
		try {

			workbook = WorkbookFactory.create(new File(DataInjection.XLSX_FILE_PATH));

			// Retrieving the number of sheets in the Workbook
			// System.out.println("Workbook has " + workbook.getNumberOfSheets()
			// + " Sheets : ");

			sheet = workbook.getSheetAt(sheetno);

			// System.out.println("\n\nIterating over Rows and Columns using
			// Iterator\n");

		} catch (EncryptedDocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sheet;

	}

	*/
}
