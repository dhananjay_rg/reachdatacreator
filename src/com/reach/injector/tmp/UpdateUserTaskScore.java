package com.reach.injector.tmp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import com.reach.util.DBUtil;

public class UpdateUserTaskScore {

	private static HashMap<Integer, Double> hm = new HashMap<Integer, Double>();
	
	
	public static void main(String[] args) {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			
			String sqlTask="select id,score from task_masters";
			PreparedStatement psTask = con.prepareStatement(sqlTask);
			ResultSet rsTask = psTask.executeQuery();
			while(rsTask.next()) {
				hm.put(rsTask.getInt("id"), rsTask.getDouble("score"));
				if(rsTask.getInt("id") == 7301) {
					System.out.println(rsTask.getDouble("score"));
				}
			}
			
			System.out.println(hm.size());
			
			PrintStream outputLogger = new PrintStream(new FileOutputStream(new File("logs/update_user_score.sql")));
			
			String sql = "select * from user_tasks where task_id=7301 order by id";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				outputLogger.println("update user_tasks set score="+hm.get(rs.getInt("task_id"))+" where id=" + rs.getInt("id") + ";");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
