package com.reach.injector.tmp;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Test {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, IOException {
		process();
		/*
		
		HashMap<String, String> reachDataType = new HashMap<>();
		reachDataType.put("Gender", "genders");
		reachDataType.put("Goal", "goals");
		reachDataType.put("ActivityLevel", "activity_levels");
		reachDataType.put("CuisinePreferences", "cusine_prefs");
		reachDataType.put("FoodPreferences", "food_prefs");
		reachDataType.put("MealPreferences", "meal_prefs");
		reachDataType.put("FoodAllergyOrIntolerance", "allergy_intolrances");
		reachDataType.put("ExercisePreferences", "exer_prefs");
		reachDataType.put("RateOfChange", "rate_changes");
		reachDataType.put("Action", "action_task_ans");
		reachDataType.put("Content", "content_task_ans");
		reachDataType.put("Social", "social_task_ans");
		
		String str = "UserData/Profile/FoodAllergyOrIntolerance";
		
		String str2 = "";
		System.out.println(str);
		if (str.lastIndexOf("/") > 0) {
			str2 = str.substring(str.lastIndexOf("/")+1, str.length());
			str = str.substring(0,str.lastIndexOf("/"));
			System.out.println("_____________");
			System.out.println(str);
			System.out.println(str2);
			
			if(reachDataType.containsKey(str2)) {
				System.out.println("__ exist __" + str2);
			} else if (str.lastIndexOf("/") > 0) {
				str2 = str.substring(str.lastIndexOf("/")+1, str.length());
				str = str.substring(0,str.lastIndexOf("/"));
				System.out.println("_____________");
				System.out.println(str);
				System.out.println(str2); 
				if(reachDataType.containsKey(str2)) {
					System.out.println("__ exist __" + str2);
				}
				
				
			} else {
				System.out.println("__ 222 exist __" + str2);
			}
			
			
		} else {
			System.out.println("__ 3333 exist __" + str);
		}
		
		*/
		
	}

	private static void process() throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook workbook = WorkbookFactory.create(new File("data/06.07.2018 - Mumbai-Daily Attendance Report.xls"));
		
		Sheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		int rowNum = 0;
		String attDate = "";
		
		while (rowIterator.hasNext()) {
			rowNum++;
			Row row = rowIterator.next();
			
//			System.out.println(rowNum + " ___ " + row.getCell(0));
			if(rowNum > 9) {
//				System.out.println(rowNum + " __ empCode _ " + row.getCell(2));
				
				String emCode = row.getCell(2).toString().trim();
//				System.out.println(row.getCell(9));
				String inTime = row.getCell(9).toString().trim();
				String outTime = row.getCell(11).toString().trim();
				System.out.println("date[ " + attDate + " ], empCode[ " + emCode + " ], inTime[ " + inTime + " ], outTime[ " + outTime + " ]");
				
			} else if(rowNum == 7) {
				attDate = row.getCell(5).toString().trim();
				System.out.println(rowNum + " __ date _ " + row.getCell(5));
			}
			
//			row.getCell(Constant.VisionColumnsIndex.VISION_COLUMN_INDEX).getStringCellValue();
	
		}
		
	}
	
	private static void process_2() throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook workbook = WorkbookFactory.create(new File("data/rgb_color.xls"));
		
		Sheet sheet = workbook.getSheetAt(0);
		Iterator<Row> rowIterator = sheet.rowIterator();

		
		
		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		int rowNum = 0;
		
		while (rowIterator.hasNext()) {
			rowNum++;
			Row row = rowIterator.next();
			
			String sql  = "insert into colors() values("+rowNum+",'"+ row.getCell(1).toString().trim() +"','"+ row.getCell(3).toString().trim() +"', 1);";
			
			System.out.println(sql);
			
		
	
		}
		
	}
}