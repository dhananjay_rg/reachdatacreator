package com.reach.injector.profile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.postgresql.util.PGobject;

import com.reach.err.DBError;
import com.reach.util.DBUtil;

public class ReachDataTypeProcessor {

	public HashMap<String, ReachDataTypeBase> reachDataType;
	public HashMap<String, ReachDataTypeBase> reachDataTypeManual;	
	
	public static void main(String[] args) throws DBError {
		process();
	}
	
	public static void process() throws DBError {
		
		ReachDataTypeProcessor m=new ReachDataTypeProcessor();
		
		 m.populateReachDataType();
		 m.updateReachDataType();
		 
		 
	}
	
	
	public static ReachDataTypeProcessor instance() {
		
		return new ReachDataTypeProcessor();
	}
	
	
	
	public void populateReachDataType() throws DBError {
		// TODO Auto-generated method stub
		reachDataTypeManual = new HashMap<>();
		reachDataType = new HashMap<>();
		
		reachDataType.put("Gender", new ReachDataTypeBase("genders", "Gender", "SO") );
		reachDataType.put("Goal", new ReachDataTypeBase("goals", "Goal", "SO"));
		reachDataType.put("ActivityLevel", new ReachDataTypeBase("activity_levels", "ActivityLevel", "SO"));
		reachDataType.put("CuisinePreferences", new ReachDataTypeBase("cusine_prefs", "CuisinePreferences", "MO"));
		reachDataType.put("FoodPreferences", new ReachDataTypeBase("food_prefs", "FoodPreferences", "MO"));
		reachDataType.put("MealPreferences", new ReachDataTypeBase("meal_prefs", "MealPreferences", "MO"));
		reachDataType.put("FoodAllergyOrIntolerance", new ReachDataTypeBase("allergy_intolrances", "FoodAllergyOrIntolerance", "MO"));
		reachDataType.put("ExercisePreferences", new ReachDataTypeBase("exer_prefs", "ExercisePreferences", "MO"));
		reachDataType.put("RateOfChange", new ReachDataTypeBase("rate_changes", "RateOfChange", "SO"));
		reachDataType.put("Action", new ReachDataTypeBase("action_task_ans", "Action", "SO"));
		reachDataType.put("Content", new ReachDataTypeBase("content_task_ans", "Content", "SO"));
		reachDataType.put("Social", new ReachDataTypeBase("social_task_ans", "Social", "SO"));
		reachDataType.put("Track", new ReachDataTypeBase("track_task_ans", "Track", "SO"));
		reachDataType.put("Upsell", new ReachDataTypeBase("upsell_task_ans", "Upsell", "SO"));
		
		String emailId="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\\\.[A-Za-z]{2,}$";
		String mobNo="^(\\\\+\\\\d{1,3}-?)?\\\\d{10}$";
		String firstName="^[a-zA-Z ]{3,30}$";
		String lastName="^[a-zA-Z ]{3,30}$";
		String countryCode="\\\\d{1,3}$";

		reachDataTypeManual.put("FirstName", new ReachDataTypeBase("{\"type\":\"REGEX\", \"regEX\":\""+ firstName +"\"}", "FirstName", "TI"));
		reachDataTypeManual.put("MiddleName", new ReachDataTypeBase("{\"type\":\"REGEX\", \"regEX\":\""+ firstName +"\"}", "MiddleName", "TI"));

		reachDataTypeManual.put("LastName", new ReachDataTypeBase("{\"type\":\"REGEX\", \"regEX\":\""+ lastName +"\"}", "LastName", "TI"));

		reachDataTypeManual.put("PersonalEmailId", new ReachDataTypeBase("{\"type\":\"REGEX\", \"regEX\":\"" + emailId + "\"}", "PersonalEmailId", "TI"));

		reachDataTypeManual.put("OfficialEmailId", new ReachDataTypeBase("{\"type\":\"REGEX\", \"regEX\":\"" + emailId + "\"}", "OfficialEmailId", "TI"));

		reachDataTypeManual.put("MobileNumber", new ReachDataTypeBase("{\"type\":\"REGEX\", \"regEX\":\"" + mobNo + "\"}", "MobileNumber", "TI"));

		reachDataTypeManual.put("CountryCode", new ReachDataTypeBase("{\"type\":\"REGEX\", \"regEX\":\"" + countryCode + "\"}", "CountryCode", "TI"));
		
		reachDataTypeManual.put("DateOfBirth",
				new ReachDataTypeBase("{ \"type\":\" Date\",\"min\":-2922, \"max\":-29210, \"format\":\"yyyy-mm-dd\"}", "DateOfBirth", "TI"));

		reachDataTypeManual.put("Height",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"feet\",\"inch\"],[\"cm\"]], \"min\":122, \"max\":214, \"inc\":0.5}", "Height", "SO"));

		reachDataTypeManual.put("Weight",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"gram\",\"scales\":[[\"kg\"],[\"pound\"]], \"min\":30000, \"max\":170000, \"inc\":100}", "Weight", "SO"));

		reachDataTypeManual.put("Chest",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"inch\"]], \"min\":63.5, \"max\":381, \"inc\":0.5}", "Chest", "SO"));

		reachDataTypeManual.put("Waist",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"inch\"]], \"min\":50.5, \"max\":254, \"inc\":0.5}", "Waist", "SO"));

		reachDataTypeManual.put("Hip",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"inch\"]], \"min\":63.5, \"max\":381, \"inc\":0.5}", "Hip", "SO"));

		reachDataTypeManual.put("Arm",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"inch\"]], \"min\":12.5, \"max\":50.5, \"inc\":0.5}", "Arm", "SO"));

		reachDataTypeManual.put("Thigh",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"inch\"]], \"min\":20, \"max\":101.5, \"inc\":0.5}", "Thigh", "SO"));

		reachDataTypeManual.put("Calf",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"cm\",\"scales\":[[\"inch\"]], \"min\":12.5, \"max\":63.5, \"inc\":0.5}", "Calf", "SO"));

		reachDataTypeManual.put("TargetWeight",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"gram\",\"scales\":[[\"kg\"],[\"pound\"]], \"min\":30000, \"max\":170000, \"inc\":100}", "TargetWeight", "SO"));

		reachDataTypeManual.put("StepCountTarget",
				new ReachDataTypeBase("{ \"type\":\"SCALE\",\"standard\":\"step\",\"scales\":[[\"step\"]], \"min\":0, \"max\":100000, \"inc\":50}", "StepCountTarget", "SO"));

	}
	
	private void updateReachDataType() throws DBError {
		Iterator<?> it = reachDataType.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			ReachDataTypeBase reachDataTypeBase = (ReachDataTypeBase) pair.getValue();
			InsertReachDataTyperForReferTable(pair.getKey().toString(), reachDataTypeBase.code, "");
			// it.remove(); // avoids a ConcurrentModificationException
		}

		Iterator<?> itmanual = reachDataTypeManual.entrySet().iterator();
		while (itmanual.hasNext()) {
			Map.Entry pair = (Map.Entry) itmanual.next();
			ReachDataTypeBase reachDataTypeBase = (ReachDataTypeBase) pair.getValue();
			InsertReachDataTyrpeForManual(reachDataTypeBase.title, "", reachDataTypeBase.code);
			// it.remove(); // avoids a ConcurrentModificationException
		}
	}
	
	private int InsertReachDataTyperForReferTable(String type_code, String refer_table, String data_domain)
			throws DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO reach_data_types (type_code, refer_table,version) VALUES (?,?,?)";

		try {
			dbConnection = DBUtil.getConnection();

			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(data_domain);

			preparedStatement = dbConnection.prepareStatement(insertTableSQL, java.sql.Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, type_code);
			preparedStatement.setString(2, refer_table);
			preparedStatement.setInt(3, 1);

			preparedStatement.executeUpdate();

			preparedStatement.getGeneratedKeys();

		} catch (SQLException e) {

			// System.out.println(e.getMessage());
			e.printStackTrace();

		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return (int) key;
	}
	
	
	private int InsertReachDataTyrpeForManual(String type_code, String refer_table, String data_domain) throws DBError {
		long key = 0;
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;

		String insertTableSQL = "INSERT INTO reach_data_types(type_code, refer_table,version,data_domain) VALUES(?,?,?,?)";

		try {
			dbConnection = DBUtil.getConnection();

			PGobject jsonObject = new PGobject();
			jsonObject.setType("json");
			jsonObject.setValue(data_domain);

			preparedStatement = dbConnection.prepareStatement(insertTableSQL);

			preparedStatement.setString(1, type_code);
			preparedStatement.setString(2, refer_table);
			preparedStatement.setInt(3, 1);
			preparedStatement.setObject(4, jsonObject);
	
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			try {
				if (preparedStatement != null) {
					preparedStatement.close();
				}

				if (dbConnection != null) {
					dbConnection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (int) key;
	}
	
}
