package com.reach.injector.profile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.util.DBUtil;

public class InsertProfileMasterData {

	private final Integer STATUS_ACTIVE = 1;

	private Workbook workbook = null;

	private Connection conn = null;

	private static PrintStream outputLogger = null;
	private static PrintStream inputFileIssuesLogger = null;
//	private static PrintStream sqlQueriesLogger = null;

	public static HashMap<String, String> hmActivityLevel = null;
	public static HashMap<String, String> hmGernder = null;
	public static HashMap<String, String> hmRateChange = null;
	public static HashMap<String, String> hmFoodPref = null;
	public static HashMap<String, String> hmMealPref = null;
	public static HashMap<String, String> hmFoodAllergy = null;
	public static HashMap<String, String> hmExercisePref = null;
	public static HashMap<String, String> hmCusinePref = null;
	public static HashMap<String, String> hmGoal = null;
	public static HashMap<String, String> hmActionTask = null;
	public static HashMap<String, String> hmContentTask = null;
	public static HashMap<String, String> hmSocialTask = null;
	public static HashMap<String, String> hmTrackTask = null;
	public static HashMap<String, String> hmUpsellTask = null;
	
	private HashMap<String, String> hmActivityLevelDB = null;
	private HashMap<String, String> hmGernderDB = null;
	private HashMap<String, String> hmRateChangeDB = null;
	private HashMap<String, String> hmFoodPrefDB = null;
	private HashMap<String, String> hmMealPrefDB = null;
	private HashMap<String, String> hmFoodAllergyDB = null;
	private HashMap<String, String> hmExercisePrefDB = null;
	private HashMap<String, String> hmCusinePrefDB = null;
	private HashMap<String, String> hmGoalDB = null;
	private HashMap<String, String> hmActionTaskDB = null;
	private HashMap<String, String> hmContentTaskDB = null;
	private HashMap<String, String> hmSocialTaskDB = null;
	private HashMap<String, String> hmTrackTaskDB = null;
	
	private HashMap<String, String> hmUpsellTaskDB = null;

	// public static void main(String[] args) throws IOException,
	// InvalidFormatException, SQLException {
	// updateBarrierData();
	// }

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, DBError, SQLException, IOException {
		
		processProfileData();
	}
	
	
	public static InsertProfileMasterData instance() {
		return new InsertProfileMasterData();
	}
	
	public static void processProfileData()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		
		InsertProfileMasterData m=new InsertProfileMasterData();
		
		try {
			m.init();
			m.populateMap();
			m.populateDBData();
			m.insertUpdateProfileData();
			m.insertUpdateTaskStaticData();
		
		} finally { 
			
			if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
			}
		}
		
		
		
	}

	public InsertProfileMasterData init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {

		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));

		conn =  DBUtil.getConnection();
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/profile_insertMasterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/profile_issueData.log")));
		
		hmActivityLevel = new HashMap<String, String>();
		hmGernder = new HashMap<String, String>();
		hmRateChange = new HashMap<String, String>();
		hmFoodPref = new HashMap<String, String>();
		hmMealPref = new HashMap<String, String>();
		hmFoodAllergy = new HashMap<String, String>();
		hmExercisePref = new HashMap<String, String>();
		hmCusinePref = new HashMap<String, String>();
		hmGoal = new HashMap<String, String>();
		hmActionTask = new HashMap<String, String>();
		hmContentTask = new HashMap<String, String>();
		hmSocialTask = new HashMap<String, String>();
		hmTrackTask = new HashMap<String, String>();
		hmUpsellTask= new HashMap<>();
		
		hmActivityLevelDB = new HashMap<String, String>();
		hmGernderDB = new HashMap<String, String>();
		hmRateChangeDB = new HashMap<String, String>();
		hmFoodPrefDB = new HashMap<String, String>();
		hmMealPrefDB = new HashMap<String, String>();
		hmFoodAllergyDB = new HashMap<String, String>();
		hmExercisePrefDB = new HashMap<String, String>();
		hmCusinePrefDB = new HashMap<String, String>();
		hmGoalDB = new HashMap<String, String>();
		hmActionTaskDB = new HashMap<String, String>();
		hmContentTaskDB = new HashMap<String, String>();
		hmSocialTaskDB = new HashMap<String, String>();
		hmTrackTaskDB = new HashMap<String, String>();
		hmUpsellTaskDB= new HashMap<>();
		
		return this;
	}

	public void populateMap() throws SQLException, IOException {
	
		// populate Activity Level
		hmActivityLevel.put("Sedentary", "sed");
		hmActivityLevel.put("Light", "lgt");
		hmActivityLevel.put("Medium", "med");
		hmActivityLevel.put("Heavy", "hvy");
		
		// populate Gender
		hmGernder.put("Male", "m");
		hmGernder.put("Female", "f");
		
		// populate Rate Range
		hmRateChange.put("0.45", "0.45");
		hmRateChange.put("0.9", "0.9");

		// populate Food Pref
		hmFoodPref.put("Veg", "veg");
		hmFoodPref.put("Egg", "egg");
		hmFoodPref.put("Chicken", "ckn");
		hmFoodPref.put("Fish", "fis");
		hmFoodPref.put("Seafood", "cfd");
		hmFoodPref.put("Pork", "prk");
		hmFoodPref.put("Beef", "bef");
		hmFoodPref.put("Mutton", "mtn");

		// populate Meal Pref
		hmMealPref.put("Early Morning", "em");
		hmMealPref.put("Breakfast", "bf");
		hmMealPref.put("Snack", "snk");
		hmMealPref.put("Lunch", "lnh");
		hmMealPref.put("Tea", "tea");
		hmMealPref.put("Dinner", "dnr");
		hmMealPref.put("Post Dinner", "pdn");
		
		// populate Allergy Pref
		hmFoodAllergy.put("None", "none");
		hmFoodAllergy.put("Cow Milk", "cowmlk");
		hmFoodAllergy.put("Eggs", "egg");
		hmFoodAllergy.put("Peanuts", "pnuts");
		hmFoodAllergy.put("Tree Nuts", "treenuts");
		hmFoodAllergy.put("Soy", "soy");
		hmFoodAllergy.put("Fishes", "fishes");
		hmFoodAllergy.put("Shellfish", "shellfish");
		hmFoodAllergy.put("Wheat", "wheat");
		hmFoodAllergy.put("Barley", "barley");
		hmFoodAllergy.put("Rye", "rye");
		hmFoodAllergy.put("Lactose", "lactose");

		// populate Exercise Pref
		hmExercisePref.put("Cardio", "crdo");
		hmExercisePref.put("Strength", "str");
		hmExercisePref.put("Yoga", "yoga");

		// populate Cuisine Pref
		hmCusinePref.put("All", "all");
		hmCusinePref.put("Indian", "indian");
		hmCusinePref.put("North Indian", "northindian");
		hmCusinePref.put("South Indian", "southindian");

		// populate Goal
		hmGoal.put("Weight Loss", "wtl");
		hmGoal.put("Weight Gain", "wtg");
		hmGoal.put("Weight Maintenance", "wtm");

		// populate Action Task
		hmActionTask.put("Completed", "yes");
		hmActionTask.put("Not Completed", "no");
		
		hmContentTask.put("Completed", "yes");
		hmContentTask.put("Not Completed", "no");
		
		hmSocialTask.put("Completed", "yes");
		hmSocialTask.put("Not Completed", "no");
		
		hmTrackTask.put("Completed", "yes");
		hmTrackTask.put("Not Completed", "no");
		
		
		//////
		hmUpsellTask.put("Completed", "yes");
		hmUpsellTask.put("Not Completed", "no");
		
		
		outputLogger.println("Populated static data into HashMap");
		outputLogger.println("=========================");
	}
	
	private void populateDBData() throws SQLException, IOException {
		
		outputLogger.println("Loading Gender from database....");
		Statement stmt = conn.createStatement();
		String selectQuery = "select * from genders";
		ResultSet rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmGernderDB.containsKey(id)) {
					throw new IOException("Duplicate Gender[ " + displayValue + " ] in Gender table");
				} else {
					hmGernderDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmGernderDB.size() + " Genders in database....");
		outputLogger.println("===============");
		
		
		outputLogger.println("Loading Acitivity level from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from activity_levels";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmActivityLevelDB.containsKey(id)) {
					throw new IOException("Duplicate Gender[ " + displayValue + " ] in Gender table");
				} else {
					hmActivityLevelDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmActivityLevelDB.size() + " Acitivity level in database....");
		outputLogger.println("===============");
		
		
		outputLogger.println("Loading Acitivity level from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from activity_levels";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmActivityLevelDB.containsKey(id)) {
					throw new IOException("Duplicate Acitivity level[ " + displayValue + " ] in Acitivity level table");
				} else {
					hmActivityLevelDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmActivityLevelDB.size() + " Acitivity level in database....");
		outputLogger.println("===============");
		
		
		outputLogger.println("Loading Rate Change from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from rate_changes";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmRateChangeDB.containsKey(id)) {
					throw new IOException("Duplicate Rate Change[ " + displayValue + " ] in Rate Change table");
				} else {
					hmRateChangeDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmRateChangeDB.size() + " Rate Change in database....");
		outputLogger.println("===============");
		
		
		outputLogger.println("Loading Food Pref from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from food_prefs";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmFoodPrefDB.containsKey(id)) {
					throw new IOException("Duplicate Food Pref[ " + displayValue + " ] in Food Pref table");
				} else {
					hmFoodPrefDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmFoodPrefDB.size() + " Food Pref in database....");
		outputLogger.println("===============");
		
		

		outputLogger.println("Loading Meal Pref from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from meal_prefs";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmMealPrefDB.containsKey(id)) {
					throw new IOException("Duplicate Meal Pref[ " + displayValue + " ] in Meal Pref table");
				} else {
					hmMealPrefDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmMealPrefDB.size() + " Meal Pref in database....");
		outputLogger.println("===============");
		

		outputLogger.println("Loading Food Allergy from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from allergy_intolarances";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmFoodAllergyDB.containsKey(id)) {
					throw new IOException("Duplicate Food Allergy[ " + displayValue + " ] in Food Allergy table");
				} else {
					hmFoodAllergyDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmFoodAllergyDB.size() + " Food Allergy in database....");
		outputLogger.println("===============");
		

		outputLogger.println("Loading Exercise Pref from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from exer_prefs";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmExercisePrefDB.containsKey(id)) {
					throw new IOException("Duplicate Exercise Pref[ " + displayValue + " ] in Exercise Pref table");
				} else {
					hmExercisePrefDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmExercisePrefDB.size() + " Exercise Pref in database....");
		outputLogger.println("===============");
		
		
		outputLogger.println("Loading Cuisine Pref from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from cusine_prefs";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmCusinePrefDB.containsKey(id)) {
					throw new IOException("Duplicate Cuisine Pref[ " + displayValue + " ] in Cuisine Pref table");
				} else {
					hmCusinePrefDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmCusinePrefDB.size() + " Cuisine Pref in database....");
		outputLogger.println("===============");
		
		

		outputLogger.println("Loading Goal from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from goals";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmGoalDB.containsKey(id)) {
					throw new IOException("Duplicate Goal[ " + displayValue + " ] in Goal table");
				} else {
					hmGoalDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmGoalDB.size() + " Goal in database....");
		outputLogger.println("===============");
		
		
		outputLogger.println("Loading Action Task Ans from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from action_task_ans";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmActionTaskDB.containsKey(id)) {
					throw new IOException("Duplicate Action Task Ans[ " + displayValue + " ] in Action Task Ans table");
				} else {
					hmActionTaskDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmActionTaskDB.size() + " Action Task Ans in database....");
		outputLogger.println("===============");
		
		
		outputLogger.println("Loading Content Task Ans from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from content_task_ans";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmContentTaskDB.containsKey(id)) {
					throw new IOException("Duplicate Content Task Ans[ " + displayValue + " ] in Content Task Ans table");
				} else {
					hmContentTaskDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmContentTaskDB.size() + " Content Task Ans in database....");
		outputLogger.println("===============");
		
		
		

		outputLogger.println("Loading Social Task Ans from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from social_task_ans";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmSocialTaskDB.containsKey(id)) {
					throw new IOException("Duplicate Social Task Ans[ " + displayValue + " ] in Social Task Ans table");
				} else {
					hmSocialTaskDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmSocialTaskDB.size() + " Social Task Ans in database....");
		outputLogger.println("===============");
		


		outputLogger.println("Loading Track Task Ans from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from track_task_ans";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmTrackTaskDB.containsKey(id)) {
					throw new IOException("Duplicate Track Task Ans[ " + displayValue + " ] in Track Task Ans table");
				} else {
					hmTrackTaskDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTrackTaskDB.size() + " Track Task Ans in database....");
		outputLogger.println("===============");
		
		
		
		////////////
		
		
		outputLogger.println("Loading Upsell Task Ans from database....");
		stmt = conn.createStatement();
		selectQuery = "select * from upsell_task_ans";
		rs = stmt.executeQuery(selectQuery);
		if (rs != null) {
			while (rs.next()) {
				String id = rs.getString("id");
				String displayValue = rs.getString("display_value");
				if (hmUpsellTaskDB.containsKey(id)) {
					throw new IOException("Duplicate Upsell Task Ans[ " + displayValue + " ] in Upsell Task Ans table");
				} else {
					hmUpsellTaskDB.put(displayValue, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmUpsellTaskDB.size() + " Upsell Task Ans in database....");
		outputLogger.println("===============");
		
		
		
	}
	
	
	private void insertUpdateProfileData() throws SQLException, IOException {
		
		// TODO Auto-generated method stub
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_PROFILE);
		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				String profileoptions;
				String tagValues = null;
				try {

					switch (row.getRowNum()) {
						case 8: {
							outputLogger.println(" Process Started for Gender");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Gender, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmGernder.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Gender, Size of static hashMap[" + hmGernder.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmGernderDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("genders", profilefieldoptionsarry[i].trim(),
												hmGernder.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("genders", profilefieldoptionsarry[i].trim(),
													hmGernder.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", gender [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Gender");
							outputLogger.println(" =============================== ");
							break;
						}
						case 18: {
							
							outputLogger.println(" Process Started for Activity Level");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Activity Level, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmActivityLevel.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Activity Level, Size of static hashMap[" + hmActivityLevel.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmActivityLevelDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("activity_levels", profilefieldoptionsarry[i].trim(),
													hmActivityLevel.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("activity_levels", profilefieldoptionsarry[i].trim(),
													hmActivityLevel.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Activity Level [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Activity Level");
							outputLogger.println(" =============================== ");
							break;
						}
						case 20: {
	
	
							outputLogger.println(" Process Started for Goal");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Goal, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmGoal.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Goal, Size of static hashMap[" + hmGoal.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmGoalDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("goals", profilefieldoptionsarry[i].trim(),
													hmGoal.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("goals", profilefieldoptionsarry[i].trim(),
													hmGoal.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Goal [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Goal");
							outputLogger.println(" =============================== ");
							break;
							
						}
						case 21: {
							
							outputLogger.println(" Process Started for Rate Change");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Rate Change, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmRateChange.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Rate Change, Size of static hashMap[" + hmRateChange.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmRateChangeDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("rate_changes", profilefieldoptionsarry[i].trim(),
													hmRateChange.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("rate_changes", profilefieldoptionsarry[i].trim(),
													hmRateChange.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Rate Change [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Rate Change");
							outputLogger.println(" =============================== ");
							break;
						}
						case 22: {
							
							outputLogger.println(" Process Started for Cuisine Pref");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Cuisine Pref, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmCusinePref.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Cuisine Pref, Size of static hashMap[" + hmCusinePref.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmCusinePrefDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("cusine_prefs", profilefieldoptionsarry[i].trim(),
													hmCusinePref.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("cusine_prefs", profilefieldoptionsarry[i].trim(),
													hmCusinePref.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Cuisine Pref [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Cuisine Pref");
							outputLogger.println(" =============================== ");
							break;
							
						}
						case 23: {
							
							outputLogger.println(" Process Started for Food Pref");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Food Pref, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmFoodPref.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Food Pref, Size of static hashMap[" + hmFoodPref.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmFoodPrefDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("food_prefs", profilefieldoptionsarry[i].trim(),
													hmFoodPref.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("food_prefs", profilefieldoptionsarry[i].trim(),
													hmFoodPref.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Food Pref [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Food Pref");
							outputLogger.println(" =============================== ");
							break;
	
						}
						case 24: {
							outputLogger.println(" Process Started for Meal Pref");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Meal Pref, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmMealPref.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Meal Pref, Size of static hashMap[" + hmMealPref.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmMealPrefDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("meal_prefs", profilefieldoptionsarry[i].trim(),
													hmMealPref.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("meal_prefs", profilefieldoptionsarry[i].trim(),
													hmMealPref.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Meal Pref [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Meal Pref");
							outputLogger.println(" =============================== ");
							break;
						}
						case 25: {
							outputLogger.println(" Process Started for Food Allergy");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Food Allergy, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmFoodAllergy.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Food Allergy, Size of static hashMap[" + hmFoodAllergy.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmFoodAllergyDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("allergy_intolarances", profilefieldoptionsarry[i].trim(),
													hmFoodAllergy.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("allergy_intolarances", profilefieldoptionsarry[i].trim(),
													hmFoodAllergy.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Allergy Food [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Food Allergy");
							outputLogger.println(" =============================== ");
							break;
							
						}
						case 26: {
							outputLogger.println(" Process Started for Exercise Pref");
							profileoptions = row.getCell(Constant.ProfileColumnsIndex.PROFILE_OPTIONS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							
							if (profileoptions.contains(",")) {
								String[] profilefieldoptionsarry = profileoptions.split(",");
								String[] profiletagArry = null;
								if(row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX) != null && !row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().equals("")) {
									profiletagArry = row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split(",");
									tagValues =  row.getCell(Constant.ProfileColumnsIndex.TAG_VALUE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									if(profilefieldoptionsarry.length != profiletagArry.length) {
										inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Exercise Pref, Size of options[" + profileoptions + "] and tag values[" + tagValues + "] does not match");
										outputLogger.println(" =============================== ");
										break;
									}
								} else {
									tagValues = null;
								}
								
								if(hmExercisePref.size() != profilefieldoptionsarry.length) {
									inputFileIssuesLogger.println("row no => " + row.getRowNum() + ", In Exercise Pref, Size of static hashMap[" + hmExercisePref.size()+ "] and file values size[" + profilefieldoptionsarry.length + "] does not match");
									outputLogger.println(" =============================== ");
									break;
								}
								for (int i = 0; i < profilefieldoptionsarry.length; i++) {
									if(hmExercisePrefDB.get(profilefieldoptionsarry[i].trim()) == null) {
										if(tagValues != null) {
											insertProfileData("exer_prefs", profilefieldoptionsarry[i].trim(),
													hmExercisePref.get(profilefieldoptionsarry[i].trim()), profiletagArry[i].trim(), false);
										} else {
											insertProfileData("exer_prefs", profilefieldoptionsarry[i].trim(),
													hmExercisePref.get(profilefieldoptionsarry[i].trim()), "", false);
										}
									} else {
										outputLogger.println("row no => " + row.getRowNum() + ", Exercise Pref [" + profilefieldoptionsarry[i].trim() + "] aleady exist in DB");
									}
								}
							}
							outputLogger.println(" Process Finished for Exercise Pref");
							outputLogger.println(" =============================== ");
							break;
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				}

			
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		
	}
	
	private static void insertProfileData(String tableName, String displayValue, String id, String tagValue,
			boolean userTyped) throws DBError {
		try {
			Connection dbConnection = null;
			PreparedStatement preparedStatement = null;

			String insertTableSQL = "INSERT INTO " + tableName + "(id,display_value,tag_value,user_typed) VALUES"
					+ "(?,?,?,?)";
			//system.out.println("Record inserting into " + tableName + ",with displayValue[" + displayValue + "] id[" + id + "] and tagValue[" + tagValue + "]");
			if(id == null || id.equals("")) {
				inputFileIssuesLogger.println("id is null for  table [" + tableName + "] and displayValue[" + displayValue + "]");
			} else {
				try {
					dbConnection = DBUtil.getConnection();
	
					preparedStatement = dbConnection.prepareStatement(insertTableSQL);
					preparedStatement.setString(1, id);
					preparedStatement.setString(2, displayValue);
					preparedStatement.setString(3, tagValue);
					preparedStatement.setBoolean(4, userTyped);
					//system.out.println(" __ " + preparedStatement);
					preparedStatement.executeQuery();
					
					//system.out.println("Record is inserted into " + tableName + ",with displayValue[" + displayValue + "] id[" + id + "] and tagValue[" + tagValue + "]");
	
				} catch (SQLException e) {
	
					System.out.println(e.getMessage());
	
				} finally {
	
					if (preparedStatement != null) {
						preparedStatement.close();
					}
	
					if (dbConnection != null) {
						dbConnection.close();
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void insertStaticTaskData(String tableName, String displayValue, String id,
			boolean userTyped) throws DBError {
		try {
			Connection dbConnection = null;
			PreparedStatement preparedStatement = null;

			String insertTableSQL = "INSERT INTO " + tableName + "(id,display_value,user_typed) VALUES"
					+ "(?,?,?)";
			System.out.println("Record inserting into " + tableName + ",with displayValue[" + displayValue + "] id[" + id + "]");
			if(id == null || id.equals("")) {
				inputFileIssuesLogger.println("id is null for  table [" + tableName + "] and displayValue[" + displayValue + "]");
			} else {
				try {
					dbConnection = DBUtil.getConnection();
	
					preparedStatement = dbConnection.prepareStatement(insertTableSQL);
					preparedStatement.setString(1, id);
					preparedStatement.setString(2, displayValue);
					preparedStatement.setBoolean(3, userTyped);

					System.out.println(" __ " + preparedStatement);
					preparedStatement.executeQuery();
					
					System.out.println("Record is inserted into " + tableName + ",with displayValue[" + displayValue + "] id[" + id + "] ");
	
				} catch (SQLException e) {
	
					System.out.println(e.getMessage());
	
				} finally {
	
					if (preparedStatement != null) {
						preparedStatement.close();
					}
	
					if (dbConnection != null) {
						dbConnection.close();
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void insertUpdateTaskStaticData() {
		
		try {
			Iterator it = hmActionTask.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println("__________ " + pair.getKey() + " = " + pair.getValue());
		        if(hmActionTaskDB.get(pair.getKey().toString()) == null) {
					insertStaticTaskData("action_task_ans", pair.getKey().toString(), pair.getValue().toString(), false);
				} else {
					outputLogger.println(" Action Task [" + pair.getKey().toString() + "] aleady exist in DB");
				}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			Iterator it = hmContentTask.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println("__________ " + pair.getKey() + " = " + pair.getValue());
		        if(hmContentTaskDB.get(pair.getKey().toString()) == null) {
					insertStaticTaskData("content_task_ans", pair.getKey().toString(), pair.getValue().toString(), false);
				} else {
					outputLogger.println(" Content Task [" + pair.getKey().toString() + "] aleady exist in DB");
				}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		try {
			Iterator it = hmSocialTask.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println("__________ " + pair.getKey() + " = " + pair.getValue());
		        if(hmSocialTaskDB.get(pair.getKey().toString()) == null) {
					insertStaticTaskData("social_task_ans", pair.getKey().toString(), pair.getValue().toString(), false);
				} else {
					outputLogger.println(" Social Task [" + pair.getKey().toString() + "] aleady exist in DB");
				}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	
		try {
			Iterator it = hmTrackTask.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println("__________ " + pair.getKey() + " = " + pair.getValue());
		        if(hmTrackTaskDB.get(pair.getKey().toString()) == null) {
					insertStaticTaskData("track_task_ans", pair.getKey().toString(), pair.getValue().toString(), false);
				} else {
					outputLogger.println(" Track Task [" + pair.getKey().toString() + "] aleady exist in DB");
				}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		try {
			Iterator it = hmUpsellTask.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry pair = (Map.Entry)it.next();
		        System.out.println("__________ " + pair.getKey() + " = " + pair.getValue());
		        if(hmUpsellTaskDB.get(pair.getKey().toString()) == null) {
					insertStaticTaskData("upsell_task_ans", pair.getKey().toString(), pair.getValue().toString(), false);
				} else {
					outputLogger.println(" Upsell Task [" + pair.getKey().toString() + "] aleady exist in DB");
				}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		outputLogger.println("===============");
		
	}
	
	
}