package com.reach.injector.profile;

public class ReachDataTypeBase {
	
	public final String code;
	public final String title;
	public final String selection;
	
	public ReachDataTypeBase(String code, String title, String selection) {
		super();
		this.code = code;
		this.title = title;
		this.selection = selection;
	}
	
	

}
