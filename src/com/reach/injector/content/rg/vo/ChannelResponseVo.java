package com.reach.injector.content.rg.vo;

import java.util.List;

public class ChannelResponseVo {

	private List<ChannelVo> channels;

	public List<ChannelVo> getChannels() {
		return channels;
	}

	public void setChannels(List<ChannelVo> channels) {
		this.channels = channels;
	}
	
}
