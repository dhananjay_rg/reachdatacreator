package com.reach.injector.content.rg.vo;

import java.util.ArrayList;
import java.util.List;

public class ChannelTopicMapVo {
	
	List<String> items  = new ArrayList<>();

	public List<String> getItems() {
		return items;
	}

	public void setItems(List<String> items) {
		this.items = items;
	}
	
	
}
