package com.reach.injector.content.rg.vo;

import java.util.List;

import javax.xml.transform.Source;

public class ChannelVo {

	private String displayName;
	private String image;
	private String description;
	private String code;
	private String[] healthday;
	private List<Source> sources;
	private int[] ailments;

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String[] getHealthday() {
		return healthday;
	}

	public void setHealthday(String[] healthday) {
		this.healthday = healthday;
	}

	public List<Source> getSources() {
		return sources;
	}

	public void setSources(List<Source> sources) {
		this.sources = sources;
	}

	public int[] getAilments() {
		return ailments;
	}

	public void setAilments(int[] ailments) {
		this.ailments = ailments;
	}

}
