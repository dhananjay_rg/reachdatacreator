package com.reach.injector.content.rg.vo;

public class OrganisationVo {

	private String org_app_id;
	private String org_name;
	private String[] channels;
	private String[] default_channels;
	private String last_updated_pb;
	private String last_updated_cf;

	
	public String getOrg_app_id() {
		return org_app_id;
	}

	public void setOrg_app_id(String org_app_id) {
		this.org_app_id = org_app_id;
	}

	public String getOrg_name() {
		return org_name;
	}

	public void setOrg_name(String org_name) {
		this.org_name = org_name;
	}

	public String[] getChannels() {
		return channels;
	}

	public void setChannels(String[] channels) {
		this.channels = channels;
	}

	public String[] getDefault_channels() {
		return default_channels;
	}

	public void setDefault_channels(String[] default_channels) {
		this.default_channels = default_channels;
	}

	public String getLast_updated_pb() {
		return last_updated_pb;
	}

	public void setLast_updated_pb(String last_updated_pb) {
		this.last_updated_pb = last_updated_pb;
	}

	public String getLast_updated_cf() {
		return last_updated_cf;
	}

	public void setLast_updated_cf(String last_updated_cf) {
		this.last_updated_cf = last_updated_cf;
	}

}
