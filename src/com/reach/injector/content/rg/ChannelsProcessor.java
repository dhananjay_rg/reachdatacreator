package com.reach.injector.content.rg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class ChannelsProcessor {

	private static PrintStream outputLogger = null;
	private static PrintStream errorLogger = null;
	
	public static void main(String[] args) throws FileNotFoundException {
		process();
	}
	
	public static void process() throws FileNotFoundException {
		init();
		getChannelList();
		getChannelByCode();
	}
	
	private static void init() throws FileNotFoundException {
		outputLogger = new PrintStream(new FileOutputStream(new File("logs_content/channels.log")));
		errorLogger = new PrintStream(new FileOutputStream(new File("logs_content/channel_issue.log")));
		
		
	}
	
	private static void getChannelList() {
		
		outputLogger.println("############### CHANNEL LIST PROCESS STARTED ##########");
		
		String url = "http://dev.contentstream.crosshealthexchange.com:9080/channels";
		String ret = "";
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);

			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("Content-type", "application/json");

			outputLogger.println("Executing get method");
			
			HttpResponse response = client.execute(httpGet);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			outputLogger.println("Reading response");
			
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
			outputLogger.println("Response JSON = " + ret);
			outputLogger.println("Parsing JSON....");
			//new TypeReference<List<MyClass>>(){});

//			ChannelResponseVo channels = JsonMapper.mapper.readValue(ret, ChannelResponseVo.class);
//			outputLogger.println("Channel List size=" + channels.getChannels().size());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("____" + ret);
		outputLogger.println("############### CHANNEL LIST PROCESS FINISHED ##########");
		outputLogger.println("");
	}
	

	private static void getChannelByCode() {
		
		outputLogger.println("############### GET CHANNEL BY CODE PROCESS STARTED ##########");
		
		String url = "http://dev.contentstream.crosshealthexchange.com:9080/channels/cnr";
		String ret = "";
		try {
			
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("Content-type", "application/json");

			outputLogger.println("Executing get method");
			
			HttpResponse response = client.execute(httpGet);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			outputLogger.println("Reading response");
			
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
			outputLogger.println("Response JSON = " + ret);
			outputLogger.println("Parsing JSON....");
			//new TypeReference<List<MyClass>>(){});

//			ChannelResponseVo channels = JsonMapper.mapper.readValue(ret, ChannelResponseVo.class);
//			outputLogger.println("Channel List size=" + channels.getChannels().size());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("____" + ret);
		outputLogger.println("############### GET CHANNEL BY CODE PROCESS FINISHED ##########");
		outputLogger.println("");
	}
}
