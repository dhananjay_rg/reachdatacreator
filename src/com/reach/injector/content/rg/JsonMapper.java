/**
 * 
 */
package com.reach.injector.content.rg;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Sid
 *
 */
public class JsonMapper {

	
	
	private JsonMapper() {
	}

	public static ObjectMapper mapper = getJsonMapper();

	private static ObjectMapper getJsonMapper() {
		if(mapper!=null){
			return mapper;
		}else{
			return createMapperObject();
		}
	}

	private static ObjectMapper createMapperObject() {
		ObjectMapper jsonObjMapper = new ObjectMapper();
		jsonObjMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		//jsonObjMapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
		jsonObjMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		jsonObjMapper.setDateFormat(df);
/*		jsonObjMapper.getFactory().configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
*/		return jsonObjMapper;
	
	}



}
