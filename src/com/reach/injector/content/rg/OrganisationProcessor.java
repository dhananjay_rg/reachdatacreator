package com.reach.injector.content.rg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.reach.injector.content.rg.vo.OrganisationVo;

public class OrganisationProcessor {

	private static PrintStream outputLogger = null;
	private static PrintStream errorLogger = null;
	
	public static void main(String[] args) throws FileNotFoundException {
		process();
	}
	
	public static void process() throws FileNotFoundException {
		init();
		getOrganisationList();
//		getChannelByCode();
	}
	
	private static void init() throws FileNotFoundException {
		outputLogger = new PrintStream(new FileOutputStream(new File("logs_content/organisation.log")));
		errorLogger = new PrintStream(new FileOutputStream(new File("logs_content/organisation_issue.log")));
		
		
	}
	
	private static void getOrganisationList() {
		
		outputLogger.println("############### ORGANISATION LIST PROCESS STARTED ##########");
		
		String url = "http://dev.contentstream.crosshealthexchange.com:9080/organisations";
		String ret = "";
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpGet httpGet = new HttpGet(url);

			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("Content-type", "application/json");

			outputLogger.println("Executing get method");
			
			HttpResponse response = client.execute(httpGet);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			outputLogger.println("Reading response");
			
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
			outputLogger.println("Response JSON = " + ret);
			outputLogger.println("Parsing JSON....");
			
			TypeReference<List<OrganisationVo>> mapType = new TypeReference<List<OrganisationVo>>() {};
			
			List<OrganisationVo> organisanions = JsonMapper.mapper.readValue(ret, mapType);
			
			outputLogger.println(" organisations list size=" + organisanions.size());
			
			StringBuilder sb = new StringBuilder("");
			for(int i=0;i<organisanions.size();i++) {
				if(sb == null || sb.toString().equals("")) {
					sb.append(organisanions.get(i).getOrg_name());
				} else {
					sb.append(", " + organisanions.get(i).getOrg_name());
				}
			}
			
			outputLogger.println(" organisations are [" + sb.toString() +"]");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("____" + ret);
		outputLogger.println("############### CHANNEL LIST PROCESS FINISHED ##########");
		outputLogger.println("");
	}
	
}
