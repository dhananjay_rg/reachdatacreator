package com.reach.injector.content.rg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.reach.injector.constant.Constant;
import com.reach.injector.content.rg.vo.TopicVo;
import com.reach.util.DBUtil;
import com.reach.util.ReachProperties;

public class TopicProcessor {

	private static PrintStream outputLogger = null;

	public static void main(String[] args) throws FileNotFoundException {
		process();
	}

	public static void process() throws FileNotFoundException {
		init();
		createTopics();
	}

	private static void init() throws FileNotFoundException {
		outputLogger = new PrintStream(new FileOutputStream(new File("topics.log")));

	}

	private static void createTopics() {

		outputLogger.println("############### TOPIC PROCESS STARTED ##########");

		String sql = "select * from habits where user_id is null order by id";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		try {

			con = DBUtil.getConnection();
			ps = con.prepareStatement(sql);
			System.out.println(ps);
			rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("_____ id=" + rs.getString("id"));
				TopicVo topic = new TopicVo();
				topic.setCode(Constant.Content.TOPIC_PREFIX + rs.getString("id"));
				topic.setName(rs.getString("title"));
				topic.setDescription(rs.getString("title"));
				topic.setOrgId(Constant.Content.ORG_ID);
				postData(topic); 
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
					con = null;
				} catch (SQLException e) {
				}
			}
		}

	}

	private static void postData(TopicVo topic) {

		System.out.println("---");
		String url = ReachProperties.instance.getContentServerPush4Topic();
		String ret = "";
		try {
			String reqJson = JsonMapper.mapper.writeValueAsString(topic);

			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(url);

			StringEntity entity = new StringEntity(reqJson);
			httpPost.setEntity(entity);
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setHeader("Accept", "*/*");
			httpPost.setHeader("org", Constant.Content.ORG_ID);

			HttpResponse response = client.execute(httpPost);

			outputLogger.println("=========================");

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			outputLogger.println("Reading response");

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
			System.out.println(ret);
			outputLogger.println("Response JSON = " + ret);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
