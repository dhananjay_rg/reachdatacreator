package com.reach.injector.content.rg.blogs.update;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.reach.injector.constant.Constant;
import com.reach.injector.content.rg.JsonMapper;
import com.reach.injector.content.vo.ContentResponseVo;
import com.reach.injector.content.vo.Meta;
import com.reach.injector.content.vo.ResponseErrorVo;
import com.reach.injector.content.vo.UpdateContentRequestVo;
//import com.reach.injector.wordpress.ContentObino;
import com.reach.util.DBUtil;
import com.reach.util.ReachProperties;

public class UpdateObinoToRGOffline {

	private static PrintStream outputLogger = null;
	
	public static int STATUS_ACTIVE = 1;
	public static int STATUS_INACTIVE = 2;
	public static int STATUS_COMPLETED = 3;
	public static int STATUS_FAILURE = 4;

	public static void main(String[] args) {
		try {
			process();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void process() throws Exception {
		Connection con = null;
		try {
			
			con = DBUtil.getConnection();
//			String sql = "select * from contents_tmp where source='Obino' and id in (15003,15004,15005,15006,15007,15008,15009,15010)";
			String sql = "select * from contents_tmp where source='Obino' and content_type='Blog' and status=0 order by id limit 20";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			String sqlTopic = "SELECT habit_id from content_habit_map where content_id=?";
			PreparedStatement psTopic = null;
			
			String sqlUp = "update contents_tmp set status=?, req_uuid=?, res_uuid=?, response_text=? where id=?";
			PreparedStatement psUp = null;
			ResultSet rsTopic = null;

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			while (rs.next()) {
				outputLogger = new PrintStream(new FileOutputStream(new File("logs_content/"+rs.getString("id")+".html")));
				
			//	String bodyText = ContentObino.process(rs.getString("url"));
				
//				 System.out.println(bodyText);
				
				/*
				String[] tagsArray = rs.getString("tags").split(",");
				
				UpdateContentRequestVo req = new UpdateContentRequestVo();
				req.setArticleId(rs.getString("id"));
				req.setBody(bodyText);
				req.setHeadline(rs.getString("title"));
				req.setBlurb(rs.getString("title"));
				req.setReadtime(rs.getString("duration"));
				req.setArchivedate(sdf.format(new Date()));
				req.setPostdate(sdf.format(new Date()));
				req.setFeatureImage(rs.getString("img_url"));
				req.setUuid(rs.getString("res_uuid"));
				if(rs.getString("content_type").equalsIgnoreCase("Blog")) {
					req.setDisplayType("article");
				} else if(rs.getString("content_type").equalsIgnoreCase("Video")) {
					req.setDisplayType("video");
				} else if(rs.getString("content_type").equalsIgnoreCase("Recipie")) {
					req.setDisplayType("recipie");
				}
				
				Meta meta = new Meta();
				meta.setType(rs.getString("content_type"));
				meta.setSource(rs.getString("source"));
				
				
				List<String> tags = new ArrayList<>();
				List<String> topics = new ArrayList<>();
				if(tagsArray.length>0) {
					for(String tag: tagsArray){
						tags.add(tag);
					}
				}
				
				// put topic in list
				psTopic = con.prepareStatement(sqlTopic);
				psTopic.setLong(1, rs.getLong("id"));
				rsTopic = psTopic.executeQuery();
				while(rsTopic.next()) {
					topics.add(Constant.Content.TOPIC_PREFIX +rsTopic.getString(1));
				}
				
				req.setMeta(meta);
				req.setTags(tags);
				req.setTopics(topics);
				
				// clean some data
				try {
					if(rsTopic != null){
						rsTopic.close();
						rsTopic = null;
					}
					if(psTopic != null){
						psTopic.close();
						psTopic = null;
					}
				} catch(Exception s) {}
				// clean some data
				
				String reqJson = JsonMapper.mapper.writeValueAsString(req);
//				outputLogger.println("___Req JSON=" + reqJson);
				System.out.println(reqJson);
				
				String resJson = postContent(reqJson, rs.getString("res_uuid"));
				System.out.println("_______res = " + resJson);
				
				psUp = con.prepareStatement(sqlUp);
				
				if (!resJson.contains("Bad Request")) {
					ContentResponseVo res = JsonMapper.mapper.readValue(resJson, ContentResponseVo.class);
					psUp.setInt(1, STATUS_ACTIVE);
					psUp.setString(2, "");
					psUp.setString(3, res.getUuid());
					psUp.setString(4, "success");
					psUp.setLong(5, rs.getLong("id"));
					psUp.executeUpdate();
					System.out.println("__ psupdate= " + psUp);
				} else {
					ResponseErrorVo resError = JsonMapper.mapper.readValue(resJson, ResponseErrorVo.class);
					System.out.println("___Error=" + resError.getMessage());
					System.out.println(resError.getException());
					System.out.println(resError.getStatus());
					psUp.setInt(1, STATUS_FAILURE);
					psUp.setString(2, "");
					psUp.setString(3, "");
					psUp.setString(4, resError.getMessage());
					psUp.setLong(5, rs.getLong("id"));
					psUp.executeUpdate();
				}
				
				 */
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Process Finished...");
		}
	}

	private static String getContentText(String fileName) throws Exception{
		
	     String line = null;
		 FileReader fileReader =  new FileReader(fileName);

		 BufferedReader bufferedReader = new BufferedReader(fileReader);
		 StringBuilder sb = new StringBuilder();
         while((line = bufferedReader.readLine()) != null) {
            sb.append(line);
         }  
         bufferedReader.close();
         return sb.toString();
	}
	
	private static String postContent(String json, String uuid) {
		String ret = "";
		try {
			HttpClient client = HttpClientBuilder.create().build();
			String url = ReachProperties.instance.getContentServerPutUrl4Article() + uuid;
			System.out.println("___ url=" + url);
			HttpPut httpPost = new HttpPut(url);

			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(httpPost);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
