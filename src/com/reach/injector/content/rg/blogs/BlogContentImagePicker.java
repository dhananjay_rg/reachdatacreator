package com.reach.injector.content.rg.blogs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.reach.util.DBUtil;

public class BlogContentImagePicker {

	private static PrintStream outputLogger = null;
	private static List<String> urlList = new ArrayList<>();

	public static void main(String[] args) {
		try {
			outputLogger = new PrintStream(new FileOutputStream(new File("obino_featured_images.txt")));
			
			process();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void loadUrl() {
		String sql = "select url from contents where source='Obino' and content_type='Blog' order by id";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		
		try {
			con = DBUtil.getConnection();
			ps = con.prepareStatement(sql);		
			System.out.println(ps);
			rs = ps.executeQuery();
			while(rs.next()) {
				urlList.add(rs.getString(1));
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(con!=null) { try { con.close(); } catch (Exception e) { }}
		}
	}
	private static void process() throws Exception {
	
		loadUrl();
		
//		urlList.add("http://www.obino.in/blog/eating-healthy-go/");		
//		urlList.add("http://www.obino.in/blog/7-days-sugar-free-challenge/");
		
		try {
			
			for(int i=0;i<urlList.size();i++) {
				
				Document doc = Jsoup.connect(urlList.get(i)).get();
				Elements imgHeadline = doc.getElementsByClass("post-image");
				String str2 = "";
				Elements elmnts2 = imgHeadline.get(0).getAllElements();
				str2 = elmnts2.get(0).html();
				
				getHeaderImage(urlList.get(i), str2);
				
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			System.out.println("Process Finished...");
		}
	}

	
	private static void getHeaderImage(String urlStr, String text)  throws Exception{
	    String urlRegex = "((https?|https):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
	    Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
	    Matcher urlMatcher = pattern.matcher(text);
	    URL url;
	    URLConnection conn;
	    int size;
	    int counter = 0;
	    boolean flag = false;
//	    System.out.println(urlStr);
		while (urlMatcher.find()) {
			
			String urlstring = text.substring(urlMatcher.start(0),urlMatcher.end(0));
			if(!urlstring.contains("logo.png") && counter<5 && !flag && (urlstring.contains(".png") || urlstring.contains(".jpeg") || urlstring.contains(".jpg") || urlstring.contains(".gif"))) {
	    		counter++;
	    		url = new URL(urlstring);
	    	      conn = url.openConnection();
	    	      size = conn.getContentLength();
	    	      if(urlstring.contains("420x") || urlstring.contains("300x")) {
	    	    	  String str = urlStr;
	    	    	  str = str.replace("http://www.obino.in/blog/", "");
	    	    	  str = urlStr.replace("/", ""); 
	    	        outputLogger.println(str + ":" + urlstring);
	    	        flag = true;
	    	        Connection con =null;
	    	        try {
	    	        	con = DBUtil.getConnection();
	    	        	String sql = "update contents set featured_image_url='" + urlstring + "' where url='" + urlStr + "'";
	    	        	PreparedStatement ps = con.prepareStatement(sql);
	    				ps.executeUpdate();
	    	        } catch(Exception e) {
	    	        	e.printStackTrace();
	    	        }finally {
	    	        	if(con!=null) { try { con.close(); } catch (Exception e) { }}
	    	        }
	    	      }
	    	      
	    	      conn.getInputStream().close();
	    	
	        
	    	}
	    }
	}
	

}
