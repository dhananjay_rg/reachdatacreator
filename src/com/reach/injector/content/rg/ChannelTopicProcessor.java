package com.reach.injector.content.rg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.ddf.EscherColorRef.SysIndexSource;

import com.reach.injector.constant.Constant;
import com.reach.injector.content.rg.vo.ChannelTopicMapVo;
import com.reach.injector.content.rg.vo.TopicVo;
import com.reach.util.DBUtil;
import com.reach.util.ReachProperties;

public class ChannelTopicProcessor {

	private static PrintStream outputLogger = null;

	public static void main(String[] args) throws FileNotFoundException {
		process();
	}

	public static void process() throws FileNotFoundException {
		init();
		createTopics();
	}

	private static void init() throws FileNotFoundException {
		outputLogger = new PrintStream(new FileOutputStream(new File("channel_topics.log")));

	}

	private static void createTopics() {

		outputLogger.println("############### TOPIC PROCESS STARTED ##########");

		String sqlC = "select * from habit_types where id>1 order by id";
		PreparedStatement psC = null;
		ResultSet rsC = null;

		String sql = "select * from habits where h_type_id=? order by id";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		try {

			con = DBUtil.getConnection();

			psC = con.prepareStatement(sqlC);
			rsC = psC.executeQuery();
			while (rsC.next()) {

				String channel = Constant.Content.CHANNEL_PREFIX + "" + rsC.getString("title").toLowerCase();
				channel = channel.replaceAll(" ", "_");
				ps = con.prepareStatement(sql);
				ps.setInt(1, rsC.getInt("id"));
				rs = ps.executeQuery();
				List<String> topics = new ArrayList<>();
				while (rs.next()) {
					topics.add(Constant.Content.TOPIC_PREFIX + rs.getInt("id"));
				}
				ChannelTopicMapVo map = new ChannelTopicMapVo();
				map.setItems(topics);
				postData(channel, map);
				// System.out.println(channel + " == " + topics);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
					con = null;
				} catch (SQLException e) {
				}
			}
		}

	}

	private static void postData(String channel, ChannelTopicMapVo topics) {

		System.out.println("---");
		String url = ReachProperties.instance.getContentServerPush4Channel() + channel + "/topic/";
		String ret = "";
		try {
			String reqJson = JsonMapper.mapper.writeValueAsString(topics);

			System.out.println(reqJson);
			System.out.println(url);

			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(url);

			StringEntity entity = new StringEntity(reqJson);
			httpPost.setEntity(entity);
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setHeader("Accept", "*/*");
			httpPost.setHeader("org", Constant.Content.ORG_ID);

			HttpResponse response = client.execute(httpPost);

			outputLogger.println("=========================");

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			outputLogger.println("Reading response");

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
			System.out.println(ret);
			outputLogger.println("Response JSON = " + ret);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
