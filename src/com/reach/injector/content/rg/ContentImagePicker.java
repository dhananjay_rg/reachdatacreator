package com.reach.injector.content.rg;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.helpers.UtilLoggingLevel;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.reach.injector.constant.Constant;
import com.reach.injector.content.vo.ContentRequestVo;
import com.reach.injector.content.vo.ContentResponseVo;
import com.reach.injector.content.vo.Meta;
import com.reach.injector.content.vo.ResponseErrorVo;
import com.reach.util.DBUtil;
import com.reach.util.ReachProperties;

public class ContentImagePicker {

	private static PrintStream outputLogger = null;
	private static List<String> urlList = new ArrayList<>();

	public static void main(String[] args) {
		try {
			// outputLogger = new PrintStream(new FileOutputStream(new
			// File("logs/content_image_size.log")));

			process();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void loadUrl() {
		try {
			// urlList.add("http://www.obino.in/blog/eating-healthy-go/");
			String path = Constant.INPUT_DATA_FILE_PATH;
			File f = new File(path);
			FileInputStream ios = new FileInputStream(f);
			XSSFWorkbook workbook = new XSSFWorkbook(ios);
			XSSFSheet sheet = workbook.getSheet(Constant.SHEET_NAME_CONTENT);
			Iterator<Row> rowIterator = sheet.iterator();
			int count = 0;
			while (rowIterator.hasNext()) {
				count++;
				Row row = rowIterator.next();
				if (count < 2) {
					// Ignore Header
				} else {
					if (row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX) != null
							&& row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX).toString().trim()
									.equalsIgnoreCase("New")) {
						if (row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX) != null
								&& !row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX).toString().trim()
										.equals("")) {
							if (row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX) != null
									&& !row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX).toString().trim()
											.equals("")
									&& row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX).toString().trim()
											.equals("Video")) {
								urlList.add(
										row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX).toString().trim());
							}
						}
					}
				}
			}
			System.out.println("Size=" + urlList.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void process() throws Exception {

		// loadUrl();

		// List<String> urlList = new ArrayList<>();
		urlList.add("http://www.obino.in/blog/top-5-foods-to-fight-stress/");
		// urlList.add("https://www.doctorinsta.com/blog-content/sugar-a-sweet-health-hazard");
		// urlList.add("https://www.doctorinsta.com/blog-content/late-night-snacks-that-wont-make-you-gain-weight");
		// urlList.add("https://www.doctorinsta.com/blog-content/5-habits-that-wreck-your-gallbladder");
		/*
		 * urlList.add("http://www.obino.in/blog/7-ways-to-teatox-your-system/");
		 * urlList.add("http://www.obino.in/blog/17-simple-home-made-detox-drinks/");
		 * urlList.add("http://www.obino.in/blog/top-5-foods-to-fight-stress/");
		 * urlList.add("http://www.obino.in/blog/right-way-to-have-green-tea/");
		 * urlList.add("http://www.obino.in/blog/25-grams-proteins-look-like/");
		 * urlList.add("http://www.obino.in/blog/top-20-natural-foods-weight-loss/");
		 * urlList.add(
		 * "http://www.obino.in/blog/8-easy-ways-to-beat-the-festive-calorie-load/");
		 * urlList.add("http://www.obino.in/blog/6-health-buddies-life/");
		 * urlList.add("http://www.obino.in/blog/top-6-nutrition-facts-healthier/");
		 * urlList.add("http://www.obino.in/blog/regular-recipes-healthier-twist/");
		 * urlList.add(
		 * "http://www.obino.in/blog/sipping-away-extra-kilos-meal-replacers-pills-healthy/"
		 * ); urlList.add("http://www.obino.in/blog/may-lost-weight-lose-fat/");
		 * urlList.add("http://www.obino.in/blog/ashvin-eating-right-key-weight-loss/");
		 * urlList.add(
		 * "http://www.obino.in/blog/vegetables-vegetable-juices-daily-dilemma/");
		 * urlList.add("http://www.obino.in/blog/healthy-twist-burgers-chips-drinks/");
		 */

		try {

			for (int i = 0; i < urlList.size(); i++) {
				// System.out.println(i + " ...." + urlList.get(i));

				ContentFromURLReaderUtil content = new ContentFromURLReaderUtil();
				String data = content.getContent(urlList.get(i));
				String text = data.substring(data.indexOf("<body"), data.indexOf("</body")) + "</body>";
				// System.out.println(text);
				// System.out.println(i + " =============");
				Document doc = Jsoup.connect(urlList.get(i)).get();
				// System.out.println(doc.body());
				doc.getElementsByClass("sharedaddy sd-sharing-enabled").remove();
				Elements newsHeadlines = doc.getElementsByClass("entry-content clearfix");
				String str1 = "";
				Elements elmnts1 = newsHeadlines.get(0).getAllElements();
				str1 = elmnts1.get(0).html();
				System.out.println(str1);
				System.out.println("------");
				Elements imgHeadline = doc.getElementsByClass("post-image");
				String str2 = "";
				Elements elmnts2 = imgHeadline.get(0).getAllElements();
				str2 = elmnts2.get(0).html();
				System.out.println(str2);

				getHeaderImage(str2);

				// System.out.println(str2);
				// System.out.println("---");
				// System.out.println(text);
				// System.out.println("---");

				/*
				 * // List<String> containedUrls = new ArrayList<String>(); // String urlRegex =
				 * "((https?|https):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)"; //
				 * Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE); //
				 * Matcher urlMatcher = pattern.matcher(text);
				 * 
				 * URL url; URLConnection conn; int size; int counter = 0; boolean flag = false;
				 * while (urlMatcher.find()) { String urlstring =
				 * text.substring(urlMatcher.start(0),urlMatcher.end(0)); //
				 * System.out.println(urlstring); if(!urlstring.contains("logo.png") &&
				 * counter<5 && !flag && (urlstring.contains(".png") ||
				 * urlstring.contains(".jpeg") || urlstring.contains(".jpg") ||
				 * urlstring.contains(".gif"))) { // System.out.println(urlstring); counter++;
				 * try { url = new URL(urlstring); conn = url.openConnection(); //
				 * conn.setReadTimeout(500); conn.setReadTimeout(500); //Read timeout - .5
				 * seconds conn.setConnectTimeout(500); size = conn.getContentLength()/1024;
				 * System.out.println(" size=" + size); if(size<20) { flag = true; //
				 * System.out.println(i +" size=" + size); } // if(urlstring.contains("420x") ||
				 * urlstring.contains("300x")) { // System.out.println(urlstring + " __ Size: "
				 * + size + "- " + size/1024 + " - smallest"); // flag = true; // } else { //
				 * System.out.println(urlstring + " __ Size: " + size + "- " + size/1024); // }
				 * // conn.getInputStream().close();
				 * 
				 * containedUrls.add(text.substring(urlMatcher.start(0), urlMatcher.end(0)));
				 * url = null; } catch(Exception ee) {
				 * System.out.println("Errorrr...reading in URL " + urlstring); }
				 * 
				 * } else { // System.out.println("Invalid urlstring = " + urlstring); } }
				 * if(!flag) { outputLogger.println("row[" + (i+1) +
				 * "] bigger sieze image, URL["+ urlList.get(i) +"]"); } //
				 * System.out.println("");
				 * 
				 */
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// System.out.println("Process Finished...");
		}
	}

	private static void checkAllURL() {

		Connection con = null;
		try {

			con = DBUtil.getConnection();
			// String sql = "select * from contents where status=1 and injest_status=0 and
			// (url is not null and url<>'') order by id limit 1";
			String sql = "select * from contents where id>12394 and id<12656";
			PreparedStatement ps = con.prepareStatement(sql);
			// System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			int totalSuccess = 0;
			int totalFailure = 0;
			int count = 0;
			while (rs.next()) {
				count++;
				try {
					System.out.println(
							"_____ processing.. id[" + rs.getInt("id") + "], title[" + rs.getString("title") + "]");
					ContentFromURLReaderUtil content = new ContentFromURLReaderUtil();
					String data = content.getContent(rs.getString("url"));
					String bodyText = data.substring(data.indexOf("<body"), data.indexOf("</body")) + "</body>";
					totalSuccess++;
				} catch (Exception ee) {
					totalFailure++;
					System.out.println("Error.... " + ee.getMessage());
				}

			}

			System.out.println("======== SUMMARY =========");
			System.out.println("Total = " + count);
			System.out.println("Succes = " + totalSuccess);
			System.out.println("Failure = " + totalFailure);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Process Finished...");
		}

	}

	private static void checkStaticURL() {

		try {

			List<String> list = new ArrayList<>();
			list.add("https://www.gympik.com/articles/india-physiotherapy/");
			list.add("https://www.gympik.com/articles/ingeniously-simple-ways-water-intake-summer/");
			// list.add("https://www.gympik.com/articles/get-drool-worthy-Armss-with-these-exercises/");
			list.add("https://www.gympik.com/articles/why-zumba/");
			list.add("https://www.gympik.com/articles/3-exercises-Armss-triceps/");
			list.add("https://www.gympik.com/articles/practise-yoga-poses-improve-digestion/");
			list.add("https://www.gympik.com/articles/why-your-diet-needs-flax seeds/");
			list.add("https://www.gympik.com/articles/dance-all-the-way/");
			list.add("https://www.gympik.com/articles/dancercising-the-desi-way/");

			int totalSuccess = 0;
			int totalFailure = 0;
			int count = 0;
			for (int i = 0; i < list.size(); i++) {
				count++;
				try {
					System.out.println("_____ processing.. url[" + list.get(i) + "]");
					ContentFromURLReaderUtil content = new ContentFromURLReaderUtil();
					String data = content.getContent(list.get(i));
					String bodyText = data.substring(data.indexOf("<body"), data.indexOf("</body")) + "</body>";
					totalSuccess++;
				} catch (Exception ee) {
					totalFailure++;
					System.out.println("Error.... " + ee.getMessage());
				}

			}

			System.out.println("======== SUMMARY =========");
			System.out.println("Total = " + count);
			System.out.println("Succes = " + totalSuccess);
			System.out.println("Failure = " + totalFailure);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Process Finished...");
		}

	}

	private static void getHeaderImage(String text) {
		String urlRegex = "((https?|https):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
		Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
		Matcher urlMatcher = pattern.matcher(text);
		URL url;
		URLConnection conn;
		int size;
		while (urlMatcher.find()) {
			String urlstring = text.substring(urlMatcher.start(0), urlMatcher.end(0));
			if ((urlstring.contains(".png") || urlstring.contains(".jpeg") || urlstring.contains(".jpg")
					|| urlstring.contains(".gif"))) {
				System.out.println(urlstring);
			}
		}
	}

	private static String postContent(String json) {
		String ret = "";
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(ReachProperties.instance.getContentServerPushUrl4Article());

			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(httpPost);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
