package com.reach.injector.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.reach.injector.constant.Constant;
import com.reach.util.DBUtil;

public class ContentSheetReader {

	private static PrintStream outputLogger = null;
	private static PrintStream errorLogger = null;

	ContentSheetReader() {
		System.out.println(".......... XlsReader Started..");
	}

	public static void main(String[] args) {
		process();
	}

	public static String getYoutubeFeaturedImageUrl(String youtubeVideoUrl) throws Exception {
		if (youtubeVideoUrl.indexOf("www.youtube.com") != -1) {
			URL videoUrl = new URL(youtubeVideoUrl);
			Map<String, String> query_pairs = new LinkedHashMap<String, String>();
			String query = videoUrl.getQuery();
			if (query != null) {
				String[] pairs = query.split("&");
				for (String pair : pairs) {
					int idx = pair.indexOf("=");
					query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
							URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
				}
				Set keySet = query_pairs.keySet();
				if (query_pairs.get("v") != null) {
					return "https://img.youtube.com/vi/" + query_pairs.get("v") + "/maxresdefault.jpg";
				}
			}
		} else if(youtubeVideoUrl.indexOf("youtu.be") != -1) {
			int lastNdx = youtubeVideoUrl.lastIndexOf("/");
			return "https://img.youtube.com/vi/" + youtubeVideoUrl.substring(lastNdx+1) + "/maxresdefault.jpg";
		}
		return "";
	}


	public static String getEmbeddedUrl(String youtubeVideoUrl) throws Exception {
		if (youtubeVideoUrl.indexOf("www.youtube.com") != -1) {
			URL videoUrl = new URL(youtubeVideoUrl);
			Map<String, String> query_pairs = new LinkedHashMap<String, String>();
			String query = videoUrl.getQuery();
			if (query != null) {
				String[] pairs = query.split("&");
				for (String pair : pairs) {
					int idx = pair.indexOf("=");
					query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"),
							URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
				}
				Set keySet = query_pairs.keySet();
				if (query_pairs.get("v") != null) {
					return "https://www.youtube.com/embed/" + query_pairs.get("v") + "?rel=0";
				}
			}
		}
		return "";
	}

	public static void process() {
		System.out.println("Content Reading Process Started...");

		String path = Constant.INPUT_DATA_FILE_PATH;
		Connection con = null;
		try {
			outputLogger = new PrintStream(new FileOutputStream(new File("logs/Content_insert.log")));
			errorLogger = new PrintStream(new FileOutputStream(new File("logs/Content_issue.log")));

			con = DBUtil.getConnection();

			outputLogger.println("Loading Contents from DB");
			String sqlContent = "select * from contents where status=1";
			PreparedStatement psContent = con.prepareStatement(sqlContent);
			ResultSet rsContent = psContent.executeQuery();
			List<String> listContent = new ArrayList<String>();
			while (rsContent.next()) {
				listContent.add(rsContent.getString("title"));
			}
			List<String> listNewContent = new ArrayList<String>();
			outputLogger.println("Loaded " + listContent.size() + " Content from DB");

			outputLogger.println("------------------------------");

			long startId = 10000l;
			String sql = "insert into contents(id, title, url, source, content_type, duration, tags, featured_image_url, cs_content_id) values(?,?,?,?,?,?,?,?,?)";
			PreparedStatement ps = null;

			ps = con.prepareStatement(sql);

			File f = new File(path);
			FileInputStream ios = new FileInputStream(f);
			XSSFWorkbook workbook = new XSSFWorkbook(ios);
			XSSFSheet sheet = workbook.getSheet(Constant.SHEET_NAME_CONTENT);
			Iterator<Row> rowIterator = sheet.iterator();
			int count = 0;
			int countsuccess = 0;
			int countexist = 0;
			int countError = 0;
			int countDuplicate = 0;
			int countFailure = 0;
			int countUrlError = 0;
			int countSourceError = 0;
			int countContentTypeError = 0;
			int countHabitTypeError = 0;
			while (rowIterator.hasNext()) {
				count++;
				Row row = rowIterator.next();
				if (count < 2) {
					// Ignore Header
				} else {

					if (row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX) != null
							&& row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX).toString().trim()
									.equalsIgnoreCase("New")) {
						if (row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX) != null
								&& !row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX).toString().trim()
										.equals("")) {
							String csContentId = row.getCell(Constant.ContentColumnsIndex.CS_CONTENT_ID_COLUMN_INDEX).toString().trim();

							String title = row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX).toString().trim();

							if (listContent.contains(title)) {
								countexist++;
								outputLogger.println("row " + count + " => Found Content [" + title
										+ "] & it is already exist in DB ");
							} else {
								try {
									Thread.sleep(10L);
								} catch (Exception ex) {
								}
								startId++;
								String url = "";
//								String embeddedVideoUrl = "";
								String featuredImageUrl = "";
								if (listNewContent.contains(row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
										.toString().trim())) {
									countDuplicate++;
									countFailure++;
									System.out.println("___ Failed:: row " + count + " Reason: Duplicatel");
									errorLogger.println(
											"Error: row " + count + " => Content [" + title + "], Duplicate entry ");
								} else {
									listNewContent.add(row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
											.toString().trim());
									countsuccess++;
									outputLogger.println(
											"row " + count + " => Found Content [" + title + "] & adding into Batch ");
									ps.setLong(1, startId);
									ps.setString(2, row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
											.toString().trim()); // title

									if (row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX) != null
											&& !row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX).toString()
													.trim().equals("")) {
										url = row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX).toString()
												.trim();
										ps.setString(3, url); // url
									} else {
										if (row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX).toString()
												.trim().toLowerCase().equals("recipe")) {
											ps.setString(3, row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX)
													.toString().trim());
										} else {
											countError++;
											countUrlError++;
											ps.setString(3, "");
											errorLogger.println("Error: row " + count + " => Content Type["
													+ row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX)
													+ "] , Content [" + title + "],  status["
													+ row.getCell(
															Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX)
													+ "] URL is blank ");
										}
									}

									if (row.getCell(Constant.ContentColumnsIndex.SOURCE_COLUMN_INDEX) != null
											&& !row.getCell(Constant.ContentColumnsIndex.SOURCE_COLUMN_INDEX).toString()
													.trim().equals("")) {
										ps.setString(4, row.getCell(Constant.ContentColumnsIndex.SOURCE_COLUMN_INDEX)
												.toString().trim()); // source
									} else {
										countError++;
										countSourceError++;
										ps.setString(4, "");
										errorLogger.println(
												"Error: row " + count + " => Content [" + title + "],  Source is blank ");
									}

									if (row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX) != null
											&& !row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX)
													.toString().trim().equals("")) {
										String contentType = row
												.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX).toString()
												.trim();
										ps.setString(5, contentType); // content type
										if (contentType.equalsIgnoreCase("Video") && (url != null)) {
											featuredImageUrl = getYoutubeFeaturedImageUrl(url);
										}
									} else {
										countError++;
										countContentTypeError++;
										ps.setString(5, "");
										errorLogger.println("Error: row " + count + " => Content [" + title
												+ "],  Content Type is blank ");
									}

									if (row.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX) != null
											&& !row.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX)
													.toString().trim().equals("")) {
										if (row.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX)
												.getCellType() == Cell.CELL_TYPE_STRING) {
											// double duration =
											// row.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX).getNumericCellValue()
											// * Constant.DURATION_MULTIPLIER;
											// ps.setDouble(6, duration); // duration

											double duration = 0;
											String strDuration = row
													.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX)
													.toString().trim();
											if (strDuration.contains(".")) {
												String arrDuration[] = strDuration.split("\\.");
												duration = (Double.parseDouble(arrDuration[0])
														* Constant.DURATION_MULTIPLIER)
														+ Integer.parseInt(arrDuration[1]);
											} else if (strDuration.contains(":")) {
												String arrDuration[] = strDuration.split(":");
												duration = (Double.parseDouble(arrDuration[0])
														* Constant.DURATION_MULTIPLIER)
														+ Integer.parseInt(arrDuration[1]);
											} else {
												duration = (Double) row
														.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX)
														.getNumericCellValue();
											}
											ps.setDouble(6, duration);
										} else {
											countError++;
											countContentTypeError++;
											ps.setDouble(6, 0);
											errorLogger.println("Error: row " + count + " => Content [" + title
													+ "],  Duration is not String ");
										}

									} else {
										if (row.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX).toString()
												.trim().toLowerCase().equalsIgnoreCase("video")
												|| row.getCell(Constant.ContentColumnsIndex.DURATION_COLUMN_INDEX)
														.toString().trim().toLowerCase().equalsIgnoreCase("audio")) {
											ps.setString(3, row.getCell(Constant.ContentColumnsIndex.URL_COLUMN_INDEX)
													.toString().trim());
											countError++;
											countContentTypeError++;
											ps.setDouble(6, 0);
											errorLogger.println("Error: row " + count + " => Content [" + title
													+ "],  Duration is blank ");
										} else {
											ps.setDouble(6, 0);
										}
									}

									if (row.getCell(Constant.ContentColumnsIndex.HABIT_TYPE_1_COLUMN_INDEX) == null
											|| row.getCell(Constant.ContentColumnsIndex.HABIT_TYPE_1_COLUMN_INDEX)
													.toString().trim().equals("")) {
										countError++;
										countHabitTypeError++;
										errorLogger.println("Error: row " + count + " => Content [" + title
												+ "],  habitType-1 is blank ");
									}

									// tags
									String tags = "";
									int startcolumn = 34;
									int endcolumn = 41;
									int counter = 0;

									for (int i = startcolumn; i <= endcolumn; i++) {
										counter++;
										if (counter == 1) {
											if (row.getCell(i) != null
													&& !row.getCell(i).toString().trim().equals("")) {
												tags = row.getCell(i).toString().trim();
											}
										} else {
											if (row.getCell(i) != null
													&& !row.getCell(i).toString().trim().equals("")) {
												if (tags == null || tags.equals("")) {
													tags = row.getCell(i).toString().trim();
												} else {
													tags = tags + "," + row.getCell(i).toString().trim();
												}
											}
										}
									}

									if (tags == null) {
										ps.setString(7, "");
									} else {
										ps.setString(7, tags);
									}

									if (row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX) != null
											&& !row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX)
													.toString().trim().equals("")) {
										ps.setString(8, featuredImageUrl); // Youtube Embedded URL
										ps.setString(9,csContentId); // ContentSheet Content Id
										ps.addBatch();
									}
								}
							}
						}

					}
				}
			}

			outputLogger.println("row " + count + " => saving content batch... ");
			ps.executeBatch();

			outputLogger.println("_______ SUMMARY___________");
			outputLogger.println("Total rows = " + count);
			outputLogger.println("Total success inserted = " + countsuccess);
			outputLogger.println("Total altready exist = " + countexist);
			outputLogger.println("Total errors = " + countError);
			outputLogger.println("Total duplicates = " + countDuplicate);
			outputLogger.println("Total Failed = " + countFailure);
			outputLogger.println("============================== ");
			outputLogger.println("total URL Error = " + countUrlError);
			outputLogger.println("total source Error = " + countSourceError);
			outputLogger.println("total content Type Error = " + countContentTypeError);
			outputLogger.println("total Habit Type Error = " + countHabitTypeError);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("Content Reading Process Finished...");
		}
	}

}