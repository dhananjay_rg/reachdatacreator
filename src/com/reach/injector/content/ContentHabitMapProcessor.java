package com.reach.injector.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.reach.injector.constant.Constant;
import com.reach.util.DBUtil;

public class ContentHabitMapProcessor {

	private static PrintStream outputLogger = null;
	private static PrintStream errorLogger = null;

	ContentHabitMapProcessor() {

	}

	public static void main(String[] args) {
		process();
	}

	public static void process() {
		System.out.println("ContentHabitMap Started..");

		String path = Constant.INPUT_DATA_FILE_PATH;
		Connection con = null;

		try {

			outputLogger = new PrintStream(new FileOutputStream(new File("logs/Habit_insertContentHabitMap.log")));
			errorLogger = new PrintStream(new FileOutputStream(new File("logs/Habit_issueContentHabitMap.log")));

			con = DBUtil.getConnection();

			outputLogger.println("Loading habit from DB");
			String sqlHabit = "select * from habits where status=1";
			PreparedStatement psHabit = con.prepareStatement(sqlHabit);
			ResultSet rsHabit = psHabit.executeQuery();
			HashMap<String, Integer> hmHabit = new HashMap<>();
			while (rsHabit.next()) {
				hmHabit.put(rsHabit.getString("title").toLowerCase(), rsHabit.getInt("id"));
			}
			outputLogger.println("Loaded " + hmHabit.size() + " habit types from DB");

			outputLogger.println("------------------------------");

			outputLogger.println("Loading contents from DB");
			String sqlContent = "select * from contents where status=0 or status=1";
			PreparedStatement psContent = con.prepareStatement(sqlContent);
			ResultSet rsContent = psContent.executeQuery();
			HashMap<String, Long> hmContent = new HashMap<>();
			while (rsContent.next()) {
				hmContent.put(rsContent.getString("title"), rsContent.getLong("id"));
			}
			outputLogger.println("Loaded " + hmContent.size() + " contents from DB");

			outputLogger.println("------------------------------");

			outputLogger.println("Loading HabitsMap from DB");
			String sqlHabitMap = "select * from content_habit_map where status=1";
			PreparedStatement psHabitMap = con.prepareStatement(sqlHabitMap);
			ResultSet rsHabitMap = psHabitMap.executeQuery();
			List<String> listHabitMap = new ArrayList<String>();
			while (rsHabitMap.next()) {
				listHabitMap.add(rsHabitMap.getInt("habit_id") + "-" + rsHabitMap.getInt("content_id"));
			}
			outputLogger.println("Loaded " + listHabitMap.size() + " Habitmap from DB");

			outputLogger.println("------------------------------");

			String sql = "insert into content_habit_map(habit_id, content_id, content_type) values(?,?,?)";
			PreparedStatement ps = null;

			File f = new File(path);
			FileInputStream ios = new FileInputStream(f);
			XSSFWorkbook workbook = new XSSFWorkbook(ios);
			XSSFSheet sheet = workbook.getSheet(Constant.SHEET_NAME_CONTENT);
			Iterator<Row> rowIterator = sheet.iterator();
			int count = 0;
			int insertCount = 0;
			int issueCount = 0;
			while (rowIterator.hasNext()) {
				count++;
				Row row = rowIterator.next();
				if (count < 2) {
					// Ignore Header
				} else {
					if (row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX) != null
							&& row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX).toString().trim()
									.equalsIgnoreCase("New")) {
						if (row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX) != null
								&& !row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX).toString().trim()
										.equals("")
								&& row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX) != null
								&& !row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX).toString().trim()
										.equals("")) {
							int startcolumn = Constant.ContentColumnsIndex.HABIT_START_COLUMN_COLUMN_INDEX;
							int endcolumn = Constant.ContentColumnsIndex.HABIT_END_COLUMN_COLUMN_INDEX;
							int counter = 0;
							for (int i = startcolumn; i <= endcolumn; i++) {
								counter++;
								if (row.getCell(i) != null && !row.getCell(i).getStringCellValue().trim().equals("")) {
									String str = (row.getCell(i).toString()).trim();
									if (hmHabit.containsKey(str.toLowerCase())) {
										if (hmContent.containsKey(
												row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX).toString()
														.trim())) {
											if (!listHabitMap.contains(hmHabit.get(str) + "-"
													+ hmContent.get(
															row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
																	.toString().trim()))) {
												outputLogger.println("row " + count + " => Found Habit" + counter + " ["
														+ str + "] & inserting into ContentHabitMap ");
												ps = con.prepareStatement(sql);
												ps.setInt(1, hmHabit.get(str.toLowerCase()));
												ps.setLong(2, hmContent.get(
														row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
																.toString().trim()));
												ps.setString(3,
														row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX)
																.toString().trim());
												ps.executeUpdate();
												insertCount++;
											} else {
												outputLogger.println("row " + count + " => Map of Habit" + counter
														+ " [" + str + "] & Content ["
														+ row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
																.toString().trim()
														+ "] already exist");
											}
										} else {
											issueCount++;
											errorLogger.println("Error:  row [" + count + "] => Found Habit" + counter
													+ " [" + str + "] & content title ["
													+ row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
															.toString().trim()
													+ "] does not exist in master DB ");
										}
									} else {
										issueCount++;
										errorLogger.println("Error:  row [" + count + "] => Found Habit" + counter
												+ " [" + str + "]  & it does not exist in master Habit ");
									}
								}
							}
						}
					}

				}
			}

			outputLogger.println("_______ SUMMARY___________");
			outputLogger.println("Total success insert = " + insertCount);
			outputLogger.println("Total issue written in logs = " + issueCount);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
				}
			}

			System.out.println("ContentHabitMap Process Finished...");
		}
	}

}
