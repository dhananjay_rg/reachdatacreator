package com.reach.injector.content.vo;

public class Meta {
	private String author = "Obino Auth";
	private String project = "Reach";
	private String source = ""; // source
	private String type = ""; // content type Blog/Video
//	private String uuid = "8dcb2bff-4c3b-4619-bf83-419e82351b65";
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
//	public String getUuid() {
//		return uuid;
//	}
//	public void setUuid(String uuid) {
//		this.uuid = uuid;
//	}
//	
	
}
