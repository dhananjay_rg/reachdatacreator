package com.reach.injector.content.vo;

import com.reach.injector.constant.Constant;

import java.util.List;

public class ContentRequestVo {

	private String archivedate = ""; // put curr date format 2018-03-29
	private String articleId = "";
	private String articleType = Constant.Content.ARTICLE_TYPE;
	private String attr = "";
	private String blurb = "";
	private String body = ""; // body text
	private String byline = Constant.Content.BY_LINE;
	private String cmsSource = Constant.Content.CMS_SOURCE;
	private String copyright = Constant.Content.COPY_WTRITE;
	private String displayType = Constant.Content.DISPLAY_TYPE;
	private String featureBlurb = "";
	private String featureImage = "";
	private String headline = ""; // tille
	private String newsType = Constant.Content.NEWS_TYPE;
	private String postdate = ""; // put curr date format 2018-03-29
	private String readtime = ""; // duration
	private String sourceUrl = "";
	private String src = "";
	private Meta meta;
	private List<String> tags;
	private List<String> topics;

	public String getArchivedate() {
		return archivedate;
	}

	public void setArchivedate(String archivedate) {
		this.archivedate = archivedate;
	}

	public String getArticleId() {
		return articleId;
	}

	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}

	public String getArticleType() {
		return articleType;
	}

	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}

	public String getBlurb() {
		return blurb;
	}

	public void setBlurb(String blurb) {
		this.blurb = blurb;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getByline() {
		return byline;
	}

	public void setByline(String byline) {
		this.byline = byline;
	}

	public String getCmsSource() {
		return cmsSource;
	}

	public void setCmsSource(String cmsSource) {
		this.cmsSource = cmsSource;
	}

	public String getCopyright() {
		return copyright;
	}

	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	public String getDisplayType() {
		return displayType;
	}

	public void setDisplayType(String displayType) {
		this.displayType = displayType;
	}

	public String getFeatureBlurb() {
		return featureBlurb;
	}

	public void setFeatureBlurb(String featureBlurb) {
		this.featureBlurb = featureBlurb;
	}

	public String getFeatureImage() {
		return featureImage;
	}

	public void setFeatureImage(String featureImage) {
		this.featureImage = featureImage;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getNewsType() {
		return newsType;
	}

	public void setNewsType(String newsType) {
		this.newsType = newsType;
	}

	public String getPostdate() {
		return postdate;
	}

	public void setPostdate(String postdate) {
		this.postdate = postdate;
	}

	
	public String getReadtime() {
		return readtime;
	}

	public void setReadtime(String readtime) {
		this.readtime = readtime;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public List<String> getTopics() {
		return topics;
	}

	public void setTopics(List<String> topics) {
		this.topics = topics;
	}

	
}
