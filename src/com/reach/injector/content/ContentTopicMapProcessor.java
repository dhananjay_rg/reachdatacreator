package com.reach.injector.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.reach.injector.constant.Constant;
import com.reach.util.DBUtil;

public class ContentTopicMapProcessor {

	private static PrintStream outputLogger = null;
	private static PrintStream errorLogger = null;

	ContentTopicMapProcessor() {

	}

	public static void main(String[] args) {
		// process();
		System.out.println("This code has been removed.. drop table and refrence things.");
	}

	public static void process() {
		System.out.println("ContentTopicMap process Started..");

		String path = Constant.INPUT_DATA_FILE_PATH;
		Connection con = null;

		try {

			outputLogger = new PrintStream(new FileOutputStream(new File("logs/Topic_insertContentTopicMap.log")));
			errorLogger = new PrintStream(new FileOutputStream(new File("logs/Topic_issueContentTopicMap.log")));

			con = DBUtil.getConnection();

			outputLogger.println("Loading habit types from DB");
			String sqlType = "select * from habit_types where status=1";
			PreparedStatement psType = con.prepareStatement(sqlType);
			ResultSet rsType = psType.executeQuery();
			HashMap<String, Integer> hmType = new HashMap<>();
			while (rsType.next()) {
				hmType.put(rsType.getString("title").toLowerCase(), rsType.getInt("id"));
			}
			outputLogger.println("Loaded " + hmType.size() + " habit types from DB");

			outputLogger.println("------------------------------");

			outputLogger.println("Loading contents from DB");
			String sqlContent = "select * from contents where status=0 or status=1";
			PreparedStatement psContent = con.prepareStatement(sqlContent);
			ResultSet rsContent = psContent.executeQuery();
			HashMap<String, Integer> hmContent = new HashMap<>();
			while (rsContent.next()) {
				hmContent.put(rsContent.getString("title"), rsContent.getInt("id"));
			}
			outputLogger.println("Loaded " + hmContent.size() + " contents from DB");

			outputLogger.println("------------------------------");

			outputLogger.println("Loading Topic from DB");
			String sqlTopic = "select * from content_topic_map where status=1";
			PreparedStatement psTopic = con.prepareStatement(sqlTopic);
			ResultSet rsTopic = psTopic.executeQuery();
			List<String> listTopic = new ArrayList<String>();
			while (rsTopic.next()) {
				listTopic.add(rsTopic.getInt("topic_id") + "-" + rsTopic.getInt("content_id"));
			}
			outputLogger.println("Loaded " + listTopic.size() + " Topic from DB");

			outputLogger.println("------------------------------");

			String sql = "insert into content_topic_map(topic_id, content_id, content_type) values(?,?,?)";
			PreparedStatement ps = null;

			List<String> errorTypeList = new ArrayList<>();

			File f = new File(path);
			FileInputStream ios = new FileInputStream(f);
			XSSFWorkbook workbook = new XSSFWorkbook(ios);
			XSSFSheet sheet = workbook.getSheet(Constant.SHEET_NAME_CONTENT);
			Iterator<Row> rowIterator = sheet.iterator();
			int count = 0;
			int insertCount = 0;
			int issueCount = 0;
			while (rowIterator.hasNext()) {
				count++;
				Row row = rowIterator.next();
				if (count < 2) {
					// Ignore Header
				} else {
					if (row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX) != null
							&& row.getCell(Constant.ContentColumnsIndex.TO_BE_PROCESSED_COLUMN_INDEX).toString().trim()
									.equalsIgnoreCase("New")) {
						if (row.getCell(0) != null && !row.getCell(0).toString().trim().equals("")
								&& row.getCell(3) != null && !row.getCell(3).toString().trim().equals("")) {
							int startcolumn = Constant.ContentColumnsIndex.HABIT_TYPE_1_COLUMN_INDEX;
							int endcolumn = Constant.ContentColumnsIndex.HABIT_TYPE_6_COLUMN_INDEX;
							int counter = 0;
							for (int i = startcolumn; i <= endcolumn; i++) {
								counter++;
								if (row.getCell(i) != null && !row.getCell(i).toString().trim().equals("")) {
									String str = (row.getCell(i).toString()).trim();
									if (hmType.containsKey(str.toLowerCase())) {
										if (!listTopic.contains(hmType.get(str) + "-"
												+ hmContent.get(
														row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
																.toString().trim()))) {
											outputLogger.println("row " + count + " => Found Topic" + counter + " ["
													+ str + "] & inserting into ContentTopicMap ");
											ps = con.prepareStatement(sql);
											ps.setInt(1, hmType.get(str.toLowerCase()));
											ps.setInt(2,
													hmContent.get(
															row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
																	.toString().trim()));
											ps.setString(3,
													row.getCell(Constant.ContentColumnsIndex.TYPE_OF_COLUMN_INDEX)
															.toString().trim());
											ps.executeUpdate();
											insertCount++;
										} else {
											outputLogger.println("row " + count + " => Map of Topic" + counter + " ["
													+ str + "] & Content ["
													+ row.getCell(Constant.ContentColumnsIndex.TITLE_COLUMN_INDEX)
															.toString().trim()
													+ "] already exist");
										}
									} else {
										issueCount++;
										if (!errorTypeList.contains(str)) {
											errorTypeList.add(str);
										}
										errorLogger.println("Error:  row [" + count + "] => Found Topic" + counter
												+ " [" + str + "]  & it does not exist in master Habit Type ");
									}
								}
							}
						}
					}
				}
			}

			outputLogger.println("_______ SUMMARY___________");
			outputLogger.println("Total success insert = " + insertCount);
			outputLogger.println("Total issue written in logs = " + issueCount);
			outputLogger.println("error list count = " + errorTypeList.size());
			if (errorTypeList.size() > 0) {
				for (int i = 0; i < errorTypeList.size(); i++) {
					outputLogger
							.println("Error: habit Type [" + errorTypeList.get(i) + "] does not exist in master DB");
				}
			}

		} catch (

		Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("ContentTopicMap Process Finished...");
		}
	}

}
