package com.reach.injector.content;

public class ContentProcessStarter {

	public static void main(String[] args) {
		try {
			System.out.println("_______ STARTED._________");
			ContentSheetReader.process();
		 	Thread.sleep(2000);
//			System.out.println("-----------------");
//			ContentTopicMapProcessor.process();
//			Thread.sleep(2000);
			System.out.println("-----------------");
			ContentHabitMapProcessor.process();
			System.out.println("_______ FINISHED._________");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
