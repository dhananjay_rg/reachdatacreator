package com.reach.injector.content.content_html;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.reach.injector.content.rg.ContentFromURLReaderUtil;
import com.reach.util.ReachProperties;

public class ZoojoobiContent {

	private static List<String> urlList = new ArrayList<>();

	public static void main(String[] args) {
		try {
//			outputLogger = new PrintStream(new FileOutputStream(new File("logs/content_image_size.log")));
			
			process();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private static void process() throws Exception {
	
		urlList.add("http://blog.zoojoo.be/4-habits-of-highly-fit-people");
//		urlList.add("http://blog.zoojoo.be/beyond-carrots-foods-for-healthy-eyes");
		
		try {
			
			for(int i=0;i<urlList.size();i++) {
//				System.out.println(i + " ...." + urlList.get(i));
				
				ContentFromURLReaderUtil content = new ContentFromURLReaderUtil();
				String data = content.getContent(urlList.get(i));
				String text = data.substring(data.indexOf("<body"), data.indexOf("</body")) + "</body>";
//				System.out.println(text);
//				System.out.println(i + " =============");
				Document doc = Jsoup.connect(urlList.get(i)).get();
//				System.out.println(doc.body());
			
				Elements header = doc.getElementsByTag("head");
				String strHead = "";
				Elements elmntsHead = header.get(0).getAllElements();
				strHead = header.get(0).html();
				System.out.println("<head>"+strHead+"</head>");
				
				doc.getElementsByClass("post-header").remove();
				doc.getElementsByClass("cresta-share-icon third_style").remove();
				doc.getElementsByClass("post-navigation").remove();
				doc.getElementsByClass("comments-container").remove();
				doc.getElementsByClass("post-tags").remove();
//				doc.getElementsMatchingOwnText("post type-post status-publish format-standard has-post-thumbnail hentry category-diet tag-foods-for-eye-health tag-miladyhealth")
				
				Elements newsHeadlines = doc.getElementsByClass("content");
				String str1 = "";
				Elements elmnts1 = newsHeadlines.get(0).getAllElements();
				str1 = elmnts1.get(0).html();
				str1 = strHead + "<body style='background:white'><div class=\"entry-content clearfix\">"+str1+"</div></body>";
				System.out.println(str1);
//				System.out.println("------");
//				Elements imgHeadline = doc.getElementsByClass("post-image");
//				String str2 = "";
//				Elements elmnts2 = imgHeadline.get(0).getAllElements();
//				str2 = elmnts2.get(0).html();
//				System.out.println(str2);
				
//				getHeaderImage(str2);
				

			}
		
		
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			System.out.println("Process Finished...");
		}
	}


	
	private static void getHeaderImage(String text) {
	    String urlRegex = "((https?|https):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
	    Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
	    Matcher urlMatcher = pattern.matcher(text);
	    URL url;
	    URLConnection conn;
	    int size;
		while (urlMatcher.find()) {
			String urlstring = text.substring(urlMatcher.start(0),urlMatcher.end(0));
			if((urlstring.contains(".png") || urlstring.contains(".jpeg") || urlstring.contains(".jpg") || urlstring.contains(".gif"))) {
		    	System.out.println(urlstring);
			}
	    }
	}
	
	private static String postContent(String json) {
		String ret = "";
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(ReachProperties.instance.getContentServerPushUrl4Article());

			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(httpPost);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
