package com.reach.injector.content.content_html;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.reach.util.DBUtil;
import com.reach.util.ReachProperties;

public class UpdateObinoContentDump {

	private static PrintStream outputLogger = null;
	
	public static void main(String[] args) {
		try {
			
			process();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void process() throws Exception {
		Connection con = null;
		try {
			con = DBUtil.getConnection();
			String sql = "select * from contents_tmp where source='Obino' and id in (15003,15004,15005,15006,15007,15008,15009,15010)";
			PreparedStatement ps = con.prepareStatement(sql);
						
			ResultSet rs = ps.executeQuery();
			int count = 0;
			while (rs.next()) {
				count++;
				try {	
					System.out.println(rs.getString("id"));
					outputLogger = new PrintStream(new FileOutputStream(new File("content_html/obino/update_"+rs.getString("id")+"_content.txt")));
					
					Document doc = Jsoup.connect(rs.getString("url")).get();
					
					doc.getElementsByClass("sharedaddy sd-sharing-enabled").remove();
					Elements newsHeadlines = doc.getElementsByClass("entry-content clearfix");
					String str1 = "";
					Elements elmnts1 = newsHeadlines.get(0).getAllElements();
					str1 = elmnts1.get(0).html();
					str1 = "<html><body style='background:white'><div class=\"entry-content clearfix\">"+str1+"</div></body></html>";
//					System.out.println(str1);
//					System.out.println("------");
					Elements imgHeadline = doc.getElementsByClass("post-image");
					String str2 = "";
					Elements elmnts2 = imgHeadline.get(0).getAllElements();
					str2 = elmnts2.get(0).html();
					
					String img = getHeaderImage(str2);
					
					outputLogger.println(str1);
					
				} catch(Exception ee) {
					ee.printStackTrace();
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			System.out.println("Process Finished...");
		}
	}


	
	private static String getHeaderImage(String text) {
	    String urlRegex = "((https?|https):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
	    Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
	    Matcher urlMatcher = pattern.matcher(text);
	    URL url;
	    URLConnection conn;
	    int size;
	    String ret = null;
		while (urlMatcher.find()) {
			String urlstring = text.substring(urlMatcher.start(0),urlMatcher.end(0));
			if((urlstring.contains(".png") || urlstring.contains(".jpeg") || urlstring.contains(".jpg") || urlstring.contains(".gif"))) {
//		    	System.out.println(urlstring);
		    	ret = urlstring;
			}
	    }
		return ret;
	}
	
	private static String postContent(String json) {
		String ret = "";
		try {
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost httpPost = new HttpPost(ReachProperties.instance.getContentServerPushUrl4Article());

			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(httpPost);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			ret = result.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
