package com.reach.injector.visioning_task;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.injector.constant.Constant.ProcessStatus;
import com.reach.util.DBUtil;

public class InsertVisionBarrierMapData {
 
	private final Integer STATUS_ACTIVE = 1;
	private final Integer DEFAULT_SEQUENCE = 1;

	private  Workbook workbook = null;

	private   Connection conn = null;

	private   PrintStream outputLogger = null;
	private   PrintStream inputFileIssuesLogger = null;
	private   PrintStream sqlQueriesLogger = null;

	private   HashMap<String, Integer> barriers = null;
	private   HashMap<String, Integer> visions = null;
	private   ArrayList<String> barriersVisionsMapList = null;

	public static void main(String[] args) throws IOException, InvalidFormatException, SQLException, EncryptedDocumentException, DBError {
		processVisionBarrierMapData();
	}
	
	public static void processVisionBarrierMapData()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		
		InsertVisionBarrierMapData m=new InsertVisionBarrierMapData();
		
		
		try {
			m.processVisionBarrierMapDataNow();
			} finally { 
				
				if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
				}
			}
		
	}

	private void processVisionBarrierMapDataNow()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		init();
		loadBarriers();
		loadVisions();
		loadBarrierVisionMap();
		checkOldRecords();
		updateBarrierVisionMap();
		
	}

	private   void init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		// loadSheets();

		conn = DBUtil.getConnection();
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/insertVisionBarrierHabitMapData.log")));
		inputFileIssuesLogger = new PrintStream(
				new FileOutputStream(new File("logs/VisionBarrierHabitMapDataIssues.log")));
		sqlQueriesLogger = new PrintStream(new FileOutputStream(new File("logs/sqlQueriesMasterData.log")));

		barriers = new HashMap<String, Integer>();
		visions = new HashMap<String, Integer>();
		barriersVisionsMapList = new ArrayList<String>();
	}

	private   void loadBarriers() throws SQLException, IOException {
		outputLogger.println("Loading Barriers from database....");
		Statement stmt = conn.createStatement();
		String selectBarriersQuery = "select b.id, b.cs_barrier_id from barriers b where b.status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectBarriersQuery);
		if (rs != null) {
			while (rs.next()) {
				int barrierId = rs.getInt("id");
				String csBarrierId = rs.getString("cs_barrier_id");
				if (barriers.containsKey(csBarrierId)) {
					throw new IOException("Duplicate Entry [ " + csBarrierId + " ] in Barriers table");
				} else {
					barriers.put(csBarrierId, barrierId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + barriers.size() + " Barriers in database....");
		outputLogger.println("===============");
	}

	private   void loadVisions() throws SQLException, IOException {
		outputLogger.println("Loading Visions from database....");
		Statement stmt = conn.createStatement();
		String selectVisionsQuery = "select v.id, v.cs_vision_id from visions v where v.status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectVisionsQuery);
		if (rs != null) {
			while (rs.next()) {
				int visionId = rs.getInt("id");
				String csVisionId = rs.getString("cs_vision_id");
				if (visions.containsKey(csVisionId)) {
					throw new IOException("Duplicate Vision[ " + csVisionId + " ] in Visions table");
				} else {
					visions.put(csVisionId, visionId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + visions.size() + " Visions in database....");
		outputLogger.println("===============");
	}

		private   void loadBarrierVisionMap() throws SQLException, IOException {
		outputLogger.println("Loading Barriers-Visions-Map from database....");
		Statement stmt = conn.createStatement();
		String selectBarriersQuery = "select barrier_id, vision_id from barrier_vision_map where status="
				+ STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectBarriersQuery);
		barriersVisionsMapList.clear();
		
		
		if (rs != null) {
			while (rs.next()) {
				int barrierId = rs.getInt("barrier_id");
				int visionId = rs.getInt("vision_id");
				if (barriersVisionsMapList.contains(barrierId + "~" + visionId)) {
					throw new IOException(
							"Duplicate Entry [ " + barrierId + "," + visionId + " ] in Barriers-Visions-Map table");
				} else {
					barriersVisionsMapList.add(barrierId + "~" + visionId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + barriersVisionsMapList.size() + " Barriers-Visions-Mappings in database....");
		outputLogger.println("===============");
	}

	private void checkOldRecords() {
		
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_VISION_BARRIER_MAP);
		Iterator<Row> rowIterator = sheet.rowIterator();
	
		int rowNum = 0;
		// Ignore header row
		if (rowIterator.hasNext()) {
			rowIterator.next();
			rowNum++;
		}
		
		ArrayList<String> visionBarrierList = new ArrayList<String>();		
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			rowNum++;
			
			String processStatus = row.getCell(0).getStringCellValue().trim();
			
			if (processStatus.equalsIgnoreCase(ProcessStatus.old)) {
				String visionIdBarrierId = row.getCell(3).getStringCellValue().trim();
				
				if (!visionBarrierList.contains(visionIdBarrierId)) {
					visionBarrierList.add(visionIdBarrierId);
				}
			}
		}
		
		System.out.println("Total size of barriersVisionsMapList=" + barriersVisionsMapList.size());
		
		for(int i=0;i<visionBarrierList.size();i++) {
			String ids[] = visionBarrierList.get(i).split("\\.");
			if(barriersVisionsMapList.contains(barriers.get(ids[1]) + "~" + visions.get(ids[0]))) {
				barriersVisionsMapList.remove(barriers.get(ids[1]) + "~" + visions.get(ids[0]));
			}
		}
		System.out.println("Remain size of barriersVisionsMapList=" + barriersVisionsMapList.size());
		
		if(barriersVisionsMapList.size() > 0) {
			for(int i=0;i<barriersVisionsMapList.size();i++) {
				String ids[] = visionBarrierList.get(i).split("~");
				removeBarrierVisionMapByBarrierIdVisionId(Integer.parseInt(ids[1]), Integer.parseInt(ids[0]));
			}
		}
		
	}
	
	private   void updateBarrierVisionMap() throws SQLException, IOException {
		
		loadBarrierVisionMap();
		
		outputLogger.println("Updating Barriers-Visions-Map from input file....");
		PreparedStatement pstmt = conn
				.prepareStatement("insert into barrier_vision_map (barrier_id, vision_id, seq) values (?,?,?)");
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_VISION_BARRIER_MAP);
		Iterator<Row> rowIterator = sheet.rowIterator();
		int cnt = 0;
		int insertUpdate = 0;

		int rowNum = 0;
		// Ignore header row
		if (rowIterator.hasNext()) {
			rowIterator.next();
			rowNum++;
		}

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			rowNum++;

			
			String processStatus = row.getCell(0).getStringCellValue().trim();
			
			if (processStatus.equalsIgnoreCase(ProcessStatus.insert) || processStatus.equalsIgnoreCase(ProcessStatus.update)) {
			
				cnt++;
				
				Cell visionCell = row.getCell(2);
				if (visionCell == null || visionCell.getStringCellValue() == null
						|| visionCell.getStringCellValue().equals("")) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
							+ " ] ==> Unable to process Vision is not defined.");
					continue;
				}
				String visionIdBarrierId = row.getCell(3).getStringCellValue().trim();
				String ids[] = visionIdBarrierId.split("\\.");
				int visionId = visions.get(ids[0]);
				int barrierId = barriers.get(ids[1]);
				if (visionId == 0) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + (rowNum + 1)
							+ " ] ==> Vision [ " + visionCell.getStringCellValue() + " ] not exist in Masterdata.");
					continue;
				}
				
				if (barrierId == 0) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + (rowNum + 1)
							+ " ] ==> Barrier [ " + row.getCell(4).getStringCellValue().trim() + " ] not exist in Masterdata.");
					continue;
				}
				
				pstmt.setInt(1, barrierId);
				pstmt.setInt(2, visionId);
				pstmt.setInt(3, DEFAULT_SEQUENCE);
				pstmt.executeUpdate();
				barriersVisionsMapList.add(barrierId + "~" + visionId);
				insertUpdate++;
				
			}
			
			
		}

		pstmt.close();
		outputLogger.println("To be Inserted " + cnt);
		outputLogger.println("ActualInserted " + insertUpdate);
		outputLogger.println("===============");
	}
	
	
	private void removeBarrierVisionMapByBarrierIdVisionId(int barrierId, int visionId) {
		
		try {
			String stmt = "update barrier_vision_map set status= " + Constant.STATUS_INACTIVE + " where barrier_id=? and vision_id=? and status=" + Constant.STATUS_ACTIVE;
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.setInt(1, barrierId);
			ps.setInt(2, visionId);
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
}