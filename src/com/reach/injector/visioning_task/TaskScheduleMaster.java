package com.reach.injector.visioning_task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.util.DBUtil;

public class TaskScheduleMaster {
	private   PrintStream outputLogger = null;
	private   PrintStream inputFileIssuesLogger = null;
	
	//private static FileWriter writer;
	private   Sheet worksheet;
	
	private   final int dayIndex=0;
	private   final int[] taskStartIndex;
	private   final int[] taskEndIndex;
	
	private   final int LAST_ROW_INDEX=8;
	
	private   Connection dbCon;
	
	 {
		taskStartIndex=new int[]{1,4,7,10};
		taskEndIndex=new int[]{3,6,9,10};
	}
	
	public static void main(String[] args) throws Exception {
		
		process();

	}
	
	public static void process() throws EncryptedDocumentException, DBError, InvalidFormatException, IOException, SQLException {
		
		TaskScheduleMaster m= new TaskScheduleMaster();
		
		try {
			m.processNow();
			} finally { 
				
				if(m.dbCon!=null) { try { m.dbCon.close(); } catch (Exception e) { }
				}
			}
		
	}
	
	private void processNow() throws DBError, IOException, EncryptedDocumentException, InvalidFormatException, SQLException {
		System.out.println("--- process started");
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/taskschedule_insertMasterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/taskschedule_issueData.log")));
		
		dbCon=DBUtil.getConnection();
		
		//initDBQueryLogger();
		
		
		Workbook workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));

		worksheet = workbook.getSheet(Constant.SHEET_NAME_TASK_SCHEDULE);
		
		formulaEval = workbook.getCreationHelper().createFormulaEvaluator();

		try {
			
			dbCon.setAutoCommit(false);
			
			populateTaskScheduleMaster();

			dbCon.commit();
			
		} catch(Exception e){
			
			e.printStackTrace();
			
			dbCon.rollback();
		}
		
		dbCon.setAutoCommit(true);
		
		releaseResources();
		
		System.out.println("--- process ficished");
	}
	
	
	//pstmtCTS= Create Task Schedule master
	private   PreparedStatement pstmtCTS;
	
	
	
	//pstmtGTSST= get Task SUB SUB Type Id by Title;
	private   PreparedStatement pstmtGTSST;

	private   void populateTaskScheduleMaster() throws Exception {
		
		pstmtGTSST=dbCon.prepareStatement(" select id, tst_id from task_sub_sub_types where title=? and status in(1,0) ");
		pstmtCTS=dbCon.prepareStatement(" insert into task_schedule_masters(user_day, task_no, task_st, task_sst, task_alt_seq, task_alt_subseq, status) values(?, ?, ?, ?, ?, ?, 1) ");
		
		
		Iterator<Row> rowIterator = worksheet.rowIterator();
		
		
		ResultSet rsGTSST=null;
		
		while (rowIterator.hasNext()) {
			
			Row row = rowIterator.next();
			
			if(row==null) {
				
				System.out.println("Row found null, exiting the Process...");
				
				break;
			}
			
			if(row.getRowNum()>=LAST_ROW_INDEX) {
				
				System.out.println("End Of Row...");
				
				break;
			}
			
			outputLogger.println("Row No [ " + row.getRowNum() + " ] process started.");
			
			if(row.getRowNum()==0) {
				//skip the header
//				System.out.println("skip the header ...");
				outputLogger.println("Row No [ " + row.getRowNum() + " ] Header skiping...");
				continue;
			}
			
			
			String dataInCell= getCellValueAsString(row.getCell(dayIndex));
	
			//Task day
			int taskDay= Double.valueOf(dataInCell).intValue();		
			
			//read through the tasks 
			
			for(int taskNo=0; taskNo<taskStartIndex.length; taskNo++) {
				
				int taskAlt=0;
				
				for(int cellIndex=taskStartIndex[taskNo];  cellIndex<=taskEndIndex[taskNo]; cellIndex++) {
					
					dataInCell= getCellValueAsString(row.getCell(cellIndex));
					
					if(dataInCell==null || dataInCell.trim().equals("")) {
						outputLogger.println("Row No [ " + row.getRowNum() + " ] No entry for Task Sub Sub Type in the cell.");
						continue;
					} else {
						outputLogger.println("Row No [ " + row.getRowNum() + " ] found data[" + dataInCell + "] and process started");
					}
					
					taskAlt=taskAlt+1;
					
					String[] tsstArr=dataInCell.split(",");
					boolean noSubSequence= tsstArr.length==1;
					
					for(int subSeq=0; subSeq<tsstArr.length; subSeq++) {
						
						pstmtGTSST.setString(1, tsstArr[subSeq].trim());
						
						try {
							rsGTSST=pstmtGTSST.executeQuery();
							
							if(rsGTSST==null || !rsGTSST.next()) {
								inputFileIssuesLogger.println("Row No [ " + row.getRowNum() + " ] There is no entry for Sub-Sub-Type [ "+tsstArr[subSeq]+" ] in the Table task_sub_sub_types");
								throw new Exception("There is no entry for Sub-Sub-Type [ "+tsstArr[subSeq]+" ] in the Table task_sub_sub_types");
							}
							
							//id, tst_id 
							
							int tsstId=rsGTSST.getInt("id");
							int tstId=rsGTSST.getInt("tst_id");
							
							
							//user_day, task_no, task_st, task_sst, task_alt_seq, task_alt_subseq
							int i=0;
							
							pstmtCTS.setInt(++i, taskDay);
							pstmtCTS.setInt(++i, taskNo+1);
							pstmtCTS.setInt(++i, tstId);
							pstmtCTS.setInt(++i, tsstId);
							pstmtCTS.setInt(++i, taskAlt);
							
							if(noSubSequence) {
								pstmtCTS.setInt(++i, 0);
							}else {
								pstmtCTS.setInt(++i, subSeq+1);
							}
							
							outputLogger.println("Row No [ " + row.getRowNum() + " ] found data[" + dataInCell + "] inserting in to schedule table");
							
							pstmtCTS.executeUpdate();
							
						} finally {
							if(rsGTSST!=null) { try { rsGTSST.close(); } catch (Exception e) { } }
						}
						
					}
					
				}
				
			}
			
			
		}
			
		
	}
	
	private   FormulaEvaluator formulaEval ;
	
	private   String getCellValueAsString(Cell cell) {
		
//		System.out.println("getCellValueAsString: Cell["+cell.getColumnIndex()+"], DataType["+cell.getCellTypeEnum().name()+"] ");
		
		switch(cell.getCellTypeEnum()) {
		
			case BOOLEAN:{
				
				return String.valueOf(cell.getBooleanCellValue());
			}
			
			case BLANK:{
				return null;
			}
			
			case ERROR:{
				return null;
			}
			
			case FORMULA:{
				return formulaEval.evaluate(cell).formatAsString();
			}
			
			case NUMERIC:{
				return String.valueOf( cell.getNumericCellValue() );
			}
			
			case STRING:{
				return cell.getStringCellValue();
			}
			
			case _NONE:
			default:
			{
				return null;
			}
			
		
		}
		
	}
	
	private   void releaseResources() {
		
		/*try {
			//writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/
		try { dbCon.close(); } catch (Exception e) { e.printStackTrace(); }
	}

	private   void initDBQueryLogger() throws IOException {
		
		File file=new File("output/QueriesLogMaster3.txt");
		
		if(!file.exists()) {
			file.createNewFile();
		}
		
		//writer = new FileWriter("output/QueriesLogMaster3.txt", true);

	}


}
