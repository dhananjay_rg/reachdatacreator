package com.reach.injector.visioning_task;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.injector.constant.Constant.ProcessStatus;
import com.reach.util.DBUtil;

public class InsertBarrierHabitMapData {

	private final Integer STATUS_ACTIVE = 1;
	private final Integer DEFAULT_SEQUENCE = 1;

	private  Workbook workbook = null;

	private   Connection conn = null;

	private   PrintStream outputLogger = null;
	private   PrintStream inputFileIssuesLogger = null;
	private   PrintStream sqlQueriesLogger = null;

	private   HashMap<String, Integer> habits = null;
	private   HashMap<String, Integer> barriers = null;
	private   ArrayList<String> habitsBarriersMapList = null;

	public static void main(String[] args) throws IOException, InvalidFormatException, SQLException, EncryptedDocumentException, DBError {
		processBarrierHabitMapData();
	}
	
	public static void processBarrierHabitMapData()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		
		InsertBarrierHabitMapData m=new InsertBarrierHabitMapData();
		
		
		try {
			m.processBarrierHabitMapDataNow();
			} finally { 
				
				if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
				}
			}
		
	}

	private void processBarrierHabitMapDataNow()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		init();
		loadHabits();
		loadBarriers();
		loadHabitBarrierMap();
		checkOldRecords();
		updateHabitBarrierMap();
		
	}

	private   void init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		// loadSheets();

		conn = DBUtil.getConnection();
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/insertVisionBarrierHabitMapData.log")));
		inputFileIssuesLogger = new PrintStream(
				new FileOutputStream(new File("logs/VisionBarrierHabitMapDataIssues.log")));
		sqlQueriesLogger = new PrintStream(new FileOutputStream(new File("logs/sqlQueriesMasterData.log")));

		habits = new HashMap<String, Integer>();
		barriers = new HashMap<String, Integer>();
		habitsBarriersMapList = new ArrayList<String>();
	}

	private   void loadHabits() throws SQLException, IOException {
		outputLogger.println("Loading Habits from database....");
		Statement stmt = conn.createStatement();
		String selectVisionsQuery = "select id, cs_habit_id from habits where status=" + STATUS_ACTIVE + " and cs_habit_id is not null";
		ResultSet rs = stmt.executeQuery(selectVisionsQuery);
		if (rs != null) {
			while (rs.next()) {
				int habitId = rs.getInt("id");
				String csHabitId = rs.getString("cs_habit_id");
				if (habits.containsKey(csHabitId)) {
					throw new IOException("Duplicate Habit[ " + csHabitId + " ] in Habits table");
				} else {
					habits.put(csHabitId, habitId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + habits.size() + " Habits in database....");
		outputLogger.println("===============");
	}

	private   void loadBarriers() throws SQLException, IOException {
		outputLogger.println("Loading Barriers from database....");
		Statement stmt = conn.createStatement();
		String selectBarriersQuery = "select b.id, b.cs_barrier_id from barriers b where b.status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectBarriersQuery);
		if (rs != null) {
			while (rs.next()) {
				int barrierId = rs.getInt("id");
				String csBarrierId = rs.getString("cs_barrier_id");
				if (barriers.containsKey(csBarrierId)) {
					throw new IOException("Duplicate Entry [ " + csBarrierId + " ] in Barriers table");
				} else {
					barriers.put(csBarrierId, barrierId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + barriers.size() + " Barriers in database....");
		outputLogger.println("===============");
	}

	private   void loadHabitBarrierMap() throws SQLException, IOException {
		outputLogger.println("Loading Habits-Barriers-Map from database....");
		Statement stmt = conn.createStatement();
		String selectBarriersQuery = "select habit_id, barrier_id from habit_barrier_map where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectBarriersQuery);
		habitsBarriersMapList.clear();
		if (rs != null) {
			while (rs.next()) {
				int habitId = rs.getInt("habit_id");
				int barrierId = rs.getInt("barrier_id");

				if (habitsBarriersMapList.contains(habitId + "~" + barrierId)) {
					throw new IOException(
							"Duplicate Entry [ " + habitId + "," + barrierId + " ] in Habits-Barriers-Map table");
				} else {
					habitsBarriersMapList.add(habitId + "~" + barrierId);
				}
			}
		}
		rs.close();
		stmt.close();
		System.out.println("Found " + habitsBarriersMapList.size() + " Habits-Barriers-Map in database....");
		outputLogger.println("Found " + habitsBarriersMapList.size() + " Habits-Barriers-Map in database....");
		outputLogger.println("===============");
	}

	private void checkOldRecords() {
		
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_VISION_BARRIER_HABIT_MAP);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int rowNum = 0;
		// Ignore header row
		if (rowIterator.hasNext()) {
			rowIterator.next();
			rowNum++;
		}
		
		ArrayList<String> barrierHabitList = new ArrayList<String>();		
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			rowNum++;
			
			String processStatus = row.getCell(0).getStringCellValue().trim();
			
			if (processStatus.equalsIgnoreCase(ProcessStatus.old)) {
				String barrierIdHabitId = row.getCell(5).getStringCellValue().trim();
				
				if (!barrierHabitList.contains(barrierIdHabitId)) {
					barrierHabitList.add(barrierIdHabitId);
				} else {
					System.out.println( barrierIdHabitId + " already exist");
				}
			}
		}
		
		System.out.println("Total size of barrierHabitList=" + barrierHabitList.size());
		
		// habitId + "~" + barrierId
		for(int i=0;i<barrierHabitList.size();i++) {
			String ids[] = barrierHabitList.get(i).split("\\.");
			if(habitsBarriersMapList.contains(habits.get(ids[2].trim()) + "~" + barriers.get(ids[1].trim()))) {
				habitsBarriersMapList.remove(habits.get(ids[2].trim()) + "~" + barriers.get(ids[1].trim()));
			} else {
//				System.out.println(barrierHabitList.get(i) + " not exist in db" );
//				System.out.println("select * from habit_barrier_map where habit_id=" + habits.get(ids[2].trim()) + "  and barrier_id=" + barriers.get(ids[1].trim()));
				
			}
		}
		
		System.out.println("Remain size of habitsBarriersMapList=" + habitsBarriersMapList.size());
		
		if(habitsBarriersMapList.size() > 0) {
			for(int i=0;i<habitsBarriersMapList.size();i++) {
				String ids[] = habitsBarriersMapList.get(i).split("~");
				removeHabitBarrierMapByhabitIdBarrierId(Integer.parseInt(ids[0].trim()), Integer.parseInt(ids[1].trim()));
			}
		}
		
	}
	
	
	private   void updateHabitBarrierMap() throws SQLException, IOException {
		
		loadHabitBarrierMap();
		
		outputLogger.println("Updating Habit-Barriers-Map from input file....");
		PreparedStatement pstmt = conn
				.prepareStatement("insert into habit_barrier_map (habit_id, barrier_id, seq) values (?,?,?)");
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_VISION_BARRIER_HABIT_MAP);
		Iterator<Row> rowIterator = sheet.rowIterator();
		int cnt = 0;
		int insertUpdate = 0;
		int old = 0;
		int delete = 0;
		int error = 0;
		int oldError = 0;
		int duplicateMappingError = 0;
				
		int rowNum = 0;
		// Ignore header row
		if (rowIterator.hasNext()) {
			rowIterator.next();
			rowNum++;
		}

		ArrayList<String> tmpList = new ArrayList<String>();
		ArrayList<String> tmpDuplicateList = new ArrayList<String>();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			rowNum++;
//System.out.println(rowNum+1);
			
			String processStatus = row.getCell(0).getStringCellValue().trim();
			cnt++;
			
			if (processStatus.equalsIgnoreCase(ProcessStatus.insert) || processStatus.equalsIgnoreCase(ProcessStatus.update)) {
			
				String visionIdBarrierIdHabitId = row.getCell(5).getStringCellValue().trim();
				String ids[] = visionIdBarrierIdHabitId.split("\\.");
				int habitId = habits.get(ids[2]);
				int barrierId = barriers.get(ids[1]);
				if (habitId == 0) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + (rowNum + 1)
							+ " ] ==> Habit [ " + row.getCell(6).getStringCellValue().trim()  + " ] not exist in Masterdata.");
					error++;
					continue;
					
				}
				
				if (barrierId == 0) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + (rowNum + 1)
							+ " ] ==> Barrier [ " + row.getCell(4).getStringCellValue().trim() + " ] not exist in Masterdata.");
					error++;
					continue;
				}
				
				pstmt.setInt(1, habitId);
				pstmt.setInt(2, barrierId);
				pstmt.setInt(3, DEFAULT_SEQUENCE);
//				System.out.println(pstmt);
//				pstmt.executeUpdate();
				habitsBarriersMapList.add(habitId + "~" + barrierId);
				insertUpdate++;
				
			} else if (processStatus.equalsIgnoreCase(ProcessStatus.old)) {
				old++;
				String visionIdBarrierIdHabitId = row.getCell(5).getStringCellValue().trim();
				String ids[] = visionIdBarrierIdHabitId.split("\\.");
				int habitId = habits.get(ids[2]);
				int barrierId = barriers.get(ids[1]);
				if (tmpList.contains(habitId + "~" + barrierId)) {
					duplicateMappingError++;
				} else {
					tmpList.add(habitId + "~" + barrierId);
				}
				
				if(tmpDuplicateList.contains(visionIdBarrierIdHabitId)) {
					System.out.println("Warninf: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + (rowNum) + " ], VISION_ID.BARRIER_ID.HABIT_ID="+visionIdBarrierIdHabitId+"  is duplicate");
				} else {
					tmpDuplicateList.add(visionIdBarrierIdHabitId);
				}
				
				
				if(!habitsBarriersMapList.contains(habitId + "~" + barrierId)) { 
					oldError++;
					System.out.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + (rowNum) + " ], VISION_ID.BARRIER_ID.HABIT_ID="+visionIdBarrierIdHabitId+"  not exist in Masterdata.");
				}
			} else if (processStatus.equalsIgnoreCase(ProcessStatus.delete)) {
				delete++;
			}
			
		}
		// }

		pstmt.close();
		outputLogger.println("To be Inserted " + cnt);
		outputLogger.println("ActualInserted " + insertUpdate);
		outputLogger.println("===============");
		
		System.out.println("_____ summary _____");
		System.out.println("Total Records: " + cnt);
		System.out.println("Total InsertUpdate: " + insertUpdate);
		System.out.println("Total old: " + old);
		System.out.println("Total delete: " + delete);
		System.out.println("Total old error: " + oldError);
		System.out.println("Total old duplicate mapping: " + duplicateMappingError);
		
		
		
	}
	
	private void removeHabitBarrierMapByhabitIdBarrierId(int habitId, int barrierId) {
		
		try {
			String stmt = "update habit_barrier_map set status= " + Constant.STATUS_INACTIVE + " where habit_id=? and barrier_id=? and status=" + Constant.STATUS_ACTIVE;
			PreparedStatement ps = conn.prepareStatement(stmt);
			ps.setInt(1, habitId);
			ps.setInt(2, barrierId);
			System.out.println(ps);
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}