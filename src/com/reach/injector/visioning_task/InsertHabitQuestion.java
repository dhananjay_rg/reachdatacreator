package com.reach.injector.visioning_task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reach.err.DBError;
import com.reach.injector.CommonMasterDBQuery;
import com.reach.injector.constant.Constant;
import com.reach.injector.content.vo.IntListDomain;
import com.reach.injector.content.vo.StringListDomain;
import com.reach.injector.profile.InsertProfileMasterData;
import com.reach.util.DBUtil;
import com.reach.util.FloatListDomain;

public class InsertHabitQuestion {

	private final Integer STATUS_ACTIVE = 1;
	
	private PrintStream outputLogger = null;
	private PrintStream inputFileIssuesLogger = null;
	
	private HashMap<String, Integer> hmTaskTypes = null;
	private HashMap<String, Integer> hmTaskSubTypes = null;
	private HashMap<String, Integer> hmTaskSubSubTypes = null;
	private HashMap<String, String> hmReachDataTypes = null;
	private HashMap<String, Integer> hmTaskMasters = null;
	private HashMap<String, String> hmTaskHabitMap = null;
	private HashMap<String,String> hmFieldMap; 
	private HashMap<String, Integer> hmTaskGroup = null;
	
	private List<String> listGender = null;
	private List<String> listGoal = null;
	private List<String> listActivityLevel = null;
	private List<String> listCuisinePref = null;
	private List<String> listFoodPref = null;
	private List<String> listMealPref = null;
	private List<String> listFoodAllergy = null;
	private List<String> listExercisePref = null;
	private List<String> listRateChange = null;
	private List<String> listTrackTask = null;
	
	
	private Connection conn = null;
	
	
	public static void main(String[] args) throws DBError, SQLException, IOException, EncryptedDocumentException, InvalidFormatException {
		process();
		/*init();
		String[] strArray = "Early Morning, Breakfast, Snack, Lunch, Tea, Dinner, Post Dinner".split(",");
		String ret = checkMasterReachDataType(strArray);
		System.out.println(ret);
		*/
	}
	
	private void init() throws DBError, SQLException, IOException, EncryptedDocumentException, InvalidFormatException {
	
		conn = DBUtil.getConnection();
		
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/habitquestion_insertMasterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/habitquestion_issueData.log")));
		 
		hmTaskTypes = new HashMap<String, Integer>();
		hmTaskSubTypes = new HashMap<String, Integer>();
		hmTaskSubSubTypes = new HashMap<String, Integer>();
		hmReachDataTypes = new HashMap<String, String>();
		hmTaskMasters = new HashMap<String, Integer>();
		hmTaskHabitMap = new HashMap<String, String>();
		hmTaskGroup = new HashMap<String, Integer>();
		
		
		hmFieldMap = StaticFieldParams.getStaticFieldMap();
		
		listGender = new ArrayList<String>();
		listGoal = new ArrayList<String>();
		listActivityLevel = new ArrayList<String>();
		listCuisinePref = new ArrayList<String>();
		listFoodPref = new ArrayList<String>();
		listMealPref = new ArrayList<String>();
		listFoodAllergy = new ArrayList<String>();
		listExercisePref = new ArrayList<String>();
		listRateChange = new ArrayList<String>();
		listTrackTask = new ArrayList<String>();
		
		
		initilizeGenrciReachDataType();
		loadTaskTypes();
		loadTaskSubTypes();
		loadTasSubkSubTypes();
		loadReachDataTypes();
		loadTaskMasterData();
		loadTaskHabitMap();
		
	}
	
	private void initilizeGenrciReachDataType() throws EncryptedDocumentException, InvalidFormatException, DBError, SQLException, IOException {
		

		InsertProfileMasterData.instance().init().populateMap();
		
		// Gender
		Set<String> keys = InsertProfileMasterData.hmGernder.keySet();
		Iterator<String> itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listGender.add(key.toLowerCase());
		}
		
		// Goal
		keys = InsertProfileMasterData.hmGoal.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listGoal.add(key.toLowerCase());
		}
		
		// Activity Level
		keys = InsertProfileMasterData.hmActivityLevel.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listActivityLevel.add(key.toLowerCase());
		}
			
		// Cuisine Pref
		keys = InsertProfileMasterData.hmCusinePref.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listCuisinePref.add(key.toLowerCase());
		}
		
		// Food Pref
		keys = InsertProfileMasterData.hmFoodPref.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listFoodPref.add(key.toLowerCase());
		}
			
		// Meal Pref
		keys = InsertProfileMasterData.hmMealPref.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listMealPref.add(key.toLowerCase());
		}
			
		// Food Allergy
		keys = InsertProfileMasterData.hmFoodAllergy.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listFoodAllergy.add(key.toLowerCase());
		}
		
		// Exercise Pref
		keys = InsertProfileMasterData.hmExercisePref.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listExercisePref.add(key.toLowerCase());
		}
		
		// Rate of Change
		keys = InsertProfileMasterData.hmRateChange.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			listRateChange.add(key.toLowerCase());
		}
		
		// Track
		
		listTrackTask.add("yes");
		listTrackTask.add("no");
		
		System.out.println("__ ");
	}
	
	private String checkMasterReachDataType(String[] strArray, String taskType) {		
		String ret = null;
		boolean flag = false;
		int countFount = 0;
			if(!flag) {
				if(strArray.length == listGender.size() && listGender.contains(strArray[0].toLowerCase())) {
//					System.out.println(strArray[0] + " is present in Gender");
					for(int j=0;j<strArray.length;j++) {
						if(listGender.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "Gender";
					}
				}
			}
			
			if(!flag) {
				if(strArray.length == listGoal.size() && listGoal.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in Goal");
					for(int j=0;j<strArray.length;j++) {
						if(listGoal.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "Goal";
					}
				}

			}
		
			if(!flag) {
				if(strArray.length == listActivityLevel.size() && listActivityLevel.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in Activity Level");
					for(int j=0;j<strArray.length;j++) {
						if(listActivityLevel.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "ActivityLevel";
					}
				}

			}
			
			if(!flag) {
				if(strArray.length == listCuisinePref.size() && listCuisinePref.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in Cuisine");
					for(int j=0;j<strArray.length;j++) {
						if(listCuisinePref.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "CuisinePreferences";
					}
				}

			}
			
			if(!flag) {
				if(strArray.length == listFoodPref.size() && listFoodPref.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in Food Pref");
					for(int j=0;j<strArray.length;j++) {
						if(listFoodPref.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "FoodPreferences";
					}
				}

			}
			
			if(!flag) {
				if(strArray.length == listMealPref.size() && listMealPref.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in Meal Pref");
					for(int j=0;j<strArray.length;j++) { 
//						System.out.println(j + " ___ " + strArray[j].toLowerCase() + " -> " + listMealPref.get(j));
						if(listMealPref.contains(strArray[j].toLowerCase())) {
//							System.out.println("... True");
							countFount++;
						} else {
//							System.out.println("... xxx ");
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "MealPreferences";
					}
				}
			}
			
			if(!flag) {
				if(strArray.length == listFoodAllergy.size() && listFoodAllergy.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in Allergy");
					for(int j=0;j<strArray.length;j++) {
						if(listFoodAllergy.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "FoodAllergyOrIntolerance";
					}
				}
			}
			
			if(!flag) {
				if(strArray.length == listExercisePref.size() && listExercisePref.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in Exer Pref");
					for(int j=0;j<strArray.length;j++) {
						if(listExercisePref.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "ExercisePreferences";
					}
				}
			}
			
			if(!flag) {
				if(strArray.length == listRateChange.size() && listRateChange.contains(strArray[0].toLowerCase())) {
					System.out.println(strArray[0] + " is present in RateChange");
					for(int j=0;j<strArray.length;j++) {
						if(listRateChange.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "RateOfChange";
					}
				}
			}
			
			if(!flag && taskType.equalsIgnoreCase("Track")) {
//				System.out.println("__ taskType=" + taskType + " -" + strArray[0] + "-"+ strArray[1]);
				if(strArray.length == listTrackTask.size() && listTrackTask.contains(strArray[0].toLowerCase())) {
//					System.out.println(strArray[0] + " is present in Track");
					for(int j=0;j<strArray.length;j++) {
						if(listTrackTask.contains(strArray[j].toLowerCase())) {
							countFount++;
						}
					}
					if(countFount == strArray.length) {
						flag = true;
						ret = "Track";
					}
				}
			}
			
			return ret;
	}
	
	
	private void loadTaskTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskTypes from database....");
		Statement stmt = conn.createStatement();
		String selectTaskTypesQuery = "select * from task_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int taskTypeId = rs.getInt("id");
				String taskType = rs.getString("title");
//				System.out.println("taskType=" + taskType);
				if (hmTaskTypes.containsKey(taskType)) {
					throw new IOException("Duplicate Entry  AS integer[ " + taskType + " ] in TaskTypes table");
				} else {
					hmTaskTypes.put(taskType, taskTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskTypes.size() + " TaskTypes in database....");
		outputLogger.println("===============");
	}
	
	private void loadTaskSubTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskSubTypes from database....");
		Statement stmt = conn.createStatement();
		String selectTaskSubTypesQuery = "select * from task_sub_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskSubTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int taskTypeId = rs.getInt("id");
				String taskType = rs.getString("title");
				if (hmTaskSubTypes.containsKey(taskType)) {
					throw new IOException("Duplicate Entry  AS integer[ " + taskType + " ] in TaskSubTypes table");
				} else {
					hmTaskSubTypes.put(taskType, taskTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskSubTypes.size() + " TaskSubTypes in database....");
		outputLogger.println("===============");
	}
	
	private void loadTasSubkSubTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskSubSubTypes from database....");
		Statement stmt = conn.createStatement();
		String selectVisionTypesQuery = "select * from task_sub_sub_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectVisionTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				if (hmTaskSubSubTypes.containsKey(title)) {
					throw new IOException("Duplicate Entry  AS integer[ " + title + " ] in TaskSubSubTypes table");
				} else { 
					hmTaskSubSubTypes.put(title, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskSubSubTypes.size() + " TaskSubSubTypes in database....");
		outputLogger.println("===============");
	}
	
	
	private void loadReachDataTypes() throws SQLException, IOException {
		outputLogger.println("Loading Reach Data Types from database....");
		Statement stmt = conn.createStatement();
		String selectTaskTypesQuery = "select * from reach_data_types";
		ResultSet rs = stmt.executeQuery(selectTaskTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				String code = rs.getString("type_code");
				String data_domain = rs.getString("data_domain");
				hmReachDataTypes.put(code, data_domain);
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmReachDataTypes.size() + " Reach Data Types in database....");
		outputLogger.println("===============");
	}
	
	private void loadTaskMasterData() throws SQLException, IOException {
		outputLogger.println("Loading Task Master from database....");
		Statement stmt = conn.createStatement();
		String selectTaskTypesQuery = "select * from task_masters where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				int taskGroupId = rs.getInt("task_group_id");
				if(!hmTaskMasters.containsKey(title)) {
					hmTaskMasters.put(title, id);
				}
				if(!hmTaskGroup.containsKey(title)) {
					hmTaskGroup.put(title, taskGroupId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskMasters.size() + " Task Master in database....");
		outputLogger.println("===============");
	}
	
	private void loadTaskHabitMap() throws SQLException, IOException {
		outputLogger.println("Loading Task Habit Map from database....");
		Statement stmt = conn.createStatement();
		String selectTaskTypesQuery = "select * from task_habit_map where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				String taskId = rs.getString("task_id");
				String habitId = rs.getString("habit_id");
				String groupType = rs.getString("group_type");
				hmTaskHabitMap.put(taskId + "-" + habitId , groupType);
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskHabitMap.size() + " Task Habit Map in database....");
		outputLogger.println("===============");
	}
	
	public static void process() throws EncryptedDocumentException, DBError, InvalidFormatException, SQLException, IOException {
		InsertHabitQuestion m=new InsertHabitQuestion();
		
		
		try {
			m.processNow();
			} finally { 
				
				if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
				}
			}
		
	}
	
	
	public void processNow() throws DBError, SQLException, IOException, EncryptedDocumentException, InvalidFormatException {
		System.out.println("--- process started");
		
		
		init();
		
		// TODO Auto-generated method stub
		Row row = null;
		int habitid = 0;

		Time time_start = null;
		Time time_end = null;
		try {
			
			SimpleDateFormat format = new SimpleDateFormat("HH.mm.ss");
			java.util.Date benchmarkStartDate = (java.util.Date) format.parse("11.0.0");
			java.util.Date benchmarkEndDate = (java.util.Date) format.parse("15.59.59");

			java.util.Date trackingStartDate = (java.util.Date) format.parse("11.0.0");
			java.util.Date trackingEndDate = (java.util.Date) format.parse("15.59.59");

			Workbook workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
			Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_HABIT);
			Iterator<Row> rowIterator = sheet.rowIterator();
			
			int tt_id_b = 0, tst_id_b = 0, tsst_id_b = 0;

			Iterator<Row> rowIteratorforvision = sheet.rowIterator();
			int count = 0;

			
			if (rowIteratorforvision.hasNext())
				rowIteratorforvision.next();
				while (rowIteratorforvision.hasNext()) {
					count++;
					int taskGroupId = 0;
					if(count == 1) {
						continue;
					} 
					
					String bundleTaskIds = "";
					int bundleTaskDuration = 0;
					
					String benchmarkq1 = "", benchmarkq2 = null, benchmarkq3 = null, assessmentq1 = null,
							assessmentq2 = null, assessmentq3 = null, trackingq = null;
					String benchmarkans1 = null, benchmarkans2 = null, benchmarkans3 = null, assessmentans1 = null,
							assessmentans2 = null, assessmentans3 = null, trackingans = null;
	
					String benchmarkans1input = null, benchmarkans2input = null, benchmarkans3input = null,
							assessmentans1input = null, assessmentans2input = null, assessmentans3input = null,
							trackingansinput = null;
					String asses1UinOptions = "", asses2UinOptions = "", asses3UinOptions = "", bench1UinOptions = "",
							bench2UinOptions = "", bench3UinOptions = "", trackingUinOptions = "";
					String habit = null;
					String data_domain = "";
	
					try {
						row = rowIteratorforvision.next();
						habit = row.getCell(Constant.HabitColumnsIndex.HABIT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
						if (habit != null && !habit.equals("")) {
							habitid = CommonMasterDBQuery.GetTaskIDFromHabits(habit);
						}
					} catch (Exception e7) {
						// TODO Auto-generated catch block
						e7.printStackTrace();
					}
					if (row.getRowNum() != 0 && habit != null && habitid != 0) {
						
						try {
							tt_id_b = hmTaskTypes.get("Assessment");
							tst_id_b = hmTaskSubTypes.get("Assessment/(Habit)/(HabitBenchmarkQuestion)");
							tsst_id_b = hmTaskSubSubTypes.get("Assessment/(Habit)/(HabitBenchmarkQuestion)");
							
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null 
									&& !row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_COLUMN_INDEX)
									.getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkq1 = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_COLUMN_INDEX).getStringCellValue();
							}
							
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null 
									&& !row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkans1 = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue();
							}
							
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkans1input = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue();
							}
							
							try {
								if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null
										&& !row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
									if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
											.equalsIgnoreCase("User Input (Single Answer From Options)")) {
										bench1UinOptions = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
									} else if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
											.equalsIgnoreCase("User Input (Multiple Answers From Options)")) {
										bench1UinOptions = Constant.ANSWER_TYPE_MULTIPLE_OPTIONS;
									} else {
										bench1UinOptions = Constant.ANSWER_TYPE_TYPE_INPUT;
									}
	
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							if (benchmarkq1 != null && !benchmarkq1.equals("")) {
								String[] benchmarks = null;
								if (benchmarkans1.contains(" or more")) {
									benchmarkans1 = benchmarkans1.replace(" or more", "");
								}
								if (benchmarkans1.contains(",")) {
									benchmarkans1 = benchmarkans1.replaceAll(", ", ",");
									benchmarks = benchmarkans1.split(",");
								}
	
								if (benchmarkans1input.contains("Number")) {
									
									if(benchmarkans1.contains(".")) {
										FloatListDomain floatDomain = new FloatListDomain();
										floatDomain.data = new ArrayList<>();
										for (int i = 0; i < benchmarks.length; i++) {
											floatDomain.data.add(Float.parseFloat(benchmarks[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(floatDomain);
									} else {
										IntListDomain intDomain = new IntListDomain();
										intDomain.data = new ArrayList<>();
										for (int i = 0; i < benchmarks.length; i++) {
											intDomain.data.add(Integer.parseInt(benchmarks[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(intDomain);
									}
									
								} else if (benchmarkans1input.contains("Text")) {
									
									if(benchmarks == null ||  benchmarks.length<=0) {
										inputFileIssuesLogger.println("Error: --> row Number[" + row.getRowNum() + "], Habit[" + habit + "], Bencgmark Que -1 [ " + benchmarkq1 + " ],  options list is not defined.");
										continue;
									}
									
									StringListDomain stringDomain = new StringListDomain();
									stringDomain.data = new ArrayList<>();
									for (int i = 0; i < benchmarks.length; i++) {
										stringDomain.data.add(benchmarks[i].trim());
	
									}
									data_domain = new ObjectMapper().writeValueAsString(stringDomain);
								}
	
								outputLogger.println("row Number[" + row.getRowNum() + "], Got Benchmark Que -1 [ " + benchmarkq1 + " ] ");
								
								String typeCode  = "H" + habitid + "B1";
//								System.out.println("____benchmarkans1= " + benchmarkans1);
								String ret = checkMasterReachDataType(benchmarks, "Assessment");
//								System.out.println("ret= " + ret);
								
								if(ret == null) {
									if(!hmReachDataTypes.containsKey(typeCode)) {
										CommonMasterDBQuery.InsertReachDataTyrpeForManual(typeCode, "", data_domain);
									}
								} else {
									typeCode = ret;
								}
								
								int taskId = 0;
								
								
								if (benchmarkq1.contains("<") && benchmarkq1.contains(">")) {
									String str = benchmarkq1.substring(benchmarkq1.indexOf("<"), benchmarkq1.indexOf(">")+1);
//									System.out.println("str="  +str);
									if(hmFieldMap.containsKey(str.toLowerCase())) {
										benchmarkq1 = benchmarkq1.substring(0,benchmarkq1.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + benchmarkq1.substring(benchmarkq1.indexOf(">")+1,benchmarkq1.length());
									}
								}
//								System.out.println("____ " + hmTaskMasters.containsKey(benchmarkq1) + " -- " + benchmarkq1);
								if(!hmTaskMasters.containsKey(benchmarkq1)) {
									time_start = new java.sql.Time(benchmarkStartDate.getTime());
									time_end = new java.sql.Time(benchmarkEndDate.getTime());
									taskGroupId = 0;
									taskId = CommonMasterDBQuery.InsertTaskMaster(benchmarkq1, null, taskGroupId, tt_id_b, tst_id_b, tsst_id_b, 0, Constant.TASK_REAPEATABLE_FALSE, Constant.DEFAULT_BECNCHMARK_TASK_FREQUENCY, Constant.DEFAULT_BECNCHMARK_TASK_DURATION,
											time_start, time_end, Constant.DEFAULT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, bench1UinOptions,
											typeCode, Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_FALSE, "", Constant.STATUS_ACTIVE, null);
									
									taskGroupId = taskId;
									hmTaskMasters.put(benchmarkq1, taskId);
									hmTaskGroup.put(benchmarkq1, taskId);
								} else {
									taskId = hmTaskMasters.get(benchmarkq1);
									taskGroupId = hmTaskGroup.get(benchmarkq1);
								}
								if(bundleTaskIds.equals("")) {
									bundleTaskIds = "" + taskId;
								} else {
									bundleTaskIds = bundleTaskIds + "," + taskId;
								}
								bundleTaskDuration = bundleTaskDuration + Constant.DEFAULT_BECNCHMARK_TASK_DURATION;
//								if(!hmTaskHabitMap.containsKey(taskId+"-"+habitid)) {
//									CommonMasterDBQuery.InsertTaskHabitMapping(taskId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_QUES);
//								}
								
							}
						} catch (Exception e6) {
							e6.printStackTrace();
							System.out.println("Error at rowe number["+row.getRowNum()+"], habit[" + habit + "], Benchmark Q-1 -> taskTypeId["+tt_id_b+"], taskSubTypeId["+tst_id_b+"], taskSubSubTypeId["+tsst_id_b+"]");
						}
	
						try {
	
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkq2 = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_COLUMN_INDEX).getStringCellValue();
							}
							
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkans2 = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_ANS_OPTNS_COLUMN_INDEX).getStringCellValue();
							}
							
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkans2input = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_INPUT_COLUMN_INDEX).getStringCellValue();
							}
	
							try {
								if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Single Answer From Options)")) {
									bench2UinOptions = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Multiple Answers From Options)")) {
									bench2UinOptions = Constant.ANSWER_TYPE_MULTIPLE_OPTIONS;
								} else {
									bench2UinOptions = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
							if (benchmarkq2 != null && !benchmarkq2.equals("")) {
	
								String[] benchmarks = null;
								if (benchmarkans2.contains(" or more")) {
									benchmarkans2 = benchmarkans2.replace(" or more", "");
								}
								if (benchmarkans2.contains(",")) {
									benchmarkans2 = benchmarkans2.replaceAll(", ", ",");
									benchmarks = benchmarkans2.split(",");
								}
	
								if (benchmarkans2input.contains("Number")) {
									
									if(benchmarkans2.contains(".")) {
										FloatListDomain floatDomain = new FloatListDomain();
										floatDomain.data = new ArrayList<>();
										for (int i = 0; i < benchmarks.length; i++) {
											floatDomain.data.add(Float.parseFloat(benchmarks[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(floatDomain);
									} else {
										IntListDomain intDomain = new IntListDomain();
										intDomain.data = new ArrayList<>();
										for (int i = 0; i < benchmarks.length; i++) {
											intDomain.data.add(Integer.parseInt(benchmarks[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(intDomain);
									}
									
								} else if (benchmarkans2input.contains("Text")) {
									
									if(benchmarks == null || benchmarks.length<=0) {
										inputFileIssuesLogger.println("Error: --> row Number[" + row.getRowNum() + "], Habit[" + habit + "], Bencgmark Que -2 [ " + benchmarkq2 + " ],  options list is not defined.");
										continue;
									}
									
									StringListDomain stringDomain = new StringListDomain();
									stringDomain.data = new ArrayList<>();
									for (int i = 0; i < benchmarks.length; i++) {
										stringDomain.data.add(benchmarks[i].trim());
	
									}
									data_domain = new ObjectMapper().writeValueAsString(stringDomain);
								}
								
								outputLogger.println("row Number[" + row.getRowNum() + "], Got Benchmark Que -2 [ " + benchmarkq2 + " ] ");
								
								String typeCode  = "H" + habitid + "B2";
								String ret = checkMasterReachDataType(benchmarks, "Assessment");
								if(ret == null) {
									if(!hmReachDataTypes.containsKey(typeCode)) {
										CommonMasterDBQuery.InsertReachDataTyrpeForManual(typeCode, "", data_domain);
									}
								} else {
									typeCode = ret;
								}
								
								int taskId = 0;
								if (benchmarkq2.contains("<") && benchmarkq2.contains(">")) {
									String str = benchmarkq2.substring(benchmarkq2.indexOf("<"), benchmarkq2.indexOf(">")+1);
//									System.out.println("str="  +str);
									if(hmFieldMap.containsKey(str.toLowerCase())) {
										benchmarkq2 = benchmarkq2.substring(0,benchmarkq2.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + 
												benchmarkq2.substring(benchmarkq2.indexOf(">")+1,benchmarkq2.length());
									}
								}
								if(!hmTaskMasters.containsKey(benchmarkq2)) {
									time_start = new java.sql.Time(benchmarkStartDate.getTime());
									time_end = new java.sql.Time(benchmarkEndDate.getTime());
									taskGroupId = 0;
									taskId = CommonMasterDBQuery.InsertTaskMaster(benchmarkq2, null, taskGroupId, tt_id_b, tst_id_b, tsst_id_b, 0, Constant.TASK_REAPEATABLE_FALSE, Constant.DEFAULT_BECNCHMARK_TASK_FREQUENCY, Constant.DEFAULT_BECNCHMARK_TASK_DURATION,
											time_start, time_end, Constant.DEFAULT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, bench2UinOptions,
											typeCode, Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_FALSE, "", Constant.STATUS_ACTIVE, null);
									taskGroupId = taskId;
									hmTaskMasters.put(benchmarkq2, taskId);
									hmTaskGroup.put(benchmarkq2, taskId);
								} else {
									taskId = hmTaskMasters.get(benchmarkq2);
									taskGroupId = hmTaskGroup.get(benchmarkq2);
								}
								if(bundleTaskIds.equals("")) {
									bundleTaskIds = "" + taskId;
								} else {
									bundleTaskIds = bundleTaskIds + "," + taskId;
								}
								bundleTaskDuration = bundleTaskDuration + Constant.DEFAULT_BECNCHMARK_TASK_DURATION;
//								if(!hmTaskHabitMap.containsKey(taskId+"-"+habitid)) {
//									CommonMasterDBQuery.InsertTaskHabitMapping(taskId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_QUES);
//								}
							}
						} catch (Exception e5) {
							e5.printStackTrace();
							System.out.println("Error at rowe number["+row.getRowNum()+"], habit[" + habit + "], Benchmark Q-2 -> taskTypeId["+tt_id_b+"], taskSubTypeId["+tst_id_b+"], taskSubSubTypeId["+tsst_id_b+"]");
						}
	
						try {
	
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null 
									&& !row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkq3 = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkans3 = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_ANS_OPTNS_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								benchmarkans3input = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_INPUT_COLUMN_INDEX).getStringCellValue();
							}
	
							try {
								if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Single Answer From Options)")) {
									bench3UinOptions = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Multiple Answers From Options)")) {
									bench3UinOptions = Constant.ANSWER_TYPE_MULTIPLE_OPTIONS;
								} else {
									bench3UinOptions = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (benchmarkq3 != null && !benchmarkq3.equals("")) {
								String[] benchmarks = null;
								if (benchmarkans3.contains(" or more")) {
									benchmarkans3 = benchmarkans3.replace(" or more", "");
								}
								if (benchmarkans3.contains(",")) {
									benchmarkans3 = benchmarkans3.replaceAll(", ", ",");
									benchmarks = benchmarkans3.split(",");
								}
								if (benchmarkans3input.contains("Number")) {
									
									if(benchmarkans3.contains(".")) {
										FloatListDomain floatDomain = new FloatListDomain();
										floatDomain.data = new ArrayList<>();
										for (int i = 0; i < benchmarks.length; i++) {
											floatDomain.data.add(Float.parseFloat(benchmarks[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(floatDomain);
									} else {
										IntListDomain intDomain = new IntListDomain();
										intDomain.data = new ArrayList<>();
										for (int i = 0; i < benchmarks.length; i++) {
											intDomain.data.add(Integer.parseInt(benchmarks[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(intDomain);
									}
	
								} else if (benchmarkans3input.contains("Text")) {
									
									if(benchmarks == null || benchmarks.length<=0) {
										inputFileIssuesLogger.println("Error: --> row Number[" + row.getRowNum() + "], Habit[" + habit + "], Bencgmark Que -3 [ " + benchmarkq3 + " ],  options list is not defined.");
										continue;
									}
									
									StringListDomain stringDomain = new StringListDomain();
									stringDomain.data = new ArrayList<>();
									for (int i = 0; i < benchmarks.length; i++) {
										stringDomain.data.add(benchmarks[i].trim());
	
									}
									data_domain = new ObjectMapper().writeValueAsString(stringDomain);
								}
								
								outputLogger.println("row Number[" + row.getRowNum() + "], Got Benchmark Que -3 [ " + benchmarkq3 + " ] ");
								
								String typeCode  = "H" + habitid + "B3";
								String ret = checkMasterReachDataType(benchmarks, "Assessment");
								if(ret == null) {
									if(!hmReachDataTypes.containsKey(typeCode)) {
										CommonMasterDBQuery.InsertReachDataTyrpeForManual(typeCode, "", data_domain);
									}
								} else {
									typeCode = ret;
								}
								
								int taskId = 0;
								if (benchmarkq3.contains("<") && benchmarkq3.contains(">")) {
									String str = benchmarkq3.substring(benchmarkq3.indexOf("<"), benchmarkq3.indexOf(">")+1);
//									System.out.println("str="  +str);
									if(hmFieldMap.containsKey(str.toLowerCase())) {
										benchmarkq3 = benchmarkq3.substring(0,benchmarkq3.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + 
												benchmarkq3.substring(benchmarkq3.indexOf(">")+1,benchmarkq3.length());
									}
								}
								if(!hmTaskMasters.containsKey(benchmarkq3)) {
									time_start = new java.sql.Time(benchmarkStartDate.getTime());
									time_end = new java.sql.Time(benchmarkEndDate.getTime());
									taskGroupId = 0;
									taskId = CommonMasterDBQuery.InsertTaskMaster(benchmarkq3, null, taskGroupId, tt_id_b, tst_id_b, tsst_id_b, 0, Constant.TASK_REAPEATABLE_FALSE, Constant.DEFAULT_BECNCHMARK_TASK_FREQUENCY, Constant.DEFAULT_BECNCHMARK_TASK_DURATION,
											time_start, time_end, Constant.DEFAULT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, bench3UinOptions,
											typeCode, Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_FALSE, "", Constant.STATUS_ACTIVE, null);
									
									taskGroupId = taskId;
									hmTaskMasters.put(benchmarkq3, taskId);
									hmTaskGroup.put(benchmarkq3, taskId);
								} else {
									taskId = hmTaskMasters.get(benchmarkq3);
									taskGroupId = hmTaskGroup.get(benchmarkq3);
								}
								if(bundleTaskIds.equals("")) {
									bundleTaskIds = "" + taskId;
								} else {
									bundleTaskIds = bundleTaskIds + "," + taskId;
								}
								bundleTaskDuration = bundleTaskDuration + Constant.DEFAULT_BECNCHMARK_TASK_DURATION;
//								if(!hmTaskHabitMap.containsKey(taskId+"-"+habitid)) {
//									CommonMasterDBQuery.InsertTaskHabitMapping(taskId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_QUES);
//								}
//	
							}
						} catch (Exception e4) {
							// TODO Auto-generated catch block
							e4.printStackTrace();
							System.out.println("Error at rowe number["+row.getRowNum()+"], habit[" + habit + "], Benchmark Q-3 -> taskTypeId["+tt_id_b+"], taskSubTypeId["+tst_id_b+"], taskSubSubTypeId["+tsst_id_b+"]");
						}
	
						
						// insert bundle task
						if(bundleTaskIds != null && !bundleTaskIds.equals("")) {
							int taskBundleId = CommonMasterDBQuery.InsertTaskMaster(Constant.BENCHMARK_BUNDLE_TASK_TITLE, null, 0, tt_id_b, tst_id_b, tsst_id_b, 0, Constant.TASK_REAPEATABLE_FALSE, Constant.DEFAULT_BECNCHMARK_BUNDLE_TASK_FREQUENCY, bundleTaskDuration,
									time_start, time_end, Constant.DEFAULT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, "",
									"", Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_TRUE, bundleTaskIds, Constant.STATUS_ACTIVE, null);
							CommonMasterDBQuery.InsertTaskHabitMapping(taskBundleId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_QUES);
						}
						
						
						
						try {
							tt_id_b = hmTaskTypes.get("Assessment");
							tst_id_b = hmTaskSubTypes.get("Assessment/(Habit)/(HabitProgressAssessmentQuestion)");
							tsst_id_b = hmTaskSubSubTypes.get("Assessment/(Habit)/(HabitProgressAssessmentQuestion)");
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null 
									&& !row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentq1 = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentans1 = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentans1input = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue();
							}
	
							try {
								if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Single Answer From Options)")) {
									asses1UinOptions = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Multiple Answers From Options)")) {
									asses1UinOptions = Constant.ANSWER_TYPE_MULTIPLE_OPTIONS;
								} else {
									asses1UinOptions = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	
							if (assessmentq1 != null && !assessmentq1.equals("")) {
								String[] assessmentArray = null;
								if (assessmentans1.contains(" or more")) {
									assessmentans1 = assessmentans1.replace(" or more", "");
								}
								if (assessmentans1.contains(",")) {
									assessmentans1 = assessmentans1.replaceAll(", ", ",");
									assessmentArray = assessmentans1.split(",");
								}
								if (assessmentans1input.contains("Number")) {
									
									if(assessmentans1.contains(".")) {
										FloatListDomain floatDomain = new FloatListDomain();
										floatDomain.data = new ArrayList<>();
										for (int i = 0; i < assessmentArray.length; i++) {
											floatDomain.data.add(Float.parseFloat(assessmentArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(floatDomain);
									} else {
										IntListDomain intDomain = new IntListDomain();
										intDomain.data = new ArrayList<>();
										for (int i = 0; i < assessmentArray.length; i++) {
											intDomain.data.add(Integer.parseInt(assessmentArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(intDomain);
									}
	
								} else if (assessmentans1input.contains("Text")) {
									
									if(assessmentArray == null || assessmentArray.length<=0) {
										inputFileIssuesLogger.println("Error: --> row Number[" + row.getRowNum() + "], Habit[" + habit + "], Assessment Que -1 [ " + assessmentq1 + " ],  options list is not defined.");
										continue;
									}
									
									StringListDomain stringDomain = new StringListDomain();
									stringDomain.data = new ArrayList<>();
									for (int i = 0; i < assessmentArray.length; i++) {
										stringDomain.data.add(assessmentArray[i].trim());
									}
									data_domain = new ObjectMapper().writeValueAsString(stringDomain);
								}
	
								outputLogger.println("row Number[" + row.getRowNum() + "], Got Assessment Que -1 [ " + assessmentq1 + " ] & processing..");
								
								String typeCode  = "H" + habitid + "A1";
								String ret = checkMasterReachDataType(assessmentArray, "Assessment");
								if(ret == null) {
									if(!hmReachDataTypes.containsKey(typeCode)) {
										CommonMasterDBQuery.InsertReachDataTyrpeForManual(typeCode, "", data_domain);
									}
								} else {
									typeCode = ret;
								}
								
								int taskId = 0;
								if (assessmentq1.contains("<") && assessmentq1.contains(">")) {
									String str = assessmentq1.substring(assessmentq1.indexOf("<"), assessmentq1.indexOf(">")+1);
//									System.out.println("str="  +str);
									if(hmFieldMap.containsKey(str.toLowerCase())) {
										assessmentq1 = assessmentq1.substring(0,assessmentq1.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + 
												assessmentq1.substring(assessmentq1.indexOf(">")+1,assessmentq1.length());
									}
								}
								if(!hmTaskMasters.containsKey(assessmentq1)) {
									time_start = new java.sql.Time(benchmarkStartDate.getTime());
									time_end = new java.sql.Time(benchmarkEndDate.getTime());
									taskGroupId = 0;
									taskId = CommonMasterDBQuery.InsertTaskMaster(assessmentq1, null, taskGroupId, tt_id_b, tst_id_b, tsst_id_b, 0, true, Constant.DEFAULT_ASSESSMENT_TASK_FREQUENCY,
											Constant.DEFAULT_ASSESSMENT_TASK_DURATION, time_start, time_end, Constant.ASSESSMENT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, asses1UinOptions,
										typeCode, Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_FALSE, "", Constant.STATUS_ACTIVE, null);
									taskGroupId = taskId;
									hmTaskMasters.put(assessmentq1, taskId);
									hmTaskGroup.put(assessmentq1, taskId);
								} else {
									taskId = hmTaskMasters.get(assessmentq1);
									taskGroupId = hmTaskGroup.get(assessmentq1);
								}
	
								if(!hmTaskHabitMap.containsKey(taskId+"-"+habitid)) {
									CommonMasterDBQuery.InsertTaskHabitMapping(taskId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_ASSMNT_QUES);
								}
							}
						} catch (Exception e3) {
							// TODO Auto-generated catch block
							e3.printStackTrace();
							System.out.println("Error at rowe number["+row.getRowNum()+"], habit[" + habit + "], Assesment Q-1 -> taskTypeId["+tt_id_b+"], taskSubTypeId["+tst_id_b+"], taskSubSubTypeId["+tsst_id_b+"]");
						}
	
						try {
	
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentq2 = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentans2 = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentans2input = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_INPUT_COLUMN_INDEX).getStringCellValue().trim();
							}
	
							try {
								if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Single Answer From Options)")) {
									asses2UinOptions = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Multiple Answers From Options)")) {
									asses2UinOptions = Constant.ANSWER_TYPE_MULTIPLE_OPTIONS;
								} else {
									asses2UinOptions = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (assessmentq2 != null && !assessmentq2.trim().equals("")) {
								String[] assessmentArray = null;
								if (assessmentans2.contains(" or more")) {
									assessmentans2 = assessmentans2.replace(" or more", "");
								}
								if (assessmentans2.contains(",")) {
									assessmentans2 = assessmentans2.replaceAll(", ", ",");
									assessmentArray = assessmentans2.split(",");
								}
	
								if (assessmentans2input.contains("Number")) {
									
									if(assessmentans2.contains(".")) {
										FloatListDomain floatDomain = new FloatListDomain();
										floatDomain.data = new ArrayList<>();
										for (int i = 0; i < assessmentArray.length; i++) {
											floatDomain.data.add(Float.parseFloat(assessmentArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(floatDomain);
									} else {
										IntListDomain intDomain = new IntListDomain();
										intDomain.data = new ArrayList<>();
										for (int i = 0; i < assessmentArray.length; i++) {
											intDomain.data.add(Integer.parseInt(assessmentArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(intDomain);
									}
	
								} else if (assessmentans2input.contains("Text")) {
									
									if(assessmentArray == null || assessmentArray.length<=0) {
										inputFileIssuesLogger.println("Error: --> row Number[" + row.getRowNum() + "], Habit[" + habit + "], Assessment Que -2 [ " + assessmentq2 + " ],  options list is not defined.");
										continue;
									}
									
									StringListDomain stringDomain = new StringListDomain();
									stringDomain.data = new ArrayList<>();
									for (int i = 0; i < assessmentArray.length; i++) {
										stringDomain.data.add(assessmentArray[i].trim());
	
									}
									data_domain = new ObjectMapper().writeValueAsString(stringDomain);
								}
	
								outputLogger.println("row Number[" + row.getRowNum() + "], Got Assessment Que -2 [ " + assessmentq2 + " ] & processing..");
								
								String typeCode  = "H" + habitid + "A2";
								String ret = checkMasterReachDataType(assessmentArray, "Assessment");
								if(ret == null) {
									if(!hmReachDataTypes.containsKey(typeCode)) {
										CommonMasterDBQuery.InsertReachDataTyrpeForManual(typeCode, "", data_domain);
									}
								} else {
									typeCode = ret;
								}
								
								int taskId = 0;
								if (assessmentq2.contains("<") && assessmentq2.contains(">")) {
									String str = assessmentq2.substring(assessmentq2.indexOf("<"), assessmentq2.indexOf(">")+1);
									System.out.println("str="  +str);
									if(hmFieldMap.containsKey(str.toLowerCase())) {
										assessmentq2 = assessmentq2.substring(0,assessmentq2.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + 
												assessmentq2.substring(assessmentq2.indexOf(">")+1,assessmentq2.length());
									}
								}
								if(!hmTaskMasters.containsKey(assessmentq2)) {
									time_start = new java.sql.Time(benchmarkStartDate.getTime());
									time_end = new java.sql.Time(benchmarkEndDate.getTime());
									taskGroupId = 0;
									taskId = CommonMasterDBQuery.InsertTaskMaster(assessmentq2, null, taskGroupId, tt_id_b, tst_id_b, tsst_id_b, 0, true, Constant.DEFAULT_ASSESSMENT_TASK_FREQUENCY,
											Constant.DEFAULT_ASSESSMENT_TASK_DURATION, time_start, time_end, Constant.ASSESSMENT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, asses2UinOptions,
										typeCode, Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_FALSE, "", Constant.STATUS_ACTIVE, null);
									taskGroupId = taskId;
									hmTaskMasters.put(assessmentq2, taskId);
									hmTaskGroup.put(assessmentq2, taskId);
								} else {
									taskId = hmTaskMasters.get(assessmentq2);
									taskGroupId = hmTaskGroup.get(assessmentq2);
								}
	
								if(!hmTaskHabitMap.containsKey(taskId+"-"+habitid)) {
									CommonMasterDBQuery.InsertTaskHabitMapping(taskId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_ASSMNT_QUES);
								}
							}
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
							System.out.println("Error at row number["+row.getRowNum()+"], habit[" + habit + "], Assesment Q-2 -> taskTypeId["+tt_id_b+"], taskSubTypeId["+tst_id_b+"], taskSubSubTypeId["+tsst_id_b+"]");
						}
	
						try {
	
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentq3 = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentans3 = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_ANS_OPTNS_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								assessmentans3input = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_INPUT_COLUMN_INDEX).getStringCellValue();
							}
	
							try {
								if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Single Answer From Options)")) {
									asses3UinOptions = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Multiple Answers From Options)")) {
									asses3UinOptions = Constant.ANSWER_TYPE_MULTIPLE_OPTIONS;
								} else {
									asses3UinOptions = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	
							if (assessmentq3 != null && !assessmentq3.equals("")) {
								String[] assessmentArray = null;
								if (assessmentans3.contains(" or more")) {
									assessmentans3 = assessmentans3.replace(" or more", "");
								}
								if (assessmentans3.contains(",")) {
									assessmentans3 = assessmentans3.replaceAll(", ", ",");
									assessmentArray = assessmentans3.split(",");
								}
								if (assessmentans3input.contains("Number")) {
									
									if(assessmentans3.contains(".")) {
										FloatListDomain floatDomain = new FloatListDomain();
										floatDomain.data = new ArrayList<>();
										for (int i = 0; i < assessmentArray.length; i++) {
											floatDomain.data.add(Float.parseFloat(assessmentArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(floatDomain);
									} else {
										IntListDomain intDomain = new IntListDomain();
										intDomain.data = new ArrayList<>();
										for (int i = 0; i < assessmentArray.length; i++) {
											intDomain.data.add(Integer.parseInt(assessmentArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(intDomain);
									}
	
								} else if (assessmentans3input.contains("Text")) {
									
									if(assessmentArray == null || assessmentArray.length<=0) {
										inputFileIssuesLogger.println("Error: --> row Number[" + row.getRowNum() + "], Habit[" + habit + "], Assessment Que -3 [ " + assessmentq3 + " ],  options list is not defined.");
										continue;
									}
									
									StringListDomain stringDomain = new StringListDomain();
									stringDomain.data = new ArrayList<>();
									for (int i = 0; i < assessmentArray.length; i++) {
										stringDomain.data.add(assessmentArray[i].trim());
	
									}
									data_domain = new ObjectMapper().writeValueAsString(stringDomain);
								}
	
								outputLogger.println("row Number[" + row.getRowNum() + "], Got Assessment Que -3 [ " + assessmentq3 + " ] & processing..");
								
								String typeCode  = "H" + habitid + "A3";
								String ret = checkMasterReachDataType(assessmentArray, "Assessment");
								if(ret == null) {
									if(!hmReachDataTypes.containsKey(typeCode)) {
										CommonMasterDBQuery.InsertReachDataTyrpeForManual(typeCode, "", data_domain);
									}
								} else {
									typeCode = ret;
								}
								
								int taskId = 0;
								if (assessmentq3.contains("<") && assessmentq3.contains(">")) {
									String str = assessmentq3.substring(assessmentq3.indexOf("<"), assessmentq3.indexOf(">")+1);
//									System.out.println("str="  +str);
									if(hmFieldMap.containsKey(str.toLowerCase())) {
										assessmentq3 = assessmentq3.substring(0,assessmentq3.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + 
												assessmentq3.substring(assessmentq3.indexOf(">")+1,assessmentq3.length());
									}
								}
								if(!hmTaskMasters.containsKey(assessmentq3)) {
									time_start = new java.sql.Time(benchmarkStartDate.getTime());
									time_end = new java.sql.Time(benchmarkEndDate.getTime());
									taskGroupId = 0;
									taskId = CommonMasterDBQuery.InsertTaskMaster(assessmentq3, null, taskGroupId, tt_id_b, tst_id_b, tsst_id_b, 0, true, Constant.DEFAULT_ASSESSMENT_TASK_FREQUENCY,
											Constant.DEFAULT_ASSESSMENT_TASK_DURATION, time_start, time_end, Constant.ASSESSMENT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, asses3UinOptions,
										typeCode, Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_FALSE, "", Constant.STATUS_ACTIVE, null);
									taskGroupId = taskId;
									hmTaskMasters.put(assessmentq3, taskId);
									hmTaskGroup.put(assessmentq3, taskId);
								} else {
									taskId = hmTaskMasters.get(assessmentq3);
									taskGroupId = hmTaskGroup.get(assessmentq3);
								}
	
								if(!hmTaskHabitMap.containsKey(taskId+"-"+habitid)) {
									CommonMasterDBQuery.InsertTaskHabitMapping(taskId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_ASSMNT_QUES);
								}
								
							}
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							System.out.println("Error at rowe number["+row.getRowNum()+"], habit[" + habit + "], Assesment Q-3 -> taskTypeId["+tt_id_b+"], taskSubTypeId["+tst_id_b+"], taskSubSubTypeId["+tsst_id_b+"]");
						}
	
						try {
	
							tt_id_b = hmTaskTypes.get("Track");
							tst_id_b = hmTaskSubTypes.get("Track/(Habit)");
							tsst_id_b = hmTaskSubSubTypes.get("Track/(Habit)");						
							
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								trackingq = row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								trackingans = row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_ANS_OPTNS_COLUMN_INDEX).getStringCellValue();
							}
							if (row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ") != null && !row
									.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("")) {
								trackingansinput = row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_INPUT_COLUMN_INDEX).getStringCellValue();
							}
	
							try {
								if (row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Single Answer From Options)")) {
									trackingUinOptions = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else if (row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_INPUT_TYPE_COLUMN_INDEX).getStringCellValue().trim()
										.equalsIgnoreCase("User Input (Multiple Answers From Options)")) {
									trackingUinOptions = Constant.ANSWER_TYPE_MULTIPLE_OPTIONS;
								} else {
									trackingUinOptions = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	
							if (trackingq != null && !trackingq.equals("")) {
								String[] trackingArray = null;
								if (trackingans.contains(" or more")) {
									trackingans = trackingans.replace(" or more", "");
								}
								if (trackingans.contains(",")) {
									trackingans = trackingans.replaceAll(", ", ",");
									trackingArray = trackingans.split(",");
								}
								if (trackingansinput.contains("Number")) {
									
									if(trackingans.contains(".")) {
										FloatListDomain floatDomain = new FloatListDomain();
										floatDomain.data = new ArrayList<>();
										for (int i = 0; i < trackingArray.length; i++) {
											floatDomain.data.add(Float.parseFloat(trackingArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(floatDomain);
									} else {
										IntListDomain intDomain = new IntListDomain();
										intDomain.data = new ArrayList<>();
										for (int i = 0; i < trackingArray.length; i++) {
											intDomain.data.add(Integer.parseInt(trackingArray[i].trim()));
										}
										data_domain = new ObjectMapper().writeValueAsString(intDomain);
									}
	
								} else if (trackingansinput.contains("Text")) {
									
									if(trackingArray == null || trackingArray.length<=0) {
										inputFileIssuesLogger.println("Error: --> row Number[" + row.getRowNum() + "], Habit[" + habit + "], Tracking Que -1 [ " + trackingq + " ],  options list is not defined.");
										continue;
									}
									
									StringListDomain stringDomain = new StringListDomain();
									stringDomain.data = new ArrayList<>();
									for (int i = 0; i < trackingArray.length; i++) {
										stringDomain.data.add(trackingArray[i].trim());
	
									}
									data_domain = new ObjectMapper().writeValueAsString(stringDomain);
								}
	
								outputLogger.println("row Number[" + row.getRowNum() + "], Got Tracking Que -1 [ " + trackingq + " ] & processing..");
//								System.out.println("__options[" + trackingans + "]");
								String typeCode  = "H" + habitid + "T";
								String ret = checkMasterReachDataType(trackingArray,"Track");
								if(ret == null) {
									if(!hmReachDataTypes.containsKey(typeCode)) {
										CommonMasterDBQuery.InsertReachDataTyrpeForManual(typeCode, "", data_domain);
									}
								} else {
									typeCode = ret;
								}
								
								int taskId = 0;
								if (trackingq.contains("<") && trackingq.contains(">")) {
									String str = trackingq.substring(trackingq.indexOf("<"), trackingq.indexOf(">")+1);
//									System.out.println("str="  +str);
									if(hmFieldMap.containsKey(str.toLowerCase())) {
										trackingq = trackingq.substring(0,trackingq.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + 
												trackingq.substring(trackingq.indexOf(">")+1,trackingq.length());
									}
								}
								if(!hmTaskMasters.containsKey(trackingq)) {
									time_start = new java.sql.Time(trackingStartDate.getTime());
									time_end = new java.sql.Time(trackingEndDate.getTime());
									taskGroupId = 0;
									taskId = CommonMasterDBQuery.InsertTaskMaster(trackingq, null, taskGroupId, tt_id_b, tst_id_b, tsst_id_b, 0, Constant.TASK_REAPEATABLE_TRUE, Constant.DEFAULT_TRACKING_TASK_FREQUENCY, Constant.DEFAULT_TRACKING_TASK_DURATION,
										time_start, time_end, Constant.DEFAULT_TASK_DAY, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, null, "", "", Constant.TASK_TAGABLE_FALSE, trackingUinOptions,
										typeCode, Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_FALSE, "", Constant.STATUS_ACTIVE, null);
									taskGroupId = taskId;
									hmTaskMasters.put(trackingq, taskId);
									hmTaskGroup.put(trackingq, taskId);
								} else {
									taskId = hmTaskMasters.get(trackingq);
									taskGroupId = hmTaskGroup.get(trackingq);
									CommonMasterDBQuery.UpdateTaskGlobalStatus(taskId, false);
								}
								
								if(!hmTaskHabitMap.containsKey(taskId+"-"+habitid)) {
									CommonMasterDBQuery.InsertTaskHabitMapping(taskId, taskGroupId, habitid, row.getRowNum(), Constant.TASK_GROUP_HABIT_BENCHMARK_TRACK_QUES);
								}
								
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("Error at rowe number["+row.getRowNum()+"], habit[" + habit + "], Track Q-1 -> taskTypeId["+tt_id_b+"], taskSubTypeId["+tst_id_b+"], taskSubSubTypeId["+tsst_id_b+"]");
						}
	
					} else {
						inputFileIssuesLogger.println("row Number[" + row.getRowNum() + "], Habit [ " + habit + " ] not found in Habit Master.");
					}
				}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("--- process finished");
	}

	
	
}
