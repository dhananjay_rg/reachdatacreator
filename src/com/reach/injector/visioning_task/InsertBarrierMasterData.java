package com.reach.injector.visioning_task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.injector.constant.Constant.ProcessStatus;
import com.reach.util.DBUtil;

public class InsertBarrierMasterData {

	private final Integer STATUS_ACTIVE = 1;

	private Workbook workbook = null;

	private Connection conn = null;

	private  PrintStream outputLogger = null;
	private PrintStream outputLoggerInsert = null;
	private PrintStream outputLoggerUpdate = null;
	private PrintStream outputLoggerDelete = null;
	private  PrintStream inputFileIssuesLogger = null;
	private  PrintStream sqlQueriesLogger = null;

	private  HashMap<String, Integer> barrierTypes = null;
	private  HashMap<String, Integer> barriers = null;
	private  HashMap<Integer, Integer> barriersMap = null;

	 public static void main(String[] args) throws IOException,
	 InvalidFormatException, SQLException, EncryptedDocumentException, DBError {
		 processBarrierData();
	 }

	public static void processBarrierData()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		
		InsertBarrierMasterData m= new InsertBarrierMasterData();
		
		
		try {
			m.processBarrierDataNow();
		
		} finally { 
			
			if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
			}
		}

	}
	
	public void processBarrierDataNow()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		init();
		loadBarrierTypes();
		loadBarriers();
		updateBarrierTypes();
		updateBarriers();

	}

	private void init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		// loadSheets();

		conn =  DBUtil.getConnection();
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/barrier_MasterData.log")));
		outputLoggerInsert = new PrintStream(new FileOutputStream(new File("logs/barrier_insertMsterData.log")));
		outputLoggerUpdate = new PrintStream(new FileOutputStream(new File("logs/barrier_updateMsterData.log")));
		outputLoggerDelete = new PrintStream(new FileOutputStream(new File("logs/barrier_deleteMsterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/barrier_issueData.log")));
		sqlQueriesLogger = new PrintStream(new FileOutputStream(new File("logs/barrier_sqlQueries.log")));

		barrierTypes = new HashMap<String, Integer>();
		barriers = new HashMap<String, Integer>();
		barriersMap = new HashMap<Integer, Integer>();
	}

	private void loadBarrierTypes() throws SQLException, IOException {
		outputLogger.println("Loading BarrierTypes from database....");
		Statement stmt = conn.createStatement();
		String selectBarrierTypesQuery = "select * from barrier_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectBarrierTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int barrierTypeId = rs.getInt("id");
				String barrierType = rs.getString("title");
				if (barrierTypes.containsKey(barrierType)) {
					throw new IOException("Duplicate BarrierType[ " + barrierType + " ] in BarrierTypes table");
				} else {
					barrierTypes.put(barrierType, barrierTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + barrierTypes.size() + " BarrierTypes in database....");
		outputLogger.println("===============");
	}

	private void loadBarriers() throws SQLException, IOException {
		outputLogger.println("Loading Barriers from database....");
		Statement stmt = conn.createStatement();
		String selectBarriersQuery = "select b.id, b.title, bt.id as bt_id,cs_barrier_id from barriers b, barrier_types bt where b.status="
				+ STATUS_ACTIVE + " and b.b_type_id=bt.id";
		ResultSet rs = stmt.executeQuery(selectBarriersQuery);
		if (rs != null) {
			while (rs.next()) {
				int barrierId = rs.getInt("id");
				String csBarrierId = rs.getString("cs_barrier_id");
				String barrier = rs.getString("title");
				int barrierTypeId = rs.getInt("bt_id");
				if (barriers.containsKey(csBarrierId)) {
					throw new IOException("Duplicate Entry barrier[ " + barrier + " ], csBarrierId [ " + csBarrierId + " ] in Barriers table");
				} else {
					barriers.put(barrier, barrierId);
					barriersMap.put(barrierId, barrierTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + barriers.size() + " Barriers in database....");
		outputLogger.println("===============");
	}

	private void updateBarrierTypes() throws SQLException {
		outputLogger.println("Updating BarrierTypes from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into barrier_types (title) values (?)",
				java.sql.Statement.RETURN_GENERATED_KEYS);
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			String barrierType = row.getCell(Constant.DropdownColumnsIndex.BARRIER_TYPE_COLUMN_INDEX).getStringCellValue();
			if (barrierType == null || barrierType.equals(""))
				break;
			else {
				barrierType = barrierType.trim().replaceAll(" +", " ");
				outputLogger.println("Got BarrierType[ " + barrierType
						+ " ] from input file, checking if it exists in database or not.");
				if (barrierTypes.containsKey(barrierType)) {
					outputLogger.println("BarrierType[ " + barrierType + " ] already exists in database.");
				} else {
					outputLogger.println(
							"BarrierType[ " + barrierType + " ] does not exist in database. So, inserting in DB");
					pstmt.setString(1, barrierType);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						barrierTypes.put(barrierType, (int) key);
					}
					rs.close();
				}
			}
		}
		pstmt.close();
		outputLogger.println("Inserted " + cnt + " BarrierTypes in database....");
		outputLogger.println("===============");
	}

	private void updateBarriers() throws SQLException {
		outputLogger.println("Updating Barriers from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into barriers (title, b_type_id, buid, cs_barrier_id) values (?,?,?,?)",
				Statement.RETURN_GENERATED_KEYS);
		PreparedStatement pstmtUpdate = conn.prepareStatement("update barriers set title=?, b_type_id=? where id=?");
		PreparedStatement pstmtDelete = conn.prepareStatement("update barriers set status=" + Constant.STATUS_INACTIVE + " where id=?");
		
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_BARRIER);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;
		int old = 0;
		int insert = 0;
		int update = 0;
		int delete = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		int rowNum = 0;
		while (rowIterator.hasNext()) {
			rowNum++;
			Row row = rowIterator.next();
			System.out.println(row.getCell(0));
			
			String csBarrierId = row.getCell(Constant.BarrierColumnsIndex.CS_BARRIER_ID_COLUMN_INDEX).getStringCellValue();
			String barrier = row.getCell(Constant.BarrierColumnsIndex.BARRIER_COLUMN_INDEX).getStringCellValue();
			String barrierType = row.getCell(Constant.BarrierColumnsIndex.BARRIER_TYPE_COLUMN_INDEX).getStringCellValue();
			String processType = row.getCell(Constant.BarrierColumnsIndex.BARRIER_PROCESS_TYPE_COLUMN_INDEX).getStringCellValue();
			if (barrier == null || barrier.equals(""))
				break;
			else {
				cnt++;
				barrier = barrier.trim().replaceAll(" +", " ");
				barrierType = barrierType.trim().replaceAll(" +", " ");
				processType = processType.trim().replaceAll(" +", " ").toUpperCase();

				if (processType.equalsIgnoreCase(ProcessStatus.old)) {
					old++;
					outputLogger.println(
							"Got Barrier[ " + barrier + " ] from input file, its Process type is OLD, so not processing");
					continue;
				} else if (processType.equalsIgnoreCase(ProcessStatus.update)) {
					
					outputLogger.println(
							"Got Barrier[ " + barrier + " ] from input file, checking if it exists in database or not.");
					if (barriers.containsKey(csBarrierId)) {
						outputLogger.println("Barrier[ " + barrier + " ], csBarrierId[ " + csBarrierId + " ] already exists in database. so updating...");
						
						Integer barrierTypeId = barrierTypes.get(barrierType);
						if (barrierTypeId == null) {
							inputFileIssuesLogger
									.println("Error while processing Sheet[ " + sheet.getSheetName() + " ]. Row[ " + rowNum
											+ " ] ==> Unable to update Barrier [ " + barrier + " ] as BarrierType[ "
											+ barrierType + " ] as it does not exist in BarrierType Masterdata.");
						} else {
							update++;
							pstmtUpdate.setString(1, barrier);
							pstmtUpdate.setInt(2, barrierTypeId);
							pstmtUpdate.setInt(3, barriers.get(csBarrierId));
							pstmtUpdate.executeUpdate();
							outputLoggerUpdate.println(pstmtUpdate);
						}
					} else {
						inputFileIssuesLogger.println(
								"Barrier[ " + barrier + " ], csBarrierId[ " + csBarrierId + " ] not exists in database. so can not UPDATE...");
					}
					
				} else if (processType.equalsIgnoreCase(ProcessStatus.delete)) {
					
					outputLogger.println(
							"Got Barrier[ " + barrier + " ] from input file, checking if it exists in database or not.");
					if (barriers.containsKey(csBarrierId)) {
						outputLogger.println("Barrier[ " + barrier + " ], csBarrierId[ " + csBarrierId + " ] already exists in database. so deleting...");
						
						Integer barrierTypeId = barrierTypes.get(barrierType);
						if (barrierTypeId == null) {
							inputFileIssuesLogger
									.println("Error while processing Sheet[ " + sheet.getSheetName() + " ]. Row[ " + rowNum
											+ " ] ==> Unable to delete Barrier [ " + barrier + " ] as BarrierType[ "
											+ barrierType + " ] as it does not exist in BarrierType Masterdata.");
						} else {
							delete++;
							pstmtDelete.setInt(1, barriers.get(csBarrierId));
							pstmtDelete.executeUpdate();
							outputLoggerDelete.println(pstmtDelete);
						}
					} else {
						inputFileIssuesLogger.println(
								"Barrier[ " + barrier + " ], csBarrierId[ " + csBarrierId + " ] not exists in database. so can not DELETE...");
					}
				} else if (processType.equalsIgnoreCase(ProcessStatus.insert)) {
				
					outputLogger.println(
							"Got Barrier[ " + barrier + " ] from input file, checking if it exists in database or not.");
					if (barriers.containsKey(barrier)) {
						outputLogger.println("Barrier[ " + barrier + " ] already exists in database.");
					} else {
						outputLogger.println("Barrier[ " + barrier + " ] does not exist in database. So, inserting in DB");
						Integer barrierTypeId = barrierTypes.get(barrierType);
						if (barrierTypeId == null) {
							inputFileIssuesLogger
									.println("Error while processing Sheet[ " + sheet.getSheetName() + " ]. Row[ " + rowNum
											+ " ] ==> Unable to insert Barrier [ " + barrier + " ] as BarrierType[ "
											+ barrierType + " ] as it does not exist in BarrierType Masterdata.");
						} else {
							insert++;
							pstmt.setString(1, barrier);
							pstmt.setInt(2, barrierTypeId);
							pstmt.setObject(3,UUID.randomUUID());
							pstmt.setString(4, csBarrierId);
							pstmt.executeUpdate();
							outputLoggerInsert.println(pstmt);
							
							ResultSet rs = pstmt.getGeneratedKeys();
	
							if (rs != null && rs.next()) {
								int key = rs.getInt(1);
								barriers.put(barrier, key);
								barriersMap.put(key, barrierTypeId);
							}
							rs.close();
							
							
						}
					}
				}
			}
		}
		pstmt.close();
		pstmtDelete.close();
		pstmtUpdate.close();
		outputLogger.println("======= BARRIER SUMMARY ========");
		outputLogger.println("Total Records: " + cnt);
		outputLogger.println("Total Old Records: " + old);
		outputLogger.println("Total Update Records: " + update);
		outputLogger.println("Total delete Records: " + delete);
		outputLogger.println("Total Insert Records: " + insert);
		outputLogger.println("===============");

		outputLogger.println("BarrierTypes hashmap output....");
		Set<String> keys = barrierTypes.keySet();
		Iterator<String> itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			int value = barrierTypes.get(key);
			outputLogger.println(key + " : " + value);
		}
		outputLogger.println("===============");
		outputLogger.println("Barriers hashmap output....");
		keys = barriers.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			int value = barriers.get(key);
			outputLogger.println(key + " : " + value);
		}
		outputLogger.println("===============");
		outputLogger.println("Barriers-BarrierTypes mapping hashmap output....");
		Set<Integer> keys1 = barriersMap.keySet();
		Iterator<Integer> itr1 = keys1.iterator();
		while (itr1.hasNext()) {
			int key = itr1.next();
			int value = barriersMap.get(key);
			outputLogger.println(key + " : " + value);
		}
	}
}