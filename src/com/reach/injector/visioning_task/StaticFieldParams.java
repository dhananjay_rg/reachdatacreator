package com.reach.injector.visioning_task;

import java.util.HashMap;

public class StaticFieldParams {

	private static HashMap<String,String> hmFieldMap = new HashMap<>(); 
	
	static {
		hmFieldMap.put("<habit name>", "@@habit.title@@");
		hmFieldMap.put("<task name>", "@@taskMaster.title@@");
		hmFieldMap.put("<challenge name>", "@@challenge.name@@");
		hmFieldMap.put("<blog title>", "@@content.title@@");
		hmFieldMap.put("<video title>", "@@content.title@@");
		hmFieldMap.put("<audio title>", "@@content.title@@");
		hmFieldMap.put("<recipe name>", "@@content.title@@");
	}
	
	public static HashMap<String,String> getStaticFieldMap() {
		return hmFieldMap;
	}
	
}
