package com.reach.injector.visioning_task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.CommonMasterDBQuery;
import com.reach.injector.constant.Constant;
import com.reach.injector.constant.Constant.ProcessStatus;
import com.reach.util.DBUtil;

public class InsertHabitMasterData {

	private final Integer STATUS_ACTIVE = 1;
	private  final String DEFAULT_HABIT_DIFFICULTY_LEVEL = "Easy";
	private  final Integer DEFAULT_HABIT_DEV_PERIOD = 21;
	private  final String DEFAULT_HABIT_FREQUENCY_PERIOD = "Daily";

	private  Workbook workbook = null;

	private  Connection conn = null;

	private  PrintStream outputLogger = null;
	private  PrintStream outputLoggerInsert = null;
	private  PrintStream outputLoggerUpdate = null;
	private  PrintStream outputLoggerDelete = null;
	private  PrintStream inputFileIssuesLogger = null;
	private  PrintStream inputFileWarningLogger = null;
	private  PrintStream sqlQueriesLogger = null;

	private  HashMap<String, Integer> habitTypes = null;
	private  HashMap<String, Integer> habits = null;
	private  HashMap<Integer, Integer> habitDevPeriods = null;
	private  HashMap<String, Integer> habitDifficultyLevels = null;
	private  HashMap<String, Integer> habitFrequencyPeriods = null;
	private  HashMap<Integer, Integer> habitsMap = null;

	 public static void main(String[] args) throws IOException,
	 InvalidFormatException, SQLException, EncryptedDocumentException, DBError {
		 processHabitData();
	 }
	 
	 public static void processHabitData() throws EncryptedDocumentException, InvalidFormatException, DBError, SQLException, IOException {
		 InsertHabitMasterData m=new InsertHabitMasterData();
		 
		 
		 try {
			 m.processHabitDataNow();
			
			} finally { 
				
				if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
				}
			}
		 
	 }

	public void processHabitDataNow()
			throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		init();
		loadHabitTypes();
		loadHabitDevPeriods();
		loadHabitDifficultyLevels();
		loadHabitFrequencyPeriods();
		loadHabits();
		
		updateHabitTypes();
		updateHabitDevPeriods();
		updateHabitDifficultyLevels();
		updateHabits();

	}

	private void init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		// loadSheets();

		conn = DBUtil.getConnection();
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/habit_MasterData.log")));
		outputLoggerInsert = new PrintStream(new FileOutputStream(new File("logs/habit_insertMsterData.log")));
		outputLoggerUpdate = new PrintStream(new FileOutputStream(new File("logs/habit_updateMsterData.log")));
		outputLoggerDelete = new PrintStream(new FileOutputStream(new File("logs/habit_deleteMsterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/habit_issueData.log")));
		inputFileWarningLogger = new PrintStream(new FileOutputStream(new File("logs/habit_warningData.log")));
		sqlQueriesLogger = new PrintStream(new FileOutputStream(new File("logs/habit_sqlQueries.log")));

		habitTypes = new HashMap<String, Integer>();
		habits = new HashMap<String, Integer>();
		habitDevPeriods = new HashMap<Integer, Integer>();
		habitDifficultyLevels = new HashMap<String, Integer>();
		habitFrequencyPeriods = new HashMap<String, Integer>();
		habitsMap = new HashMap<Integer, Integer>();
	}

	private void loadHabitTypes() throws SQLException, IOException {
		outputLogger.println("Loading HabitTypes from database....");
		Statement stmt = conn.createStatement();
		String selectHabitTypesQuery = "select * from habit_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectHabitTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int habitTypeId = rs.getInt("id");
				String habitType = rs.getString("title");
				if (habitTypes.containsKey(habitType)) {
					throw new IOException("Duplicate HabitType [ " + habitType + " ] in HabitTypes table");
				} else {
					habitTypes.put(habitType, habitTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + habitTypes.size() + " HabitTypes in database....");
		outputLogger.println("===============");
	}

	private void loadHabitDevPeriods() throws SQLException, IOException {
		outputLogger.println("Loading HabitDevPeriods from database....");
		Statement stmt = conn.createStatement();
		String selectHabitDevPeriodsQuery = "select * from habit_dev_periods where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectHabitDevPeriodsQuery);
		if (rs != null) {
			while (rs.next()) {
				int habitDevPeriodId = rs.getInt("id");
				int habitDevPeriod = rs.getInt("title");
				if (habitDevPeriods.containsKey(habitDevPeriod)) {
					throw new IOException("Duplicate Entry[ " + habitDevPeriod + " ] in HabitDevPeriods table");
				} else {
					habitDevPeriods.put(habitDevPeriod, habitDevPeriodId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + habitDevPeriods.size() + " HabitDevPeriods in database....");
		outputLogger.println("===============");
	}

	private void loadHabitDifficultyLevels() throws SQLException, IOException {
		outputLogger.println("Loading HabitDifficultyLevels from database....");
		Statement stmt = conn.createStatement();
		String selectHabitDifficultyLevelsQuery = "select * from habit_difficulty_levels where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectHabitDifficultyLevelsQuery);
		if (rs != null) {
			while (rs.next()) {
				int habitDifficultyLevelId = rs.getInt("id");
				String habitDifficultyLevel = rs.getString("title");
				if (habitDifficultyLevels.containsKey(habitDifficultyLevel)) {
					throw new IOException(
							"Duplicate Entry[ " + habitDifficultyLevel + " ] in HabitDifficultyLevels table");
				} else {
					habitDifficultyLevels.put(habitDifficultyLevel, habitDifficultyLevelId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + habitDifficultyLevels.size() + " HabitDifficultyLevels in database....");
		outputLogger.println("===============");
	}

	private  void loadHabitFrequencyPeriods() throws SQLException, IOException {
		outputLogger.println("Loading HabitHabitFrequencyPeriods from database....");
		Statement stmt = conn.createStatement();
		String selectHabitFrequencyPeriodsQuery = "select * from habit_frequency_periods where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectHabitFrequencyPeriodsQuery);
		if (rs != null) {
			while (rs.next()) {
				int habitFrequencyPeriodId = rs.getInt("id");
				String habitFrequencyPeriod = rs.getString("title");
				if (habitFrequencyPeriods.containsKey(habitFrequencyPeriod)) {
					throw new IOException(
							"Duplicate Entry[ " + habitFrequencyPeriod + " ] in HabitFrequencyPeriods table");
				} else {
					habitFrequencyPeriods.put(habitFrequencyPeriod, habitFrequencyPeriodId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + habitFrequencyPeriods.size() + " HabitFrequencyPeriods in database....");
		outputLogger.println("===============");
	}

	private  void loadHabits() throws SQLException, IOException {
		outputLogger.println("Loading Habits from database....");
		Statement stmt = conn.createStatement();
		String selectHabitsQuery = "select h.id, h.title, ht.id as ht_id,cs_habit_id from habits h, habit_types ht where h.status="
				+ STATUS_ACTIVE + " and h.h_type_id=ht.id and cs_habit_id is not null";
		ResultSet rs = stmt.executeQuery(selectHabitsQuery);
		if (rs != null) {
			while (rs.next()) {
				int habitId = rs.getInt("id");
				String csHabitId = rs.getString("cs_habit_id");
				if (habits.containsKey(csHabitId)) {
					throw new IOException("Duplicate csHabitId [ " + csHabitId + " ] in Habits table");
				} else {
					habits.put(csHabitId, habitId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + habits.size() + " Habits in database....");
		outputLogger.println("===============");
	}

	private  void updateHabitTypes() throws SQLException {
		outputLogger.println("Updating HabitTypes from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into habit_types (title) values (?)",
				java.sql.Statement.RETURN_GENERATED_KEYS);
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
		
			if (row.getCell(Constant.DropdownColumnsIndex.HABIT_TYPE_COLUMN_INDEX) == null || row.getCell(Constant.DropdownColumnsIndex.HABIT_TYPE_COLUMN_INDEX).getStringCellValue().equals(""))
				break;
			else {
				String habitType = row.getCell(Constant.DropdownColumnsIndex.HABIT_TYPE_COLUMN_INDEX).getStringCellValue();
				habitType = habitType.trim().replaceAll(" +", " ");
				outputLogger.println("Got HabitType [ " + habitType
						+ " ] from input file, checking if it exists in database or not.");
				if (habitTypes.containsKey(habitType)) {
					outputLogger.println("HabitType [ " + habitType + " ] already exists in database.");
				} else {
					outputLogger
							.println("HabitType [ " + habitType + " ] does not exist in database. So, inserting in DB");
					pstmt.setString(1, habitType);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						habitTypes.put(habitType, (int) key);
					}
					rs.close();
				}
			}
		}
		pstmt.close();
		outputLogger.println("Inserted " + cnt + " HabitTypes in database....");
		outputLogger.println("===============");
	}

	private  void updateHabitDevPeriods() throws SQLException {
		outputLogger.println("Updating HabitDevPeriods from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into habit_dev_periods (title) values (?)",
				java.sql.Statement.RETURN_GENERATED_KEYS);
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();
		// System.out.println(Cell.CELL_TYPE_BLANK); //3
		// System.out.println(Cell.CELL_TYPE_BOOLEAN); //4
		// System.out.println(Cell.CELL_TYPE_ERROR); //5
		// System.out.println(Cell.CELL_TYPE_FORMULA); //2
		// System.out.println(Cell.CELL_TYPE_NUMERIC);// 0
		// System.out.println(Cell.CELL_TYPE_STRING);// 1

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			Cell cell = row.getCell(Constant.DropdownColumnsIndex.HABIT_DEV_PERIOD_COLUMN_INDEX);
			if (cell != null) {
				if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
					break;
				else {
					Number habitDevPeriod = cell.getNumericCellValue();
					outputLogger.println("Got HabitDevPeriod[ " + habitDevPeriod.intValue()
							+ " ] from input file, checking if it exists in database or not.");
					if (habitDevPeriods.containsKey(habitDevPeriod.intValue())) {
						outputLogger.println(
								"HabitDevPeriod[ " + habitDevPeriod.intValue() + " ] already exists in database.");
					} else {
						outputLogger.println("HabitDevPeriod[ " + habitDevPeriod.intValue()
								+ " ] does not exist in database. So, inserting in DB");
						pstmt.setInt(1, habitDevPeriod.intValue());
						pstmt.executeUpdate();
						cnt++;

						ResultSet rs = pstmt.getGeneratedKeys();

						if (rs != null && rs.next()) {
							long key = rs.getLong(1);
							habitDevPeriods.put(habitDevPeriod.intValue(), (int) key);
						}
						rs.close();
					}
				}
			}
		}
		pstmt.close();
		outputLogger.println("Inserted " + cnt + " HabitDevPeriods in database....");
		outputLogger.println("===============");
	}

	private  void updateHabitDifficultyLevels() throws SQLException {
		outputLogger.println("Updating HabitDifficultyLevels from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into habit_difficulty_levels (title) values (?)",
				java.sql.Statement.RETURN_GENERATED_KEYS);
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			if (row.getCell(Constant.DropdownColumnsIndex.HABIT_DIFFICULTY_LEVEL_COLUMN_INDEX) == null || row.getCell(Constant.DropdownColumnsIndex.HABIT_DIFFICULTY_LEVEL_COLUMN_INDEX).getStringCellValue().equals(""))
				break;
			else {
				String habitDifficultyLevel = row.getCell(Constant.DropdownColumnsIndex.HABIT_DIFFICULTY_LEVEL_COLUMN_INDEX).getStringCellValue();
				habitDifficultyLevel = habitDifficultyLevel.trim().replaceAll(" +", " ");
				outputLogger.println("Got HabitDifficultyLevel[ " + habitDifficultyLevel
						+ " ] from input file, checking if it exists in database or not.");
				if (habitDifficultyLevels.containsKey(habitDifficultyLevel)) {
					outputLogger.println(
							"HabitDifficultyLevel[ " + habitDifficultyLevel + " ] already exists in database.");
				} else {
					outputLogger.println("HabitDifficultyLevel[ " + habitDifficultyLevel
							+ " ] does not exist in database. So, inserting in DB");
					pstmt.setString(1, habitDifficultyLevel);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						habitDifficultyLevels.put(habitDifficultyLevel, (int) key);
					}
					rs.close();
				}
			}
		}
		pstmt.close();
		outputLogger.println("Inserted " + cnt + " HabitDifficultyLevels in database....");
		outputLogger.println("===============");
	}

	private  void updateHabits() throws SQLException {
		outputLogger.println("Updating Habits from input file....");
		PreparedStatement pstmt = conn.prepareStatement(
				"insert into habits (title, h_type_id, hdp_id, hdl_id, hfp_id, description, huid, cs_habit_id) values (?,?,?,?,?,?,?,?)",
				Statement.RETURN_GENERATED_KEYS);
		PreparedStatement pstmtUpdate = conn.prepareStatement("update habits set title=?, h_type_id=?, hdp_id=?, hdl_id=?, hfp_id=?, description=? where id=?");
		PreparedStatement pstmtDelete = conn.prepareStatement("update habits set status=" + Constant.STATUS_INACTIVE + " where id=?");
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_HABIT);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;
		int old = 0;
		int insert = 0;
		int update = 0;
		int delete = 0;
		
		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		int rowNum = 0;
		while (rowIterator.hasNext()) {
			try {
				cnt++;
				rowNum++;
				Row row = rowIterator.next();

				String csHabitId = row.getCell(Constant.HabitColumnsIndex.CS_HABIT_ID_COLUMN_INDEX).getStringCellValue();
				String processType = row.getCell(Constant.HabitColumnsIndex.HABIT_PROCESS_TYPE_COLUMN_INDEX).getStringCellValue();
				processType = processType.trim().replaceAll(" +", " ").toUpperCase();

				Cell habitCell = row.getCell(Constant.HabitColumnsIndex.HABIT_COLUMN_INDEX);
				if (habitCell == null) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
							+ " ] ==> Unable to process Habit is not defined.");
					continue;
				}
				String habit = habitCell.getStringCellValue();

				Cell habitDescriptionCell = row.getCell(Constant.HabitColumnsIndex.HABIT_DESC_COLUMN_INDEX);
				if (habitDescriptionCell == null) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
							+ " ] ==> Unable to process HabitDescription is not defined.");
					continue;
				}
				String habitDescription = habitDescriptionCell.getStringCellValue();

				Cell habitTypeCell = row.getCell(Constant.HabitColumnsIndex.HABIT_TYPE_COLUMN_INDEX);
				if (habitTypeCell == null) {
					inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
							+ " ] ==> Unable to process HabitType is not defined.");
					continue;
				}
				String habitType = habitTypeCell.getStringCellValue();

				Cell habitDevPeriodCell = row.getCell(Constant.HabitColumnsIndex.HABIT_DEV_PERIOD_COLUMN_INDEX);
				Number habitDevPeriod = null;
				if (habitDevPeriodCell == null) {
					inputFileWarningLogger.println("Warning: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
							+ " ] ==> Inserting Habit [ " + habit + " ] with default HabitDevPeriod [ "
							+ DEFAULT_HABIT_DEV_PERIOD + " ] as it's not defined.");
					habitDevPeriod = DEFAULT_HABIT_DEV_PERIOD;
				} else {

					habitDevPeriod = habitDevPeriodCell.getNumericCellValue();
				}

				Cell habitDifficultyLevelCell = row.getCell(Constant.HabitColumnsIndex.HABIT_DIFFICULTY_LEVEL_COLUMN_INDEX);
				String habitDifficultyLevel = null;
				if (habitDifficultyLevelCell == null) {
					inputFileWarningLogger.println("Warning: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
							+ " ] ==> Inserting Habit [ " + habit + " ] with default HabitDifficultyLevel [ "
							+ DEFAULT_HABIT_DIFFICULTY_LEVEL + " ] as it's not defined.");
					habitDifficultyLevel = DEFAULT_HABIT_DIFFICULTY_LEVEL;
				} else {

					habitDifficultyLevel = habitDifficultyLevelCell.getStringCellValue();
				}

				// Column for Habit Frequency is not available in Habit Master Sheet as on 31-Mar-18. So, initializing with null.
				String habitFrequencyPeriod = DEFAULT_HABIT_FREQUENCY_PERIOD;

				if (habit == null || habit.equals("")) {
					break;
				} else {
					habit = habit.trim().replaceAll(" +", " ");

					if (habitDescription != null && !habitDescription.trim().equals("")) {
						habitDescription = habitDescription.trim().replaceAll(" +", " ");
					} else {
						inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
								+ " ] ==> Unable to insert Habit [ " + habit
								+ " ] as HabitDescription is not defined.");
						continue;
					}

					if (habitType != null && !habitType.trim().equals("")) {
						habitType = habitType.trim().replaceAll(" +", " ");
					} else {
						inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
								+ " ] ==> Unable to insert Habit [ " + habit + " ] as HabitType is not defined.");
						continue;
					}

					if (habitDevPeriod.intValue() == 0) {
						inputFileWarningLogger.println("Warning: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
								+ " ] ==> Inserting Habit [ " + habit + " ] with default HabitDevPeriod [ "
								+ DEFAULT_HABIT_DEV_PERIOD + " ] as it's not defined.");
						habitDevPeriod = DEFAULT_HABIT_DEV_PERIOD;
					} else {
						// System.out.println("habitDevPeriod = " +
						// habitDevPeriod);
					}

					if (habitDifficultyLevel != null && !habitDifficultyLevel.trim().equals("")) {
						habitDifficultyLevel = habitDifficultyLevel.trim().replaceAll(" +", " ");
					} else {
						inputFileWarningLogger.println("Warning: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + rowNum
								+ " ] ==> Inserting Habit [ " + habit + " ] with default HabitDifficultyLevel [ "
								+ DEFAULT_HABIT_DIFFICULTY_LEVEL + " ] as it's not defined.");
						habitDifficultyLevel = DEFAULT_HABIT_DIFFICULTY_LEVEL;
					}
					
					
					if (processType.equalsIgnoreCase(ProcessStatus.old)) {
						old++;
						outputLogger.println(
								"Got Habit[ " + habit + " ] from input file, its Process type is OLD, so not processing");
						continue;
					} else if (processType.equalsIgnoreCase(ProcessStatus.update)) {
						outputLogger.println(
								"Got Habit[ " + habit + " ], csHabitId[ " + csHabitId + " ] from input file, checking if it exists in database or not.");
						
						Integer habitTypeId = habitTypes.get(habitType);
						if (habitTypeId == null) {
							inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
									+ rowNum + " ] ==> Unable to update Habit [ " + habit + " ], csHabitId[ " + csHabitId + " ] as HabitType [ "
									+ habitType + " ] does not exist in HabitType Masterdata.");
							continue;
						}

						Integer habitDevPeriodId = habitDevPeriods.get(habitDevPeriod.intValue());
						if (habitDevPeriodId == null) {
							inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
									+ rowNum + " ] ==> Unable to update Habit [ " + habit + " ], csHabitId[ " + csHabitId + " ] as HabitDevPeriod [ "
									+ habitDevPeriod + " ] does not exist in HabitDevPeriod Masterdata.");
							continue;
						}

						Integer habitDifficultyLevelId = habitDifficultyLevels.get(habitDifficultyLevel);
						if (habitDifficultyLevelId == null) {
							inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
									+ rowNum + " ] ==> Unable to update Habit [ " + habit
									+ " ], csHabitId[ " + csHabitId + " ] as HabitDifficultyLevel [ " + habitDevPeriod
									+ " ] does not exist in HabitDifficultyLevel Masterdata.");
							continue;
						}

						Integer habitFrequencyPeriodId = habitFrequencyPeriods.get(habitFrequencyPeriod);
						if (habitFrequencyPeriodId == null) {
							inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
									+ rowNum + " ] ==> Unable to update Habit [ " + habit
									+ " ], csHabitId[ " + csHabitId + " ] as HabitFrequencyPeriod [ " + habitFrequencyPeriodId
									+ " ] does not exist in HabitFrequencyPeriod Masterdata.");
							continue;
						}
						
						
						if (habits.containsKey(csHabitId)) {	
							pstmtUpdate.setString(1, habit);
							pstmtUpdate.setInt(2, habitTypeId);
							pstmtUpdate.setInt(3, habitDevPeriodId);
							pstmtUpdate.setInt(4, habitDifficultyLevelId);
							pstmtUpdate.setInt(5, habitFrequencyPeriodId);
							pstmtUpdate.setString(6, habitDescription);
							pstmtUpdate.setInt(7, habits.get(csHabitId));
							pstmtUpdate.executeUpdate();
							outputLoggerUpdate.println(pstmtUpdate);
							update++;
						} else {
							inputFileIssuesLogger.println(
									"Habit[ " + habit + " ], csHabitId[ " + csHabitId + " ] not exists in database. so can not UPDATE...");
						}
						
					} else if (processType.equalsIgnoreCase(ProcessStatus.delete)) {
						
						Integer habitTypeId = habitTypes.get(habitType);
						if (habitTypeId == null) {
							inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
									+ rowNum + " ] ==> Unable to update Habit [ " + habit + " ], csHabitId[ " + csHabitId + " ] as HabitType [ "
									+ habitType + " ] does not exist in HabitType Masterdata.");
							continue;
						}
						
						if (habits.containsKey(csHabitId)) {
							pstmtDelete.setInt(1, habits.get(csHabitId));
							pstmtDelete.executeUpdate();
							outputLoggerDelete.println(pstmtDelete);
							
							CommonMasterDBQuery.deleteTaskHabitMappingByHabitId(habits.get(csHabitId));;
							
							delete++;
						}  else {
							inputFileIssuesLogger.println(
									"Habit[ " + habit + " ], csHabitId[ " + csHabitId + " ] not exists in database. so can not DELETE...");
						}
						
					} else if (processType.equalsIgnoreCase(ProcessStatus.insert)) {
						outputLogger.println(
								"Got Habit [ " + habit + " ] from input file, checking if it exists in database or not.");
						if (habits.containsKey(habit)) {
							outputLogger.println("Habit [ " + habit + " ] already exists in database.");
						} else {
							outputLogger.println("Habit [ " + habit + " ] does not exist in database. So, inserting in DB");
	
							Integer habitTypeId = habitTypes.get(habitType);
							if (habitTypeId == null) {
								inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
										+ rowNum + " ] ==> Unable to insert Habit [ " + habit + " ] as HabitType [ "
										+ habitType + " ] does not exist in HabitType Masterdata.");
								continue;
							}
	
							Integer habitDevPeriodId = habitDevPeriods.get(habitDevPeriod.intValue());
							if (habitDevPeriodId == null) {
								inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
										+ rowNum + " ] ==> Unable to insert Habit [ " + habit + " ] as HabitDevPeriod [ "
										+ habitDevPeriod + " ] does not exist in HabitDevPeriod Masterdata.");
								continue;
							}
	
							Integer habitDifficultyLevelId = habitDifficultyLevels.get(habitDifficultyLevel);
							if (habitDifficultyLevelId == null) {
								inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
										+ rowNum + " ] ==> Unable to insert Habit [ " + habit
										+ " ] as HabitDifficultyLevel [ " + habitDevPeriod
										+ " ] does not exist in HabitDifficultyLevel Masterdata.");
								continue;
							}
	
							Integer habitFrequencyPeriodId = habitFrequencyPeriods.get(habitFrequencyPeriod);
							if (habitFrequencyPeriodId == null) {
								inputFileIssuesLogger.println("Error: Sheet [ " + sheet.getSheetName() + " ]. Row [ "
										+ rowNum + " ] ==> Unable to insert Habit [ " + habit
										+ " ] as HabitFrequencyPeriod [ " + habitFrequencyPeriodId
										+ " ] does not exist in HabitFrequencyPeriod Masterdata.");
								continue;
							}
	
							pstmt.setString(1, habit);
							pstmt.setInt(2, habitTypeId);
							pstmt.setInt(3, habitDevPeriodId);
							pstmt.setInt(4, habitDifficultyLevelId);
							pstmt.setInt(5, habitFrequencyPeriodId);
							pstmt.setString(6, habitDescription);
							pstmt.setObject(7, UUID.randomUUID());
							pstmt.setString(8, csHabitId);
							pstmt.executeUpdate();
							outputLoggerInsert.println(pstmt);
							insert++;
							
							ResultSet rs = pstmt.getGeneratedKeys();
	
							if (rs != null && rs.next()) {
								int key = rs.getInt(1);
								habits.put(habit, key);
							}
							rs.close();
						}
					}
						
				}
			} catch (Exception ex) {
				System.out.println("Exception: Sheet [ " + sheet.getSheetName() + " ]. Row [ " + (rowNum + 1) + " ]");
				ex.printStackTrace();
			}
		}
		pstmt.close();
		pstmtDelete.close();
		pstmtUpdate.close();
		outputLogger.println("======= HABIT SUMMARY ========");
		outputLogger.println("Total Records: " + cnt);
		outputLogger.println("Total Old Records: " + old);
		outputLogger.println("Total Update Records: " + update);
		outputLogger.println("Total delete Records: " + delete);
		outputLogger.println("Total Insert Records: " + insert);
		outputLogger.println("===============");

		outputLogger.println("HabitTypes hashmap output....");
		Set<String> keys = habitTypes.keySet();
		Iterator<String> itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			int value = habitTypes.get(key);
			outputLogger.println(key + " : " + value);
		}
		outputLogger.println("===============");
		outputLogger.println("Habits hashmap output....");
		keys = habits.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			int value = habits.get(key);
			outputLogger.println(key + " : " + value);
		}
		outputLogger.println("===============");
		outputLogger.println("Habits-HabitTypes mapping hashmap output....");
		Set<Integer> keys1 = habitsMap.keySet();
		Iterator<Integer> itr1 = keys1.iterator();
		while (itr1.hasNext()) {
			int key = itr1.next();
			int value = habitsMap.get(key);
			outputLogger.println(key + " : " + value);
		}
	}
}