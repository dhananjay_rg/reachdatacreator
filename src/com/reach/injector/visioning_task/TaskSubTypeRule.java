package com.reach.injector.visioning_task;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.util.DBUtil;

public class TaskSubTypeRule {

	private Workbook workbook = null;

	private Connection conn = null;

	// private static PrintStream System.out = null;

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException, DBError, SQLException, IOException {
		process();
	}

	public static void process() throws EncryptedDocumentException, InvalidFormatException, DBError, SQLException, IOException {

		TaskSubTypeRule m = new TaskSubTypeRule();

		try {
			m.processNow();
		} finally {

			if (m.conn != null) {
				try {
					m.conn.close();
				} catch (Exception e) {
				}
			}
		}

	}

	private void processNow() throws EncryptedDocumentException, InvalidFormatException, DBError, SQLException, IOException {
		System.out.println(new Date() + " ___ Rule Process Started..");
		init();

		processSubTypeRule();

		System.out.println(new Date() + " ___ Rule Process Finished..");

	}

	private void init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		conn = DBUtil.getConnection();
	}

	private void processSubTypeRule() {
		System.out.println("------- TASK SUB TYPE ---------");
		String sql1 = "update task_sub_types set day_ripit_tsk_btwn_habits=true where trim(both ' ' from lower(title) )= trim(both ' ' from lower(?) )";
		PreparedStatement ps1 = null;

		String sql2 = "update task_sub_sub_types set day_ripit_tsk_btwn_habits=true where trim(both ' ' from lower(title) )= trim(both ' ' from lower(?) ) or tst_id in( select id from task_sub_types where trim(both ' ' from lower(title) )= trim(both ' ' from lower(?) ) ) ";
		PreparedStatement ps2 = null;

		try {
			ps1 = conn.prepareStatement(sql1);
			ps2 = conn.prepareStatement(sql2);

			Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_SCHEDULE_RULE);
			Iterator<Row> rowIterator = sheet.rowIterator();

			// Ignore header row
			if (rowIterator.hasNext()) {
				rowIterator.next();
			}

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				if (row.getCell(Constant.ScheduleRuleColumnsIndex.TASK_TYPE_COLUMN_INDEX) == null
						|| row.getCell(Constant.ScheduleRuleColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim().equals(""))
					break;
				else {
					try {
						String taskSubType = row.getCell(Constant.ScheduleRuleColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim();
						// System.out.println(taskSubType);
						if (row.getCell(Constant.ScheduleRuleColumnsIndex.EEPETION_APPL_COLUMN_INDEX) != null
								&& !row.getCell(Constant.ScheduleRuleColumnsIndex.EEPETION_APPL_COLUMN_INDEX).getStringCellValue().trim().equals("")
								&& row.getCell(Constant.ScheduleRuleColumnsIndex.EEPETION_APPL_COLUMN_INDEX).getStringCellValue().trim().equalsIgnoreCase("Y")) {

							System.out.println("___ updating... for Type " + taskSubType );
							ps1.setString(1, taskSubType);
							
							System.out.println("___ Query1 " + ps1.toString() );
							
							ps1.executeUpdate();

							
							////
							
							ps2.setString(1, taskSubType);
							ps2.setString(2, taskSubType);
							
							System.out.println("___ Query2 " + ps2.toString() );
							ps2.executeUpdate();

						}

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			if (ps1 != null) {
				try {
					ps1.close();
				} catch (Exception e) {
				}
			}

			if (ps2 != null) {
				try {
					ps2.close();
				} catch (Exception e) {
				}
			}

		}

	}

}
