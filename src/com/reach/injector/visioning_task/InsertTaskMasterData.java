package com.reach.injector.visioning_task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.CommonMasterDBQuery;
import com.reach.injector.constant.Constant;
import com.reach.injector.constant.Constant.ProcessStatus;
import com.reach.injector.content.vo.StringtagListDomain;
import com.reach.injector.profile.ReachDataTypeBase;
import com.reach.injector.profile.ReachDataTypeProcessor;
import com.reach.injector.vo.TaskMasterVo;
import com.reach.util.DBUtil;
import com.reach.util.ReachCodes.ReachWeekDay;

public class InsertTaskMasterData {

	private   final Integer STATUS_ACTIVE = 1;

	private   Workbook workbook = null;

	private   Connection conn = null;

	private   PrintStream outputLogger = null;
	private   PrintStream inputFileIssuesLogger = null;
	private   PrintStream sqlQueriesLogger = null;

	private   HashMap<String, Integer> hmTaskTypes = null;
	private   HashMap<String, Integer> hmTaskSubTypes = null;
	private   HashMap<String, Integer> hmTaskSubSubTypes = null;
	private   HashMap<String, String> hmTaskLabels = null;
	private   List<String> taskSubTypeList = null;
	private   HashMap<String, Boolean> hmTaskTags = null;
	private   HashMap<String, Integer> hmTaskMaster = null;
	private   HashMap<String, Integer> hmTaskGroup = null;

	static HashMap<String, ReachDataTypeBase> reachDataType;

	private static HashMap<String, String> hmFieldMap;

	public static void main(String[] args) {
		try {

			processTaskData();

//			
//			init();
//			String str = "UserData/Profile/Height";
//			ReachDataTypeBase obj = getReachDataType(str);
//			System.out.println("___ " + obj.title);
//		
//			loadTaskTypes();
//			loadTaskSubTypes();
//			loadTasSubkSubTypes();
//			loadTaskLebels();
//			addStaticTaskType();
//			updateProfileTaskData();
//			loadTaskTags();
//			updateTaskTypes();
//			updateTaskSubTypes();
//			updateTaskSubSubTypes();
//			updateTaskTags();
//			
//			updateTaskMaster();
//			updateTaskHabitMap();
//			
//			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void processTaskData() throws EncryptedDocumentException, InvalidFormatException, DBError, SQLException, IOException {
		
		
		InsertTaskMasterData m=new InsertTaskMasterData();
		
		try {
			m.processTaskDataNow();
			} finally { 
				
				if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
				}
			}
		
	}
	
	

	private void processTaskDataNow() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		System.out.println(new Date() + " ___ Task Process Started..");
		init();

		loadTaskTypes();
		loadTaskSubTypes();
		loadTasSubkSubTypes();
		loadTaskLebels();
		loadTaskTags();
		updateTaskTypes();
		updateTaskSubTypes();
		updateTaskSubSubTypes();
		updateTaskTags();
		loadTaskMaster();
		updateTaskMaster();
		updateTaskHabitMap();
		addStaticTaskType();
		updateProfileTaskData();

		System.out.println(new Date() + " ___ Task Process Finished..");
	}

	private   void init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		// loadSheets();

		conn = DBUtil.getConnection();
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/task_insertMasterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/task_issueData.log")));
		sqlQueriesLogger = new PrintStream(new FileOutputStream(new File("logs/task_sqlQueries.log")));

		hmTaskTypes = new HashMap<String, Integer>();
		hmTaskSubTypes = new HashMap<String, Integer>();
		hmTaskSubSubTypes = new HashMap<String, Integer>();
		hmTaskLabels = new HashMap<String, String>();
		taskSubTypeList = new ArrayList<>();
		hmTaskTags = new HashMap<String, Boolean>();
		hmTaskMaster = new HashMap<String, Integer>();
		hmTaskGroup = new HashMap<String, Integer>();

		reachDataType = new HashMap<>();
		
		ReachDataTypeProcessor reachDataTypeProcessor=ReachDataTypeProcessor.instance();
		
		reachDataTypeProcessor.populateReachDataType();
		reachDataType = reachDataTypeProcessor.reachDataType;
		reachDataType.putAll(reachDataTypeProcessor.reachDataTypeManual);

		hmFieldMap = StaticFieldParams.getStaticFieldMap();

	}

	private   void loadTaskTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskTypes from database....");
		Statement stmt = conn.createStatement();
		String selectTaskTypesQuery = "select * from task_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int taskTypeId = rs.getInt("id");
				String taskType = rs.getString("title");
//				System.out.println("taskType=" + taskType);
				if (hmTaskTypes.containsKey(taskType)) {
					throw new IOException("Duplicate Entry  AS integer[ " + taskType + " ] in TaskTypes table");
				} else {
					hmTaskTypes.put(taskType, taskTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskTypes.size() + " TaskTypes in database....");
		outputLogger.println("===============");
	}

	private   void loadTaskSubTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskSubTypes from database....");
		Statement stmt = conn.createStatement();
		String selectTaskSubTypesQuery = "select * from task_sub_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskSubTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int taskTypeId = rs.getInt("id");
				String taskType = rs.getString("title");
				if (hmTaskSubTypes.containsKey(taskType)) {
					throw new IOException("Duplicate Entry  AS integer[ " + taskType + " ] in TaskSubTypes table");
				} else {
					hmTaskSubTypes.put(taskType, taskTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskSubTypes.size() + " TaskSubTypes in database....");
		outputLogger.println("===============");
	}

	private   void loadTasSubkSubTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskSubSubTypes from database....");
		Statement stmt = conn.createStatement();
		String selectVisionTypesQuery = "select * from task_sub_sub_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectVisionTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				if (hmTaskSubSubTypes.containsKey(title)) {
					throw new IOException("Duplicate Entry  AS integer[ " + title + " ] in TaskSubSubTypes table");
				} else {
					hmTaskSubSubTypes.put(title, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskSubSubTypes.size() + " TaskSubSubTypes in database....");
		outputLogger.println("===============");
	}

	private   void loadTaskLebels() throws SQLException {
		outputLogger.println("Loading TaskTypes from input file....");
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_TASK_LABEL);
		Iterator<Row> rowIterator = sheet.rowIterator();

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getCell(Constant.TaskLabelColumnsIndex.TASK_TYPE_COLUMN_INDEX) == null || row.getCell(Constant.TaskLabelColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim().equals(""))
				break;
			else {
				String taskType = row.getCell(Constant.TaskLabelColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
				String taskLabel = row.getCell(Constant.TaskLabelColumnsIndex.TASK_LEBELE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");

				if (!hmTaskLabels.containsKey(taskType)) {
					hmTaskLabels.put(taskType, taskLabel);
				}
			}
		}
		outputLogger.println("loaded " + hmTaskLabels.size() + " Tasklabels ....");
		outputLogger.println("===============");
	}

	private   void loadTaskTags() throws SQLException, IOException {
		outputLogger.println("Loading TasTags from database....");
		Statement stmt = conn.createStatement();
		String selectTaskSubTypesQuery = "select * from tags_criteria";
		ResultSet rs = stmt.executeQuery(selectTaskSubTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				String tag = rs.getString("tag");
				Boolean include = rs.getBoolean("include");
				if (hmTaskTags.containsKey(tag)) {
					throw new IOException("Duplicate Entry TasTags[ " + tag + " ] in TasTags table");
				} else {
					hmTaskTags.put(tag, include);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskTags.size() + " TasTags in database....");
		outputLogger.println("===============");
	}

	private   void loadTaskMaster() throws SQLException, IOException {
		outputLogger.println("Loading TaskMaster from database....");
		Statement stmt = conn.createStatement();
		String selectTaskSubTypesQuery = "select * from task_masters where status=1";
		ResultSet rs = stmt.executeQuery(selectTaskSubTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				String title = rs.getString("title");
				int id = rs.getInt("id");
				String csTaskId = rs.getString("cs_task_id");
				if (hmTaskMaster.containsKey(csTaskId)) {
//					throw new IOException("Duplicate Entry TaskMaster[ " + title + " ] in TaskMaster table");
				} else {
					hmTaskMaster.put(csTaskId, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskMaster.size() + " TaskMaster in database....");
		outputLogger.println("===============");
		
		outputLogger.flush();
		
	}

	private   void updateTaskTypes() throws SQLException {
		outputLogger.println("Updating TaskTypes from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into task_types (title) values (?)", java.sql.Statement.RETURN_GENERATED_KEYS);
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getCell(Constant.DropdownColumnsIndex.TASK_TYPE_COLUMN_INDEX) == null || row.getCell(Constant.DropdownColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim().equals(""))
				break;
			else {
				String taskType = row.getCell(Constant.DropdownColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue();
//				System.out.println(taskType);
				taskType = taskType.trim().replaceAll(" +", " ");
				outputLogger.println("Got TaskType[ " + taskType + " ] from input file, checking if it exists in database or not.");
				if (hmTaskTypes.containsKey(taskType)) {
					outputLogger.println("TaskType[ " + taskType + " ] already exists in database.");
				} else {
					outputLogger.println("TaskType[ " + taskType + " ] does not exist in database. So, inserting in DB");
					pstmt.setString(1, taskType);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						hmTaskTypes.put(taskType, (int) key);
					}
					rs.close();
				}
			}
		}
		pstmt.close();
		outputLogger.println("Inserted " + cnt + " TaskTypes in database....");
		outputLogger.println("===============");
		outputLogger.flush();
	}

	private   void addStaticTaskType() throws DBError {
		try {
			String title1 = "Bundle";
			String title2 = "Bundle/Profile";
			int taskTypeId = 0, taskSubTypeId = 0;
			if (!hmTaskTypes.containsKey(title1)) {
				taskTypeId = CommonMasterDBQuery.InsertTaskTypeData(title1, 1);
				hmTaskTypes.put(title1, taskTypeId);
			} else {
				taskTypeId = hmTaskTypes.get(title1);
				System.out.println("task Type[ " + title1 + " ] already exist");
			}

			if (!hmTaskSubTypes.containsKey(title2)) {
				taskSubTypeId = CommonMasterDBQuery.InsertSubTaskTypeData(title2, 1, taskTypeId);
				hmTaskSubTypes.put(title2, taskSubTypeId);
			} else {
				taskSubTypeId = hmTaskSubTypes.get(title2);
				System.out.println("task sub Type[ " + title2 + " ] already exist");
			}

			if (!hmTaskSubSubTypes.containsKey(title2)) {
				int taskSubSubTypeId = CommonMasterDBQuery.InsertSubSubTaskTypeData(title2, taskSubTypeId, true, "Update");
				hmTaskSubSubTypes.put(title2, taskSubSubTypeId);
			} else {
				System.out.println("task sub sub Type[ " + title2 + " ] already exist");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private   void updateTaskSubTypes() throws SQLException {
		outputLogger.println("Updating TaskSubTypes from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into task_sub_types (title, tt_id) values (?,?)", java.sql.Statement.RETURN_GENERATED_KEYS);
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();

			if (row.getCell(Constant.DropdownColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX) == null
					|| row.getCell(Constant.DropdownColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().equals(""))
				break;
			else {

				String taskSubType = row.getCell(Constant.DropdownColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");

				if (!taskSubTypeList.contains(taskSubType)) {
					taskSubTypeList.add(taskSubType);
				}
			}
		}

//		System.out.println("taskSubTypeList.size()=" + taskSubTypeList.size());
		if (taskSubTypeList.size() > 0) {
			for (int i = 0; i < taskSubTypeList.size(); i++) {
				String taskSubType = taskSubTypeList.get(i);

				String str = taskSubType.substring(0, taskSubType.lastIndexOf("/"));
//				System.out.println("getting task type str=" + str);
				int taskTypeId = 0;
				if (hmTaskTypes.get(str) != null) {
					taskTypeId = hmTaskTypes.get(str);
				} else {
					str = str.substring(0, str.lastIndexOf("/"));
					taskTypeId = hmTaskTypes.get(str);
				}

				outputLogger.println("Got TaskType[ " + taskTypeId + " ] and taskSubType[ " + taskSubType + " ] from input file, checking if it exists in database or not.");
				if (hmTaskSubTypes.containsKey(taskSubType)) {
					outputLogger.println("TaskSubType[ " + taskSubType + " ] already exists in database.");
				} else {
					outputLogger.println("TaskSubType[ " + taskSubType + " ] does not exist in database. So, inserting in DB");
					pstmt.setString(1, taskSubType);
					pstmt.setInt(2, taskTypeId);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						hmTaskSubTypes.put(taskSubType, (int) key);
					}
					rs.close();
				}
			}
		}

		pstmt.close();
		outputLogger.println("Inserted " + cnt + " TaskSubTypes in database....");
		outputLogger.println("===============");
		
		outputLogger.flush();
	}

	private   void updateTaskSubSubTypes() throws SQLException {
		outputLogger.println("Updating TaskSubSubTypes from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into task_sub_sub_types (title, tst_id, label, virtual) values (?,?,?,?)", java.sql.Statement.RETURN_GENERATED_KEYS);
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		List<String> taskSubSubTypeList = new ArrayList<>();

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();

			if (row.getCell(Constant.DropdownColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX) == null
					|| row.getCell(Constant.DropdownColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().equals(""))
				break;
			else {
				String taskSubSubType = row.getCell(Constant.DropdownColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
				if (!taskSubSubTypeList.contains(taskSubSubType)) {
//					System.out.println("____ adding sub sub type " + taskSubSubType);
					taskSubSubTypeList.add(taskSubSubType);
				}
//				System.out.println(taskSubSubTypeList.size());
			}
		}

		System.out.println("inserting taskSubTypeList in to task_sub_sub_types. taskSubTypeList.size()=" + taskSubTypeList.size());
		if (taskSubTypeList.size() > 0) {
			for (int i = 0; i < taskSubTypeList.size(); i++) {
				String taskSubType = taskSubTypeList.get(i);

				String str = taskSubType;
				int taskTypeId = 0;
				if (hmTaskSubTypes.get(str) != null) {
					taskTypeId = hmTaskSubTypes.get(str);
				} else {
					if (str.lastIndexOf("/") > 0) {
						str = str.substring(0, str.lastIndexOf("/"));
						if (hmTaskSubTypes.get(str) != null) {
							taskTypeId = hmTaskSubTypes.get(str);
						} else {
							if (str.lastIndexOf("/") > 0) {
								str = str.substring(0, str.lastIndexOf("/"));
								taskTypeId = hmTaskSubTypes.get(str);
							}
						}
					}
				}

				outputLogger.println("Got TaskType[ " + taskTypeId + " ] and taskSubType[ " + taskSubType + " ] from input file, checking if it exists in database or not.");
				if (hmTaskSubSubTypes.containsKey(taskSubType)) {
					outputLogger.println("TaskSubSubType[ " + taskSubType + " ] already exists in database.");
				} else {
					outputLogger.println("TaskSubSubType[ " + taskSubType + " ] does not exist in database. So, inserting in DB");
					String str2 = taskSubType;
					String label = hmTaskLabels.get(str2);
					if ((label == null || label.equals("")) && str2.lastIndexOf("/") > 0) {
						str2 = str2.substring(0, str2.lastIndexOf("/"));
						label = hmTaskLabels.get(str2);
					}
					if ((label == null || label.equals("")) && str2.lastIndexOf("/") > 0) {
						str2 = str2.substring(0, str2.lastIndexOf("/"));
						label = hmTaskLabels.get(str2);
					}
					if (label == null) {
						label = "";
					}
					pstmt.setString(1, taskSubType);
					pstmt.setInt(2, taskTypeId);
					pstmt.setString(3, label);
					pstmt.setBoolean(4, true);
//					System.out.println(pstmt);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						hmTaskSubSubTypes.put(taskSubType, (int) key);
					}
					rs.close();
				}
			}
		}

//		System.out.println("taskSubSubTypeList.size()=" + taskSubSubTypeList.size());
		if (taskSubSubTypeList.size() > 0) {
			for (int i = 0; i < taskSubSubTypeList.size(); i++) {
				String taskSubSubType = taskSubSubTypeList.get(i);

				int taskSubTypeId = 0;

				String str = taskSubSubType;

				if (hmTaskSubTypes.get(str) != null) {
					taskSubTypeId = hmTaskSubTypes.get(str);
				} else {
					if (str.lastIndexOf("/") > 0) {
						str = str.substring(0, str.lastIndexOf("/"));
						if (hmTaskSubTypes.get(str) != null) {
							taskSubTypeId = hmTaskSubTypes.get(str);
						} else {
							if (str.lastIndexOf("/") > 0) {
								str = str.substring(0, str.lastIndexOf("/"));
								taskSubTypeId = hmTaskSubTypes.get(str);
							}
						}
					}

				}

				outputLogger.println("Got TaskSubType[ " + taskSubTypeId + " ] and taskSubSubType[ " + taskSubSubType + " ] from input file, checking if it exists in database or not.");
				if (hmTaskSubSubTypes.containsKey(taskSubSubType)) {
					outputLogger.println("TaskSubSubType[ " + taskSubSubType + " ] already exists in database.");
				} else {
					outputLogger.println("TaskSubSubType[ " + taskSubSubType + " ] does not exist in database. So, inserting in DB");
					String str2 = taskSubSubType;
					String label = hmTaskLabels.get(str2);
					if ((label == null || label.equals("")) && str2.lastIndexOf("/") > 0) {
						str2 = str2.substring(0, str2.lastIndexOf("/"));
						label = hmTaskLabels.get(str2);
					}
					if ((label == null || label.equals("")) && str2.lastIndexOf("/") > 0) {
						str2 = str2.substring(0, str2.lastIndexOf("/"));
						label = hmTaskLabels.get(str2);
					}
					if (label == null) {
						label = "";
					}

					pstmt.setString(1, taskSubSubType);
					pstmt.setInt(2, taskSubTypeId);
					pstmt.setString(3, label);
					pstmt.setBoolean(4, false);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						hmTaskSubSubTypes.put(taskSubSubType, (int) key);
					}
					rs.close();
				}
			}
		}

		pstmt.close();
		outputLogger.println("Inserted " + cnt + " TaskSubSubTypes in database....");
		outputLogger.println("===============");
		
		outputLogger.flush();
	}

	private   void updateTaskTags() {
		// TODO Auto-generated method stub

		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_TASK_TAG);

		Iterator<Row> rowIterator = sheet.rowIterator();
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				String tasktag = null;
				try {
					boolean iclusioncriteria = false;
					// Now let's iterate over the columns of the current row
					if (row.getCell(Constant.TaskTagColumnsIndex.TASK_TAG_COLUMN_INDEX) != null) {
						tasktag = row.getCell(Constant.TaskTagColumnsIndex.TASK_TAG_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
						iclusioncriteria = row.getCell(Constant.TaskTagColumnsIndex.TAG_INCLUSION_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").equals("Y");
						outputLogger.println("row Number [ " + row.getRowNum() + " ], Found TaskTag[ " + tasktag + " ] nad processing.");
						if (!tasktag.trim().equals("") && row.getRowNum() != 0) {
							if (!hmTaskTags.containsKey(tasktag)) {
								CommonMasterDBQuery.InsertTagData(tasktag, iclusioncriteria);
							} else {
								outputLogger.println("row Number [ " + row.getRowNum() + " ], TaskTag[ " + tasktag + " ] already exist in database.");
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					outputLogger.println("Error: -> row Number [ " + row.getRowNum() + " ], TaskTag[ " + tasktag + " ], error=" + e.getMessage());
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
			outputLogger.println("Error: -> while processing TaskTag. error=" + e.getMessage());
		}
		
		outputLogger.flush();
		
	}

	private   void updateTaskMaster() {
		// TODO Auto-generated method stub
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_TASK);

		Iterator<Row> rowIterator = sheet.rowIterator();
		int count = 0;
//		int countProcessing = 0;
//		int countInsert = 0;
		int countError = 0;

		System.out.println("---- TASK MASTER PROCESS STARTED --------------");

		int old = 0;
		int insert = 0;
		int update = 0;
		int delete = 0;
		
		try {

			boolean flagError = false;

			while (rowIterator.hasNext()) {
				flagError = false;
				count++;
				Row row = rowIterator.next();
				try {
					String csTaskId, title, description, param_csv, uin_option, uin_data_type, bundle_task_csv, dare_title=null;
					String[] tag_list = null;
					int tt_id = 0, tst_id = 0, tsst_id = 0, level = 0, freq = 0, duration = 0, task_day = 0, score = 0, status = 1;
					Integer user_id = null;
					Time time_start = null, time_end = null;
					boolean repeatable = true, dare = false, custom = false, tagable_output = false, global = true, bundle_task;
					// Now let's iterate over the columns of the current row
					if (row.getRowNum() != 0) {

						String statusColumn= null;
						
						if(row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX) != null) {
							statusColumn= row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX).getStringCellValue().trim();
						}

						if (statusColumn!=null && statusColumn.equalsIgnoreCase(Constant.ProcessStatus.insert)) {
							title = row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							description = row.getCell(Constant.TaskColumnsIndex.DESCRIPTION).getStringCellValue().trim().replaceAll(" +", " ");;
							param_csv = "";
							uin_option = "";
							uin_data_type = "";
							bundle_task_csv = "";
							csTaskId = null;

							if (row.getCell(Constant.TaskColumnsIndex.TASK_ID) != null && !row.getCell(Constant.TaskColumnsIndex.TASK_ID).toString().trim().equals("")) {
								csTaskId = row.getCell(Constant.TaskColumnsIndex.TASK_ID).toString().trim();
							}
							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX) != null) {
									tt_id = hmTaskTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Type is blank");
									flagError = true;
								}
							} catch (Exception e1) {
								outputLogger.println(
										"Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Getting TaskType from HashMap. Got error [ "
												+ e1.getMessage() + " ] in cellNo - " + row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).getAddress());
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX) != null) {
									if (hmTaskSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ")) != null) {
										tst_id = hmTaskSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
									} else {
										inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Sub Type["
												+ row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX) + "] not exist in Master");
										flagError = true;
									}
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Sub Type is blank");
									flagError = true;
								}
							} catch (Exception e1) {
								outputLogger.println(
										"Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Getting TaskSubType from HashMap. Got error [ "
												+ e1.getMessage() + " ] in cellNo - " + row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getAddress());
								e1.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).toString().equals("")) {
									tsst_id = hmTaskSubSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
								}
							} catch (Exception e1) {
								outputLogger.println("Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum()
										+ " ], Getting TaskSubSubType from HashMap. Got error [ " + e1.getMessage() + " ] in cellNo -"
										+ row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).getAddress());
								e1.printStackTrace();
							}

							if (tsst_id == 0) {
								try {
									tsst_id = hmTaskSubSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
								} catch (Exception e1) {
									outputLogger.println("Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum()
											+ " ], Getting TaskSubSubType from HashMap. Got error [ " + e1.getMessage() + " ] in cellNo - "
											+ row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getAddress());
									e1.printStackTrace();
								}
							}
							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX) != null) {
									if (Cell.CELL_TYPE_STRING == row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getCellType()) {

										String str = row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
										freq = Integer.parseInt(str.substring(str.indexOf("/") + 1, str.indexOf("/") + 2));

									} else if (Cell.CELL_TYPE_NUMERIC == row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getCellType()) {

										freq = (int) row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getNumericCellValue();
									}
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Frequency is blank");
									flagError = true;
								}

							} catch (Exception e) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Getting Frequency from Sheet. Got error [ "
										+ e.getMessage() + " ] in cellNo -" + row.getCell(5).getAddress());
								e.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).toString().trim().equals("")) {

									String strDuration = row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).toString().trim();
//									System.out.println(" 11111 " + strDuration);
									if (strDuration.contains(".")) {
										String arrDuration[] = strDuration.split("\\.");
//										System.out.println(" 22222 " + arrDuration.length);
										duration = (Integer.parseInt(arrDuration[0]) * Constant.DURATION_MULTIPLIER) + Integer.parseInt(arrDuration[1]);
									} else {
										duration = (int) row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).getNumericCellValue();
									}

								} else {
									inputFileIssuesLogger.println("Warning : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], title[" + title
											+ "] Duration is blank. processing with default value 0");
									duration = 0;
								}
							} catch (Exception e) {
								outputLogger.println("Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e.getMessage()
										+ " ] in cellNo -" + row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).getAddress());
								e.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).toString().trim().equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").equalsIgnoreCase("Weekend")) {
										task_day = ReachWeekDay._WeekEnd;
									} else if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").equalsIgnoreCase("All")) {
										task_day = ReachWeekDay._ALL;
									} else if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").equalsIgnoreCase("Weekday")) {
										task_day = ReachWeekDay._WeekDay;
									}
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Day is blank");
									flagError = true;
								}
							} catch (Exception e2) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e2.getMessage() + " ] in cellNo -"
										+ row.getCell(7).getAddress());

								e2.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_TIMESLOT_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_TIMESLOT_COLUMN_INDEX).toString().trim().equals("")) {
									String time = row.getCell(Constant.TaskColumnsIndex.TASK_TIMESLOT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									String[] timearray = time.substring(time.indexOf("(") + 1, time.indexOf(")")).split("-");

									SimpleDateFormat format = new SimpleDateFormat("HH.mm.ss");
									Calendar cal = Calendar.getInstance();

									String strD1 = timearray[0].trim().replace(" am", ".00").replace(" pm", ".00");
									String strD2 = timearray[1].trim().replace(" am", ".00").replace(" pm", ".00");
									cal.setTime(format.parse(strD1));
									java.util.Date d1;
									java.util.Date d2;

									if (timearray[0].contains("pm")) {
										cal.add(Calendar.HOUR, 12);
									}
									d1 = cal.getTime();

									cal.setTime(format.parse(strD2));
									cal.add(Calendar.MINUTE, -1);
									cal.set(Calendar.SECOND, 59);
									if (timearray[0].contains("pm") || timearray[1].contains("pm")) {
										cal.add(Calendar.HOUR, 12);
									}
									d2 = cal.getTime();

									time_start = new java.sql.Time(d1.getTime());
									time_end = new java.sql.Time(d2.getTime());
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Time Slot is blank.");
									flagError = true;
								}
							} catch (Exception e1) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
										+ row.getCell(8).getAddress());

								SimpleDateFormat format = new SimpleDateFormat("hh.mm.ss");
								java.util.Date d1 = (java.util.Date) format.parse("11.0.0");
								java.util.Date d2 = (java.util.Date) format.parse("15.59.59");

								time_start = new java.sql.Time(d1.getTime());
								time_end = new java.sql.Time(d2.getTime());

								e1.printStackTrace();
							}

							try {
								dare = row.getCell(Constant.TaskColumnsIndex.DARE_APPLICABILITY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("Y");
								if(dare) {
									dare_title = row.getCell(Constant.TaskColumnsIndex.DARE_TITLE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
								}
							} catch (Exception e1) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
										+ row.getCell(10).getAddress());

								e1.printStackTrace();
							}
							ArrayList<String> tagList = new ArrayList<>();

							// processing tags
							for (int i = Constant.TaskColumnsIndex.TASK_TAG_START_COLUMN_INDEX; i < Constant.TaskColumnsIndex.TASK_TAG_END_COLUMN_INDEX; i++) {
								if (row.getCell(i) != null && !row.getCell(i).getStringCellValue().trim().equals("")) {
									String tag = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
									tagList.add(tag);
								}
							}
							if (row.getRowNum() == 192) {
//								System.out.println("111111 _ tagList=" + tagList + ", size=" + tagList.size());
							}

							try {
								if (tagList.size() > 0) {
									StringtagListDomain listDomain = new StringtagListDomain();
									listDomain.tags = new ArrayList<>();
									for (String temp : tagList) {
										listDomain.tags.add(temp);
									}

									if (listDomain.tags.size() > 0) {

										tag_list = listDomain.tags.toArray(new String[listDomain.tags.size()]);
									}
								}
							} catch (Exception e2) {
								e2.printStackTrace();
							}

							if (title.contains("<") && title.contains(">")) {
								String str = title.substring(title.indexOf("<"), title.indexOf(">") + 1);
//								System.out.println("str="  +str);
								if (hmFieldMap.containsKey(str.toLowerCase())) {
									title = title.substring(0, title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">") + 1, title.length());
									param_csv = hmFieldMap.get(str.toLowerCase());
								} else {
									inputFileIssuesLogger.println(
											"____ Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Param Fields [" + str + "]  Undefined in the tile[" + title + "]");
								}
							}
							// get 2nd
							if (title.contains("<") && title.contains(">")) {
								String str = title.substring(title.indexOf("<"), title.indexOf(">") + 1);
//								System.out.println("str="  +str);
								if (hmFieldMap.containsKey(str.toLowerCase())) {
									title = title.substring(0, title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">") + 1, title.length());
									param_csv = param_csv + "," + hmFieldMap.get(str.toLowerCase());
								} else {
									inputFileIssuesLogger.println(
											"____ Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Param Fields [" + str + "]  Undefined in the tile[" + title + "]");
								}
							}
							// dynamic field value end

							String key = null;
							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).toString().trim().equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).toString().trim().split("/").length > 3) {
										key = row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split("/")[2];
										tagable_output = reachDataType.containsKey(key);
									}

								}
							} catch (Exception e) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
										+ row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).getAddress());

								e.printStackTrace();
							}
							try {
								if (tagable_output) {
									uin_option = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else {
									uin_option = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).toString().trim().equals("")) {
									String str = row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).toString().trim();
									ReachDataTypeBase reachDataTypeBase = getReachDataType(str);
									if (reachDataTypeBase != null) {
										uin_data_type = reachDataTypeBase.title;
										uin_option = reachDataTypeBase.selection;
									}
								}
								if (uin_data_type == null || uin_data_type.equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX) != null
											&& !row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).toString().trim().equals("")) {
										String str = row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).toString().trim();
										ReachDataTypeBase reachDataTypeBase = getReachDataType(str);
										if (reachDataTypeBase != null) {
											uin_data_type = reachDataTypeBase.title;
											uin_option = reachDataTypeBase.selection;
										}
									}
								}
								if (uin_data_type == null || uin_data_type.equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX) != null
											&& !row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).toString().trim().equals("")) {
										String str = row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).toString().trim();
										ReachDataTypeBase reachDataTypeBase = getReachDataType(str);
										if (reachDataTypeBase != null) {
											uin_data_type = reachDataTypeBase.title;
											uin_option = reachDataTypeBase.selection;
										}
									}
								}

							} catch (Exception e1) {
								e1.printStackTrace();
							}

							if (uin_data_type == null || uin_data_type.equals("")) {
								inputFileIssuesLogger.println("row number [" + row.getRowNum() + "],Task Master,    title[" + title + "], Task Master uin_data_type[" + uin_data_type + "] is null");
							} else {
								bundle_task = false;
								bundle_task_csv = "";
								if (!title.trim().equals("")) {
									if (!flagError) {

										if (hmTaskMaster.containsKey(csTaskId)) {
											inputFileIssuesLogger.println("row number [" + row.getRowNum() + "], Task Master, title[" + title + "],  Task exist in DB but flag is INSERT IN FILE");
										} else {
											outputLogger.println("row number [" + row.getRowNum() + "], Task Master title[" + title + "], inserting into DB...");
											int taskGroupId = 0;
											if (hmTaskGroup.containsKey(title)) {
												taskGroupId = hmTaskGroup.get(title);
												int id = CommonMasterDBQuery.InsertTaskMaster(title, csTaskId, taskGroupId, tt_id, tst_id, tsst_id, level, repeatable, freq, duration, time_start, time_end,
														task_day, score, dare, dare_title, custom, user_id, description, param_csv, tagable_output, uin_option, uin_data_type, global, bundle_task, bundle_task_csv,
														status, tag_list);
												hmTaskMaster.put(csTaskId, id);
											} else {
												int id = CommonMasterDBQuery.InsertTaskMaster(title, csTaskId, taskGroupId, tt_id, tst_id, tsst_id, level, repeatable, freq, duration, time_start,
														time_end, task_day, score, dare, dare_title, custom, user_id, description, param_csv, tagable_output, uin_option, uin_data_type, global, bundle_task,
														bundle_task_csv, status, tag_list);
												hmTaskGroup.put(title, id);
												hmTaskMaster.put(csTaskId, id);
											}
											insert++;
										}
									} else {
										outputLogger.println("row number [" + row.getRowNum() + "], some issue");
										countError++;
									}
								}
							}
						} // insert close
						else if (statusColumn!=null && statusColumn.equalsIgnoreCase(Constant.ProcessStatus.update)) {

							title = row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							description = row.getCell(Constant.TaskColumnsIndex.DESCRIPTION).getStringCellValue().trim().replaceAll(" +", " ");;
							param_csv = "";
							uin_option = "";
							uin_data_type = "";
							bundle_task_csv = "";
							csTaskId = null;

							if (row.getCell(Constant.TaskColumnsIndex.TASK_ID) != null && !row.getCell(Constant.TaskColumnsIndex.TASK_ID).toString().trim().equals("")) {
								csTaskId = row.getCell(Constant.TaskColumnsIndex.TASK_ID).toString().trim();
							}
							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX) != null) {
									tt_id = hmTaskTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Type is blank");
									flagError = true;
								}
							} catch (Exception e1) {
								outputLogger.println(
										"Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Getting TaskType from HashMap. Got error [ "
												+ e1.getMessage() + " ] in cellNo - " + row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).getAddress());
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX) != null) {
									if (hmTaskSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ")) != null) {
										tst_id = hmTaskSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
									} else {
										inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Sub Type["
												+ row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX) + "] not exist in Master");
										flagError = true;
									}
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Sub Type is blank");
									flagError = true;
								}
							} catch (Exception e1) {
								outputLogger.println(
										"Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Getting TaskSubType from HashMap. Got error [ "
												+ e1.getMessage() + " ] in cellNo - " + row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getAddress());
								e1.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).toString().equals("")) {
									tsst_id = hmTaskSubSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
								}
							} catch (Exception e1) {
								outputLogger.println("Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum()
										+ " ], Getting TaskSubSubType from HashMap. Got error [ " + e1.getMessage() + " ] in cellNo -"
										+ row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).getAddress());
								e1.printStackTrace();
							}

							if (tsst_id == 0) {
								try {
									tsst_id = hmTaskSubSubTypes.get(row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " "));
								} catch (Exception e1) {
									outputLogger.println("Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum()
											+ " ], Getting TaskSubSubType from HashMap. Got error [ " + e1.getMessage() + " ] in cellNo - "
											+ row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).getAddress());
									e1.printStackTrace();
								}
							}
							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX) != null) {
									if (Cell.CELL_TYPE_STRING == row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getCellType()) {

										String str = row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
										freq = Integer.parseInt(str.substring(str.indexOf("/") + 1, str.indexOf("/") + 2));

									} else if (Cell.CELL_TYPE_NUMERIC == row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getCellType()) {

										freq = (int) row.getCell(Constant.TaskColumnsIndex.TASK_FREQUENCY_COLUMN_INDEX).getNumericCellValue();
									}
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Frequency is blank");
									flagError = true;
								}

							} catch (Exception e) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Getting Frequency from Sheet. Got error [ "
										+ e.getMessage() + " ] in cellNo -" + row.getCell(5).getAddress());
								e.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).toString().trim().equals("")) {

									String strDuration = row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).toString().trim();
//									System.out.println(" 11111 " + strDuration);
									if (strDuration.contains(".")) {
										String arrDuration[] = strDuration.split("\\.");
//										System.out.println(" 22222 " + arrDuration.length);
										duration = (Integer.parseInt(arrDuration[0]) * Constant.DURATION_MULTIPLIER) + Integer.parseInt(arrDuration[1]);
									} else {
										duration = (int) row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).getNumericCellValue();
									}

								} else {
									inputFileIssuesLogger.println("Warning : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], title[" + title
											+ "] Duration is blank. processing with default value 0");
									duration = 0;
								}
							} catch (Exception e) {
								outputLogger.println("Exception : -> Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e.getMessage()
										+ " ] in cellNo -" + row.getCell(Constant.TaskColumnsIndex.TASK_DURATION_COLUMN_INDEX).getAddress());
								e.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).toString().trim().equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").equalsIgnoreCase("Weekend")) {
										task_day = ReachWeekDay._WeekEnd;
									} else if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").equalsIgnoreCase("All")) {
										task_day = ReachWeekDay._ALL;
									} else if (row.getCell(Constant.TaskColumnsIndex.TASK_DAY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").equalsIgnoreCase("Weekday")) {
										task_day = ReachWeekDay._WeekDay;
									}
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Task Day is blank");
									flagError = true;
								}
							} catch (Exception e2) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e2.getMessage() + " ] in cellNo -"
										+ row.getCell(7).getAddress());

								e2.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_TIMESLOT_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_TIMESLOT_COLUMN_INDEX).toString().trim().equals("")) {
									String time = row.getCell(Constant.TaskColumnsIndex.TASK_TIMESLOT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
									String[] timearray = time.substring(time.indexOf("(") + 1, time.indexOf(")")).split("-");

									SimpleDateFormat format = new SimpleDateFormat("HH.mm.ss");
									Calendar cal = Calendar.getInstance();

									String strD1 = timearray[0].trim().replace(" am", ".00").replace(" pm", ".00");
									String strD2 = timearray[1].trim().replace(" am", ".00").replace(" pm", ".00");
									cal.setTime(format.parse(strD1));
									java.util.Date d1;
									java.util.Date d2;

									if (timearray[0].contains("pm")) {
										cal.add(Calendar.HOUR, 12);
									}
									d1 = cal.getTime();

									cal.setTime(format.parse(strD2));
									cal.add(Calendar.MINUTE, -1);
									cal.set(Calendar.SECOND, 59);
									if (timearray[0].contains("pm") || timearray[1].contains("pm")) {
										cal.add(Calendar.HOUR, 12);
									}
									d2 = cal.getTime();

									time_start = new java.sql.Time(d1.getTime());
									time_end = new java.sql.Time(d2.getTime());
								} else {
									inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Time Slot is blank.");
									flagError = true;
								}
							} catch (Exception e1) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
										+ row.getCell(8).getAddress());

								SimpleDateFormat format = new SimpleDateFormat("hh.mm.ss");
								java.util.Date d1 = (java.util.Date) format.parse("11.0.0");
								java.util.Date d2 = (java.util.Date) format.parse("15.59.59");

								time_start = new java.sql.Time(d1.getTime());
								time_end = new java.sql.Time(d2.getTime());

								e1.printStackTrace();
							}

							try {
								dare = row.getCell(Constant.TaskColumnsIndex.DARE_APPLICABILITY_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").trim().equals("Y");
								if(dare) {
									dare_title = row.getCell(Constant.TaskColumnsIndex.DARE_TITLE_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
								}
							} catch (Exception e1) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e1.getMessage() + " ] in cellNo - "
										+ row.getCell(10).getAddress());

								e1.printStackTrace();
							}
							ArrayList<String> tagList = new ArrayList<>();

							// processing tags
							for (int i = Constant.TaskColumnsIndex.TASK_TAG_START_COLUMN_INDEX; i < Constant.TaskColumnsIndex.TASK_TAG_END_COLUMN_INDEX; i++) {
								if (row.getCell(i) != null && !row.getCell(i).getStringCellValue().trim().equals("")) {
									String tag = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
									tagList.add(tag);
								}
							}
							if (row.getRowNum() == 192) {
//								System.out.println("111111 _ tagList=" + tagList + ", size=" + tagList.size());
							}

							try {
								if (tagList.size() > 0) {
									StringtagListDomain listDomain = new StringtagListDomain();
									listDomain.tags = new ArrayList<>();
									for (String temp : tagList) {
										listDomain.tags.add(temp);
									}

									if (listDomain.tags.size() > 0) {

										tag_list = listDomain.tags.toArray(new String[listDomain.tags.size()]);
									}
								}
							} catch (Exception e2) {
								e2.printStackTrace();
							}

							if (title.contains("<") && title.contains(">")) {
								String str = title.substring(title.indexOf("<"), title.indexOf(">") + 1);
//								System.out.println("str="  +str);
								if (hmFieldMap.containsKey(str.toLowerCase())) {
									title = title.substring(0, title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">") + 1, title.length());
									param_csv = hmFieldMap.get(str.toLowerCase());
								} else {
									inputFileIssuesLogger.println(
											"____ Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Param Fields [" + str + "]  Undefined in the tile[" + title + "]");
								}
							}
							// get 2nd
							if (title.contains("<") && title.contains(">")) {
								String str = title.substring(title.indexOf("<"), title.indexOf(">") + 1);
//								System.out.println("str="  +str);
								if (hmFieldMap.containsKey(str.toLowerCase())) {
									title = title.substring(0, title.indexOf("<")) + hmFieldMap.get(str.toLowerCase()) + title.substring(title.indexOf(">") + 1, title.length());
									param_csv = param_csv + "," + hmFieldMap.get(str.toLowerCase());
								} else {
									inputFileIssuesLogger.println(
											"____ Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Param Fields [" + str + "]  Undefined in the tile[" + title + "]");
								}
							}
							// dynamic field value end

							String key = null;
							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).toString().trim().equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).toString().trim().split("/").length > 3) {
										key = row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ").split("/")[2];
										tagable_output = reachDataType.containsKey(key);
									}

								}
							} catch (Exception e) {
								outputLogger.println("Issue while processing sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], Got error [ " + e.getMessage() + " ] in cellNo -"
										+ row.getCell(Constant.TaskColumnsIndex.TASK_LEVEL_COLUMN_INDEX).getAddress());

								e.printStackTrace();
							}
							try {
								if (tagable_output) {
									uin_option = Constant.ANSWER_TYPE_SINGLE_OPTIONS;
								} else {
									uin_option = Constant.ANSWER_TYPE_TYPE_INPUT;
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							try {
								if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX) != null
										&& !row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).toString().trim().equals("")) {
									String str = row.getCell(Constant.TaskColumnsIndex.TASK_SUB_SUB_TYPE_COLUMN_INDEX).toString().trim();
									ReachDataTypeBase reachDataTypeBase = getReachDataType(str);
									if (reachDataTypeBase != null) {
										uin_data_type = reachDataTypeBase.title;
										uin_option = reachDataTypeBase.selection;
									}
								}
								if (uin_data_type == null || uin_data_type.equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX) != null
											&& !row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).toString().trim().equals("")) {
										String str = row.getCell(Constant.TaskColumnsIndex.TASK_SUB_TYPE_COLUMN_INDEX).toString().trim();
										ReachDataTypeBase reachDataTypeBase = getReachDataType(str);
										if (reachDataTypeBase != null) {
											uin_data_type = reachDataTypeBase.title;
											uin_option = reachDataTypeBase.selection;
										}
									}
								}
								if (uin_data_type == null || uin_data_type.equals("")) {
									if (row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX) != null
											&& !row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).toString().trim().equals("")) {
										String str = row.getCell(Constant.TaskColumnsIndex.TASK_TYPE_COLUMN_INDEX).toString().trim();
										ReachDataTypeBase reachDataTypeBase = getReachDataType(str);
										if (reachDataTypeBase != null) {
											uin_data_type = reachDataTypeBase.title;
											uin_option = reachDataTypeBase.selection;
										}
									}
								}

							} catch (Exception e1) {
								e1.printStackTrace();
							}

							if (uin_data_type == null || uin_data_type.equals("")) {
								inputFileIssuesLogger.println("row number [" + row.getRowNum() + "],Task Master,    title[" + title + "], Task Master uin_data_type[" + uin_data_type + "] is null");
							} else {
								bundle_task = false;
								bundle_task_csv = "";
								if (!title.trim().equals("")) {
									if (!flagError) {
										
										if (hmTaskMaster.containsKey(csTaskId)) {
											
											outputLogger.println("row number [" + row.getRowNum() + "], Task Master title[" + title + "], updating into DB...");
											int taskGroupId = 0;
											if (hmTaskGroup.containsKey(title)) {
												taskGroupId = hmTaskGroup.get(title);
											}
												
											CommonMasterDBQuery.updateTaskMaster(title, csTaskId, taskGroupId, tt_id, tst_id, tsst_id, level, repeatable, freq, duration, time_start, time_end,
														task_day, score, dare, dare_title, custom, user_id, description, param_csv, tagable_output, uin_option, uin_data_type, global, bundle_task, bundle_task_csv,
														status, tag_list, hmTaskMaster.get(csTaskId));
											CommonMasterDBQuery.deleteTaskHabitMappingByTaskId(hmTaskMaster.get(csTaskId));
											update++;
										} else {
											inputFileIssuesLogger.println("row number [" + row.getRowNum() + "], Task Master, title[" + title + "],  Not exist in DB while flag is UPDATE.");
										}
									} else {
										outputLogger.println("row number [" + row.getRowNum() + "], some issue");
										countError++;
									}
								}
							}
						}  // update block closed
						else if (statusColumn!=null && statusColumn.equalsIgnoreCase(Constant.ProcessStatus.delete)) {
							csTaskId = row.getCell(Constant.TaskColumnsIndex.TASK_ID).toString().trim();
							title = row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							if (hmTaskMaster.containsKey(csTaskId)) {
								CommonMasterDBQuery.deleteTaskMaster(hmTaskMaster.get(csTaskId));
								CommonMasterDBQuery.deleteTaskHabitMappingByTaskId(hmTaskMaster.get(csTaskId));
								delete++;
							}  else {
								inputFileIssuesLogger.println("row number [" + row.getRowNum() + "], Task Master, title[" + title + "],  Not exist in DB while flag is DELETE.");
							}
						} else if (statusColumn!=null && (statusColumn.equalsIgnoreCase(Constant.ProcessStatus.old) || statusColumn.equalsIgnoreCase(Constant.ProcessStatus.ignore))) {
							title = row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							outputLogger.println("row number [" + row.getRowNum() + "], title[" + title + "], flag is OLD");
							old++;
						}

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				// System.out.println();

			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		System.out.println("============== SUMMARY =============");
		System.out.println("TOTAL RECORDS = " + count);
		System.out.println("TOTAL ERROR ROWS = " + countError);
		
		outputLogger.println("======= TASK SUMMARY ========");
		outputLogger.println("Total Records: " + count);
		outputLogger.println("Total Old Records: " + count);
		outputLogger.println("Total Update Records: " + update);
		outputLogger.println("Total delete Records: " + delete);
		outputLogger.println("Total Insert Records: " + insert);
		
		outputLogger.flush();

	}

	private   void updateTaskHabitMap() {
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_TASK);
		Iterator<Row> rowIterator = sheet.rowIterator();
		System.out.println("---- TASK HABIT MAP PROCESS STARTED --------------");
		try {
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (row.getRowNum() != 0) {

					System.out.println("---- TASK HABIT MAP: Row[" + row.getRowNum() + "] ");

					if (row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX) != null
							&& row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX).getStringCellValue().trim().equalsIgnoreCase(Constant.ProcessStatus.insert)) {
						for (int i = Constant.TaskColumnsIndex.HABIT_START_COLUMN_INDEX; i <= Constant.TaskColumnsIndex.HABIT_END_COLUMN_INDEX; i++) {
							try {
								if (row.getCell(i) != null && !row.getCell(i).toString().trim().equals("")) {
									String habit = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
									if (habit != null && !habit.trim().equals("")) {
										int habitID = CommonMasterDBQuery.GetHabitIDFromTable(habit);
										if (habitID != 0) {
											String task = row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
											if (task != null) {
												TaskMasterVo taskIbj = CommonMasterDBQuery.GetTaskIDFromTask(task, row.getCell(Constant.TaskColumnsIndex.TASK_ID).toString());
												if (taskIbj != null) {
													CommonMasterDBQuery.InsertTaskHabitMapping(taskIbj.getId(), taskIbj.getTaskGroupId(), habitID, row.getRowNum(),
															Constant.TASK_GROUP_NORMAL_TASK_QUES);
													CommonMasterDBQuery.UpdateTaskGlobalStatus(taskIbj.getId(), false);
												}
											}
										} else {
											inputFileIssuesLogger.println("row number [" + row.getRowNum() + "],Task Master,    title["
													+ row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).toString().trim() + "], habit[" + habit + "] not exist in master Habit");
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								System.out.println(
										"Row Num[" + row.getRowNum() + "], title[" + row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX) + "], column[" + i + "], habit[" + row.getCell(i) + "]");
							}
						}
					} else if (row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX) != null
							&& row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX).getStringCellValue().trim().equalsIgnoreCase(Constant.ProcessStatus.update)) {
						for (int i = Constant.TaskColumnsIndex.HABIT_START_COLUMN_INDEX; i <= Constant.TaskColumnsIndex.HABIT_END_COLUMN_INDEX; i++) {
							try {
								if (row.getCell(i) != null && !row.getCell(i).toString().trim().equals("")) {
									String habit = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
									if (habit != null && !habit.trim().equals("")) {
										int habitID = CommonMasterDBQuery.GetHabitIDFromTable(habit);
										if (habitID != 0) {
											String task = row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
											if (task != null) {
												TaskMasterVo taskIbj = CommonMasterDBQuery.getTaskDetailBycsTaskId(row.getCell(Constant.TaskColumnsIndex.TASK_ID).toString());
												if (taskIbj != null) {
													CommonMasterDBQuery.InsertTaskHabitMapping(taskIbj.getId(), taskIbj.getTaskGroupId(), habitID, row.getRowNum(),
															Constant.TASK_GROUP_NORMAL_TASK_QUES);
													CommonMasterDBQuery.UpdateTaskGlobalStatus(taskIbj.getId(), false);
												}
											}
										} else {
											inputFileIssuesLogger.println("row number [" + row.getRowNum() + "],Task Master,    title["
													+ row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX).toString().trim() + "], habit[" + habit + "] not exist in master Habit");
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								System.out.println(
										"Row Num[" + row.getRowNum() + "], title[" + row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX) + "], column[" + i + "], habit[" + row.getCell(i) + "]");
							}
						}
					} else if (row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX) != null
							&& row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX).getStringCellValue().trim().equalsIgnoreCase(Constant.ProcessStatus.delete)) {
						// nothinf to do
					} else {
						// nothing to do
					}
					
					
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private   int updateProfileTaskData() {
		// TODO Auto-generated method stub
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_HABIT);
		int habitkey = 0;
		Iterator<Row> rowIterator = sheet.rowIterator();
		System.out.println("---- PROFILE TASK PROCESS STARTED --------------");

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			String profileQuestion = "";
			String habit = "", label_2 = "";
			int duration = 0;
			int frequency = 0;

			if (row.getRowNum() != 0 && row.getCell(0) != null && !row.getCell(0).toString().trim().equals("")) {
				try {
					
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_PROCESS_TYPE_COLUMN_INDEX) != null && row.getCell(Constant.HabitColumnsIndex.HABIT_PROCESS_TYPE_COLUMN_INDEX).toString().trim().equalsIgnoreCase(ProcessStatus.insert)) {	
						
						if (row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_1_COLUMN_INDEX) == null || row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_1_COLUMN_INDEX).toString().trim().equals("")) {
							if (row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX) != null
									&& !row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX).toString().trim().equals("")) {
								profileQuestion = row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							}
							if (!profileQuestion.trim().equals("")) {
								outputLogger.println("row number [" + row.getRowNum() + "], Found profile question [" + profileQuestion + "] and processing...");
								habit = row.getCell(Constant.HabitColumnsIndex.HABIT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
								habitkey = CommonMasterDBQuery.GetHabitIDFromTable(habit);
								if (habitkey != 0) {
									try {
										SimpleDateFormat format = new SimpleDateFormat("HH.mm.ss");
										java.util.Date d2 = (java.util.Date) format.parse("11.0.0");
										java.util.Date d1 = (java.util.Date) format.parse("15.59.59");
	
										Time time_start = new java.sql.Time(d1.getTime());
										Time time_end = new java.sql.Time(d2.getTime());
										TaskMasterVo taskObj = CommonMasterDBQuery.GetTaskIDFromTask(profileQuestion);
										duration = duration + taskObj.getDuration();
										frequency = taskObj.getFrequency();
										if (taskObj != null) {
											long bundletaskID = CommonMasterDBQuery.InsertTaskMaster(Constant.PROFILE_BUNDLE_TASK_TITLE, null, 0, hmTaskTypes.get(Constant.PROFILE_TASK_TYPE),
													hmTaskSubTypes.get(Constant.PROFILE_TASK_SUB_TYPE), hmTaskSubSubTypes.get(Constant.PROFILE_TASK_SUB_SUB_TYPE), 0, Constant.TASK_REAPEATABLE_FALSE,
													frequency, duration, time_start, time_end, 0, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, 0, null, "", Constant.TASK_TAGABLE_FALSE, "", "",
													Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_TRUE, taskObj.getId() + "", Constant.STATUS_ACTIVE, null);
											CommonMasterDBQuery.InsertTaskHabitMapping(bundletaskID, bundletaskID, habitkey, row.getRowNum(), Constant.TASK_GROUP_PROFILE_QUES);
											CommonMasterDBQuery.UpdateTaskGlobalStatus(bundletaskID, false);
											outputLogger.println("row number [" + row.getRowNum() + "], Iserted profile question");
										} else {
											inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], profileQuestion [" + profileQuestion
													+ "] does not exist in Task Master table");
										}
	
									} catch (Exception e) {
										e.printStackTrace();
										outputLogger.println("row number [" + row.getRowNum() + "], some issue while iserting profile question");
									}
								}
	
							}
						} else {
							habit = row.getCell(Constant.HabitColumnsIndex.HABIT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
							habitkey = CommonMasterDBQuery.GetHabitIDFromTable(habit);
							if (habitkey != 0) {
								try {
	
									SimpleDateFormat format = new SimpleDateFormat("HH.mm.ss");
									java.util.Date d1 = (java.util.Date) format.parse("11.0.0");
									java.util.Date d2 = (java.util.Date) format.parse("15.59.59");
	
									Time time_start = new java.sql.Time(d1.getTime());
									Time time_end = new java.sql.Time(d2.getTime());
									profileQuestion = row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_1_COLUMN_INDEX).getStringCellValue().trim();
	
									if (row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX) != null
											&& !row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX).toString().trim().equals("")) {
										String profileQuestion2 = row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
										TaskMasterVo taskObj1 = CommonMasterDBQuery.GetTaskIDFromTask(profileQuestion);
										duration = duration + taskObj1.getDuration();
										frequency = taskObj1.getFrequency();
	
										TaskMasterVo taskObj2 = CommonMasterDBQuery.GetTaskIDFromTask(profileQuestion2);
										if (taskObj1 != null && taskObj2 != null) {
											duration = duration + taskObj2.getDuration();
											frequency = taskObj2.getFrequency();
	
											String taskID = taskObj1.getId() + "," + taskObj2.getId();
											long bundletaskID = CommonMasterDBQuery.InsertTaskMaster(Constant.PROFILE_BUNDLE_TASK_TITLE, null, 0, hmTaskTypes.get(Constant.PROFILE_TASK_TYPE),
													hmTaskSubTypes.get(Constant.PROFILE_TASK_SUB_TYPE), hmTaskSubSubTypes.get(Constant.PROFILE_TASK_SUB_SUB_TYPE), 0, Constant.TASK_REAPEATABLE_FALSE,
													frequency, duration, time_start, time_end, 0, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, 0, null, "", Constant.TASK_TAGABLE_FALSE, "", "",
													Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_TRUE, taskID + "", Constant.STATUS_ACTIVE, null);
											CommonMasterDBQuery.InsertTaskHabitMapping(bundletaskID, bundletaskID, habitkey, row.getRowNum(), Constant.TASK_GROUP_PROFILE_QUES);
											CommonMasterDBQuery.UpdateTaskGlobalStatus(bundletaskID, false);
										} else {
											if (taskObj2 == null) {
												inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], profileQuestion2 [" + profileQuestion2
														+ "] does not exist in TaskMaster table");
											}
										}
									} else {
										outputLogger.println("row number [" + row.getRowNum() + "], profileQuestion[" + profileQuestion + "] processing...");
										TaskMasterVo taskObj = CommonMasterDBQuery.GetTaskIDFromTask(profileQuestion);
										if (taskObj != null) {
											duration = duration + taskObj.getDuration();
											frequency = taskObj.getFrequency();
											long bundletaskID = CommonMasterDBQuery.InsertTaskMaster(Constant.PROFILE_BUNDLE_TASK_TITLE, null, 0, hmTaskTypes.get(Constant.PROFILE_TASK_TYPE),
													hmTaskSubTypes.get(Constant.PROFILE_TASK_SUB_TYPE), hmTaskSubSubTypes.get(Constant.PROFILE_TASK_SUB_SUB_TYPE), 0, Constant.TASK_REAPEATABLE_FALSE,
													frequency, duration, time_start, time_end, 0, 0, Constant.TASK_DARE_FALSE, null, Constant.TASK_CUSTOM_FALSE, 0, null, "", Constant.TASK_TAGABLE_FALSE, "", "",
													Constant.TASK_GLOBAL_FALSE, Constant.TASK_BUNDLE_TRUE, taskObj.getId() + "", Constant.STATUS_ACTIVE, null);
											CommonMasterDBQuery.InsertTaskHabitMapping(bundletaskID, bundletaskID, habitkey, row.getRowNum(), Constant.TASK_GROUP_PROFILE_QUES);
											CommonMasterDBQuery.UpdateTaskGlobalStatus(bundletaskID, false);
											outputLogger.println("row number [" + row.getRowNum() + "], Iserted profile question");
										} else {
											inputFileIssuesLogger.println("Error : -> sheet [ " + sheet.getSheetName() + " ] row [ " + row.getRowNum() + " ], profileQuestion [" + profileQuestion
													+ "] does not exist in Task Master table");
										}
	
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		
		outputLogger.flush();
		
		return habitkey;
	}

	private   ReachDataTypeBase getReachDataType(String str) {
		ReachDataTypeBase ret = null;
		String str2 = "";
		if (str.lastIndexOf("/") > 0) {
			str2 = str.substring(str.lastIndexOf("/") + 1, str.length());
			str = str.substring(0, str.lastIndexOf("/"));
			if (reachDataType.containsKey(str2)) {
				ret = reachDataType.get(str2);
			} else if (str.lastIndexOf("/") > 0) {
				str2 = str.substring(str.lastIndexOf("/") + 1, str.length());
				str = str.substring(0, str.lastIndexOf("/"));
				if (reachDataType.containsKey(str2)) {
					ret = reachDataType.get(str2);
				}
			} else {
				ret = reachDataType.get(str2);
			}
		} else {
			ret = reachDataType.get(str);
		}

		return ret;
	}

}