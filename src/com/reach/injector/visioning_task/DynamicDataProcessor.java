package com.reach.injector.visioning_task;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.CommonMasterDBQuery;
import com.reach.injector.constant.Constant;
import com.reach.injector.content.vo.TaskMasterUtil;
import com.reach.util.DBUtil;

public class DynamicDataProcessor {

	private   final Integer STATUS_ACTIVE = 1;
	
	private   Workbook workbook = null;

	private   PrintStream outputLogger = null;
	private   PrintStream inputFileIssuesLogger = null;
	
	private   HashMap<String, Integer> hmTaskTypes = null;
	private   HashMap<String, Integer> hmTaskSubTypes = null;
	private   HashMap<String, Integer> hmTaskSubSubTypes = null;
	private   HashMap<String, String> hmTaskLabels = null;
	
	private   Connection conn = null;
	
	public static void main(String[] args) throws EncryptedDocumentException, DBError, InvalidFormatException, SQLException, IOException {
		process();
	}
	
	private   void init() throws DBError, SQLException, IOException, EncryptedDocumentException, InvalidFormatException {
		conn = DBUtil.getConnection();
		
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/dynamicdata_insertMasterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/dynamicdata_issueData.log")));
		 
		hmTaskTypes = new HashMap<String, Integer>();
		hmTaskSubTypes = new HashMap<String, Integer>();
		hmTaskSubSubTypes = new HashMap<String, Integer>();
		hmTaskLabels = new HashMap<String, String>();
		
		loadTaskTypes();
		loadTaskSubTypes();
		loadTasSubkSubTypes();
		loadTaskLebels();
		
	}
	
	public static void process() throws EncryptedDocumentException, DBError, InvalidFormatException, SQLException, IOException {
		
		DynamicDataProcessor m= new DynamicDataProcessor();
		
		try {
			m.processNow();
			} finally { 
				
				if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }
				}
			}
		
	}
	
	private  void processNow() throws EncryptedDocumentException, DBError, InvalidFormatException, SQLException, IOException {
		System.out.println("--- process started");
		init();
		updateDynamicDataBundle();
		System.out.println("--- process ficished..");	
	}
	
	private   void loadTaskTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskTypes from database....");
		Statement stmt = conn.createStatement();
		String selectTaskTypesQuery = "select * from task_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int taskTypeId = rs.getInt("id");
				String taskType = rs.getString("title");
//				System.out.println("taskType=" + taskType);
				if (hmTaskTypes.containsKey(taskType)) {
					throw new IOException("Duplicate Entry  AS integer[ " + taskType + " ] in TaskTypes table");
				} else {
					hmTaskTypes.put(taskType, taskTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskTypes.size() + " TaskTypes in database....");
		outputLogger.println("===============");
	}
	
	private   void loadTaskSubTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskSubTypes from database....");
		Statement stmt = conn.createStatement();
		String selectTaskSubTypesQuery = "select * from task_sub_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectTaskSubTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int taskTypeId = rs.getInt("id");
				String taskType = rs.getString("title");
				if (hmTaskSubTypes.containsKey(taskType)) {
					throw new IOException("Duplicate Entry  AS integer[ " + taskType + " ] in TaskSubTypes table");
				} else {
					hmTaskSubTypes.put(taskType, taskTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskSubTypes.size() + " TaskSubTypes in database....");
		outputLogger.println("===============");
	}
	
	private   void loadTasSubkSubTypes() throws SQLException, IOException {
		outputLogger.println("Loading TaskSubSubTypes from database....");
		Statement stmt = conn.createStatement();
		String selectVisionTypesQuery = "select * from task_sub_sub_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectVisionTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				if (hmTaskSubSubTypes.containsKey(title)) {
					throw new IOException("Duplicate Entry  AS integer[ " + title + " ] in TaskSubSubTypes table");
				} else { 
					hmTaskSubSubTypes.put(title, id);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + hmTaskSubSubTypes.size() + " TaskSubSubTypes in database....");
		outputLogger.println("===============");
	}
	
	private   void loadTaskLebels() throws SQLException {
		outputLogger.println("Loading TaskTypes from input file....");
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_TASK_LABEL);
		Iterator<Row> rowIterator = sheet.rowIterator();

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getCell(Constant.TaskLabelColumnsIndex.TASK_TYPE_COLUMN_INDEX) == null
					|| row.getCell(Constant.TaskLabelColumnsIndex.TASK_TYPE_COLUMN_INDEX).getStringCellValue().trim()
							.equals(""))
				break;
			else {
				String taskType = row.getCell(Constant.TaskLabelColumnsIndex.TASK_TYPE_COLUMN_INDEX)
						.getStringCellValue().trim().replaceAll(" +", " ");
				String taskLabel = row.getCell(Constant.TaskLabelColumnsIndex.TASK_TYPE_COLUMN_INDEX)
						.getStringCellValue().trim().replaceAll(" +", " ");

				if (!hmTaskLabels.containsKey(taskType)) {
					hmTaskLabels.put(taskType, taskLabel);
				}
			}
		}
		outputLogger.println("loaded " + hmTaskLabels.size() + " Tasklabels ....");
		outputLogger.println("===============");
	}
	
	
	private   void updateDynamicDataBundle() {
		// TODO Auto-generated method stub
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_HABIT);
		String label = "";

		if(hmTaskLabels.containsKey("UserData/DynamicData/")) {
			label = hmTaskLabels.get("UserData/DynamicData/");
		}
		
		String csTaskId = null;
		int taskGroupId = 0;
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			// 35 to 41
			String taskmasterUds = "";
			TaskMasterUtil masterUtil = null;
			Time startTime = null,endTime = null;
			int duration = 0;
			Row row = rowIterator.next();
			if (row.getRowNum() != 0) {
				for (int i = 35; i < 42; i++) {
					String dynamicData = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
					if (hmTaskSubSubTypes.containsKey(dynamicData)) {
						outputLogger.println("Row No [" + row.getRowNum() + "] dynamicData[" + dynamicData + "] exist in subsubtype");
						masterUtil = CommonMasterDBQuery.getMasterTaskDetailsBySubSubTaskId(hmTaskSubSubTypes.get(dynamicData));
						if (masterUtil.getId() != 0) {
							if (!taskmasterUds.equalsIgnoreCase("")) {
								taskmasterUds = taskmasterUds + ",";
							}
							if(startTime!=null){
								if(startTime.after(masterUtil.getTime_start())){
									startTime=masterUtil.getTime_start();
								}
							}else{
								startTime=masterUtil.getTime_start();
							}
							if(endTime!=null){
								if(endTime.before(masterUtil.getTime_end())){
									endTime=masterUtil.getTime_end();
								}
							}else{
								endTime=masterUtil.getTime_end();
							}
							duration = duration + masterUtil.getDuration();
							taskmasterUds = taskmasterUds + masterUtil.getId();
						}

					} else {
						outputLogger.println("Row No [" + row.getRowNum() + "] dynamicData[" + dynamicData + "] does not exist in subsubtype");
					}
				}

				String habit = row.getCell(Constant.HabitColumnsIndex.HABIT_COLUMN_INDEX).getStringCellValue().trim().replaceAll(" +", " ");
				int habitkey = CommonMasterDBQuery.GetTaskIDFromHabits(habit);
				
				if (habitkey != 0) {
					try {
						if (!taskmasterUds.equals("")) {
//							System.out.println("row["+row.getRowNum()+"],habitkey["+habitkey+"],  habit["+habit+"], taskmasterUds["+taskmasterUds+"]");
							int bundletaskID = CommonMasterDBQuery.InsertTaskMaster(Constant.DYNAMIC_BUNDLE_TASK_TITLE, csTaskId, taskGroupId,
									hmTaskTypes.get("UserData"), hmTaskSubTypes.get("UserData/DynamicData"),
									hmTaskSubSubTypes.get("UserData/DynamicData"), masterUtil.getLevel(), true,
									masterUtil.getFreq(), duration, startTime,
									endTime, masterUtil.getTask_day(), masterUtil.getScore(),
									Constant.TASK_DARE_FALSE, null, false, 0, null, "", false, Constant.ANSWER_TYPE_SINGLE_OPTIONS, "", false, true,
									taskmasterUds, 1, null);
//							System.out.println("bundletaskID=" +bundletaskID);
							CommonMasterDBQuery.InsertTaskHabitMapping(bundletaskID, bundletaskID, habitkey, row.getRowNum(), Constant.TASK_GROUP_DYNAMIC_DATA);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}

		}
	}
}
