package com.reach.injector.visioning_task;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.err.DBError;
import com.reach.injector.constant.Constant;
import com.reach.injector.constant.Constant.ProcessStatus;
import com.reach.util.DBUtil;

public class InsertVisionMasterData {

	private final Integer STATUS_ACTIVE = 1;

	private Workbook workbook = null;

	private Connection conn = null;

	private PrintStream outputLogger = null;
	private PrintStream outputLoggerInsert = null;
	private PrintStream outputLoggerUpdate = null;
	private PrintStream outputLoggerDelete = null;
	private PrintStream inputFileIssuesLogger = null;
	private PrintStream sqlQueriesLogger = null;

	private HashMap<String, Integer> visionTypes = null;
	private HashMap<String, Integer> visions = null;
	private HashMap<Integer, Integer> visionsMap = null;

	 public static void main(String[] args) throws IOException,
	 InvalidFormatException, SQLException, EncryptedDocumentException, DBError {
		 System.out.println("hello");
		 processVisionData();
	 }

	public static void processVisionData() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		
		InsertVisionMasterData m=new InsertVisionMasterData();
		
		
		
		
		try {
			m.init();
			m.loadVisionTypes();
			m.loadVisions();
			m.updateVisionTypes();
			m.updateVisions();
		
		} finally { 
			
			if(m.conn!=null) { try { m.conn.close(); } catch (Exception e) { }}
		}
		

	}

	private void init() throws SQLException, IOException, EncryptedDocumentException, InvalidFormatException, DBError {
		workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
		// loadSheets();

		conn = DBUtil.getConnection();
		outputLogger = new PrintStream(new FileOutputStream(new File("logs/vision_MsterData.log")));
		outputLoggerInsert = new PrintStream(new FileOutputStream(new File("logs/vision_insertMsterData.log")));
		outputLoggerUpdate = new PrintStream(new FileOutputStream(new File("logs/vision_updateMsterData.log")));
		outputLoggerDelete = new PrintStream(new FileOutputStream(new File("logs/vision_deleteMsterData.log")));
		inputFileIssuesLogger = new PrintStream(new FileOutputStream(new File("logs/vision_issueData.log")));
		sqlQueriesLogger = new PrintStream(new FileOutputStream(new File("logs/vision_sqlQueries.log")));

		visionTypes = new HashMap<String, Integer>();
		visions = new HashMap<String, Integer>();
		visionsMap = new HashMap<Integer, Integer>();
	}

	private void loadVisionTypes() throws SQLException, IOException {
		outputLogger.println("Loading VisionTypes from database....");
		Statement stmt = conn.createStatement();
		String selectVisionTypesQuery = "select * from vision_types where status=" + STATUS_ACTIVE;
		ResultSet rs = stmt.executeQuery(selectVisionTypesQuery);
		if (rs != null) {
			while (rs.next()) {
				int visionTypeId = rs.getInt("id");
				String visionType = rs.getString("title");
				if (visionTypes.containsKey(visionType)) {
					throw new IOException("Duplicate Entry    AS integer[ " + visionType + " ] in VisionTypes table");
				} else {
					visionTypes.put(visionType, visionTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + visionTypes.size() + " VisionTypes in database....");
		outputLogger.println("===============");
	}

	private void loadVisions() throws SQLException, IOException {
		outputLogger.println("Loading Visions from database....");
		Statement stmt = conn.createStatement();
		String selectVisionsQuery = "select v.id, v.title, v.cs_vision_id, vt.id as vt_id from visions v, vision_types vt where v.status="
				+ STATUS_ACTIVE + " and v.v_type_id=vt.id";
		ResultSet rs = stmt.executeQuery(selectVisionsQuery);
		if (rs != null) {
			while (rs.next()) {
				int visionId = rs.getInt("id");
				String vision = rs.getString("title");
				String csVisionId = rs.getString("cs_vision_id");
				int visionTypeId = rs.getInt("vt_id");
				if (visions.containsKey(csVisionId)) {
					throw new IOException("Duplicate Vision[ " + vision + " ], csVisionId[ " + csVisionId + " ] in Visions table");
				} else {
					visions.put(csVisionId, visionId);
					visionsMap.put(visionId, visionTypeId);
				}
			}
		}
		rs.close();
		stmt.close();
		outputLogger.println("Found " + visions.size() + " Visions in database....");
		outputLogger.println("===============");
	}

	private void updateVisionTypes() throws SQLException {
		outputLogger.println("Updating VisionTypes from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into vision_types (title) values (?)",
				java.sql.Statement.RETURN_GENERATED_KEYS);
		
		
		PreparedStatement pstmtUpdate = conn.prepareStatement("update vision_types  set title=? where )",
				java.sql.Statement.RETURN_GENERATED_KEYS);
		
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_DROPDOWN);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			String visionType = row.getCell(Constant.DropdownColumnsIndex.VISION_TYPE_COLUMN_INDEX).getStringCellValue();
			if (visionType == null || visionType.equals(""))
				break;
			else {
				visionType = visionType.trim().replaceAll(" +", " ");
				outputLogger.println("Got VisionType[ " + visionType
						+ " ] from input file, checking if it exists in database or not.");
				if (visionTypes.containsKey(visionType)) {
					outputLogger.println("VisionType[ " + visionType + " ] already exists in database.");
				} else {
					outputLogger.println(
							"VisionType[ " + visionType + " ] does not exist in database. So, inserting in DB");
					pstmt.setString(1, visionType);
					pstmt.executeUpdate();
					cnt++;

					ResultSet rs = pstmt.getGeneratedKeys();

					if (rs != null && rs.next()) {
						long key = rs.getLong(1);
						visionTypes.put(visionType, (int) key);
					}
					rs.close();
				}
			}
		}
		pstmt.close();
		outputLogger.println("Inserted " + cnt + " VisionTypes in database....");
		outputLogger.println("===============");
	}

	private void updateVisions() throws SQLException {
		outputLogger.println("Updating Visions from input file....");
		PreparedStatement pstmt = conn.prepareStatement("insert into visions (title, v_type_id, vuid, cs_vision_id) values (?,?,?,?)",
				Statement.RETURN_GENERATED_KEYS);
		PreparedStatement pstmtUpdate = conn.prepareStatement("update visions set title=?, v_type_id=? where id=?");
		PreparedStatement pstmtDelete = conn.prepareStatement("update visions set status=" + Constant.STATUS_INACTIVE + " where id=?");
		
		Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_VISION);
		Iterator<Row> rowIterator = sheet.rowIterator();

		int cnt = 0;
		int old = 0;
		int insert = 0;
		int update = 0;
		int delete = 0;
				

		// Ignore header row
		if (rowIterator.hasNext())
			rowIterator.next();

		int rowNum = 0;
		while (rowIterator.hasNext()) {
			rowNum++;
			Row row = rowIterator.next();
			String csVisionId = row.getCell(Constant.VisionColumnsIndex.CS_VISION_ID_COLUMN_INDEX).getStringCellValue();
			String processType = row.getCell(Constant.VisionColumnsIndex.VISION_PROCESS_TYPE_COLUMN_INDEX).getStringCellValue();
			String vision = row.getCell(Constant.VisionColumnsIndex.VISION_COLUMN_INDEX).getStringCellValue();
			String visionType = row.getCell(Constant.VisionColumnsIndex.VISION_TYPE_COLUMN_INDEX).getStringCellValue();
			if (vision == null || vision.equals(""))
				break;
			else {
				vision = vision.trim().replaceAll(" +", " ");
				processType = processType.trim().replaceAll(" +", " ").toUpperCase();
				visionType = visionType.trim().replaceAll(" +", " ");

				cnt++;
				if (processType.equalsIgnoreCase(ProcessStatus.old)) {
					old++;
					outputLogger.println(
							"Got Vision[ " + vision + " ] from input file, its Process type is OLD, so not processing");
					continue;
				} else if (processType.equalsIgnoreCase(ProcessStatus.update)) {
					
					outputLogger.println(
							"Got Vision[ " + vision + " ] from input file, checking if it exists in database or not.");
					if (visions.containsKey(csVisionId)) {
						outputLogger.println("Vision[ " + vision + " ], csVisionId[ " + csVisionId + " ] already exists in database. so updating...");
						
						Integer visionTypeId = visionTypes.get(visionType);
						if (visionTypeId == null) {
							inputFileIssuesLogger
									.println("Error while processing Sheet[ " + sheet.getSheetName() + " ]. Row[ " + rowNum
											+ " ] ==> Unable to insert Vision [ " + vision + " ] as VisionType[ "
											+ visionType + " ] as it does not exist in VisionType Masterdata.");
						} else {
							update++;
							pstmtUpdate.setString(1, vision);
							pstmtUpdate.setInt(2, visionTypeId);
							pstmtUpdate.setInt(3, visions.get(csVisionId));
							pstmtUpdate.executeUpdate();
							outputLoggerUpdate.println(pstmtUpdate);
							
						}
					} else {
						inputFileIssuesLogger.println(
								"Vision[ " + vision + " ], csVisionId[ " + csVisionId + " ] not exists in database. so can not UPDATE...");
					}
					
				} else if (processType.equalsIgnoreCase(ProcessStatus.delete)) {
					
					outputLogger.println(
							"Got Vision[ " + vision + " ] from input file, checking if it exists in database or not.");
					if (visions.containsKey(csVisionId)) {
						delete++;
						outputLogger.println("Vision[ " + vision + " ], csVisionId[ " + csVisionId + " ] already exists in database. so deleting...");
						pstmtDelete.setInt(1, visionsMap.get(csVisionId));
						pstmtDelete.executeUpdate();
						outputLoggerDelete.println(pstmtDelete);
					} else {
						inputFileIssuesLogger.println(
								"Vision[ " + vision + " ], csVisionId[ " + csVisionId + " ] not exists in database. so can not DELETE...");
					}
				} else if (processType.equalsIgnoreCase(ProcessStatus.insert)) {
					
					outputLogger.println(
							"Got Vision[ " + vision + " ] from input file, checking if it exists in database or not.");
					if (visions.containsKey(csVisionId)) {
						inputFileIssuesLogger.println(
								"Vision[ " + vision + " ], csVisionId[ " + csVisionId + " ] already exists in database. so can not INSERT...");
					} else {
						outputLogger.println("Vision[ " + vision + " ] does not exist in database. So, inserting in DB");
						Integer visionTypeId = visionTypes.get(visionType);
						if (visionTypeId == null) {
							inputFileIssuesLogger
									.println("Error while processing Sheet[ " + sheet.getSheetName() + " ]. Row[ " + rowNum
											+ " ] ==> Unable to insert Vision [ " + vision + " ] as VisionType[ "
											+ visionType + " ] as it does not exist in VisionType Masterdata.");
						} else {
							insert++;
							pstmt.setString(1, vision);
							pstmt.setInt(2, visionTypeId);
							pstmt.setObject(3, UUID.randomUUID());
							pstmt.setString(4, csVisionId);
							pstmt.executeUpdate();
							outputLoggerInsert.println(pstmt);
							
							ResultSet rs = pstmt.getGeneratedKeys();
	
							if (rs != null && rs.next()) {
								int key = rs.getInt(1);
								visions.put(vision, key);
								visionsMap.put(key, visionTypeId);
							}
							rs.close();
							outputLogger.println("Inserted " + cnt + " Visions in database....");
							
						}
					}
				}
			}
		}
		pstmt.close();
		pstmtDelete.close();
		pstmtUpdate.close();
		outputLogger.println("======= VISIONS SUMMARY ========");
		outputLogger.println("Total Records: " + cnt);
		outputLogger.println("Total Old Records: " + old);
		outputLogger.println("Total Update Records: " + update);
		outputLogger.println("Total delete Records: " + delete);
		outputLogger.println("Total Insert Records: " + insert);
		outputLogger.println("===============");

		outputLogger.println("VisionTypes hashmap output....");
		Set<String> keys = visionTypes.keySet();
		Iterator<String> itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			int value = visionTypes.get(key);
			outputLogger.println(key + " : " + value);
		}
		outputLogger.println("===============");
		outputLogger.println("Visions hashmap output....");
		keys = visions.keySet();
		itr = keys.iterator();
		while (itr.hasNext()) {
			String key = itr.next();
			int value = visions.get(key);
			outputLogger.println(key + " : " + value);
		}
		outputLogger.println("===============");
		outputLogger.println("Visions-VisionTypes mapping hashmap output....");
		Set<Integer> keys1 = visionsMap.keySet();
		Iterator<Integer> itr1 = keys1.iterator();
		while (itr1.hasNext()) {
			int key = itr1.next();
			int value = visionsMap.get(key);
			outputLogger.println(key + " : " + value);
		}
	}
}