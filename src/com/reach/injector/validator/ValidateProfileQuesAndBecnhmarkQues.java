package com.reach.injector.validator;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.injector.constant.Constant;
import com.reach.util.DBUtil;

public class ValidateProfileQuesAndBecnhmarkQues {

	private static Workbook workbook = null;
	
	private static HashMap<String, Integer> hmTaskMaster = null;
	private static Connection con = null;
	private static List<String> questionList;
	private static List<String> benchmarkList;
	
	public static void main(String[] args) {
		System.out.println("Process Started");
		loadTaskMaster();
		loadProfileQues();
		validateProfileQues();
		System.out.println("================");
		loadBenchmarkQues();
		validateBenchmarkQues();
		
		System.out.println("Process Finished");
	}

	private static void loadTaskMaster() { 
		
		String sqlTask = "select title, count(*) as ct FROM task_masters group by title order by title";
		PreparedStatement psTask = null;
		ResultSet rsTask = null;
		try {
			
			workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
			hmTaskMaster = new HashMap<String, Integer>();
			con = DBUtil.getConnection();
			questionList = new ArrayList<>();
			benchmarkList = new ArrayList<>();
			
			psTask = con.prepareStatement(sqlTask);
			rsTask = psTask.executeQuery();
			while(rsTask.next()) {
				hmTaskMaster.put(rsTask.getString("title"), rsTask.getInt("ct"));
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
			if(psTask != null) {
				psTask.close();
				psTask = null;
			}
			
			if(rsTask != null) {
				rsTask.close();
				rsTask = null;
			}
			} catch(SQLException eq) {
				
			}
		}
	}
	
	private static void loadProfileQues() { 
		
		try {
			
			workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
			questionList = new ArrayList<>();
			
			
			Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_HABIT);
			
			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				String profileQuestion = "";
				if (row.getRowNum() != 0 && row.getCell(0)!=null && !row.getCell(0).toString().trim().equals("")) {
					if (row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_1_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_1_COLUMN_INDEX).toString().trim().equals("")) {
						profileQuestion = row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_1_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!questionList.contains(profileQuestion)) {
							questionList.add(profileQuestion);
						}
					}
					if (row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX).toString().trim().equals("")) {
						profileQuestion = row.getCell(Constant.HabitColumnsIndex.PROFILE_QUE_2_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!questionList.contains(profileQuestion)) {
							questionList.add(profileQuestion);
						}
					}
				}
			}
					
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}
	
	private static void loadBenchmarkQues() { 
		
		try {
			
			workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
			questionList = new ArrayList<>();
			
			
			Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_HABIT);
			
			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				String benchmarkQuestion = "";
				if (row.getRowNum() != 0 && row.getCell(0)!=null && !row.getCell(0).toString().trim().equals("")) {
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_COLUMN_INDEX).toString().trim().equals("")) {
						benchmarkQuestion = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_1_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!benchmarkList.contains(benchmarkQuestion)) {
							benchmarkList.add(benchmarkQuestion);
						}
					}
					
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_COLUMN_INDEX).toString().trim().equals("")) {
						benchmarkQuestion = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!benchmarkList.contains(benchmarkQuestion)) {
							benchmarkList.add(benchmarkQuestion);
						}
					}
					
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_3_COLUMN_INDEX).toString().trim().equals("")) {
						benchmarkQuestion = row.getCell(Constant.HabitColumnsIndex.HABIT_BENCHMRK_QUE_2_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!benchmarkList.contains(benchmarkQuestion)) {
							benchmarkList.add(benchmarkQuestion);
						}
					}
					
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_COLUMN_INDEX).toString().trim().equals("")) {
						benchmarkQuestion = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_1_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!benchmarkList.contains(benchmarkQuestion)) {
							benchmarkList.add(benchmarkQuestion);
						}
					}
					
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_COLUMN_INDEX).toString().trim().equals("")) {
						benchmarkQuestion = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_2_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!benchmarkList.contains(benchmarkQuestion)) {
							benchmarkList.add(benchmarkQuestion);
						}
					}
					
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_COLUMN_INDEX).toString().trim().equals("")) {
						benchmarkQuestion = row.getCell(Constant.HabitColumnsIndex.HABIT_ASSESMNT_QUE_3_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!benchmarkList.contains(benchmarkQuestion)) {
							benchmarkList.add(benchmarkQuestion);
						}
					}
					
					if (row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_COLUMN_INDEX) != null && !row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_COLUMN_INDEX).toString().trim().equals("")) {
						benchmarkQuestion = row.getCell(Constant.HabitColumnsIndex.HABIT_TRACKING_QUE_1_COLUMN_INDEX).toString().trim().replaceAll(" +", " ");
						if(!benchmarkList.contains(benchmarkQuestion)) {
							benchmarkList.add(benchmarkQuestion);
						}
					}
					
				}
			}
					
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}
	
	private static void validateProfileQues() { 
		try {

			for(int i=0;i<questionList.size();i++) { 
				if(hmTaskMaster.containsKey(questionList.get(i)) && hmTaskMaster.get(questionList.get(i))>1) {
					System.out.println("_________ profile question["+ questionList.get(i) +"] size[" + hmTaskMaster.get(questionList.get(i)) + "]");
				}
			}
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void validateBenchmarkQues() { 
		try {

			for(int i=0;i<benchmarkList.size();i++) { 
				if(hmTaskMaster.containsKey(benchmarkList.get(i)) && hmTaskMaster.get(benchmarkList.get(i))>1) {
					System.out.println("_________ benchmark question["+ benchmarkList.get(i) +"] size[" + hmTaskMaster.get(benchmarkList.get(i)) + "]");
				}
			}
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
