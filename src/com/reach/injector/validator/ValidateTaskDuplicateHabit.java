package com.reach.injector.validator;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.reach.injector.constant.Constant;
import com.reach.injector.profile.ReachDataTypeBase;

public class ValidateTaskDuplicateHabit {

	private static Workbook workbook = null;
	
	public static void main(String[] args) {
		process();
	}
	
	private static void process() {
		try {
			workbook = WorkbookFactory.create(new File(Constant.INPUT_DATA_FILE_PATH));
			
	Sheet sheet = workbook.getSheet(Constant.SHEET_NAME_TASK);
			
			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (row.getRowNum() != 0) {
					if(row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX) != null && row.getCell(Constant.TaskColumnsIndex.STATUS_COLUMN_INDEX).getStringCellValue().trim().equalsIgnoreCase(Constant.ProcessStatus.insert)) {
						HashMap<String, Integer> hmHabit = new HashMap<>();
						for (int i = Constant.TaskColumnsIndex.HABIT_START_COLUMN_INDEX; i <= Constant.TaskColumnsIndex.HABIT_END_COLUMN_INDEX; i++) {
							try {
								if( row.getCell(i) != null && ! row.getCell(i).toString().trim().equals("")) {
									String habit = row.getCell(i).getStringCellValue().trim().replaceAll(" +", " ");
									if (habit != null && !habit.trim().equals("")) {
										if(hmHabit.containsKey(habit)) {
											int count = hmHabit.get(habit);
											hmHabit.put(habit, count+1);
										} else {
											hmHabit.put(habit, 1);
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								System.out.println("Row Num["+row.getRowNum()+"], title["+row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX)+"], column["+i+"], habit["+row.getCell(i)+"]");
							}
						}
						
						if(hmHabit.size()>0) {
							
							Iterator<?> it = hmHabit.entrySet().iterator();
							while (it.hasNext()) {
								Map.Entry pair = (Map.Entry) it.next();
								String key = (String) pair.getKey();
								int ct = (Integer) pair.getValue();
//								System.out.println(row.getRowNum() + ",key="+ key + ", value=" + ct);
								if(ct>1) {
									System.out.println("Row Num["+row.getRowNum()+"], Task["+row.getCell(Constant.TaskColumnsIndex.TASK_COLUMN_INDEX)+"],      Habit[" + key + "] is DUPLICATE,  repeatition count[" + ct + "]");
								}
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
