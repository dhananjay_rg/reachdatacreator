/**
 * 
 */
package com.reach.patch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.reach.util.DBUtil;
import com.reach.util.Encryption;
import com.reach.util.ReachJsonMapper;
import com.reach.util.ReachProperties;
import com.reach.util.Tupple3;

/**
 * @author siddharthasinghnegi
 *
 */
public class RGSSOPatientPartnerUpdator {

    public static void main(String[] args) {
        process();
    }

    public static void process() {
        System.out.println("RGSSOPatientPartnerUpdator process Started..");
        Connection con = null;
        String findCorpIdAndEmailQuery = " select u.uuid,u.email_id,c.cuid from users u join corporates c on (u.corp_id=c.id) where u.email_id is not null and u.email_id!='' and u.status=1 and c.status=1 ";
        PreparedStatement findCorpIdAndEmailPS = null;
        ResultSet findCorpIdAndEmailRS = null;
        try {
            con = DBUtil.getConnection();
            findCorpIdAndEmailPS = con.prepareStatement(findCorpIdAndEmailQuery);
            findCorpIdAndEmailRS = findCorpIdAndEmailPS.executeQuery();
            if (findCorpIdAndEmailRS.isBeforeFirst()) {
                Encryption enc = new Encryption();
                List<Tupple3<UUID, String, UUID>> patientPartnerList = new ArrayList<>();
                while (findCorpIdAndEmailRS.next()) {
                    patientPartnerList.add(new Tupple3<UUID, String, UUID>(UUID.fromString(findCorpIdAndEmailRS.getString("uuid")), enc.decrypt(findCorpIdAndEmailRS.getString("email_id")),
                            UUID.fromString(findCorpIdAndEmailRS.getString("cuid"))));
                }

                if (!patientPartnerList.isEmpty()) {
                    System.out.println(patientPartnerList);
                    for (Tupple3<UUID, String, UUID> tupple3 : patientPartnerList) {
                        upddatePArtnerInRGSSO(tupple3);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("RGSSOPatientPartnerUpdator Process Finished...");
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private static void upddatePArtnerInRGSSO(Tupple3<UUID, String, UUID> partner) {
        // [first=d3319d0b-312d-4b50-8936-ea22d1bf3f68,
        // second=siddhartha.negi+3@obino.in,
        // third=3a5a5400-b807-11e8-804c-99d3e3f9e336]
        try {
            HttpClient client = HttpClientBuilder.create().build();
            String urlTemplate = ReachProperties.instance.getRGSSOAddPartnerInPatientProfileURL();
            String url = urlTemplate.replace("{uuid}", partner.getFirst().toString());
            url = url.replace("{partnerId}", partner.getThird().toString());//
            System.out.println("url  " + url);
            HttpPost httpPost = new HttpPost(url);

            ArrayNode identifierArray = ReachJsonMapper.mapper.createArrayNode();

            JsonNode identifierNode = ReachJsonMapper.mapper.createObjectNode();

            ((ObjectNode) identifierNode).put("identifier_code", "email");
            ((ObjectNode) identifierNode).put("identifier_value", partner.getSecond());// "siddhartha.negi+3@obino.in");

            identifierArray.add(identifierNode);

            StringEntity entity = new StringEntity(ReachJsonMapper.mapper.valueToTree(identifierArray).toString());
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            httpPost.setHeader("project_id", ReachProperties.instance.getReachRGProjectId());
            httpPost.setHeader("client_id", ReachProperties.instance.getReachRGClientId());
            httpPost.setHeader("rg_application_version", ReachProperties.instance.getReachRGAppVersion());
            httpPost.setHeader("rg_application", ReachProperties.instance.getReachRGAppPlatform());
            httpPost.setHeader("rg_project_id", ReachProperties.instance.getReachRGProjectId());

            System.out.println(httpPost.toString());

            System.out.println(entity.toString());

            HttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            System.out.println(result.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
