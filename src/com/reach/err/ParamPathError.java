package com.reach.err;

public class ParamPathError extends ReachException {

	
	private static final long serialVersionUID = 1L;


	public ParamPathError(String message) {
		super(message);
	}

	
	public ParamPathError(String message, Throwable cause) {
		super(message, cause);
	}

	

}
