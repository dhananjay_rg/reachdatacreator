package com.reach.err;

public class ReachException extends Exception{
	
	private static final long serialVersionUID = 1L;

	public ReachException(String message) {
		super(message);
	}

	
	public ReachException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
