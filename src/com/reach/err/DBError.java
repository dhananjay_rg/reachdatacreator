package com.reach.err;

public class DBError extends ReachException {

	
	private static final long serialVersionUID = 1L;


	public DBError(String message) {
		super(message);
	}

	
	public DBError(String message, Throwable cause) {
		super(message, cause);
	}

	

}
