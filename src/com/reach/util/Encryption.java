package com.reach.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class Encryption {

	private String encrypt(String key, String salt, String value) {
		try {
			IvParameterSpec iv = new IvParameterSpec(salt.getBytes(StandardCharsets.UTF_8));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8),"AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());
			return Base64.getUrlEncoder().encodeToString(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public String encrypt(String plainText) {
		return encrypt("9c%b@s@e!C@B%S$E","9E%S!B#C)e$s*b!c", plainText);
	}

	public String decrypt(String data) {

		return decrypt("9c%b@s@e!C@B%S$E","9E%S!B#C)e$s*b!c", data);

	}

	public String encryptRefreshToken(String plainText) {
		return encrypt("9c%b@s@e!C@B%S$E","9E%S!B#C)e$s*b!c", plainText);
	}

	public String decryptRefreshToken(String data) {
		return decrypt("9c%b@s@e!C@B%S$E","9E%S!B#C)e$s*b!c", data);
	}

	private String decrypt(String key, String salt, String cipherText) {
		try {
			IvParameterSpec iv = new IvParameterSpec(salt.getBytes(StandardCharsets.UTF_8));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8),"AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] original = cipher.doFinal(Base64.getUrlDecoder().decode(cipherText));

			return new String(original);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static void main(String[] args) {
		
		Encryption enc = new Encryption();
		System.out.println(enc.encrypt("siddhartha.negi@obino.in"));
	}
}
