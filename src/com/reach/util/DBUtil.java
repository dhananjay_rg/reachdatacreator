package com.reach.util;

import java.sql.Connection;
import java.sql.DriverManager;

import com.reach.err.DBError;

public class DBUtil {

	public static Connection getConnection()  throws DBError{
		try {
			
			String driver=ReachProperties.instance.getDBDriverClass();
			String dbURL=ReachProperties.instance.getDBUrlWithSchema();
			String dbUser=ReachProperties.instance.getDBUser();
			String dbPass=ReachProperties.instance.getDBPassword();
			//////////////////////////
			Class.forName(driver).newInstance();
			Connection con = DriverManager.getConnection(dbURL, dbUser, dbPass);

			return con;

		} catch (Exception e) {
			throw new DBError("Fail to get DB-Connection", e);
		} 

	}
	
	

}
