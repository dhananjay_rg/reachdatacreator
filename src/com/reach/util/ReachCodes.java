package com.reach.util;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.Locale;

public final class ReachCodes {


	public static final String _ReachStandardDateTimeFormat = "yyyy-MM-dd'T'HH:mm:ss";

	public static interface TaskType {

		public static final String _Content = "Content";

	}

	public static interface TaskSubSubType {

		public static final String _TrackingTask = "Track/(Habit)";
		public static final String _ProfileQuestionAny = "UserData/Profile";
		public static final String _UserDynamicDataAny = "UserData/DynamicData";

	}

	public interface ReachDateFormat {
		String DOB = "yyyy-MM-dd";
		String DATE = "yyyy-MM-dd";
	}

	/** Reach week starts from Monday, so Monday=1 **/
	public static enum ReachWeekDay {

		MONDAY(1, 2), TUESDAY(2, 2), WEDNESDAY(3, 2), THURSDAY(4, 2), FRIDAY(5, 2), SATURDAY(6, 3), SUNDAY(7, 3);

		private ReachWeekDay(int id, int weekGroupDay) {
			this.id = id;
			this.weekGroupDay = weekGroupDay;
		}

		public final int id;
		public final int weekGroupDay;

		public boolean isUnderWeekGroup(int weekGroup) {
			return weekGroup == _ALL || this.weekGroupDay == weekGroup;
		}

		// ALL=1, WeekDay=2, WeekEnd=3

		public static final int _ALL = 1;
		public static final int _WeekDay = 2;
		public static final int _WeekEnd = 3;

		public static ReachWeekDay resolveFromCalendar(Calendar cal) {

			String calWeekDayName = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG_FORMAT, Locale.ENGLISH);

			for (ReachWeekDay reachWeekDay : ReachWeekDay.values()) {

				if (reachWeekDay.name().equalsIgnoreCase(calWeekDayName)) {
					return reachWeekDay;
				}

			}

			return null;

		}

	}

	public interface HabitFrequency {
		public static final int _Daily = 1;
		public static final int _Weekly = 2;
		public static final int _Yearly = 3;
	}

	public interface TaskGroupType {
		public static final String _HabitProfileQuestion = "PQ";
		public static final String _HabitBenchmarkQuestion = "HBQ";
		public static final String _HabitProgressAssessmentQuestion = "HPAQ";
		public static final String _HabitTrackingQuestion = "TQ";
		public static final String _UserDynamicDataQuestion = "DD";
		public static final String _NoneGroupQuestion = "NGQ";

	}

	public interface ValuePathRoot {

		public static final String _Token = "@@";

		public static final String _Habit = "habit";
		public static final String _Vision = "vision";
		public static final String _Barrier = "barrier";
		public static final String _User = "user";
		public static final String _Blog = "blog";
		public static final String _Recipe = "recipe";
		public static final String _Tips = "tip";

		public static final String _Sender = "sender";
		public static final String _Requester = "requester";
		public static final String _Accepter = "accepter";
		public static final String _FeedLikedBy = "liker";
		public static final String _FeedCommentedBy = "commenter";
		public static final String _Feed = "feed";
		public static final String _Dare = "dare";
		public static final String _Claimer = "claimer";
		public static final String _Rejecter = "rejecter";
		public static final String _Revoker = "revoker";

		/** one who cheers his friend who is participating in the dare **/
		public static final String _Cheere = "cheerer";

		public static final String _Receiver = "receiver";

		public static final String _MsgFromUser = "msgFromUser";

		public static final String _Group = "group";

		public static final String _UserHabit = "userHabit";
		
		public static final String _UserFriend = "userFriend";

		public static final String _HabitBuddy = "habitBuddy";

		public static final String _Corporate = "corp";

		public static final String _FeedComment = "feedComment";
		
		public static final String _FeedLike = "feedLike";

		public static final String _DareCheer = "dareCheer";

	}

	public static enum ProfileAttrib {
		FirstName, MiddleName, LastName, PersonalEmailId, OfficialEmailId, MobileNumber, CountryCode, Gender, DateOfBirth, Height, Weight, Chest, Waist, Hips, Arms, Thigh, Calf, ActivityLevel, TargetWeight, Goal, RateOfChange, CuisinePreferences, FoodPreferences, MealPreferences, FoodAllergyOrIntolerance, ExercisePreferences, StepCountTarget;
	}

	public static enum MsgChannel {

		Email(1), Desktop(2), Mobile(3);

		private MsgChannel(int id) {
			this.id = id;
		}

		public final int id;

	}

	public static enum Status {

		Pending(0), Active(1), Inactive(2), Completed(3), Pause(4);

		private Status(int code) {
			this.code = code;
		}

		public final int code;

	}

	public static enum ReminderModes {

		Daily(1), Weekly(2), Monthly(3);

		private ReminderModes(int code) {
			this.code = code;
		}

		public final int code;

	}

	public static enum AttributeParam {

		UserFirstName("user.firstName", "f_name"), UserLastName("user.lastName", "l_name"), HabitTitle("habit.title", "title"), HabitTrackingTitle("habit.trackQuest.title", "title");

		private AttributeParam(String link, String dbColName) {
			this.link = link;
			this.dbColName = dbColName;
		}

		public final String link;
		public final String dbColName;

		public static final String ParamsDelimiter = ",";

		public static AttributeParam resolveFromLink(String link2resolve) {

			for (AttributeParam paramLink : AttributeParam.values()) {

				if (paramLink.link.equals(link2resolve)) {
					return paramLink;
				}

			}

			return null;

		}

	}

	public static class TemplateCode {

		public static final int EVENT_TYPE_MASTER_HABIT = 1;
		public static final int EVENT_TYPE_CUSTOM_HABIT = 2;

	}

	public static final class UIScreen {

		public static interface Onboarding {
			public static final int VISION = 1;
			public static final int BARRIER = 2;
			public static final int HABIT = 3;
		}

		public static interface Dashboard {

		}

		public static interface Profile {

		}

		public static interface Social {

		}

	}

	public static final class ReachEvent {

		/** **/
		// public static final ReachEvent =new ReachEvent("", "");

		/** User has not done initial login after the email is sent by HR **/
		public static final ReachEvent USER_REG_PENDING = new ReachEvent("USER_REG_PENDING", "LOGIN", "User has not done initial login after the email is sent by HR");

		/** On boarding Vision-barrier-habit not set **/
		public static final ReachEvent ONBOARDING_VISION_BARRIER_HABIT_PENDING = new ReachEvent("ONBOARD_VZN_BRER_HBT_PENDING", "ONBOARD", "On boarding Vision-barrier-habit not set");

		/** User inactive for minimum 7 number of days - User has done initial login however after that user has not logged in again **/
		public static final ReachEvent USER_NOT_LOGIN_FOR_nDAYS = new ReachEvent("USER_NO_LOGIN_NDAYS", "LOGIN",
				"User inactive for minimum 7 number of days - User has done initial login however after that user has not logged in again");

		/** User active on platform but not performing any other activity - User logged in however not performing any actions **/
		public static final ReachEvent USER_LOGIN_BUT_NO_ACTIVITY = new ReachEvent("USER_LOGIN_BUT_NO_ACTVT", "LOGIN",
				"User active on platform but not performing any other activity - User logged in however not performing any actions");

		public static final class Habit {

			/** User not doing tracking task for min 3 number of days for a,b,c habits **/
			public static final ReachEvent USER_NOT_DOING_TASK_FOR_nDAYS = new ReachEvent("USER_NOT_DOING_TASK_NDAYS", "TASK",
					"User not doing tracking task for min 3 number of days for a,b,c habits");

			/** Reminders for Habit Tasks and option for Edit reminder **/
			public static final ReachEvent REMINDER_HABIT_TRACKING_QUESTION = new ReachEvent("REMINDER_TRACK_HABIT_QUEST", "TASK", "Reminders for Habit Tasks and option for Edit reminder");

		}

		public static final class SocialFriend {

			/** Friend request recieved **/
			public static final ReachEvent FRND_REQUEST_RECEIVED = new ReachEvent("FRND_REQ_RECEIVED", "SOCIAL", "Friend request recieved");

			/** Friend request accepted **/
			public static final ReachEvent FRND_REQUEST_ACCEPTED = new ReachEvent("FRND_REQ_ACCEPTED", "SOCIAL", "Friend request accepted");

			/** Friend posted/shared something on Social Feed **/
			public static final ReachEvent FRND_POSTS_SHARES = new ReachEvent("FRND_POSTED_SHARED", "SOCIAL", "Friend posted/shared something on Social Feed");

			/** Someone commented or liked User's Post/Share on Social Feed **/
			public static final ReachEvent ANYONE_COMMENTS_USERS_POSTS_AND_SHARES = new ReachEvent("ANY1KMNTS_USRS_POST&SHARE", "SOCIAL", "Someone commented User's Post/Share on Social Feed");

			/** Someone commented or liked User's Post/Share on Social Feed **/
			public static final ReachEvent ANYONE_LIKES_USERS_POSTS_AND_SHARES = new ReachEvent("ANY1LIKS_USRS_POST&SHARE", "SOCIAL", "Someone liked User's Post/Share on Social Feed");

		}

		public static final class SocialGroup {

			/** Group invitation sent to user by Admin **/
			public static final ReachEvent GROUP_INVITATION = new ReachEvent("GROUP_INVITE", "SOCIAL", "Group invitation sent to user by admin");

			/** Group joining request sent by user to Admin **/
			public static final ReachEvent GROUP_JOIN_REQUEST = new ReachEvent("GROUP_INVITE", "SOCIAL", "Group joining request sent by user to admin");

			/** Friend request accepted **/
			public static final ReachEvent GROUP_INVITE_ACCEPTED = new ReachEvent("GROUP_INVITE_ACCEPTED", "SOCIAL", "Group invitation accepted");

			/** Member posted/shared something on Social Feed **/
			public static final ReachEvent GROUP_MEMBER_POSTS_SHARES = new ReachEvent("GROUP_MEMBER_POSTS_SHARES", "SOCIAL", "Member posted/shared something on Social Feed");

			/** Any Member commented or liked User's Post/Share on Social Feed **/
			public static final ReachEvent ANYONE_COMMENTS_OR_LIKES_USERS_POSTS_AND_SHARES = new ReachEvent("GROUP_ANY1KMNTS_LIKS_USRS_POST", "SOCIAL",
					"Any Member commented or liked User's Post/Share on Social Feed");

		}

		public static final class DareChalnz {
			/** User's friend invited him for a dare **/
			public static final ReachEvent DARE_INVITE = new ReachEvent("DARE_INVITE", "DARE", "User's friend invited him for a dare");

			/** Friend accepted dare sent by user **/
			public static final ReachEvent DARE_INVITE_ACCEPT = new ReachEvent("DARE_INVITE_ACCEPT", "DARE", "Friend accepted dare sent by user");

			/** User claimed a win **/
			public static final ReachEvent DARE_CLAIM_WIN = new ReachEvent("DARE_CLAIM_WIN", "DARE", "User claimed a win");

			/** User accepted a win **/
			public static final ReachEvent DARE_ACCEPT_WIN = new ReachEvent("DARE_ACCEPT_WIN", "DARE", "User accepted a win");

			/** User rejected a win **/
			public static final ReachEvent DARE_REJECT_WIN = new ReachEvent("DARE_REJECT_WIN", "DARE", "User rejected a win");

			/** Other people cheered **/
			public static final ReachEvent DARE_CHEERED = new ReachEvent("DARE_CHEERED", "DARE", "Other people cheered");

		}

		public static final class HabitBuddy {
			/** User invited a buddy for is Habit Journey **/
			public static final ReachEvent BUDDY_INVITE = new ReachEvent("BUDDY_INVITE", "BUDDY", "User invited a buddy for is Habit Journey");

			/** Send Reminder for Buddy Invite **/
			public static final ReachEvent BUDDY_INVITE_REMINDER = new ReachEvent("BUDDY_INVITE_REMIND", "BUDDY", "Send Reminder for Buddy Invite");

			/** Buddy Invitation Accepted **/
			public static final ReachEvent BUDDY_INVITE_ACCEPT = new ReachEvent("BUDDY_INVITE_ACPT", "BUDDY", "Buddy Invitation Accepted");

			/** Buddy Invitation Rejected **/
			public static final ReachEvent BUDDY_INVITE_REJECT = new ReachEvent("BUDDY_INVITE_REJ", "BUDDY", "Buddy Invitation Rejected");

			/** Buddy Invitation Revoked **/
			public static final ReachEvent BUDDY_INVITE_REVOKE = new ReachEvent("BUDDY_INVITE_REVK", "BUDDY", "Buddy Invitation Revoked");

			/** Nudge Your Buddy **/
			public static final ReachEvent BUDDY_NUDGE = new ReachEvent("BUDDY_NUDGE", "BUDDY", "Nudge Your Buddy");

			/** Congrats your Buddy on his Progress **/
			public static final ReachEvent BUDDY_CONGRATULATE = new ReachEvent("BUDDY_CONGRATS", "BUDDY", "Congrats your Buddy on his Progress");

		}

		public static final class HabitReminder {

			/** Reminder for Custom Habit **/
			public static final ReachEvent CUSTOM_HABIT_REMINDER = new ReachEvent("KSTM_HABIT_RMNDR", "HABIT_RMNDR", "Reminder for Custom Habit");

			/** Reminder for Reach Habit **/
			public static final ReachEvent REACH_HABIT_REMINDER = new ReachEvent("RCH_HABIT_RMNDR", "HABIT_RMNDR", "Reminder for Reach Habit");

			/** User has not done task for consecutive min days **/
			public static final ReachEvent USER_NOT_DOING_TASK_FOR_MIN_DAYS = new ReachEvent("RCH_HBT_USRNDT4MD", "HABIT_RMNDR", "User has not done task for consecutive min days");

			/** User has not done task for consecutive min days **/
			public static final int USER_NOT_DOING_TASK_FOR_MIN_DAYS__TRIGGER_LAPS_DAY = 3;

			public static final LocalTime USER_NOT_DOING_TASK_FOR_MIN_DAYS__TRIGGER_TIME_ON_DAY = LocalTime.of(11, 0);

			/** User has not done task=> next trigger date after this days **/
			public static final int USER_NOT_DOING_TASK_FOR_MIN_DAYS__TRIGGER_INTERVAL_DAY = 7;

			/** User has not done task=> maximum number of time we can send notification to user for the same trigger **/
			public static final int USER_NOT_DOING_TASK_FOR_MIN_DAYS__MAX_TRIGGER_COUNT = 5;

		}

		public static final class UserLogin {

			/** User did not logged in for the first time even **/
			public static final ReachEvent USER_NEVER_LOGGED_IN = new ReachEvent("USR_NVR_LOGIN", "USR_ONBOARD", "User did not logged in for the first time even");

			/** User did not logged in for the first time even=> first trigger activation days **/
			public static final int USER_NEVER_LOGGED_IN__TRIGGER_LAPS_DAY = 7;

			public static final LocalTime USER_NEVER_LOGGED_IN__TRIGGER_TIME_ON_DAY = LocalTime.of(15, 0);

			/** User did not logged in for the first time even=> next trigger date after this days **/
			public static final int USER_NEVER_LOGGED_IN__TRIGGER_INTERVAL_DAY = 7;

			/** User did not logged in for the first time even=> maximum number of time we can send notification to user for the same trigger **/
			public static final int USER_NEVER_LOGGED_IN__MAX_TRIGGER_COUNT = 3;

		}

		//////
		private ReachEvent(String code, String iconType, String desc) {
			this.code = code;
			this.iconType = iconType;
			this.desc = desc;
		}

		public final String code;
		public final String iconType;
		public final String desc;

	}

	public static final class NotifSrvc {

		public static final class AppPayloadType {

			// INFO,SCREEN_NAVIGATION,COMMAND

			public static final String INFO = "INFO";
			public static final String SCREEN_NAVIGATION = "SCREEN_NAVIGATION";
			public static final String COMMAND = "COMMAND";
		}

		public static final class Type {
			public static final String NEW = "new";
			public static final String RESEND = "resend";
			public static final String SCHEDULED = "scheduled";
			public static final String REMINDER = "reminder";
		}

		public static final class Priority {
			public static final int NORMAL = 1;
			public static final int HIGH = 2;
		}

		public static final class DBStatus {
			public static final int BEFORE_ACTIVE = 0;
			public static final int ACTIVE = 1;
			public static final int FAILED = 2;
			public static final int SENT = 3;
		}

		public static final class Channel {
			public static final String SMS = "sms";
			public static final String EMAIL = "email";

			/** EXTMAIL is like SendGrid **/
			public static final String EXTMAIL = "extmail";

			// CloudMessage stands for Cloud messaging
			public static final class CloudMessage {

				public static final String code = "fcm";

				@Override
				public String toString() {
					return code;
				}

				public static final class Mobile {
					public static final String FCM = "fcm";
					public static final String APNS = "apns";

					public static boolean isValid(String subChannel) {
						return subChannel.equalsIgnoreCase(Mobile.FCM) || subChannel.equalsIgnoreCase(Mobile.APNS);
					}

				}

				public static final class Desktop {
					public static final String FCM_WEB = "fcm-web";
					public static final String APNS_WEB = "apns-web";

					public static boolean isValid(String subChannel) {
						return subChannel.equalsIgnoreCase(Desktop.APNS_WEB) || subChannel.equalsIgnoreCase(Desktop.FCM_WEB);
					}

				}

				public static final class SubChannel {

					public static boolean isValid(String subChannel) {
						return subChannel.equalsIgnoreCase(Mobile.FCM) || subChannel.equalsIgnoreCase(Mobile.APNS) || subChannel.equalsIgnoreCase(Desktop.APNS_WEB)
								|| subChannel.equalsIgnoreCase(Desktop.FCM_WEB);
					}

					public static String[] all() {
						return new String[] { Mobile.FCM, Mobile.APNS, Desktop.APNS_WEB, Desktop.FCM_WEB };
					}

				}

			}
		}

	}

	public static enum Screen {

		PROFILE("PROF", "profile"), SOCIAL("SOC", "social"), LEARN("LRN", "learn"), DARE("DARE", "social/dare"), MY_FRIENDS("FRND", "social/myfriends"), GROUP("GROUP",
				"social/my-groups"), FEED("FEED", "social/feed"), DASHBOARD("DASBRD", "dashboard"), HABIT_BUDDY("HBTBDY", "buddy");

		private Screen(String code, String relURL) {
			this.code = code;
			this.relURL = relURL;
		}

		public final String code;
		public final String relURL;

	}


}







