package com.reach.util.habit.progress;

 final class ProgRequest<T> {

	//Total number of days since the user has taken the habit/behaviour.
	private int daysObserved;

	//Period after which the user will form the particular habit/behaviour.
	private float leftPeriod;

	//total number of YES till date.
	private int totalYesCounter;

	//Number of NO since the last YES (last time the activity was done). Resets to 0 when a YES occurs.
	private int noContinuous;

	//Probability value of N-1 days for a YES.
	private float probabilityOfNMinus1DaysObserved;

	//Whether today the activity is done or not. 1 for YES, 0 for NO. In case of server failure,
	//send ar array or user's activity done log sorted by the date. The system generated NO should be already included
	//in the right order in the array. Eg. [1, 1, 0, 1, 1, 0]. The systemGeneratedNo before the first update in the array
	//should be passed in the parameter systemGeneratedNo, not the in between ones.
	private T activityDone;

	//Number of system generated NO, if any, since the last time request was sent.
	private int systemGeneratedNo;

	//Period needed by the user to form the habit/behaviour.
	private int totalPeriodConsidered;


	public int getDaysObserved() {
		return daysObserved;
	}

	public void setDaysObserved(int daysObserved) {
		this.daysObserved = daysObserved;
	}

	public float getLeftPeriod() {
		return leftPeriod;
	}

	public void setLeftPeriod(float leftPeriod) {
		this.leftPeriod = leftPeriod;
	}

	public int getTotalYesCounter() {
		return totalYesCounter;
	}

	public void setTotalYesCounter(int totalYesCounter) {
		this.totalYesCounter = totalYesCounter;
	}

	public int getNoContinuous() {
		return noContinuous;
	}

	public void setNoContinuous(int noContinuous) {
		this.noContinuous = noContinuous;
	}

	public float getProbabilityOfNMinus1DaysObserved() {
		return probabilityOfNMinus1DaysObserved;
	}

	public void setProbabilityOfNMinus1DaysObserved(float probabilityOfNMinus1DaysObserved) {
		this.probabilityOfNMinus1DaysObserved = probabilityOfNMinus1DaysObserved;
	}

	public T getActivityDone() {
		return activityDone;
	}

	public void setActivityDone(T activityDone) {
		this.activityDone = activityDone;
	}

	public int getSystemGeneratedNo() {
		return systemGeneratedNo;
	}

	public void setSystemGeneratedNo(int systemGeneratedNo) {
		this.systemGeneratedNo = systemGeneratedNo;
	}

	public int getTotalPeriodConsidered() {
		return totalPeriodConsidered;
	}

	public void setTotalPeriodConsidered(int totalPeriodConsidered) {
		this.totalPeriodConsidered = totalPeriodConsidered;
	}




}
