package com.reach.util.habit.progress;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reach.err.DBError;
import com.reach.util.DBUtil;
import com.reach.util.ReachJsonMapper;

public final class HabitProgressUtilBackup {

	private static final Logger logger = Logger.getLogger(HabitProgressUtilBackup.class);

	private Connection dbCon;
	private CallableStatement hpriCSTMT = null;
	private PreparedStatement saveProgressPSTMT = null;
	
	private ObjectMapper jsonMapper=ReachJsonMapper.mapperForHabitProgress();

	private String logUID;
	private HabitProgressWebClient habitProgressWebClient;

	//in inUserId bigint, in inUserHabitId bigint, in inProgressDate date, in inBeginFromDate date, OUT outProgressHstryJson jsonb, OUT outProgressReqJson jsonb

	private final int _indexOFinUserId;
	private final int _indexOFinUserHabitId;
	private final int _indexOFinProgressDate;
	private final int _indexOFinBeginFromDate;


	private final int _indexOFoutProgressJsonHstry;
	private final int _indexOFoutProgressReqJson;

	private final int _saveProgress_Indx_Progress;
	private final int _saveProgress_Indx_ProgressDate;
	private final int _saveProgress_Indx_ProgressJsonInfo;
	private final int _saveProgress_Indx_ProgressForUserId;
	private final int _saveProgress_Indx_ProgressForUserHabitId;

	public static HabitProgressUtilBackup getNewInstance(String logUID) throws DBError, SQLException {

		HabitProgressUtilBackup hp = new HabitProgressUtilBackup(DBUtil.getConnection());
		hp.logUID = logUID;

		return hp;
	}

	public static HabitProgressUtilBackup getNewInstance(String logUID, Connection dbCon) throws DBError, SQLException {

		HabitProgressUtilBackup hp = new HabitProgressUtilBackup(dbCon);
		hp.logUID = logUID;
		hp.dbCon = dbCon;
		hp.dbConLocallyCreated=false;

		return hp;
	}


	//pass null to consider the begin from date as the day before the ProgressDate
	public ProgressInfo getProgressForUser(long userId, long userHabitId, Date progressDate, Date lastAPICallDate, boolean saveProgressInDB) throws SQLException, IOException, ParseException {

		logger.info("UID["+logUID+"]: UserId[ "+userId+" ], UserHabitId[ "+userHabitId+" ], ProgressDate["+progressDate+" ], SaveProgressInDB["+saveProgressInDB+" ] calling getProgressForUser ... ");


		SimpleDateFormat sdfYYYYMMDD=new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		progressDate= sdfYYYYMMDD.parse(sdfYYYYMMDD.format(progressDate) );
		lastAPICallDate= sdfYYYYMMDD.parse(sdfYYYYMMDD.format(lastAPICallDate) );

		hpriCSTMT.setLong(_indexOFinUserId, userId);
		hpriCSTMT.setLong(_indexOFinUserHabitId, userHabitId);
		hpriCSTMT.setDate(_indexOFinProgressDate, new java.sql.Date(progressDate.getTime()) );
		hpriCSTMT.setDate(_indexOFinBeginFromDate, new java.sql.Date(lastAPICallDate.getTime()) );

		System.out.println(hpriCSTMT);

		hpriCSTMT.executeUpdate();



		Object progHistoryJsonObj= hpriCSTMT.getObject(_indexOFoutProgressJsonHstry); //JsonMapper.mapperYYYYMMDD.readValue(.toString(), JsonNode.class) ;

		JsonNode progReqJson=jsonMapper.readValue(hpriCSTMT.getObject(_indexOFoutProgressReqJson).toString(), JsonNode.class);



		//JsonNode progReqJson=JsonMapper.mapperYYYYMMDD.readValue(hpriCSTMT.getObject(_indexOFoutProgressReqJson).toString(), JsonNode.class) ;

		ProgRequest progressRequest=jsonMapper.readValue(progReqJson.toString(), ProgRequest.class);

		/*progressRequest.setLeftPeriod(hpriCSTMT.getFloat(_indexOFoutLeftPeriod) );
		progressRequest.setNoContinuous(hpriCSTMT.getInt(_indexOFoutCountinousNO));
		progressRequest.setSystemGeneratedNo(hpriCSTMT.getInt(_indexOFoutSystemGeneratedNO));
		progressRequest.setTotalPeriodConsidered(hpriCSTMT.getInt(_indexOFoutTotalPeriodConsidered));
		progressRequest.setTotalYesCounter(hpriCSTMT.getInt(_indexOFoutTotalYesCounter));
		progressRequest.setDaysObserved(hpriCSTMT.getInt(_indexOFoutTotalDaysObserved));
		progressRequest.setProbabilityOfNMinus1DaysObserved(hpriCSTMT.getFloat(_indexOFoutProbabiltyNminus1Days));*/

		String lastAPIDate2Consider= progReqJson.get("lastAPIDate2Consider").asText();

		// lastAPI call date has to be recalculated
		lastAPICallDate= sdfYYYYMMDD.parse(lastAPIDate2Consider);

		HashMap<Date,ProgressInfo> progressHistory=null;

		if(progHistoryJsonObj!=null) {

			System.out.println("progHistoryJson= "+progHistoryJsonObj.toString());

			try {
				progressHistory= jsonMapper.readValue(progHistoryJsonObj.toString(), new TypeReference<HashMap<Date,ProgressInfo>>() {});
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if(progressHistory==null) {
			progressHistory=new HashMap<>();
		}

		//////

		logger.debug("UID["+logUID+"]: UserId[ "+userId+" ], UserHabitId[ "+userHabitId+" ], ProgRequest["+jsonMapper.writeValueAsString(progressRequest)+"] calling HabitProgressUtil Web API  ... ");

		retrofit2.Response<ProgressInfo> progressRes= habitProgressWebClient.service.getProgress(progressRequest).execute();


		if(progressRes!=null && progressRes.body()!=null) {


			//remove the Progress Report (lastProgressAPIDate_which_is_considered, current_ProgressAPIDate) both dates are exclusive.
			Date key2delete=null;
			do {
				//init key2delete everytime to null
				key2delete=null;


				for(Map.Entry<Date,ProgressInfo> entry:progressHistory.entrySet()) {
					if(entry.getKey().compareTo(lastAPICallDate)>0 && entry.getKey().compareTo(progressDate)<0) {
						key2delete=entry.getKey();
						break;
					}
				}

				progressHistory.remove(key2delete);

			}while(key2delete!=null);


			//update the history with the current progress
			progressHistory.put(progressDate, progressRes.body());

			if(saveProgressInDB) {
				saveProgressPSTMT.setLong(_saveProgress_Indx_ProgressForUserId, userId);
				saveProgressPSTMT.setLong(_saveProgress_Indx_ProgressForUserHabitId, userHabitId);
				saveProgressPSTMT.setFloat(_saveProgress_Indx_Progress, progressRes.body().getLeftPeriod());
				saveProgressPSTMT.setTimestamp(_saveProgress_Indx_ProgressDate, new Timestamp(progressDate.getTime()) );
				saveProgressPSTMT.setString(_saveProgress_Indx_ProgressJsonInfo, jsonMapper.writeValueAsString(progressHistory));

				logger.debug("UID["+logUID+"]: UserId[ "+userId+" ], UserHabitId[ "+userHabitId+" ], Updating DB with latest Left Period ["+progressRes.body().getLeftPeriod()+"] ... ");

				saveProgressPSTMT.executeUpdate();
			}

			return progressRes.body();
		}

		return null;

	}

	//////////////////////////

	{
		int i = 0;

		_indexOFinUserId = ++i;
		_indexOFinUserHabitId = ++i;
		_indexOFinProgressDate = ++i;
		_indexOFinBeginFromDate = ++i;

		_indexOFoutProgressJsonHstry = ++i;
		_indexOFoutProgressReqJson = ++i;


		/////

		i = 0;
		_saveProgress_Indx_Progress = ++i;
		_saveProgress_Indx_ProgressDate = ++i;
		_saveProgress_Indx_ProgressJsonInfo = ++i;
		_saveProgress_Indx_ProgressForUserId = ++i;
		_saveProgress_Indx_ProgressForUserHabitId = ++i;




		System.out.println("_indexOfUserId = "+_indexOFinUserId );
		System.out.println("_indexOfUserHabitId = "+_indexOFinUserHabitId );
		System.out.println("_indexOfProgressDate = "+_indexOFinProgressDate );
		System.out.println("_indexOFinBeginFromDate = "+_indexOFinBeginFromDate );
		System.out.println("_indexOfProgressJsonHstry= "+_indexOFoutProgressJsonHstry);
		System.out.println("_indexOFoutProgressReqJson= "+_indexOFoutProgressReqJson);

		////
	}


	private boolean dbConLocallyCreated=true;


	private HabitProgressUtilBackup(Connection dbCon) throws DBError, SQLException {

		this.dbCon = dbCon;

		/*
		  {1=in inUserId bigint, 2=in inUserHabitId bigint, 3=in inProgressDate date, 4=in inBeginFromDate date,
		  5=OUT outProgressHstryJson jsonb, 6=OUT outProgressReqJson jsonb}
		 */

		hpriCSTMT = dbCon.prepareCall("{call HabitProgressRequestInfo(?, ?, ?, ?, ?, ?) }");


		hpriCSTMT.registerOutParameter(_indexOFoutProgressJsonHstry, java.sql.Types.OTHER);
		hpriCSTMT.registerOutParameter(_indexOFoutProgressReqJson, java.sql.Types.OTHER );




		//////

		saveProgressPSTMT = dbCon.prepareStatement("update user_habits set progress_remain_days=?, progress_rec_date=?, progress_info_json=to_jsonb(?::jsonb) where user_id=? and id=? ");

		////

		habitProgressWebClient = new HabitProgressWebClient();

	}



	private boolean releaseResourceWasCalled = false;

	public void releaseResources() {
		// release all the resources like DB connection

		//private CallableStatement hpriCSTMT = null;
		//private PreparedStatement saveProgressPSTMT = null;

		if (saveProgressPSTMT != null) {
			try { saveProgressPSTMT.close(); } catch (Exception e) { }
			saveProgressPSTMT = null;
		}

		if (hpriCSTMT != null) {
			try { hpriCSTMT.close(); } catch (Exception e) { }
			hpriCSTMT = null;
		}


		if(dbConLocallyCreated) {
			if (dbCon != null) {
				try { dbCon.close(); } catch (Exception e) { }

				dbCon = null;
			}
		}


		releaseResourceWasCalled = true;
	}

	@Override
	protected void finalize() throws Throwable {

		// it is not necessarily will be called

		if (!releaseResourceWasCalled) {
			logger.warn(" WARNING ********  You must to call releaseResources() method... ****** ");

			releaseResources();

		}

		super.finalize();
	}



	public static void main(String[] args) throws DBError, SQLException, IOException, ParseException {

		HabitProgressUtilBackup hpu=HabitProgressUtilBackup.getNewInstance(UUID.randomUUID().toString());

		Calendar cal=Calendar.getInstance(Locale.ENGLISH);

		cal.set(Calendar.DATE, 27);

		Date lastAPIDate=cal.getTime();

		cal.set(Calendar.DATE, 28);
		Date progDate=cal.getTime();

		hpu.getProgressForUser(1, 1, progDate, lastAPIDate, true);

	}




	public static void main3244353(String[] args) throws JsonProcessingException {

		HashMap<Date,ProgressInfo> latestFourProgressInfo=new HashMap<>();

		Calendar curr=Calendar.getInstance(Locale.ENGLISH);

		latestFourProgressInfo.put(curr.getTime(), new ProgressInfo().setApiDate(curr.getTime()));

		curr.add(Calendar.DATE, -1);
		latestFourProgressInfo.put(curr.getTime(), new ProgressInfo().setApiDate(curr.getTime()));

		curr.add(Calendar.DATE, -1);
		latestFourProgressInfo.put(curr.getTime(), new ProgressInfo().setApiDate(curr.getTime()));

		curr.add(Calendar.DATE, -1);
		latestFourProgressInfo.put(curr.getTime(), new ProgressInfo().setApiDate(curr.getTime()));



		System.out.println(ReachJsonMapper.mapperForHabitProgress().writeValueAsString(latestFourProgressInfo));


	}



}
