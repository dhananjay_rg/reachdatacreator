package com.reach.util.habit.progress;

import java.io.IOException;

import com.reach.util.ReachJsonMapper;
import com.reach.util.ReachProperties;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

class HabitProgressWebClient {

	public final API service;

	public HabitProgressWebClient() {

		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		// set your desired log level
		logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient.addInterceptor(logging);
		httpClient.addInterceptor(new Interceptor() {

			@Override
			public Response intercept(Chain chain) throws IOException {
				Request original = chain.request();

				Request.Builder requestBuilder = original.newBuilder().header("Accept", "application/json");

				return chain.proceed(requestBuilder.build());
			}
		});

		Retrofit baseConfig = new Retrofit.Builder().baseUrl(ReachProperties.instance.getHabitProgressBaseURL())
				.addConverterFactory(JacksonConverterFactory.create(ReachJsonMapper.mapperForHabitProgress())).client(httpClient.build()).build();
		service = baseConfig.create(HabitProgressWebClient.API.class);

	}

	public static interface API {

		@POST("/")
		public Call<ProgressInfo> getProgress(@Body ProgRequest progressRequest);

	}

	public static void main(String[] args) throws IOException {

		ProgRequest pr = new ProgRequest();
		pr.setActivityDone(1);
		pr.setDaysObserved(5);
		pr.setLeftPeriod(19);
		pr.setNoContinuous(0);
		pr.setProbabilityOfNMinus1DaysObserved(1);
		pr.setSystemGeneratedNo(0);
		pr.setTotalPeriodConsidered(21);
		pr.setTotalYesCounter(3);

		HabitProgressWebClient hPapi = new HabitProgressWebClient();

		retrofit2.Response<ProgressInfo> progressRes = hPapi.service.getProgress(pr).execute();

		System.out.println(progressRes.body().getLeftPeriod());

	}

}
