package com.reach.util.habit.progress;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reach.err.DBError;
import com.reach.util.DBUtil;
import com.reach.util.ReachCodes;
import com.reach.util.ReachJsonMapper;

public final class HabitProgressUtil {

	private static final Logger logger = Logger.getLogger(HabitProgressUtil.class);
	
	private ObjectMapper jsonMapper=ReachJsonMapper.mapperForHabitProgress();

	private Connection dbCon;

	/*usrHbtPSTMT user-habit information*/
	private PreparedStatement usrHbtPSTMT = null;

	/*totalYesCountPSTMT = find total yes-counts and latest-id when user has responded as yes*/
	private PreparedStatement totalYesCountPSTMT = null;

	/*latestYesIdPSTMT = find total yes-counts and latest-id when user has responded as yes*/
	private PreparedStatement latestYesIdPSTMT = null;

	/*noContinuPSTMT = find the continuous_no since last yes*/
	private PreparedStatement noContinuPSTMT = null;

	/*totalRespPSTMT = find the total counts of user response from the lastAPICallDate(excluded) till the ProgressDate(included).
	 So if procUserRespCountFrmLastAPICall>1 then that means, due to some reason API was not called and we need to consider the system-generated-'no' as user's-'no'*/
	private PreparedStatement totalRespPSTMT = null;


	/*activityDoneListPSTMT= find activityDone as array and SystemGeneratedNo=0 in this case*/
	private PreparedStatement activityDoneListPSTMT = null;

	/*activityDonePSTMT= find activityDone for the progressDate and SystemGeneratedNo count in this case*/
	private PreparedStatement activityDonePSTMT = null;

	/*systemGenNoPSTMT= find System-Generated-No for the progressDate in this case*/
	private PreparedStatement systemGenNoPSTMT = null;

	///
	private PreparedStatement saveProgressPSTMT = null;

	private String logUID;
	private HabitProgressWebClient habitProgressWebClient;
	private int habitTrackTypeId;

	private final int _saveProgress_Indx_Progress1;
	private final int _saveProgress_Indx_Progress2;
	private final int _saveProgress_Indx_ProgressDate;
	private final int _saveProgress_Indx_ProgressJsonInfo;
	private final int _saveProgress_Indx_ProgressForUserId;
	private final int _saveProgress_Indx_ProgressForUserHabitId;
	
	/**if updateStatus4Completions=true then status of user_habit will also be set to completed/formed when left period obtained is <=0 **/
	public static HabitProgressUtil getNewInstance(String logUID, boolean updateStatus4Completions) throws DBError, SQLException {

		HabitProgressUtil hp = new HabitProgressUtil(DBUtil.getConnection(), updateStatus4Completions);
		hp.logUID = logUID;

		return hp;
	}

	/**if updateStatus4Completions=true then status of user_habit will also be set to completed/formed when left period obtained is <=0 **/
	public static HabitProgressUtil getNewInstance(String logUID, Connection dbCon, boolean updateStatus4Completions) throws DBError, SQLException {
		HabitProgressUtil hp = new HabitProgressUtil(dbCon, updateStatus4Completions);
		hp.logUID = logUID;
		hp.dbCon = dbCon;
		hp.dbConLocallyCreated=false;

		return hp;
	}
	
	
	/////////////

	//pass null to consider the begin from date as the day before the ProgressDate
	public ProgressInfo getProgressForUser(long userId, long userHabitId, Date tempProgressDate, Date tempLastAPICallDate, boolean saveProgressInDB) throws SQLException, IOException, ParseException {

		logger.info("UID["+logUID+"]: UserId[ "+userId+" ], UserHabitId[ "+userHabitId+" ], ProgressDate["+tempProgressDate+" ], SaveProgressInDB["+saveProgressInDB+" ] calling getProgressForUser ... ");

		SimpleDateFormat sdfProgHstryKey=new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);

		Calendar progressDateCal= Calendar.getInstance(Locale.ENGLISH);
		progressDateCal.setTime(tempProgressDate);
		progressDateCal.set(Calendar.HOUR_OF_DAY, 0);
		progressDateCal.set(Calendar.MINUTE, 0);
		progressDateCal.set(Calendar.SECOND, 0);


		Calendar lastAPIDate2Consider= Calendar.getInstance(Locale.ENGLISH);
		lastAPIDate2Consider.setTime(tempLastAPICallDate);
		lastAPIDate2Consider.set(Calendar.HOUR_OF_DAY, 0);
		lastAPIDate2Consider.set(Calendar.MINUTE, 0);
		lastAPIDate2Consider.set(Calendar.SECOND, 0);


		int lastAPIDateAsInt= Integer.parseInt( sdfProgHstryKey.format( lastAPIDate2Consider.getTime()));
		int progDateAsInt= Integer.parseInt( sdfProgHstryKey.format(progressDateCal.getTime()));

		ProgRequest progressRequest=null;

		ResultSet rs=null;

		//holds key as value like yyyyMMdd
		HashMap<Integer,ProgressInfo> progressHistory=null;

		try {

			int qpi=0;

			//fetch userHabit information

			usrHbtPSTMT.setLong(++qpi, userId);
			usrHbtPSTMT.setLong(++qpi, userHabitId);

			logger.debug("UID["+logUID+"]: QUERY:usrHbtPSTMT-> "+usrHbtPSTMT.toString() );

			rs= usrHbtPSTMT.executeQuery();
			rs.next();

			Date habitStartDate= rs.getDate("habitStartDate");
			int totalPeriodConsidered=rs.getInt("totalPeriodConsidered");
			Object progHistoryJsonObj= rs.getObject("progressHistoryJson");

			try { rs.close(); } catch (Exception e) { }
			rs = null;


			if(progHistoryJsonObj!=null) {

				System.out.println("progHistoryJson= "+progHistoryJsonObj.toString());

				try {
					progressHistory= jsonMapper.readValue(progHistoryJsonObj.toString(), new TypeReference<HashMap<Integer,ProgressInfo>>() {});
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if(progressHistory==null) {
				progressHistory=new HashMap<>();

				lastAPIDate2Consider.setTime(habitStartDate);
				lastAPIDate2Consider.add(Calendar.DATE, -1);

			}else {

				while(lastAPIDate2Consider.getTime().compareTo(habitStartDate)>0) {

					if(progressHistory.get(Integer.parseInt( sdfProgHstryKey.format(lastAPIDate2Consider.getTime()) ))!=null) {

						//this is the date which is to be considered for the LastAPICallDate

						break;
					}

					lastAPIDate2Consider.add(Calendar.DATE, -1);

				}


			}


			lastAPIDateAsInt= Integer.parseInt( sdfProgHstryKey.format( lastAPIDate2Consider.getTime()));


			///////////

			/*find total yes-counts and latest-id when user has responded as yes*/
			qpi=0;
			totalYesCountPSTMT.setLong(++qpi, userId);
			totalYesCountPSTMT.setLong(++qpi, userHabitId);

			logger.debug("UID["+logUID+"]: QUERY:totalYesAndLatestYesIdPSTMT-> "+totalYesCountPSTMT.toString() );

			rs= totalYesCountPSTMT.executeQuery();
			rs.next();

			int totalYesCounts=rs.getInt("totalYesCounter");

			try { rs.close(); } catch (Exception e) { }
			rs = null;

			//////////////////////////

			qpi=0;

			latestYesIdPSTMT.setDate(++qpi, new java.sql.Date(lastAPIDate2Consider.getTimeInMillis()));
			latestYesIdPSTMT.setDate(++qpi, new java.sql.Date(progressDateCal.getTimeInMillis()) );
			latestYesIdPSTMT.setLong(++qpi, userId);
			latestYesIdPSTMT.setLong(++qpi, userHabitId);


			logger.debug("UID["+logUID+"]: QUERY:latestYesIdPSTMT-> "+latestYesIdPSTMT.toString() );

			rs= latestYesIdPSTMT.executeQuery();
			rs.next();

			long userTaskIdForLastYes= rs.getLong("lastYesId");

			try { rs.close(); } catch (Exception e) { }
			rs = null;




			///////////////
			/*find the continuous_no since last yes*/
			qpi=0;
			noContinuPSTMT.setLong(++qpi, userTaskIdForLastYes);
			noContinuPSTMT.setDate(++qpi, new java.sql.Date(lastAPIDate2Consider.getTimeInMillis()));
			noContinuPSTMT.setDate(++qpi, new java.sql.Date(progressDateCal.getTimeInMillis()) );
			noContinuPSTMT.setLong(++qpi, userId);
			noContinuPSTMT.setLong(++qpi, userHabitId);

			logger.debug("UID["+logUID+"]: QUERY:noContinuPSTMT-> "+noContinuPSTMT.toString() );

			rs= noContinuPSTMT.executeQuery();
			rs.next();

			int noContinuous=rs.getInt("noCounts");
			try { rs.close(); } catch (Exception e) { }
			rs = null;

			/////////////////////

			/*find the total counts of user response from the lastAPICallDate(excluded) till the ProgressDate(included).
			 So if procUserRespCountFrmLastAPICall>1 then that means, due to some reason API was not called and we need to consider
			 the system-generated-'no' as user's-'no' */

			qpi=0;

			totalRespPSTMT.setDate(++qpi, new java.sql.Date(lastAPIDate2Consider.getTimeInMillis()));
			totalRespPSTMT.setDate(++qpi, new java.sql.Date(progressDateCal.getTimeInMillis()) );
			totalRespPSTMT.setLong(++qpi, userId);
			totalRespPSTMT.setLong(++qpi, userHabitId);

			logger.debug("UID["+logUID+"]: QUERY:totalRespPSTMT-> "+totalRespPSTMT.toString() );

			rs= totalRespPSTMT.executeQuery();
			rs.next();

			int totalUserRespFromLastApi=rs.getInt("respCount");
			Date latestDateUserResponded=rs.getDate("latestDateUserResponded");

			try { rs.close(); } catch (Exception e) { }
			rs = null;

			if((latestDateUserResponded!=null && Integer.parseInt(sdfProgHstryKey.format(latestDateUserResponded))!=progDateAsInt && totalUserRespFromLastApi>0) || totalUserRespFromLastApi>1) {

				ProgRequest<ArrayList<Integer>> progressRequestTMP=new ProgRequest<>();
				progressRequest=progressRequestTMP;
				progressRequestTMP.setActivityDone(new ArrayList<>());

				qpi=0;
				activityDoneListPSTMT.setDate(++qpi, new java.sql.Date(lastAPIDate2Consider.getTimeInMillis()));
				activityDoneListPSTMT.setDate(++qpi, new java.sql.Date(progressDateCal.getTimeInMillis()) );
				activityDoneListPSTMT.setLong(++qpi, userId);
				activityDoneListPSTMT.setLong(++qpi, userHabitId);

				logger.debug("UID["+logUID+"]: QUERY:activityDoneListPSTMT-> "+activityDoneListPSTMT.toString() );

				rs= activityDoneListPSTMT.executeQuery();

				if(rs.next()) {
					do {

						progressRequestTMP.getActivityDone().add(rs.getInt(1));

					}while(rs.next());
				}

				try { rs.close(); } catch (Exception e) { }
				rs = null;

				progressRequestTMP.setSystemGeneratedNo(0);

			}else {

				ProgRequest<Integer> progressRequestTMP=new ProgRequest<>();
				progressRequest=progressRequestTMP;

				qpi=0;
				activityDonePSTMT.setDate(++qpi, new java.sql.Date(lastAPIDate2Consider.getTimeInMillis()));
				activityDonePSTMT.setDate(++qpi, new java.sql.Date(progressDateCal.getTimeInMillis()) );
				activityDonePSTMT.setLong(++qpi, userId);
				activityDonePSTMT.setLong(++qpi, userHabitId);

				logger.debug("UID["+logUID+"]: QUERY:activityDonePSTMT-> "+activityDonePSTMT.toString() );

				rs= activityDonePSTMT.executeQuery();

				if(rs.next()) {
					progressRequestTMP.setActivityDone(rs.getInt(1));
				}else {
					progressRequestTMP.setActivityDone(0);
				}



				try { rs.close(); } catch (Exception e) { }
				rs = null;

				//get system-generated-no

				qpi=0;
				systemGenNoPSTMT.setDate(++qpi, new java.sql.Date(lastAPIDate2Consider.getTimeInMillis()));
				systemGenNoPSTMT.setDate(++qpi, new java.sql.Date(progressDateCal.getTimeInMillis()) );
				systemGenNoPSTMT.setLong(++qpi, userId);
				systemGenNoPSTMT.setLong(++qpi, userHabitId);

				logger.debug("UID["+logUID+"]: QUERY:systemGenNoPSTMT-> "+systemGenNoPSTMT.toString() );

				rs= systemGenNoPSTMT.executeQuery();
				rs.next();

				progressRequestTMP.setSystemGeneratedNo(rs.getInt(1));

				try { rs.close(); } catch (Exception e) { }
				rs = null;
				qpi=0;

			}


			progressRequest.setTotalPeriodConsidered(totalPeriodConsidered);
			progressRequest.setTotalYesCounter(totalYesCounts);
			progressRequest.setNoContinuous(noContinuous);

			//no need to add 1 in the date as lastAPIDate2Consider has not to be taken account

			progressRequest.setDaysObserved((int) ((progressDateCal.getTimeInMillis()-lastAPIDate2Consider.getTimeInMillis())/(24*60*60*1000)));


			//init left period with the totalPeriodConsidered
			progressRequest.setLeftPeriod(totalPeriodConsidered);



			ProgressInfo lastAPIInfo=progressHistory.get(lastAPIDateAsInt);

			if(lastAPIInfo!=null) {
				progressRequest.setProbabilityOfNMinus1DaysObserved(lastAPIInfo.getProbabilityOfNDaysObserved());
				progressRequest.setLeftPeriod( lastAPIInfo.getLeftPeriod() );
			}


		} finally {
			if (rs!= null) {
				try { rs.close(); } catch (Exception e) { }
				rs = null;
			}
		}



		//////

		logger.debug("UID["+logUID+"]: UserId[ "+userId+" ], UserHabitId[ "+userHabitId+" ], ProgRequest["+jsonMapper.writeValueAsString(progressRequest)+"] calling HabitProgressUtil Web API  ... ");

		retrofit2.Response<ProgressInfo> progressRes= habitProgressWebClient.service.getProgress(progressRequest).execute();


		if(progressRes!=null && progressRes.body()!=null) {

			progressRes.body().setApiDate(progressDateCal.getTime());

			//remove the Progress Report (lastProgressAPIDate_which_is_considered, current_ProgressAPIDate) both dates are exclusive.
			Integer key2delete=null;



			do {
				//init key2delete everytime to null
				key2delete=null;

				for(Map.Entry<Integer,ProgressInfo> entry:progressHistory.entrySet()) {
					if(entry.getKey() > lastAPIDateAsInt && entry.getKey()<progDateAsInt ) {
						key2delete=entry.getKey();
						break;
					}
				}

				if(key2delete!=null) {
					progressHistory.remove(key2delete);
				}

			}while(key2delete!=null);


			//update the history with the current progress
			progressHistory.put(Integer.parseInt(sdfProgHstryKey.format(progressDateCal.getTime())), progressRes.body());

			if(saveProgressInDB) {
				saveProgressPSTMT.setLong(_saveProgress_Indx_ProgressForUserId, userId);
				saveProgressPSTMT.setLong(_saveProgress_Indx_ProgressForUserHabitId, userHabitId);
				saveProgressPSTMT.setFloat(_saveProgress_Indx_Progress1, progressRes.body().getLeftPeriod());
				saveProgressPSTMT.setFloat(_saveProgress_Indx_Progress2, progressRes.body().getLeftPeriod());
				saveProgressPSTMT.setTimestamp(_saveProgress_Indx_ProgressDate, new Timestamp(progressRes.body().getApiDate().getTime()) );
				saveProgressPSTMT.setString(_saveProgress_Indx_ProgressJsonInfo, jsonMapper.writeValueAsString(progressHistory));

				logger.debug("UID["+logUID+"]: UserId[ "+userId+" ], UserHabitId[ "+userHabitId+" ], Updating DB["+saveProgressPSTMT.toString()+"] ... ");

				saveProgressPSTMT.executeUpdate();
			}

			return progressRes.body();
		}

		return null;

	}

	//////////////////////////

	{
		int i = 0;
		_saveProgress_Indx_Progress1 = ++i;
		_saveProgress_Indx_Progress2 = ++i;
		_saveProgress_Indx_ProgressDate = ++i;
		_saveProgress_Indx_ProgressJsonInfo = ++i;
		_saveProgress_Indx_ProgressForUserId = ++i;
		_saveProgress_Indx_ProgressForUserHabitId = ++i;

		////
	}


	private boolean dbConLocallyCreated=true;
	

	private HabitProgressUtil(Connection dbCon, boolean updateStatus4Completions) throws DBError, SQLException {

		this.dbCon = dbCon;
		
		//////

		ResultSet rs=null;
		Statement stmt=null;

		try {

			stmt=dbCon.createStatement();

			rs=stmt.executeQuery(" select id from task_sub_sub_types where title='Track/(Habit)' ");

			rs.next();

			habitTrackTypeId=rs.getInt("id");

		}finally {
			if (rs != null) {
				try { rs.close(); } catch (Exception e) { }
				rs = null;
			}

			if (stmt != null) {
				try { stmt.close(); } catch (Exception e) { }
				stmt = null;
			}
		}

		//parameters inUserId, inUserHabitId
		usrHbtPSTMT= dbCon.prepareStatement("select uh.created_on::date habitStartDate, hdp.title totalPeriodConsidered, progress_info_json progressHistoryJson from user_habits uh left join habits h on uh.habit_id=h.id left join habit_dev_periods hdp on h.hdp_id=hdp.id where uh.user_id=? and uh.id=? ");

		//parameters inUserId, inUserHabitId
		totalYesCountPSTMT=dbCon.prepareStatement("select count(id) totalYesCounter from user_tasks where user_id=? and user_habit_id=? and tsst_id="+habitTrackTypeId+" and status=3 and (response is not null and  response::jsonb@>'{\"ans\":[\"yes\"]}' ) ");


		//parameters procLastApiDate, inProgressDate, inUserId, inUserHabitId
		latestYesIdPSTMT=dbCon.prepareStatement("select max(id) lastYesId from user_tasks where task_date::date>?::date and task_date::date<=?::date and user_id=? and user_habit_id=? and tsst_id="+habitTrackTypeId+" and status=3 and (response is not null and  response::jsonb@>'{\"ans\":[\"yes\"]}' ) ");


		//parameters procLastYesUserTaskId, procLastApiDate, inProgressDate, inUserId, inUserHabitId
		noContinuPSTMT=dbCon.prepareStatement("select count(id) noCounts from user_tasks where id>? and task_date::date>?::date and task_date::date<=?::date and user_id=? and user_habit_id=? and tsst_id="+habitTrackTypeId+" and status=3 and (response is not null and  response::jsonb@>'{\"ans\":[\"no\"]}' ) ");

		//parameters procLastApiDate, inProgressDate, inUserId, inUserHabitId,
		totalRespPSTMT=dbCon.prepareStatement("select count(id) respCount, max(task_date::date) latestDateUserResponded from user_tasks where task_date::date>?::date and task_date::date<=?::date and user_id=? and user_habit_id=? and tsst_id="+habitTrackTypeId+" and status=3 and response is not null ");

		//parameter procLastApiDate,inProgressDate,inUserId,inUserHabitId
		activityDoneListPSTMT=dbCon.prepareStatement("select case when response is not null and response::jsonb ->'ans'->>0='yes' then 1 else 0 end from user_tasks where task_date::date>?::date and task_date::date<=?::date and user_id=? and user_habit_id=? and tsst_id="+habitTrackTypeId+" order by task_date ");

		//parameter procLastApiDate,inProgressDate,inUserId,inUserHabitId
		activityDonePSTMT=dbCon.prepareStatement("select case when response::jsonb ->'ans'->>0='yes' then 1 else 0 end from user_tasks where response is not null and task_date::date>?::date and task_date::date<=?::date and user_id=? and user_habit_id=? and tsst_id="+habitTrackTypeId+" and status=3 order by task_date desc limit 1");

		//parameter procLastApiDate,inProgressDate,inUserId,inUserHabitId,
		systemGenNoPSTMT=dbCon.prepareStatement("select count(id) noCounts from user_tasks where task_date::date>?::date and task_date::date<=?::date and user_id=? and user_habit_id=? and tsst_id="+habitTrackTypeId+" and status=1");

		//////

		if(updateStatus4Completions) {
			saveProgressPSTMT = dbCon.prepareStatement("update user_habits set status=(case when ?<=0 then "+ReachCodes.Status.Completed.code+" else status end ), progress_remain_days=?, progress_rec_date=?, progress_info_json=to_jsonb(?::jsonb) where user_id=? and id=? ");
		}else {
			//the query below (progress_remain_days=(?+?)/2) is written in a way to support the status update condition status=(case when ?<=0 then "+ReachCodes.Status.Completed.code+" else status end ) 
			saveProgressPSTMT = dbCon.prepareStatement("update user_habits set progress_remain_days=(?+?)/2, progress_rec_date=?, progress_info_json=to_jsonb(?::jsonb) where user_id=? and id=? ");
		}

		////

		habitProgressWebClient = new HabitProgressWebClient();

	}



	private boolean releaseResourceWasCalled = false;

	public void releaseResources() {


		if (usrHbtPSTMT != null) {
			try { usrHbtPSTMT.close(); } catch (Exception e) { }
			usrHbtPSTMT = null;
		}

		if (totalYesCountPSTMT != null) {
			try { totalYesCountPSTMT.close(); } catch (Exception e) { }
			totalYesCountPSTMT = null;
		}

		if (latestYesIdPSTMT != null) {
			try { latestYesIdPSTMT.close(); } catch (Exception e) { }
			latestYesIdPSTMT = null;
		}

		if (noContinuPSTMT != null) {
			try { noContinuPSTMT.close(); } catch (Exception e) { }
			noContinuPSTMT = null;
		}

		if (totalRespPSTMT != null) {
			try { totalRespPSTMT.close(); } catch (Exception e) { }
			totalRespPSTMT = null;
		}

		if (activityDoneListPSTMT != null) {
			try { activityDoneListPSTMT.close(); } catch (Exception e) { }
			activityDoneListPSTMT = null;
		}

		if (activityDonePSTMT != null) {
			try { activityDonePSTMT.close(); } catch (Exception e) { }
			activityDonePSTMT = null;
		}

		if (systemGenNoPSTMT != null) {
			try { systemGenNoPSTMT.close(); } catch (Exception e) { }
			systemGenNoPSTMT = null;
		}

		if (saveProgressPSTMT != null) {
			try { saveProgressPSTMT.close(); } catch (Exception e) { }
			saveProgressPSTMT = null;
		}

		if(dbConLocallyCreated) {
			if (dbCon != null) {
				try { dbCon.close(); } catch (Exception e) { }

				dbCon = null;
			}
		}


		releaseResourceWasCalled = true;
	}

	@Override
	protected void finalize() throws Throwable {

		// it is not necessarily will be called

		if (!releaseResourceWasCalled) {
			logger.warn(" WARNING ********  You must to call releaseResources() method... ****** ");

			releaseResources();

		}

		super.finalize();
	}



	//////////////////////////////////////////////////////////////////////

	public static void main(String[] args) throws DBError, SQLException, IOException, ParseException {

		HabitProgressUtil hpu=HabitProgressUtil.getNewInstance(UUID.randomUUID().toString(), false);

		Calendar cal=Calendar.getInstance(Locale.ENGLISH);

		cal.set(Calendar.DATE, 30);
		Date progDate=cal.getTime();



		cal.set(Calendar.DATE, 28);
		Date lastAPIDate=cal.getTime();

		hpu.getProgressForUser(1, 1, progDate, lastAPIDate, true);

	}




	public static void mainasfdf2131321312(String[] args) throws JsonProcessingException {

		HashMap<Integer,ProgressInfo> latestFourProgressInfo=new HashMap<>();

		SimpleDateFormat sdfProgHstryKey=new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);

		Calendar curr=Calendar.getInstance(Locale.ENGLISH);

		latestFourProgressInfo.put(Integer.parseInt(sdfProgHstryKey.format(curr.getTime()) ), new ProgressInfo().setApiDate(curr.getTime()));

		curr.add(Calendar.DATE, -1);
		latestFourProgressInfo.put(Integer.parseInt(sdfProgHstryKey.format(curr.getTime()) ), new ProgressInfo().setApiDate(curr.getTime()));

		curr.add(Calendar.DATE, -1);
		latestFourProgressInfo.put(Integer.parseInt(sdfProgHstryKey.format(curr.getTime()) ), new ProgressInfo().setApiDate(curr.getTime()));

		curr.add(Calendar.DATE, -1);
		latestFourProgressInfo.put(Integer.parseInt(sdfProgHstryKey.format(curr.getTime()) ), new ProgressInfo().setApiDate(curr.getTime()));


		System.out.println(ReachJsonMapper.mapperForHabitProgress().writeValueAsString(latestFourProgressInfo));


	}



}
