package com.reach.util.habit.progress;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public final class ProgressInfo {

	// Updated total number of days since the user has taken the habit/behaviour.
	private int daysObserved;

	// Updated total number of YES till date.
	private int totalYesCounter;

	// Updated number of NO since the last YES (last time the activity was done).
	// Reset to 0 if activityDone == 1, that is a YES occurs
	private int noContinuous;

	// this will be the value obtained from server in the last call. On the very
	// first call it will be 0
	private float leftPeriod;

	// This value is obtained from server and will be sent to server for the last
	// reading
	private float probabilityOfNDaysObserved;

	private Date apiDate;


	public Date getApiDate() {
		return apiDate;
	}

	public ProgressInfo setApiDate(Date apiDate) {
		this.apiDate = apiDate;
		return this;
	}

	public int getNoContinuous() {
		return noContinuous;
	}

	public ProgressInfo setNoContinuous(int noContinuous) {
		this.noContinuous = noContinuous;
		return this;
	}

	public int getDaysObserved() {
		return daysObserved;
	}

	public ProgressInfo setDaysObserved(int daysObserved) {
		this.daysObserved = daysObserved;
		return this;
	}

	public int getTotalYesCounter() {
		return totalYesCounter;
	}

	public ProgressInfo setTotalYesCounter(int totalYesCounter) {
		this.totalYesCounter = totalYesCounter;
		return this;
	}

	public float getLeftPeriod() {
		return leftPeriod;
	}

	public ProgressInfo setLeftPeriod(float leftPeriod) {
		this.leftPeriod = leftPeriod;
		return this;
	}

	public float getProbabilityOfNDaysObserved() {
		return probabilityOfNDaysObserved;
	}

	public ProgressInfo setProbabilityOfNDaysObserved(float probabilityOfNDaysObserved) {
		this.probabilityOfNDaysObserved = probabilityOfNDaysObserved;
		return this;
	}

}
