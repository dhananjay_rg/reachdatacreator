package com.reach.util;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Properties;

public class ReachProperties extends Properties {

	private static final long serialVersionUID = 1L;

	public static final ReachProperties instance = new ReachProperties();

	private ReachProperties() {

		try {

			load(ReachProperties.class.getClassLoader().getResourceAsStream("reach.properties"));
		} catch (Exception e) {
			throw new ExceptionInInitializerError("Failed to load properties.....");
		}

	}

	public String getAllwedIPAddress() {

		String allwedIPCSV = getProperty("reach.micro.tm.allwedCSV").trim();

		if (allwedIPCSV == null) {
			allwedIPCSV = "";
		}

		try {
			String localAddress = Inet4Address.getLocalHost().getHostAddress();

			allwedIPCSV = "," + allwedIPCSV + "," + localAddress + ",";

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		return allwedIPCSV;
	}

	public String getDBUrlWithSchema() {
		return getProperty("db.urlWithSchema").trim();
	}

	public String getDBUser() {
		return getProperty("db.username").trim();
	}

	public String getDBPassword() {
		return getProperty("db.password").trim();
	}

	public String getDBDriverClass() {
		return getProperty("db.driverClass").trim();
	}

	/**
	 * when user is left with this days of task, Backend Module will prepare task
	 * for future days
	 **/
	public int getTaskBufferDays() {
		return Integer.parseInt(getProperty("taskAlgo.userTaskBufferExhaustDays").trim());
	}

	public String getContentServerPushUrl4Article() {
		return getProperty("contentServer.push.endPoint.article").trim();
	}

	public String getContentServerPush4Topic() {
		return getProperty("contentServer.push.endPoint.topic").trim();
	}

	public String getContentServerPush4Channel() {
		return getProperty("contentServer.push.endPoint.channel").trim();
	}

	public String getSendGridApiKey() {
		return getProperty("SendGrid.ApiKey").trim();
	}

	public String getReminderMailSender() {
		return getProperty("generalMailSender").trim();
	}

	public String getReachRGProjectId() {
		return getProperty("reach.RG.projectId").trim();
	}

	public String getReachRGClientId() {
		return getProperty("reach.RG.clientId").trim();
	}

	public String getReachRGAppPlatform() {
		return getProperty("reach.RG.Application.platform").trim();
	}

	public String getReachRGAppVersion() {
		return getProperty("reach.RG.Application.version").trim();
	}

	//////

	public String getRGNotificationBaseURL() {
		return getProperty("rgAPI.Notif.BaseUrl").trim();
	}

	public String getHabitProgressBaseURL() {
		return getProperty("habitProgress.api.BaseUrl").trim();
	}

	public String getContentServerPutUrl4Article() {
		return getProperty("contentServer.put.endPoint.article").trim();
	}

	public String getRGSSOAddPartnerInPatientProfileURL() {
        return getProperty("sso.rg.url.user.partner.add").trim();
    }
}
