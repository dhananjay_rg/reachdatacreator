package com.reach.util;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ListDomain<T> {
	
	public final String type="LIST";
	public String contentType;
	public List<T> data;
	
	public static void main(String[] args) throws JsonProcessingException {
		
		
		FloatListDomain floatDomain=new FloatListDomain();
		floatDomain.data=new ArrayList<>();
		
		for(int i=1; i<=5; i++) {
			floatDomain.data.add(i*1.5F);
		}
		
		System.out.println(new ObjectMapper().writeValueAsString(floatDomain));
		
		
		IntListDomain intDomain=new IntListDomain();
		intDomain.data=new ArrayList<>();
		
		for(int i=1; i<=10; i++) {
			intDomain.data.add(i);
		}
		
		System.out.println(new ObjectMapper().writeValueAsString(intDomain));
		
		
		StringListDomain strDomain=new StringListDomain();
		strDomain.data=new ArrayList<>();
		
		for(char i='A'; i<='E'; i++) {
			strDomain.data.add(""+i);
		}
		
		System.out.println(new ObjectMapper().writeValueAsString(strDomain));
		
		
		
		
	}

}
