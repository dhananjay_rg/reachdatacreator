/**
 * 
 * 
 */
package com.reach.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author siddhartha
 *
 */
public class ReachJsonMapper {

	public static final String _dateTimeFormatString = "yyyy-MM-dd'T'HH:mm:ss'Z'Z";
	public static final String _dateFormatString = "yyyy-MM-dd";

	private ReachJsonMapper() {

	}

	/**
	 * have date format yyyy-MM-dd'T'HH:mm:ss'Z'Z
	 */
	public static ObjectMapper mapper = getJsonMapper();

	/**
	 * have date format yyyy-MM-dd
	 */
	public static ObjectMapper mapperForDate = getJsonMapperForDate();

	private static ObjectMapper getJsonMapper() {
		if (mapper != null) {
			return mapper;
		} else {
			return createMapperObject(_dateTimeFormatString);
		}
	}

	private static ObjectMapper getJsonMapperForDate() {
		if (mapper != null) {
			return mapper;
		} else {
			return createMapperObject(_dateFormatString);
		}
	}

	public static ObjectMapper mapperForCrossNotif() {
		return createMapperObject(null);
	}

	public static ObjectMapper mapperForHabitProgress() {
		return createMapperObject("yyyy-MM-dd");
	}

	private static ObjectMapper createMapperObject(String dateFormat) {
		ObjectMapper jsonObjMapper = new ObjectMapper();
		jsonObjMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		jsonObjMapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);

		if (dateFormat != null && !dateFormat.trim().equals("")) {
			jsonObjMapper.setDateFormat(new SimpleDateFormat(dateFormat, Locale.ENGLISH));
		}

		return jsonObjMapper;

	}

}
