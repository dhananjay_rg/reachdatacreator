package com.reach.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.beanutils.PropertyUtils;

import com.reach.err.ParamPathError;




public class ParameterPathResolver {
	
	
	public static boolean isTemplate(String template){
		
		return template != null && template.indexOf(ReachCodes.ValuePathRoot._Token) > -1;
		
	}
	
	public static boolean hasRoot(String valuePath, String root){
		
		return valuePath!=null && valuePath.startsWith(ReachCodes.ValuePathRoot._Token + root+".");
		
	}
	
	
	
	/** this method either replaces all the token **/
	public static Object prepareFromTemplate(String template, String root, Object valueData, String valuePathWithRoot){
		
		try {
			
			Object value = null;
			
			if(valueData instanceof String) {
				value = valueData;
			}else {
				value = resolve(valueData, valuePathWithRoot.replaceAll(ReachCodes.ValuePathRoot._Token, "").replaceFirst(root+"\\.", ""));
			}

			template = template.replaceAll(valuePathWithRoot, value.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return template;
		
	}
	
	
	public static Object resolve(Object valueData, String valuePathWithoutRoot) throws ParamPathError{
		
		if(valuePathWithoutRoot==null||valuePathWithoutRoot.trim().equals("")) {
			throw new ParamPathError("value path is empty");
		}
		
		return resolveNow(valuePathWithoutRoot, valueData, null);
		
	}
	
	/** this method returns the value of date in the specified format 'dateTimeFormat', in case the attribute turns to be a date object **/
	public static Object resolve(Object valueData, String valuePathWithoutRoot, String dateTimeFormat) throws ParamPathError{
		
		if(valuePathWithoutRoot==null||valuePathWithoutRoot.trim().equals("")) {
			throw new ParamPathError("value path is empty");
		}
		
		return resolveNow(valuePathWithoutRoot, valueData, dateTimeFormat);
		
	}
	
	
	
	private static Object resolveNow(String valuePathWithoutRoot, Object valueData, String dateTimeFormat) throws ParamPathError {
		
		Object value=null;
		
		
		//boolean notTerminatingPath=true;
		
		try {
			
			value= PropertyUtils.getNestedProperty(valueData, valuePathWithoutRoot);
			
			/*if(value instanceof String || ClassUtils.isPrimitiveOrWrapper(value.getClass()) || value instanceof Date) {
				notTerminatingPath=false;
			}
			*/
			
			if(value instanceof Date) {
				
				if(dateTimeFormat!=null) {
					value= new SimpleDateFormat(dateTimeFormat).format((Date)value);
				}else {
					value= new SimpleDateFormat(ReachJsonMapper._dateTimeFormatString).format((Date)value);
				}
				
				
			}else {
				value= value.toString();
			}
			
			
		} catch (Exception e) {
			throw new ParamPathError("Unable to resolve value for ValuePath[ "+valuePathWithoutRoot+" ] ");
		}
		
		
		/*if(notTerminatingPath) {
			throw new ParamPathError("Unable to resolve value. Value path[ "+valuePathWithoutRoot+" ] is not Terminating. ");
		}*/
		
		
		return value;
		
	}
	
	
	public static void main(String[] args) {
		System.out.println( prepareFromTemplate("@@msgFromUser@@", ReachCodes.ValuePathRoot._MsgFromUser, "Hello testing....", "@@msgFromUser@@") );
	}
	

}
